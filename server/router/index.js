/**
 * @description 路由配置文件
*/

// 仅本地增、删、改
let userPermission = {
  // hosts: 'localhost',
  rules: ['user', 'admin']
}
let adminPermission = {
  // hosts: 'localhost',
  rules: ['admin']
}

/* 自定义控制器 */
const router = {
  routes: [
    // 导航
    {
      path: '/tigerlair/nave',
      controller: require('../modules/nave/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'list/treeall',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 18
          }
        },
        {
          path: 'list/tree',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 3
          }
        }
      ]
    },
    // 项目
    {
      name: 'project',
      path: '/tigerlair/project',
      controller: require('../modules/project/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    // 产品
    {
      name: 'product',
      path: '/tigerlair/product',
      controller: require('../modules/product/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        }
      ]
    },
    // 页面
    {
      name: 'panel',
      path: '/tigerlair/panel',
      controller: require('../modules/panel/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'list/treeall',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'productId', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 18
          },
          permission: userPermission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        }
      ]
    },
    {
      path: '/tigerlair/func', // 功能
      controller: require('../modules/func/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/codewell', // 编码库
      controller: require('../modules/codewell/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        }
      ]
    },
    {
      name: 'archive',
      path: '/tigerlair/archive', // 文档
      controller: require('../modules/archive/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'cat', operator: 'IN'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'items', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'cat', operator: 'IN'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'items', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/section', // 章节
      controller: require('../modules/section/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'archiveId', operator: 'EQUAL'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/explore', // 文件浏览
      controller: require('../modules/explore/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'name', operator: 'LIKE'},
              {field: 'ext', operator: 'LIKE'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 2000},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          },
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/arcfile', // 文档文件
      controller: require('../modules/arcfile/controller.js'),
      children: [
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'IN'},
              {field: 'aid', operator: 'IN'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      name: 'chapter',
      path: '/tigerlair/chapter', // 文档目录
      controller: require('../modules/chapter/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'asc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        }
      ]
    },
    // 微应用
    {
      name: 'mapp',
      path: '/tigerlair/mapp',
      controller: require('../modules/mapp/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'mock/valid',
          method: 'get',
          action: 'valid',
          permission: userPermission
        },
        {
          path: 'release/item',
          method: 'get',
          action: 'queryReleaseItem',
          permission: userPermission
        }
      ]
    },
    {
      path: '/tigerlair/viewpage', // 视图页面
      controller: require('../modules/viewpage/controller.js'),
      controllerName: 'viewpage',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'layout', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'layout', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/viewcomp', // 视图组件
      controller: require('../modules/viewcomp/controller.js'),
      controllerName: 'viewcomp',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'sync',
          method: 'post',
          action: 'syncEnvir',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/comprelease', // 组件发布表
      controller: require('../modules/comprelease/controller.js'),
      controllerName: 'comprelease',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'cid', operator: 'EQUAL'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 15},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'nid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/viewregister', // 组件注册表
      controller: require('../modules/viewregister/controller.js'),
      controllerName: 'viewregister',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'sync',
          method: 'post',
          action: 'syncEnvir',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/viewrelease', // 视图发布表
      controller: require('../modules/viewrelease/controller.js'),
      controllerName: 'viewrelease',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'vpid', operator: 'EQUAL'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 15},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'vpid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/snippet', // 片段
      controller: require('../modules/snippet/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'view',
          method: 'get',
          action: 'queryView',
          controller: require('../modules/snippet/controller.js')
        },
      ]
    },
    {
      path: '/tigerlair/snippetrelease', // 片段发布表
      controller: require('../modules/snippetrelease/controller.js'),
      controllerName: 'snippetrelease',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'sid', operator: 'EQUAL'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 15},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'nid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/viewtpl', // 模板
      controller: require('../modules/viewtpl/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/tplrelease', // 模板发布表
      controller: require('../modules/tplrelease/controller.js'),
      controllerName: 'tplrelease',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'tid', operator: 'EQUAL'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 15},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'nid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      name: 'nav',
      path: '/tigerlair/nav', // 导航
      controller: require('../modules/nav/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'list/tree',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 16
          },
          permission: userPermission
        }
      ]
    },
    {
      path: '/tigerlair/navrelease', // 导航发布表
      controller: require('../modules/navrelease/controller.js'),
      controllerName: 'navrelease',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'nid', operator: 'EQUAL'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 15},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'nid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/resource', // 数据源
      controller: require('../modules/resource/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/pool', // 资源目录
      name: 'pool',
      controller: require('../modules/pool/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          },
          permission: adminPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/pfile', // 资源文件
      name: 'pfile',
      controller: require('../modules/pfile/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            dataSource: {
              isFindAll: true,
              isIncludeDir: true
            }
          },
          permission: adminPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          excute: {
            dataSource: {
              isFindAll: true,
              isIncludeDir: true
            }
          },
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/dfile', // 文件
      controller: require('../modules/dfile/controller.js'),
      isPreInit: true,
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'originName', operator: 'LIKE'},
              {field: 'size', operator: 'LIKE'},
              {field: 'ext', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            fields: [
              {field: 'id'},
              {field: 'name'},
              {field: 'originName'},
              {field: 'size'},
              {field: 'ext'},
              {field: 'status'},
              {field: 'createTime'},
              {field: 'updateTime'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'originName', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'deploy',
          method: 'post',
          action: 'deploy',
          permission: adminPermission
        },
        {
          path: 'upload',
          method: 'post',
          action: 'upload',
          middleware: 'uploadMiddleware',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/execute', // 处理流
      controllerName: 'execute',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/exeunit', // 处理单元
      controllerName: 'exeunit',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/exegroup', // 流程组
      controllerName: 'exegroup',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/inter', // 接口
      controller: require('../modules/inter/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN'},
              {field: 'method', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'eid', operator: 'IN'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN'},
              {field: 'method', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'eid', operator: 'IN'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/collect/inter', // 采集
      controller: require('../modules/collect-inter/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        // {
        //   path: 'item',
        //   method: 'put',
        //   action: 'add',
        //   permission: adminPermission
        // },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'store',
          method: ['post', 'put'],
          action: 'store'
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/collect/inter/group', // 采集分组
      controller: require('../modules/collect-inter-group/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/collect/error', // 采集
      controller: require('../modules/collect-error/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'message', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        // {
        //   path: 'item',
        //   method: 'put',
        //   action: 'add',
        //   permission: adminPermission
        // },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'store',
          method: 'put',
          action: 'store'
        },
        {
          path: 'store',
          method: 'post',
          action: 'store'
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'saas',
          method: ['get', 'post', 'put', 'delete'],
          action: 'saas'
        }
      ]
    },
    {
      path: '/tigerlair/collect/error/group', // 采集分组
      controller: require('../modules/collect-error-group/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/collect/performance', // 采集
      controller: require('../modules/collect-performance/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'message', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        // {
        //   path: 'item',
        //   method: 'put',
        //   action: 'add',
        //   permission: adminPermission
        // },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'store',
          method: 'put',
          action: 'store'
        },
        {
          path: 'store',
          method: 'post',
          action: 'store'
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        {
          path: 'saas',
          method: ['get', 'post', 'put', 'delete'],
          action: 'saas'
        }
      ]
    },
    {
      path: '/tigerlair/collect/performance/group', // 采集分组
      controller: require('../modules/collect-performance-group/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/entity', // 实体
      controller: require('../modules/entity/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/dapi', // 接口服务
      controller: require('../modules/dapi/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'eid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        },
        // 动态api
        {
          path: 'saas/:entity/:action**',
          method: ['get', 'post', 'put', 'delete'],
          action: 'saas'
        }
      ]
    },
    {
      path: '/tigerlair/proxier', // 代理
      controller: require('../modules/proxier/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/plugins', // 插件
      controller: require('../modules/plugins/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN'},
              {field: 'method', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/dictionary', // 字典
      controllerName: 'dictionary',
      tableName: 'dictionary',
      children: [
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'cat', operator: 'EQUAL'},
              {field: 'code', operator: 'EQUAL'},
              {field: 'key', operator: 'EQUAL'},
              {field: 'value', operator: 'LIKE'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'cat', operator: 'EQUAL'},
              {field: 'code', operator: 'EQUAL'},
              {field: 'key', operator: 'EQUAL'},
              {field: 'value', operator: 'LIKE'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          excute: {
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          },
          permission: userPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'delete',
          permission: adminPermission
        },
        {
          path: 'list/tree',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 3
          },
          permission: userPermission
        }
      ]
    },
    {
      path: '/tigerlair/backup', // 备份
      controller: require('../modules/backup/controller.js'),
      tableName: 'backup',
      children: [
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          excute: {
            sort: [
              {field: 'createTime', by: 'desc'}
            ]
          },
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: adminPermission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'delete',
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/user', // 用户页面
      controllerName: 'user',
      children: [
        {
          path: 'login',
          method: 'post',
          action: 'login'
        },
        {
          path: 'logout',
          method: 'post',
          action: 'logout'
        },
        {
          path: 'queryUserInfo',
          method: 'get',
          action: 'queryUserInfo'
        }
      ]
    },
    {
      path: '/tigerlair/actuator', // 执行器
      controller: require('../modules/actuator/controller.js'),
      children: [
        {
          path: 'execute',
          method: 'post',
          action: 'execute',
          excute: {},
          permission: adminPermission
        }
      ]
    },
    {
      path: '/tigerlair/saas', // saas服务
      children: [
        // 静态渲染
        {
          path: 'snuxt/*',
          method: ['get', 'post', 'put', 'delete'],
          action: 'page',
          controller: require('../modules/snuxt/controller.js')
        },
        // 采集接口响应
        {
          path: 'collect/inter',
          method: ['get', 'post', 'put', 'delete'],
          action: 'saas',
          controller: require('../modules/collect-inter/controller.js')
        },
        // 采集接口响应
        {
          path: 'inter/mock**',
          method: ['get', 'post', 'put', 'delete'],
          action: 'saas',
          controller: require('../modules/inter/controller.js')
        },
        // 应用导航查询
        {
          path: 'mapp/release/item',
          method: ['get'],
          action: 'queryReleaseItem',
          controller: require('../modules/mapp/controller.js')
        },
        // 导航版本详情查询
        {
          path: 'nav/release/item',
          method: ['get'],
          action: 'query',
          controller: require('../modules/navrelease/controller.js')
        },
        // 视图组件查询
        {
          path: 'viewcomp/list/all',
          method: ['get'],
          action: 'queryAll',
          controller: require('../modules/viewcomp/controller.js'),
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        // 视图组件版本详情查询
        {
          path: 'viewcomp/release/item',
          method: ['get'],
          action: 'query',
          controller: require('../modules/comprelease/controller.js')
        },
        // 视图页面详情查询
        {
          path: 'viewpage/item',
          method: ['get'],
          action: 'query',
          controller: require('../modules/viewpage/controller.js')
        },
        // 视图页面版本详情查询
        {
          path: 'viewpage/release/item',
          method: ['get'],
          action: 'query',
          controller: require('../modules/viewrelease/controller.js')
        },
        // 发布的片段查看
        {
          path: 'snippet/release',
          method: 'get',
          action: 'queryRelease',
          controller: require('../modules/snippet/controller.js')
        },
        // 模板版本详情查询
        {
          path: 'tpl/release/item',
          method: ['get'],
          action: 'query',
          controller: require('../modules/tplrelease/controller.js')
        },
        {
          path: 'resource/pool/:id',
          method: ['get', 'post'],
          action: 'pool',
          controller: require('../modules/resource/controller.js')
        }
      ]
    }
  ]
}

module.exports = router