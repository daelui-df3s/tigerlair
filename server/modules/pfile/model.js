/**
 * @description 目录文件-模型
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')

class Model extends ox.Model {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 表配置
    let file = this.route.scope.CONFIG.file
    // 表名
    this.table = 'pfile'
    // 指定DB类
    let DBClass = ox.dbFactory.getDBClass(file.type)
    // 数据库
    this.$db = new DBClass(Object.assign({table: this.table}, file))
  }

  /**
   * @function 获取配置配置
   * @return {Object}
   */
  _getDefaultOptions () {
    let constant = this.route.scope.CONFIG.constant
    let resPath = constant.poolPath + '/res'
    return {
      // 数据源
      dataSource: {
        url: resPath,
        isIncludeRoot: true
      },
      table: this.table, // 表名
      pk: this.pk, // 主键
      alias: '', // 别名
      model: '', // 模型名
      where: {}, // 过滤条件
      excute: {} // 执行条件
    }
  }
}

module.exports = Model