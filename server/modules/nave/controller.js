/**
 * @description 数据键值对-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const NaveService = require('./service.js')

const Controller = ox.Controller

class NaveController extends Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new NaveService(args)
  }
}

module.exports = NaveController