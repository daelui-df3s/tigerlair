/**
 * @description 微应用-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Service = require('./service.js')
const sha1 = require('sha1')

class Controller extends ox.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new Service(args)
  }

  // token验证
  valid (action) {
    // 解析数据
    return this.solveAction(action).then(async action => {
      let params = action.params || {}
      // GET请求携带参数是个参数signature,timestamp,nonce,echostr
      const { signature, timestamp, nonce, echostr} = params

      // 服务器的token
      const token = 'TOKEN'

      // 将token、timestamp、nonce三个参数进行字典序排序 
      const arrSort = [token, timestamp, nonce]
      arrSort.sort()

      // 将三个参数字符串拼接成一个字符串进行sha1加密,npm install --save sha1
      const str = arrSort.join('')
      const shaStr = sha1(str)

      // 获得加密后的字符串可与signature对比，验证标识该请求来源于微信服务器
      if (shaStr === signature) {
        // 确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效
        return {
          type: 'PLAINTEXT',
          data: echostr
        }
        // res.send(echostr)
      } else {
        //否则接入失败。
        return {
          type: 'PLAINTEXT',
          data: 'No'
        }
        // res.send("no")
      }
    })
  }

  // 查询导航
  queryReleaseItem (action) {
    // 解析数据
    return this.solveAction(action).then(async action => {
      let params = action.params || {}
      let data = {}
      if (params.id) {
        // 查询应用
        let res = await this.$router.getControllInstance({path: '/tigerlair/mapp'}).queryAll({}, {
          action: {},
          query: {},
          params: {
            id: params.id,
            code: params.id,
            status: 1
          },
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL', calculator: 'OR'},
              {field: 'code', operator: 'EQUAL', calculator: 'OR'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        })
        let mapps = res.data || []
        mapps = Array.isArray(mapps) ? mapps : []
        mapps = mapps.sort((a, b) => a.isDefault > b.isDefault ? -1 : a.isDefault < b.isDefault ? 1 : 0)
        let mapp = mapps[0] || {}
        // 查询导航
        if (mapp.id) {
          res = await this.$router.getControllInstance({path: '/tigerlair/navrelease'}).query({}, {
            action: {},
            query: {},
            params: {
              mid: mapp.id,
              status: 1
            },
            excute: {
              operators: [
                {field: 'mid', operator: 'EQUAL'}
              ],
              sort: [
                {field: 'createTime', by: 'desc'},
                {field: 'order', by: 'desc'}
              ]
            }
          })
          data = res.data || {}
        }
      }
      return {data: data, success: 1}
    })
  }
}

module.exports = Controller