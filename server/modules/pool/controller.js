/**
 * @description 资源目录-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Service = require('./service.js')
const { filer } = require('@daelui/oxkit')

class Controller extends ox.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new Service(args)
  }

  /**
   * @function 增删改过滤
  */
  filterAction ({ name, action }) {
    if (name === 'add') {
      return this.filterAdd(action)
    }
    return { success: 1 }
  }

  /**
   * @function 增过滤
  */
  filterAdd (action) {
    let path = action.params.path
    path = path.replace(/^\s*\/+/, '').toLowerCase()
    action.params.path = path
    return {
      success: 1
    }
  }

  /**
   * @function 增删改事件触发
  */
  emit (name, action, res) {
    super.emit(name, action, res)
    res = res || {}
    if (name === 'add' && res.success) {
      this.addPoolDir(action)
    }
  }

  /**
   * @function 增删改事件触发
  */
  addPoolDir (action) {
    let path = action.params.path
    let config = this.route.scope.CONFIG
    let poolPath = config.constant.poolPath
    filer.buildPath(poolPath + '/upload/' + path)
    filer.buildPath(poolPath + '/res/' + path)
  }
}

module.exports = Controller