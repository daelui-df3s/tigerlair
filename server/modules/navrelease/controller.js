/**
 * @description 视图页面-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const fs = require('fs')
const ox = require('@daelui/oxjs')
const { filer } = require('@daelui/oxkit')
const Service = require('./service.js')

class Controller extends ox.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new Service(args)
  }

  /**
   * @function 增删改事件触发
  */
  emit (name, action, res) {
    super.emit(name, action, res)
    res = res || {}
    if (res.success && /insert/.test(name)) {
      this.deployData(action)
    }
  }

  // 数据发布
  async deployData (action) {
    let params = action.params || {}
    // 查询路径数据
    let nav = {}
    try {
      // 查询所有的分组数据
      let res = await this.$service.query({}, {
        action: {},
        query: {},
        params: {
          nid: params.nid || -9999,
          status: '1'
        },
        excute: {
          operators: [
            {field: 'nid', operator: 'EQUAL', calculator: 'AND'},
            {field: 'status', operator: 'EQUAL', calculator: 'AND'}
          ],
          sort: [
            {field: 'createTime', by: 'desc'}
          ]
        }
      })
      nav = res.data || {}
    } catch (e) {
      nav = {}
      console.log(e)
    }
    if (!nav.id) {
      return
    }
    // 应用导航元数据文件
    let constant = this.route.scope.CONFIG.constant
    let path = constant.deployPath
    path += '/' + (nav.path || nav.nid)
    let appPath = path
    if (!fs.existsSync(appPath)) {
      filer.buildPath(appPath)
    }
    appPath += '/' + (constant.deployAppFile || 'app.meta.json')
    let success = 0
    // 文件写入
    try {
      let content = JSON.stringify(nav, null, ' ')
      fs.writeFileSync(
        appPath, content
      )
      success = 1
    } catch (e) {
      success = 0
    }

    // 应用页面元数据文件
    let meta = nav.meta
    let pids = []
    try {
      let list = JSON.parse(meta)
      map(list)
      function map (list) {
        list.forEach(item => {
          pids.push(item.pageId)
          if (Array.isArray(item.children)) {
            map(item.children)
          }
        })
      }
    } catch (e) {
      pids = []
    }
    if (!pids.length) {
      return
    }
    let pages = []
    try {
      // 查询所有的分组数据
      let res = await this.$router.getControllInstance({path: '/tigerlair/viewrelease'}).queryAll({}, {
        action: {},
        query: {},
        params: {
          vpid: pids
        },
        excute: {
          operators: [
            {field: 'vpid', operator: 'IN', calculator: 'AND'}
          ],
          sort: [
            {field: 'createTime', by: 'desc'}
          ]
        }
      })
      let list = res.data || []
      list = Array.isArray(list) ? list : []
      pages = list
    } catch (e) {
      pages = []
    }

    // 文件写入
    try {
      pages.forEach(item => {
        let pagePath = path + '/pages/' + item.vpid + '.meta.json'
        let content = JSON.stringify(item, null, ' ')
        fs.writeFileSync(
          pagePath, content
        )
      })
      success = 1
    } catch (e) {
      success = 0
    }

    return {success: success}
    /*
    // 查询适配器数据
    let repList = []
    try {
      // 查询所有的分组数据
      let res = await this.$router.getControllInstance({path: '/tigerlair/dictionary'}).queryAll({}, {
        action: {},
        query: {},
        params: {
          cat: 'register-rep',
          status: '1'
        }
      })
      let list = res.data
      list = Array.isArray(list) ? list : []
      repList = list
    } catch (e) {
      repList = []
    }
    // 查询所有的注册数据
    let list = await this.$service.queryAll({
      params: {
        status: '1'
      },
      excute: {
        operators: [
          {field: 'status', operator: 'EQUAL'}
        ],
        sort: [
          {field: 'createTime', by: 'desc'},
          {field: 'order', by: 'desc'}
        ]
      }
    })
    list = list.data
    list = Array.isArray(list) ? list : []
    list = list.map(item => {
      let node = {
        name: item.name,
        version: item.version,
        path: item.path,
        type: item.type,
        text: item.text,
        cat: item.cat,
        main: item.main,
        property: item.property,
        mainCode: item.mainCode,
        propertyCode: item.propertyCode,
        icon: item.icon
      }
      // 依赖
      let deps = []
      if (item.deps) {
        try {
          deps = JSON.parse(item.deps)
          deps = Array.isArray(deps) ? deps : []
          deps = deps.filter(item => {
            return !!item.path && String(item.status) !== '0'
          })
          deps = deps.map(item => {
            return item.path
          })
        } catch (e) {}
      }
      if (deps && deps.length) {
        node.deps = deps
      }
      // 根路径
      let rootUrls = []
      if (item.rootUrls) {
        try {
          rootUrls = JSON.parse(item.rootUrls)
          rootUrls = Array.isArray(rootUrls) ? rootUrls : []
          rootUrls = rootUrls.filter(item => {
            // 路径列表匹配
            let node = pathList.find(node => node.id === item.pathId && String(node.status) === '1')
            if (node && node.value && /\w/.test(node.value) && !item.url) {
              item.url = node.value
            }
            return !!item.url && String(item.status) !== '0'
          })
          rootUrls = rootUrls.map(item => {
            return item.url
          })
        } catch (e) {}
      }
      if (rootUrls && rootUrls.length) {
        node.rootUrls = rootUrls
      }
      // 适配器
      let reps = []
      if (item.reps) {
        try {
          reps = JSON.parse(item.reps)
          reps = Array.isArray(reps) ? reps : []
          reps = reps.filter(item => {
            return !!item.repId && String(item.status) !== '0'
          })
        } catch (e) {}
      }
      if (reps && reps.length) {
        reps = reps.flatMap(item => {
          let node = repList.find(node => node.id === item.repId && String(node.status) === '1')
          if (node && node.value) {
            try {
              let list = JSON.parse(node.value)
              list = Array.isArray(list) ? list : []
              list = list.filter(item => {
                return !!item.tes && !!item.mat
              })
              return list
            } catch (e) {}
          }
          return []
        })
        reps = reps.filter(node => node.tes && node.mat)
        reps.forEach(item => {
          delete item.id
        })
        node.reps = reps
      }
      return node
    })
    // 组件注册表文件
    let constant = this.route.scope.CONFIG.constant
    let path = constant.deployPath
    if (!fs.existsSync(path)) {
      filer.buildPath(path)
    }
    path += '/' + (constant.componentRegistryFile || 'components.json')
    let success = 0
    // 文件写入
    try {
      let content = JSON.stringify(list, null, ' ')
      fs.writeFileSync(
        path, content
      )
      success = 1
    } catch (e) {
      success = 0
    }
    return {data: list, success: success}
    */
  }
}

module.exports = Controller