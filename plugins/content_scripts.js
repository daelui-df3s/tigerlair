/**
 * @function 添加元素
*/
function appendElement (tag, attrs, parentEl) {
  return new Promise(function (resolve, reject) {
    try {
      let el = document.createElement(tag)
      el.onload = function () {
        resolve(this, tag, attrs, parentEl)
      }
      for (let key in attrs) {
        if (/inner|outer/i.test(key)) {
          el[key] =  attrs[key]
        } else {
          el.setAttribute(key, attrs[key])
        }
      }
      parentEl = parentEl || document.body || document.documentElement
      if (/link|script/i.test(tag)) {
        parentEl = document.head || document.body || document.documentElement
      }
      parentEl.appendChild(el)
    } catch (e) {
      reject(e)
    }
  })
}

// 注入脚本
let protocol = location.protocol
let host = protocol + '//' + 'www.daelui.cn/df3s'
appendElement('script', {
  src: host + '/tigerlair/extensions/plugins.js'
})
appendElement('script', {
  src: host + '/tigerlair/extensions/proxier.js'
})
appendElement('script', {
  src: host + '/tigerlair/extensions/collect.js'
})
appendElement('script', {
  src: host + '/tigerlair/extensions/entry.js'
})
