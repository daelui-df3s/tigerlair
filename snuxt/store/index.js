export const state = () => ({
  siteInfo: {
    web_owner: {},
    ICP: {},
    ICP_URL: {}
  },
  siteCategory: []
})

export const getters = {
  siteInfo: state => state.siteInfo,
  siteCategory: state => state.siteCategory
}

export const mutations = {
  SET_SITE_INFO(state, data) {
    state.siteInfo = data
  },
  SET_SITE_CATEGORY(state, data) {
    state.siteCategory = data
  }
}

export const actions = {
  async nuxtServerInit({ commit }) {
    // let $service = this.app.context.$service
    // let $axios = this.app.context.$axios
    // // 系统配置
    // let res = await $service.system.queryConfig({}, {
    //   $http: $axios
    // })
    // commit('SET_SITE_INFO', res?.data?.data || {})

    // // 栏目
    // res = await $service.category.queryTree({}, {
    //   $http: $axios
    // })
    // commit('SET_SITE_CATEGORY', res?.data?.data || [])
  }
}
