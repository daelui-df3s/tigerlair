import request from '@/utils/request'

// 查询网站参数
export function queryConfig(query, options) {
  return request({
    url: '/front/config',
    method: 'get',
    params: query
  }, options)
}

// 查询首页
export function queryIndex(query, options) {
  return request({
    url: '/front/index',
    method: 'get',
    params: query
  }, options)
}