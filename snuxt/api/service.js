import request from '@/utils/request'

// 查询列表
export function queryListAll(query, options) {
  return request({
    url: '{host}/mock/data/service-all.json',
    method: 'get',
    params: query
  }, options)
}

// 查询单个
export function queryItem(query, options) {
  return request({
    url: '{host}/mock/data/service-item.json?id={id}',
    method: 'get',
    params: query
  }, options)
}
