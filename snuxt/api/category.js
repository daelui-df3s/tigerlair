import request from '@/utils/request'

// 查询列表
export function queryListPage(query, options) {
  return request({
    url: '/front/category/list',
    method: 'get',
    params: query
  }, options)
}

// 查询树
export function queryTree(query, options) {
  return request({
    url: '/front/category/treelist',
    method: 'get',
    params: query
  }, options)
}

// 查询单个
export function queryItem(query, options) {
  return request({
    url: '/front/news/{id}',
    method: 'get',
    params: query
  }, options)
}
