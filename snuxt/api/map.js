import request from '@/utils/request'

// 查询标记类型数据
export function queryType(query, options) {
  return request({
    url: '{host}/mock/data/scenicBuildType.json',
    method: 'get',
    params: query
  }, options)
}

// 查询标记数据
export function queryMarker(query, options) {
  return request({
    url: '{host}/mock/data/scenicBuild.json',
    method: 'get',
    params: query
  }, options)
}

// 查询区块数据
export function queryPolygon(query, options) {
  return request({
    url: '{host}/mock/data/scenicArea.json',
    method: 'get',
    params: query
  }, options)
}

