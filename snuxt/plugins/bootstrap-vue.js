import Vue from 'vue'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
// import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'
 
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)