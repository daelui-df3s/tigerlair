import resolver from '../components/resolver.js'
import API from '../api/service.api.js'

// api地址解析
const api = resolver.solveAPI({ api: API })
// 服务生成
const service = resolver.solveService({ api })

export default service

export {
  api
}
