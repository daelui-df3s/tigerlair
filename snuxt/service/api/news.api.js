export default {
	"queryListPage": {
		"url": "/front/news/list",
		// "url": "/mock/data/news-list.json",
		"desc": "查询列表",
		"method": "get",
		"params": {
			"orderByColumn": "isTop desc, publishDate desc, id",
			"isAsc": "DESC"
		}
	},
	"queryItem": {
		"url": "/front/news/{id}",
		"desc": "查询",
		"method": "get"
	}
}
