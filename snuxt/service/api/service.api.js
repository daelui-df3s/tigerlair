export default {
	"queryListPage": {
    "url": '{host}/mock/data/service-list.json',
		"desc": "查询列表",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}//mock/data/service-item.json",
		"desc": "查询",
		"method": "get"
	}
}
