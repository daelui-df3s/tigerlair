const base = '/tigerlair/saas/snuxt/'

export default {
  server: {
    host: '0.0.0.0', // 监听所有接口
    port: 3604
  },
  // Target: https://go.nuxtjs.dev/config-target
  // target: 'static',
  target: 'server',

  // 根路径
  router: {
    base: base
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'NuxtJS',
    htmlAttrs: {
      lang: 'zh'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: base + 'favicon.ico' },
      { rel: 'stylesheet', href: base + 'assets/css/global.css' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // './assets/css/global.css'
    // 'leaflet/dist/leaflet.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/axios',
    {src: '@/plugins/service.js'},
    {src: '@/plugins/jquery.js', ssr: false, mode: 'client'},
    {src: '@/plugins/bootstrap-vue.js', ssr: false, mode: 'client'},
    {src: '@/plugins/bootstrap.js', ssr: false, mode: 'client'},
    {src: '@/plugins/bootstrap-icons', ssr: true, mode: 'client'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    '@nuxtjs/axios',
    'bootstrap-vue/nuxt',
    '@nuxtjs/style-resources'
  ],

  axios: {
    proxy: true // 开启跨域
  },

  styleResources: {
    less: [
      './assets/css/common.less'
    ]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
  }
}
