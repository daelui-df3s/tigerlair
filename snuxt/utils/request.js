// 数据请求
import resolver from './resolver.js'

const request = function (options, config) {
  options = options || {}
  config = config || {}
  let opts = Object.assign({}, options)
  delete opts.data
  delete opts.params
  let api = { fetch: opts }
  let service = resolver.solveService({ api, $http: config.$http })
  return service.fetch(options.data || options.params, options)
}

export default request
