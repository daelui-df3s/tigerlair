/**
 * @description 应用配置
*/

// /* 运行模式配置 */
// (function () {
//   let origin = location.origin
//   let domain = /daelui.com/.test(origin) ? 'http://www.daelui.com' : 'http://www.daelui.cn'
//   // 开发环境配置
//   const dev = {
//     mode: 'dev',
//     title: '开发模式',
//     // 服务匹配规则列表
//     rules: [
//       {rule: 'host', url: '//127.0.0.1:3601'}
//     ],
//     // 环境域名
//     domain,
//     // 模块加载器
//     loader: {
//       path: '@daelui/pigjs/latest/dist/system.min.js',
//       registry: domain + '/pool/prod/packages.json',
//       rootUrl: domain + '/pool/prod/packages'
//     },
//     // 替换标识符
//     identifyMap: {},
//     // 元数据
//     meta: {
//       app: './app.meta.js',
//       page: domain + '/tigerlair/saas/viewrelease/item?vpid={pageId}',
//       components: domain + '/tigerlair/saas/viewcomp/list/all'
//     }
//   }

//   // 生产环境配置
//   const prod = {
//     mode: 'prod',
//     title: '生产模式',
//     // 服务匹配规则列表
//     rules: [
//       {rule: 'host', url: '//www.daelui.cn'}
//     ],
//     // 环境域名
//     domain,
//     // 模块加载器
//     loader: {
//       path: '@daelui/pigjs/latest/dist/system.min.js',
//       registry: domain + '/pool/prod/packages.json',
//       rootUrl: domain + '/pool/prod/packages'
//     },
//     // 替换标识符
//     identifyMap: {},
//     // 元数据
//     meta: {
//       app: './app.meta.js',
//       page: domain + '/tigerlair/saas/viewrelease/item?vpid={pageId}',
//       components: domain + '/tigerlair/saas/viewcomp/list/all'
//     }
//   }

//   /* 应用配置 */
//   const application = {
//     dev, prod,
//     // 运行模式配置
//     mode: {dev, prod}.prod
//   }

//   window.application = window.application || {}
//   window.application.app = application
// })();

// 开发环境
let domain = 'http://www.daelui.cn'
const dev = {
  protocol: 'http:',
  baseURL: 'http://192.168.110.198:9000/qianfoyan-api',
  basePATH: '/qianfoyan-api',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  rules: [
    { key: 'host', value: 'http://192.168.110.198:9000/qianfoyan-api' }
  ],
  // 模块加载器
  loader: {
    path: '@daelui/pigjs/latest/dist/system.min.js',
    registry: domain + '/pool/prod/packages.json',
    rootUrl: domain + '/pool/prod/packages'
  },
  // 替换标识符
  identifyMap: {},
  // 元数据
  meta: {
    app: './app.meta.js',
    page: domain + '/tigerlair/saas/viewrelease/item?vpid={pageId}',
    components: domain + '/tigerlair/saas/viewcomp/list/all'
  }
}

// 生产环境
domain = 'http://www.daelui.com'
const prod = {
  protocol: 'http:',
  baseURL: 'http://192.168.110.198:9000/qianfoyan-api',
  basePATH: '/qianfoyan-api',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  rules: [
    { key: 'host', value: 'http://192.168.110.198:9000/qianfoyan-api' }
  ],
  // 模块加载器
  loader: {
    path: '@daelui/pigjs/latest/dist/system.min.js',
    registry: domain + '/pool/prod/packages.json',
    rootUrl: domain + '/pool/prod/packages'
  },
  // 替换标识符
  identifyMap: {},
  // 元数据
  meta: {
    app: './app.meta.js',
    page: domain + '/tigerlair/saas/viewrelease/item?vpid={pageId}',
    components: domain + '/tigerlair/saas/viewcomp/list/all'
  }
}

const env = /dev/.test(process.env.npm_lifecycle_event) ? dev : prod

export default env