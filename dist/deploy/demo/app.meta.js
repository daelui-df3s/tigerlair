(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.unknown = mod.exports;
  }
})(this, function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    "id": "lw8uo3tluwdy",
    "name": "代码分享",
    "path": "share",
    "mid": "luateqiukepr",
    "status": 1,
    "order": 1,
    "meta": [{
      "id": "lw8unjgzb4sg",
      "name": "地图坐标定位",
      "pageId": "lv59ytb8wfc7",
      "path": "cesium-location",
      "isDefault": false
    }, {
      "id": "lw8unonfkrdi",
      "name": "加载WMTS/WMS/UT图层",
      "pageId": "luhr5t6lh8e3",
      "path": "cesium-load-layer",
      "isDefault": false
    }, {
      "id": "lw8uns3vnre3",
      "name": "在线语音播报",
      "pageId": "lva7m7go22am",
      "path": "web-speech-play",
      "isDefault": 1,
      "children": [{
        "id": "lw8unz6amh68",
        "name": "在线语音连续播报",
        "pageId": "lvaf8uowlche",
        "path": "web-speech-playlist",
        "isDefault": false
      }]
    }]
  };
});