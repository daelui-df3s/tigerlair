(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vue"), require("@daelui/vdog/dist/components.js"), require("@daelui/vdog/dist/icons.js"), require("@daelui/dogjs/dist/components.js"), require("@daelui/dogjs/dist/libs/coder.js"), require("@babel/standalone"), require("@daelui/vdog/dist/coder.js"));
	else if(typeof define === 'function' && define.amd)
		define(["vue", "@daelui/vdog/dist/components.js", "@daelui/vdog/dist/icons.js", "@daelui/dogjs/dist/components.js", "@daelui/dogjs/dist/libs/coder.js", "@babel/standalone", "@daelui/vdog/dist/coder.js"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("vue"), require("@daelui/vdog/dist/components.js"), require("@daelui/vdog/dist/icons.js"), require("@daelui/dogjs/dist/components.js"), require("@daelui/dogjs/dist/libs/coder.js"), require("@babel/standalone"), require("@daelui/vdog/dist/coder.js")) : factory(root["vue"], root["@daelui/vdog/dist/components.js"], root["@daelui/vdog/dist/icons.js"], root["@daelui/dogjs/dist/components.js"], root["@daelui/dogjs/dist/libs/coder.js"], root["@babel/standalone"], root["@daelui/vdog/dist/coder.js"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})((typeof self !== 'undefined' ? self : this), function(__WEBPACK_EXTERNAL_MODULE__748__, __WEBPACK_EXTERNAL_MODULE__4236__, __WEBPACK_EXTERNAL_MODULE__7792__, __WEBPACK_EXTERNAL_MODULE__8819__, __WEBPACK_EXTERNAL_MODULE__6470__, __WEBPACK_EXTERNAL_MODULE__9717__, __WEBPACK_EXTERNAL_MODULE__7443__) {
return /******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ 5377:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ box; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/box/index.vue?vue&type=template&id=51274f3a
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('CPRender', {
    attrs: {
      "cpr": _vm.cpr,
      "properts": _vm.properts,
      "layout": _vm.layout,
      "events": _vm.events,
      "resource": _vm.resource,
      "theme": _vm.theme,
      "childComponents": _vm.childComponents,
      "value": _vm.value
    }
  });
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./src/views/dview/page/components/cprender.vue + 5 modules
var cprender = __webpack_require__(8735);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/box/index.vue?vue&type=script&lang=js

/* harmony default export */ var boxvue_type_script_lang_js = ({
  extends: cprender/* default */.Z,
  components: {
    CPRender: cprender/* default */.Z
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/box/index.vue?vue&type=script&lang=js
 /* harmony default export */ var components_boxvue_type_script_lang_js = (boxvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/box/index.vue





/* normalize component */
;
var component = (0,componentNormalizer/* default */.Z)(
  components_boxvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var box = (component.exports);

/***/ }),

/***/ 743:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/box/property.vue?vue&type=template&id=50c1bb84&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('tabs', {
    attrs: {
      "type": "beforeline",
      "position": "top",
      "isLazyRender": true,
      "activeName": 'properts'
    }
  }, [_c('tabs-item', {
    attrs: {
      "title": "属性",
      "name": 'properts'
    }
  }, [_c('CPRender', {
    key: _vm.key,
    ref: "properts",
    attrs: {
      "cpr": _vm.property.component,
      "value": _vm.property.properts,
      "properts": _vm.property.properts,
      "layout": _vm.property.layout,
      "events": _vm.property.events,
      "resource": _vm.property.resource,
      "theme": _vm.property.theme,
      "childComponents": _vm.property.childComponents
    },
    on: {
      "update": _vm.handleUpdate
    }
  })], 1), _c('tabs-item', {
    attrs: {
      "title": "布局",
      "name": 'layout'
    }
  }, [_c('FormGroup', {
    attrs: {
      "label": "左距比例",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.property.layout.x,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout, "x", $$v);
      },
      expression: "property.layout.x"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "上距比例",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.property.layout.y,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout, "y", $$v);
      },
      expression: "property.layout.y"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "宽度比例",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.property.layout.w,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout, "w", $$v);
      },
      expression: "property.layout.w"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "高度比例",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.property.layout.h,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout, "h", $$v);
      },
      expression: "property.layout.h"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "层级",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "inputType": "number"
    },
    model: {
      value: _vm.property.layout.zIndex,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout, "zIndex", $$v);
      },
      expression: "property.layout.zIndex"
    }
  })], 1)], 1), _c('tabs-item', {
    attrs: {
      "title": "事件",
      "name": 'events'
    }
  }, [_c('FormGroup', {
    attrs: {
      "label": "created",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.created,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "created", $$v);
      },
      expression: "property.events.created"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "beforeMount",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.beforeMount,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "beforeMount", $$v);
      },
      expression: "property.events.beforeMount"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "mounted",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.mounted,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "mounted", $$v);
      },
      expression: "property.events.mounted"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "beforeDestroy",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.beforeDestroy,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "beforeDestroy", $$v);
      },
      expression: "property.events.beforeDestroy"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "destroyed",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.destroyed,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "destroyed", $$v);
      },
      expression: "property.events.destroyed"
    }
  })], 1)], 1), _c('tabs-item', {
    attrs: {
      "title": "主题",
      "name": 'theme'
    }
  }), _c('tabs-item', {
    attrs: {
      "title": "资源",
      "name": 'resource',
      "className": "coder-group"
    }
  }, [_c('div', {
    staticClass: "d-container"
  }, [_c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead"
  }, [_vm._v("执行资源"), _c('span', {
    staticClass: "d-text-lightgray d-text-small"
  }, [_vm._v("(在组件初始后(created)执行)")])]), _c('FormGroup', {
    attrs: {
      "label": "JavaScript",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.resource.js,
      callback: function callback($$v) {
        _vm.$set(_vm.property.resource, "js", $$v);
      },
      expression: "property.resource.js"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "CSS",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "language": "css",
      "height": "94vh"
    },
    model: {
      value: _vm.property.resource.css,
      callback: function callback($$v) {
        _vm.$set(_vm.property.resource, "css", $$v);
      },
      expression: "property.resource.css"
    }
  })], 1), _c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead d-layout-flex-between-center"
  }, [_c('span', [_vm._v("引用资源"), _c('span', {
    staticClass: "d-text-lightgray d-text-small"
  }, [_vm._v("(在组件初始后(created)执行)")])]), _c('span', {
    staticClass: "d-text-primary d-text-linker",
    attrs: {
      "title": "添加资源"
    },
    on: {
      "click": _vm.handleAddFile
    }
  }, [_c('i', {
    staticClass: "el-icon-plus"
  })])]), _c('div', {
    staticClass: "d-section"
  }, [_c('Grid', {
    ref: "gridList",
    attrs: {
      "isPagerAble": false
    },
    model: {
      value: _vm.files,
      callback: function callback($$v) {
        _vm.files = $$v;
      },
      expression: "files"
    }
  }, [_c('GridColumn', {
    attrs: {
      "width": 45,
      "type": "index",
      "label": "序号",
      "className": "d-vm"
    }
  }), _c('GridColumn', {
    attrs: {
      "field": "type",
      "label": "类型",
      "width": "120"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('select', {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: scope.type,
            expression: "scope.type"
          }],
          staticClass: "d-form-select",
          on: {
            "change": function change($event) {
              var $$selectedVal = Array.prototype.filter.call($event.target.options, function (o) {
                return o.selected;
              }).map(function (o) {
                var val = "_value" in o ? o._value : o.value;
                return val;
              });
              _vm.$set(scope, "type", $event.target.multiple ? $$selectedVal : $$selectedVal[0]);
            }
          }
        }, _vm._l(_vm.types, function (item) {
          return _c('option', {
            key: item.type,
            domProps: {
              "value": item.type
            }
          }, [_vm._v(_vm._s(item.text))]);
        }), 0)];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "url",
      "label": "地址"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('FormInput', {
          attrs: {
            "title": scope.url
          },
          model: {
            value: scope.url,
            callback: function callback($$v) {
              _vm.$set(scope, "url", $$v);
            },
            expression: "scope.url"
          }
        })];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "action",
      "width": 60,
      "label": "操作",
      "className": "d-vm"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('div', {
          staticClass: "d-bar-tools d-bar-tools-condensed d-grid-actions"
        }, [_c('a', {
          staticClass: "d-tools-item d-text-theme",
          on: {
            "click": function click($event) {
              return _vm.handleDelFile(scope);
            }
          }
        }, [_c('DeleteIcon', {
          attrs: {
            "size": "sm"
          }
        })], 1)])];
      }
    }])
  })], 1)], 1)], 1)])], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/element-resize-detector/src/element-resize-detector.js
var element_resize_detector = __webpack_require__(6084);
var element_resize_detector_default = /*#__PURE__*/__webpack_require__.n(element_resize_detector);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
// EXTERNAL MODULE: external "@daelui/vdog/dist/components.js"
var dist_components_js_ = __webpack_require__(4236);
// EXTERNAL MODULE: external "@daelui/vdog/dist/coder.js"
var coder_js_ = __webpack_require__(7443);
// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
// EXTERNAL MODULE: ./src/views/dview/page/components/cprender.vue + 5 modules
var cprender = __webpack_require__(8735);
// EXTERNAL MODULE: ./src/service/stock/index.js + 4 modules
var stock = __webpack_require__(3085);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/box/property.vue?vue&type=script&lang=js







/* harmony default export */ var propertyvue_type_script_lang_js = ({
  components: {
    Paner: dist_components_js_.Paner,
    Tabs: dist_components_js_.Tabs,
    TabsItem: dist_components_js_.TabsItem,
    Coder: coder_js_.Coder,
    CoderEntry: coder_js_.CoderEntry,
    CPRender: cprender/* default */.Z,
    FormGroup: dist_components_js_.FormGroup,
    FormInput: dist_components_js_.FormInput,
    FormButton: dist_components_js_.FormButton,
    Grid: dist_components_js_.Grid,
    GridColumn: dist_components_js_.GridColumn,
    DeleteIcon: icons_js_.DeleteIcon
  },
  props: {
    stock: {
      type: Object,
      default() {
        return stock/* default */.ZP;
      }
    }
  },
  data() {
    return {
      // 属性面板状态
      property: {
        component: {
          component: 'div'
        },
        properts: {},
        events: {},
        resource: {},
        theme: {},
        layout: {
          x: 0,
          y: 0,
          w: 16,
          h: 16
        }
      },
      // 组件标识key
      key: '',
      types: [{
        type: 'script',
        text: 'JavaScript'
      }, {
        type: 'link',
        text: 'CSS'
      }],
      // 文件列表
      files: [
        // { id: strer.utid(), url : '', type: 'script' }
      ]
    };
  },
  watch: {
    // 选择的组件更新
    'stock.state.selectedComponent': {
      handler() {
        this.handleUpdateSelectedComponent();
      },
      deep: true
    },
    // 事件更新
    'property.properts': {
      handler() {
        let selectedComponent = this.stock.state.selectedComponent;
        this.$set(selectedComponent, 'properts', this.property.properts || {});
      },
      deep: true
    },
    // 布局更新
    'property.layout': {
      handler() {
        let selectedComponent = this.stock.state.selectedComponent;
        this.$set(selectedComponent, 'layout', this.property.layout || {});
      },
      deep: true
    },
    // 事件更新
    'property.events': {
      handler() {
        let selectedComponent = this.stock.state.selectedComponent;
        this.$set(selectedComponent, 'events', this.property.events || {});
      },
      deep: true
    },
    // 资源更新
    'property.resource': {
      handler() {
        let selectedComponent = this.stock.state.selectedComponent;
        this.$set(selectedComponent, 'resource', this.property.resource || {});
      },
      deep: true
    }
  },
  methods: {
    // 属性更新
    handleUpdate(data) {
      this.$set(this.property, 'properts', data || {});
      this.$emit('update', data);
    },
    // 更新属性
    handleUpdateSelectedComponent() {
      let selectedComponent = this.stock.state.selectedComponent;
      if (!selectedComponent || !selectedComponent.name) {
        return;
      }
      // 组件属性视图
      this.$set(this.property, 'component', {
        name: selectedComponent.name,
        version: selectedComponent.version,
        isProperty: true
      });
      // 属性
      this.$set(this.property, 'properts', selectedComponent.properts || {});
      // 布局
      this.$set(this.property, 'layout', selectedComponent.layout || {
        x: 0,
        y: 0,
        w: 16,
        h: 16
      });
      // 事件
      this.$set(this.property, 'events', selectedComponent.events || {});
      // 主题
      this.$set(this.property, 'theme', selectedComponent.theme || {});
      // 资源
      this.$set(this.property, 'resource', selectedComponent.resource || {});
      this.property.resource.files = Array.isArray(this.property.resource.files) ? this.property.resource.files : [];
      this.files = this.property.resource.files;
      this.key = selectedComponent.id || components_js_.strer.utid();
    },
    // 添加文件
    handleAddFile() {
      this.files.push({
        id: components_js_.strer.utid(),
        url: '',
        type: 'script'
      });
    },
    // 删除文件
    handleDelFile(item) {
      this.files = this.files.filter(node => {
        return item !== node;
      });
    }
  },
  mounted() {
    this.handleUpdateSelectedComponent();
    // 监听元素变化，编辑器自适应
    let erd = element_resize_detector_default()();
    erd.listenTo(this.$el, () => {
      for (let key in this.$refs) {
        if (/coder/.test(key)) {
          let monacoEditor = this.$refs[key].monacoEditor;
          monacoEditor && monacoEditor.layout();
        }
      }
    });
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/box/property.vue?vue&type=script&lang=js
 /* harmony default export */ var box_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/box/property.vue?vue&type=style&index=0&id=50c1bb84&prod&lang=less&scoped=true
var propertyvue_type_style_index_0_id_50c1bb84_prod_lang_less_scoped_true = __webpack_require__(3259);
;// CONCATENATED MODULE: ./src/views/dview/page/components/box/property.vue?vue&type=style&index=0&id=50c1bb84&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/box/property.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  box_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "50c1bb84",
  null
  
)

/* harmony default export */ var property = (component.exports);

/***/ }),

/***/ 8735:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ cprender; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/cprender.vue?vue&type=template&id=a70b6f6e
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _vm.isLoading ? _c('div', {
    staticClass: "d-cprender-loading"
  }, [_vm.isShowLoadingIcon ? _c('RotorIcon', {
    staticClass: "dam-rotating"
  }) : _vm._e(), _vm.isShowLoadingText ? _c('span', [_vm._v("加载中...")]) : _vm._e(), _vm._t("loading")], 2) : _c(_vm.component.component, _vm._b({
    ref: "view",
    tag: "component",
    on: {
      "update": _vm.handleUpdate,
      "hook:created": function hookCreated($event) {
        return _vm.triggerEvent('created', _vm.$children);
      },
      "hook:beforeMount": function hookBeforeMount($event) {
        return _vm.triggerEvent('beforeMount', _vm.$children);
      },
      "hook:mounted": function hookMounted($event) {
        return _vm.triggerEvent('mounted', _vm.$children);
      },
      "hook:beforeDestroy": function hookBeforeDestroy($event) {
        return _vm.triggerEvent('beforeDestroy', _vm.$children);
      },
      "hook:destroyed": function hookDestroyed($event) {
        return _vm.triggerEvent('destroyed', _vm.$children);
      }
    }
  }, 'component', _vm.$props, false), [_vm._t("default")], 2);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/libs/coder.js"
var coder_js_ = __webpack_require__(6470);
var coder_js_default = /*#__PURE__*/__webpack_require__.n(coder_js_);
// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(748);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);
// EXTERNAL MODULE: ./src/service/stock/index.js + 4 modules
var stock = __webpack_require__(3085);
// EXTERNAL MODULE: external "@babel/standalone"
var standalone_ = __webpack_require__(9717);
;// CONCATENATED MODULE: ./src/views/dview/page/components/repository.js





const win = typeof self !== 'undefined' ? window : __webpack_require__.g;
win.Babel = standalone_;

/**
 * @function 获取展示组件
*/
const getComponent = function getComponent(options) {
  let component = resolveComponent(options);
  return {
    component: component.mainComponent,
    name: component.name,
    version: component.version
  };
};

/**
 * @function 获取属性组件
*/
const getPropertyComponent = function getPropertyComponent(options) {
  let component = resolveComponent(options);
  component = component || {};
  return {
    component: component.propertyComponent,
    name: component.name,
    version: component.version
  };
};

// 解析组件对象
const resolveComponent = function resolveComponent(options) {
  options = options || {};
  // 从注册表中取出
  let components = [...stock/* state */.SB.layoutCollection, ...stock/* state */.SB.componentCollection].filter(item => {
    return item.name === options.name;
  });
  // 按版本号进行排序
  components = components.sort((a, b) => {
    let arrA = String(a.version || '').split(/\s*.\s*/);
    let arrB = String(b.version || '').split(/\s*.\s*/);
    let res = components_js_.arrayer.sortGroup(arrA, arrB, 'asc', true);
    return res;
  });
  let component = components.find(item => {
    return item.version === options.version;
  });

  // 无匹配则默认选最新版本
  component = component || components[0];
  component = component || {
    component: 'div',
    name: 'div',
    version: '1.0.0'
  };
  // 远程解析
  component = resolveRemote(component);
  return component;
};

// 远程解析
const resolveRemote = function resolveRemote(component) {
  let repository = component.repository || {};
  // 远程加载
  if (!component.mainComponent) {
    // 直接地址
    if (component.main) {
      component.mainComponent = resolve => remoteComponent(resolve, component.main);
    }
    // 编码
    else if (component.mainCode) {
      component.mainComponent = resolve => coder_js_default().vue.resolve(component.mainCode, (external_vue_default()), $pig).then(res => resolve(res));
    }
    // 模板地址
    else if (repository.main) {
      let url = repository.main;
      url = url.replace('{name}', component.name).replace('{version}', component.version || 'latest');
      component.mainComponent = resolve => remoteComponent(resolve, url);
    }
  }
  if (!component.propertyComponent) {
    // 直接地址
    if (component.property) {
      component.propertyComponent = remoteComponent(resolve, component.property);
    }
    // 编码
    else if (component.propertyCode) {
      component.propertyComponent = resolve => coder_js_default().vue.resolve(component.propertyCode, (external_vue_default()), $pig).then(res => resolve(res));
    }
    // 模板地址
    else if (repository.property) {
      let url = repository.property;
      url = url.replace('{name}', component.name).replace('{version}', component.version || 'latest');
      component.propertyComponent = resolve => remoteComponent(resolve, url);
    }
  }
  return component;
};

// 获取组件
async function remoteComponent(resolve, url) {
  let res = await $pig.import(url);
  resolve(res);
}
/* harmony default export */ var repository = ({
  getComponent,
  getPropertyComponent
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/cprender.vue?vue&type=script&lang=js





const cprendervue_type_script_lang_js_win = typeof self !== 'undefined' ? window : __webpack_require__.g;
cprendervue_type_script_lang_js_win.Babel = standalone_;
/* harmony default export */ var cprendervue_type_script_lang_js = ({
  components: {
    RotorIcon: icons_js_.RotorIcon
  },
  props: {
    cpr: {
      type: Object,
      default() {
        return {
          component: 'div',
          isProperty: false
        };
      }
    },
    properts: {
      default() {
        return {};
      }
    },
    layout: {
      type: Object,
      default() {
        return {};
      }
    },
    events: {
      default() {
        return {};
      }
    },
    resource: {
      default() {
        return {};
      }
    },
    theme: {
      default() {
        return {};
      }
    },
    childComponents: {
      default() {
        return [];
      }
    },
    value: {
      default() {
        return {};
      }
    },
    isShowLoadingIcon: {
      type: Boolean,
      default: true
    },
    isShowLoadingText: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      component: {
        // 动态组件
        component: 'div'
      },
      isLoading: true // 加载中
    };
  },

  watch: {
    cpr: {
      handler() {
        this.syncComponent();
      },
      deep: true
    },
    value: {
      handler() {
        this.syncComponent();
      },
      deep: true
    }
  },
  methods: {
    // 属性更新
    handleUpdate(data) {
      this.$emit('update', data);
    },
    // 同步组件
    syncComponent() {
      let result = this.cpr.isProperty ? getPropertyComponent(this.cpr) : getComponent(this.cpr);
      this.cpr.version = result.version;
      this.component.component = result.component;
    },
    // 触发自定义事件
    triggerEvent(name, $children) {
      let vm = ($children || [])[0] || {};
      triggerEvent.call(vm, name);
      // 初始化前加载资源
      if (name === 'created') {
        this.loadResource(vm);
      }
    },
    // 加载资源
    loadResource(vm) {
      let res = vm.resource || vm.$attrs.resource || {};
      // 注入js
      if (res.js) {
        this.invoke(res.js);
      }
      // 注入css
      if (res.css) {
        components_js_.domer.appendElement('style', {
          type: 'text/css',
          innerHTML: res.css
        });
      }
      // 引入文件
      let types = {
        script: {
          type: 'text/javascript',
          src: '{url}'
        },
        link: {
          rel: 'stylesheet',
          href: '{url}'
        }
      };
      if (Array.isArray(res.files)) {
        res.files = res.files.filter(item => {
          return !!String(item.url || '').trim() || !!String(item.content || '').trim();
        });
        res.files.forEach(item => {
          // 按名称注册
          if (item.name) {
            if (item.url) {
              $pig.addSourceMap({
                map: {
                  [item.name]: item.url
                }
              });
            } else if (item.content) {
              let url = /^(https?:)?\/\//.test(item.name) ? item.name : location.origin + '/' + item.name;
              $pig.addSourceMap({
                map: {
                  [item.name]: url
                }
              });
              coder_js_default().resolve(item.content, $pig).then(mod => {
                mod = mod || {};
                $pig.set(url, mod);
              });
            }
          }
          // 地址加载
          else if (item.url) {
            let type = item.type || 'script';
            let node = types[type];
            for (let key in node) {
              node[key] = node[key].replace('{url}', item.url);
            }
            components_js_.domer.appendElement(type, node);
          }
          // 内容加载
          else if (item.content) {
            this.invoke(item.content);
          }
        });
      }
    },
    invoke(content) {
      try {
        coder_js_default().resolve(content, $pig).then(mod => {
          let func = mod.default;
          typeof func === 'function' && func.apply(this);
        });
      } catch (e) {
        console.log(e);
      }
    }
  },
  created() {
    this.syncComponent();
  },
  mounted() {
    setTimeout(() => {
      this.isLoading = false;
    }, 250);
  }
});

// 触发自定义事件
function triggerEvent(name) {
  var _this$$attrs;
  let events = this.events || ((_this$$attrs = this.$attrs) === null || _this$$attrs === void 0 ? void 0 : _this$$attrs.events) || {};
  let code = events[name];
  if (code) {
    coder_js_default().resolve(code, $pig).then(mod => {
      mod = mod || {};
      let func = mod.default;
      typeof func === 'function' && func.apply(this);
    });
  }
}
;// CONCATENATED MODULE: ./src/views/dview/page/components/cprender.vue?vue&type=script&lang=js
 /* harmony default export */ var components_cprendervue_type_script_lang_js = (cprendervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/cprender.vue?vue&type=style&index=0&id=a70b6f6e&prod&lang=less
var cprendervue_type_style_index_0_id_a70b6f6e_prod_lang_less = __webpack_require__(8689);
;// CONCATENATED MODULE: ./src/views/dview/page/components/cprender.vue?vue&type=style&index=0&id=a70b6f6e&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/cprender.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  components_cprendervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var cprender = (component.exports);

/***/ }),

/***/ 3394:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ drag_resizer; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/drag-resizer.vue?vue&type=template&id=19718626
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "d-drag-resizer",
    class: {
      'd-dr-active': _vm.isActive,
      'd-dr-dragable': _vm.isDragAble
    }
  }, [_vm._t("default"), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: !!_vm.des.length,
      expression: "!!des.length"
    }],
    staticClass: "d-dr-direction"
  }, [_c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('nesw'),
      expression: "hasDirection('nesw')"
    }],
    ref: "nesw",
    staticClass: "d-dr-de-nesw"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('n'),
      expression: "hasDirection('n')"
    }],
    ref: "n",
    staticClass: "d-dr-de d-dr-de-n"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('e'),
      expression: "hasDirection('e')"
    }],
    ref: "e",
    staticClass: "d-dr-de d-dr-de-e"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('s'),
      expression: "hasDirection('s')"
    }],
    ref: "s",
    staticClass: "d-dr-de d-dr-de-s"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('w'),
      expression: "hasDirection('w')"
    }],
    ref: "w",
    staticClass: "d-dr-de d-dr-de-w"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('nw'),
      expression: "hasDirection('nw')"
    }],
    ref: "nw",
    staticClass: "d-dr-de d-dr-de-nw"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('no'),
      expression: "hasDirection('no')"
    }],
    ref: "no",
    staticClass: "d-dr-de d-dr-de-no"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('ne'),
      expression: "hasDirection('ne')"
    }],
    ref: "ne",
    staticClass: "d-dr-de d-dr-de-ne"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('ea'),
      expression: "hasDirection('ea')"
    }],
    ref: "ea",
    staticClass: "d-dr-de d-dr-de-ea"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('se'),
      expression: "hasDirection('se')"
    }],
    ref: "se",
    staticClass: "d-dr-de d-dr-de-se"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('so'),
      expression: "hasDirection('so')"
    }],
    ref: "so",
    staticClass: "d-dr-de d-dr-de-so"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('sw'),
      expression: "hasDirection('sw')"
    }],
    ref: "sw",
    staticClass: "d-dr-de d-dr-de-sw"
  }), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.hasDirection('we'),
      expression: "hasDirection('we')"
    }],
    ref: "we",
    staticClass: "d-dr-de d-dr-de-we"
  })])], 2);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/drag-resizer.vue?vue&type=script&lang=js

/* harmony default export */ var drag_resizervue_type_script_lang_js = ({
  props: {
    // 是否可拖拽
    isDragAble: {
      type: Boolean,
      default: true
    },
    // 是否可缩放
    isResizeAble: {
      type: Boolean,
      default: true
    },
    // 方向
    directions: {
      type: Array,
      default() {
        return ['n', 'e', 's', 'w', 'nw', 'no', 'ne', 'ea', 'se', 'so', 'sw', 'we'];
      }
    },
    // 是否选中状态
    isActive: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      des: [],
      // 'nesw', 'n', 'e', 's', 'w', 'nw', 'no', 'ne', 'ea', 'se', 'so', 'sw', 'we'
      dragers: []
    };
  },
  methods: {
    // 获取移动的元素
    getParentEl() {
      return this.$el.parentNode;
    },
    // 获取移动的元素
    getTargetEl() {
      return this.$el;
    },
    // 开始监测
    handleDragStart(args) {
      let drager = args.drager;
      let options = drager.options;
      // 拖拽
      if (options.de === 'nesw') {
        this.$emit('drag-start', args);
      } else {
        // 缩放
        this.$emit('resize-start', args);
      }
    },
    // 移动监测
    handleDragMove(args) {
      let targetEl = this.getTargetEl();
      let drager = args.drager;
      let options = drager.options;
      let oldTargetStyle = args.oldTargetStyle;
      let newTargetStyle = args.newTargetStyle;
      if (options.de === 'nesw') {
        targetEl.style.top = newTargetStyle.top + 'px';
        targetEl.style.left = newTargetStyle.left + 'px';
        this.$emit('drag-move', args);
      } else if (options.de === 'se') {
        targetEl.style.width = oldTargetStyle.width + newTargetStyle.disX + 'px';
        targetEl.style.height = oldTargetStyle.height + newTargetStyle.disY + 'px';
        newTargetStyle.width = oldTargetStyle.width + newTargetStyle.disX;
        newTargetStyle.height = oldTargetStyle.height + newTargetStyle.disY;
        this.$emit('resize-move', args);
      } else if (options.de === 'ea' || options.de === 'e') {
        targetEl.style.width = oldTargetStyle.width + newTargetStyle.disX + 'px';
        newTargetStyle.width = oldTargetStyle.width + newTargetStyle.disX;
        this.$emit('resize-move', args);
      } else if (options.de === 'so' || options.de === 's') {
        targetEl.style.height = oldTargetStyle.height + newTargetStyle.disY + 'px';
        newTargetStyle.height = oldTargetStyle.height + newTargetStyle.disY;
        this.$emit('resize-move', args);
      }
    },
    // 结束监测
    handleDragEnd(args) {
      let drager = args.drager;
      let options = drager.options;
      if (options.de === 'nesw') {
        this.$emit('drag-end', args);
      } else if (/^(se|ea|e|so|s)$/.test(options.de)) {
        let targetEl = this.getTargetEl();
        let newTargetStyle = args.newTargetStyle;
        newTargetStyle.width = parseFloat(targetEl.style.width);
        newTargetStyle.height = parseFloat(targetEl.style.height);
        this.$emit('resize-end', args);
      }
    },
    // 是否含有方向
    hasDirection(de) {
      return !!this.des.find(item => item === de);
    }
  },
  mounted() {
    // 方位
    let des = [];
    // 拖拽
    if (this.isDragAble) {
      des.push('nesw');
    }
    // 缩放
    if (this.isResizeAble) {
      let defs = ['n', 'e', 's', 'w', 'nw', 'no', 'ne', 'ea', 'se', 'so', 'sw', 'we'];
      let directions = this.directions;
      directions = Array.isArray(directions) ? directions : [];
      defs.forEach(item => {
        let de = directions.find(node => node === item);
        if (de) {
          des.push(de);
        }
      });
    }
    this.des = des;

    // 事件绑定
    this.dragers = des.map(de => {
      let el = this.$refs[de];
      if (!el) {
        return false;
      }
      // 拖拽绑定
      const drager = new components_js_.Drager({
        el: el,
        targetEl: this.getTargetEl(),
        isSyncStyle: false,
        isSimulation: false,
        de
      });
      // 开始
      drager.$on('onDragStart', args => {
        this.handleDragStart(args);
      });
      // 移动
      drager.$on('onDragMove', args => {
        this.handleDragMove(args);
      });
      // 结束
      drager.$on('onDragEnd', args => {
        this.handleDragEnd(args);
      });
      return drager;
    });
  },
  beforeDestroy() {
    this.dragers.forEach(item => {
      components_js_.Drager.remove(item);
    });
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/drag-resizer.vue?vue&type=script&lang=js
 /* harmony default export */ var components_drag_resizervue_type_script_lang_js = (drag_resizervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/drag-resizer.vue?vue&type=style&index=0&id=19718626&prod&lang=less
var drag_resizervue_type_style_index_0_id_19718626_prod_lang_less = __webpack_require__(7978);
;// CONCATENATED MODULE: ./src/views/dview/page/components/drag-resizer.vue?vue&type=style&index=0&id=19718626&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/drag-resizer.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  components_drag_resizervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var drag_resizer = (component.exports);

/***/ }),

/***/ 9770:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ container; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/container.vue?vue&type=template&id=398648b8
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    class: _vm.className,
    style: _vm.containerStyle
  }, [_c('div', {
    ref: "layout",
    staticClass: "d-layout-render",
    style: _vm.renderStyle
  }, [_c('div', {
    ref: "container",
    staticClass: "d-layout-container"
  }, [_vm.isRendered ? _vm._t("default") : _vm._e()], 2)])]);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/element-resize-detector/src/element-resize-detector.js
var element_resize_detector = __webpack_require__(6084);
var element_resize_detector_default = /*#__PURE__*/__webpack_require__.n(element_resize_detector);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/constant.js
var constant = __webpack_require__(5670);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/container.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }


/* harmony default export */ var containervue_type_script_lang_js = ({
  provide() {
    return {
      container: this
    };
  },
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    },
    // 主题
    theme: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.theme);
      }
    }
  },
  watch: {
    layout: {
      handler() {
        this.syncLayoutStyle();
      },
      deep: true
    },
    theme: {
      handler() {
        this.syncLayoutStyle();
      },
      deep: true
    }
  },
  data() {
    return {
      constant: constant/* default */.Z,
      className: 'd-layout-absolute',
      // 容器样式
      containerStyle: {},
      // 渲染区域
      renderStyle: {},
      // 是否渲染完成
      isRendered: false
    };
  },
  methods: {
    // 布局配置适配
    adapterLayout() {
      let constant = this.constant;
      let cl = this.layout;
      let columns = parseInt(cl.columns);
      cl.columns = isNaN(columns) ? constant.layout.columns : columns;
      let rows = parseInt(cl.rows);
      cl.rows = isNaN(rows) ? constant.layout.rows : rows;
      let referWidth = parseInt(cl.referWidth);
      cl.referWidth = isNaN(referWidth) ? null : referWidth;
      cl.referWidth = isNaN(referWidth) ? cl.forceReferWidth ? constant.layout.referWidth : null : referWidth;
      let referHeight = parseInt(cl.referHeight);
      cl.referHeight = isNaN(referHeight) ? null : referHeight;
      cl.margin = cl.margin || constant.layout.margin;
      cl.padding = cl.padding || constant.layout.padding;
    },
    // 适应容器样式
    fitContainerStyle() {
      let contaienr = this.$refs.container;
      let nodes = contaienr.childNodes || [];
      nodes = Array.from(nodes);
      let max = 0;
      nodes.forEach(item => {
        let style = getComputedStyle(item);
        let top = parseFloat(style.top);
        let height = parseFloat(style.height);
        max = top + height > max ? top + height : max;
      });
      if (this.$el) {
        let layoutStyle = this.getContainerStyle();
        max = max > layoutStyle.height ? max : layoutStyle.height;
      }
      if (max) {
        contaienr.style.height = max + 'px';
      }
    },
    // 同步布局样式
    syncLayoutStyle() {
      this.adapterLayout();
      let cl = this.layout;
      let m = cl.margin;
      let p = cl.padding;
      // 获取宽度
      const w = col => {
        // 直接单位
        if (/px/.test(col)) {
          return col;
        }
        return col + 'px';
      };
      let margin = "".concat(w(m.top), " ").concat(w(m.right), " ").concat(w(m.bottom), " ").concat(w(m.left));
      let padding = "".concat(w(p.top), " ").concat(w(p.right), " ").concat(w(p.bottom), " ").concat(w(p.left));
      this.renderStyle = {
        margin,
        padding
      };
      let theme = this.theme || {};
      let bgColor = theme.bgColor || '';
      let bgImage = theme.bgImage || '';
      let bgSize = theme.bgSize || '';
      let containerStyle = {};
      if (bgColor) {
        containerStyle.backgroundColor = bgColor;
      }
      if (bgImage) {
        containerStyle.backgroundImage = 'url(' + bgImage + ')';
      }
      if (bgSize) {
        containerStyle.backgroundSize = bgSize;
      }
      this.containerStyle = containerStyle;
    },
    // 获取容器margin,padding
    getBoxStyle() {
      let cl = this.layout;
      let m = cl.margin;
      let p = cl.padding;
      // 获取宽度
      const w = col => {
        col = parseFloat(col) || 0;
        col = col < 0 ? 0 : col;
        return col;
      };
      for (let k in m) {
        m[k] = w(m[k]);
      }
      for (let k in p) {
        p[k] = w(p[k]);
      }
      return {
        margin: m,
        padding: p
      };
    },
    // 获取计算后的容器样式
    getContainerStyle() {
      let container = this.$el || document.body;
      let style = getComputedStyle(container);
      let boxStyle = this.getBoxStyle();
      let width = parseFloat(style.width);
      let height = parseFloat(style.height);
      let margin = boxStyle.margin;
      let padding = boxStyle.padding;
      width = width - margin.left - margin.right - padding.left - padding.right;
      height = height - margin.top - margin.bottom - padding.top - padding.bottom;
      return {
        width: width,
        height: height
      };
    },
    // 获取容器参考宽高
    getRefer() {
      let cl = this.layout;
      // 容器样式
      let style = this.getContainerStyle();
      // 参考宽度
      let width = parseInt(cl.referWidth);
      // 值不存在取容器宽度
      if (isNaN(width)) {
        width = style.width;
      }
      // 默认取参考高度
      let height = parseInt(cl.referHeight);
      // 值不存在取容器宽度
      if (isNaN(height)) {
        height = style.width;
      }
      // 高度适配取容器高度
      if (cl.isFixHeight) {
        height = style.height;
      }
      return {
        width,
        height
      };
    },
    // 获取每项样式
    getItemStyle(layoutItem) {
      this.adapterLayout();
      let layout = layoutItem.layout || {};
      let l = parseInt(layout.x || 0);
      let t = parseInt(layout.y || 0);
      let w = parseInt(layout.w || 16);
      let h = parseInt(layout.h || 16);
      let cl = this.layout;
      // 参考宽高
      let refer = this.getRefer();
      // 获取宽度
      let width = refer.width;
      let height = refer.height;
      const cw = (col, w) => {
        // 比例换算成单位
        return col * w / cl.columns + 'px';
      };
      const ch = (col, h) => {
        // 比例换算成单位
        return col * h / cl.rows + 'px';
      };
      let itemStyle = {
        left: cw(l, width),
        width: cw(w, width),
        top: ch(t, height),
        height: ch(h, height)
      };
      if (layout.zIndex) {
        itemStyle['z-index'] = layout.zIndex;
      }
      return itemStyle;
    },
    // 获取每项布局配置
    getItemLayout(layoutItem, args) {
      let columns = this.layout.columns;
      let rows = this.layout.rows;
      let layout = layoutItem.layout || {};
      let drager = args.drager;
      let options = drager.options;
      let newTargetStyle = args.newTargetStyle;
      // 参考宽高
      let refer = this.getRefer();
      // 获取宽度
      let width = refer.width;
      let height = refer.height;
      if (options.de === 'nesw') {
        let tc = Math.round(rows * newTargetStyle.top / height);
        let lc = Math.round(columns * newTargetStyle.left / width);
        lc = lc > columns - layout.w ? columns - layout.w : lc;
        layout.y = tc;
        layout.x = lc;
      } else if (options.de === 'se') {
        let wc = Math.round(columns * newTargetStyle.width / width);
        let hc = Math.round(rows * newTargetStyle.height / height);
        hc = hc > columns - layout.lc ? columns - layout.lc : hc;
        layout.w = wc;
        layout.h = hc;
      }
      return layout;
    },
    /**
     * @function 更新视图
    */
    updateView() {
      this.syncLayoutStyle();
      // 渲染完成
      setTimeout(() => {
        this.isRendered = true;
      }, 40);
    }
  },
  mounted() {
    // 更新视图
    this.updateView();
    // 自适应容器高度
    this.fitTimer = setInterval(() => {
      this.fitContainerStyle();
    }, 1000);
    // 监听元素变化，编辑器自适应
    let erd = element_resize_detector_default()();
    erd.listenTo(this.$refs.container, () => {
      // 更新视图
      this.updateView();
      this.$emit('changeStyle');
    });
  },
  beforeDestroy() {
    clearInterval(this.fitTimer);
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/container.vue?vue&type=script&lang=js
 /* harmony default export */ var absolute_containervue_type_script_lang_js = (containervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/container.vue?vue&type=style&index=0&id=398648b8&prod&lang=less
var containervue_type_style_index_0_id_398648b8_prod_lang_less = __webpack_require__(3528);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/container.vue?vue&type=style&index=0&id=398648b8&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/container.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  absolute_containervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var container = (component.exports);

/***/ }),

/***/ 9399:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ absolute; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/index.vue?vue&type=template&id=4446a4ba
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('LayoutContainer', {
    ref: "container",
    attrs: {
      "layout": _vm.layout,
      "theme": _vm.theme
    },
    on: {
      "changeStyle": _vm.updateView
    },
    nativeOn: {
      "click": function click($event) {
        $event.stopPropagation();
        return _vm.handleSelectLayout.apply(null, arguments);
      }
    }
  }, _vm._l(_vm.components, function (item) {
    return _c('LayoutItem', {
      key: item.id,
      ref: "layoutItem",
      refInFor: true,
      class: {
        'd-layout-item-design': _vm.isDesign
      },
      style: _vm.isContrast ? _vm.contrastStyle : {},
      attrs: {
        "layout": item.layout,
        "isDragAble": _vm.isDesign && _vm.isDragAble,
        "isResizeAble": _vm.isDesign && _vm.isResizeAble,
        "isActive": _vm.isDesign && _vm.state.selectedComponent === item
      },
      nativeOn: {
        "click": function click($event) {
          $event.stopPropagation();
          return _vm.handleSelectComponent(item);
        }
      }
    }, [_c('BoxRender', {
      attrs: {
        "cpr": item,
        "value": item.properts,
        "layout": item.layout,
        "events": item.events,
        "resource": item.resource,
        "theme": item.theme,
        "childComponents": item.childComponents
      }
    }), _vm.isDesign ? _c('div', {
      staticClass: "d-layout-item-remove",
      attrs: {
        "title": "移除"
      }
    }, [_c('CloseIcon', {
      nativeOn: {
        "click": function click($event) {
          $event.stopPropagation();
          return _vm.handleRemoveComponent(item);
        }
      }
    })], 1) : _vm._e()], 1);
  }), 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/container.vue + 4 modules
var container = __webpack_require__(9770);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/item.vue + 4 modules
var item = __webpack_require__(4597);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/property.vue + 4 modules
var property = __webpack_require__(6431);
// EXTERNAL MODULE: ./src/views/dview/page/components/box/index.vue + 3 modules
var box = __webpack_require__(5377);
// EXTERNAL MODULE: ./src/views/dview/page/components/box/property.vue + 4 modules
var box_property = __webpack_require__(743);
// EXTERNAL MODULE: ./src/service/stock/index.js + 4 modules
var stock = __webpack_require__(3085);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/constant.js
var constant = __webpack_require__(5670);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/index.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }








/* harmony default export */ var absolutevue_type_script_lang_js = ({
  components: {
    LayoutContainer: container/* default */.Z,
    LayoutItem: item/* default */.Z,
    BoxRender: box/* default */.Z,
    CloseIcon: icons_js_.CloseIcon
  },
  props: {
    stock: {
      type: Object,
      default() {
        return {
          state: stock/* state */.SB,
          setter: stock/* setter */.IS
        };
      }
    },
    // 布局属性
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    },
    // 主题
    theme: {
      type: Object,
      default() {
        return {};
      }
    },
    // 事件
    events: {
      type: Object,
      default() {
        return {};
      }
    },
    // 资源
    resource: {
      type: Object,
      default() {
        return {};
      }
    },
    // 子组件
    childComponents: {
      default() {
        return [];
      }
    },
    cpr: {
      type: Object,
      default() {
        return {
          component: 'div',
          isProperty: false
        };
      }
    }
  },
  watch: {
    layout: {
      handler() {
        this.syncLayout();
      },
      deep: true
    },
    childComponents: {
      handler() {
        this.components = this.childComponents || [];
      },
      deep: true
    }
  },
  data() {
    return {
      LayoutProperty: property/* default */.Z,
      state: stock/* state */.SB,
      constant: constant/* default */.Z,
      isDesign: stock/* state */.SB.isDesign,
      // 设计态
      isDragAble: true,
      isResizeAble: true,
      isContrast: false,
      // 对照态
      contrastStyle: {},
      // 对照样式
      components: ((stock/* state */.SB.page || {}).meta || {}).childComponents || []
    };
  },
  methods: {
    // 同步布局
    syncLayout() {
      let contrast = (this.layout || {}).contrast;
      this.isContrast = (contrast || {}).isAble;
      let contrastStyle = {
        'background-color': (contrast || {}).bgColor
      };
      this.contrastStyle = contrastStyle;
    },
    // 选中布局
    handleSelectLayout() {
      if (!this.isDesign) {
        return false;
      }
      stock/* setter */.IS.selectPropertyView(this.LayoutProperty);
      stock/* setter */.IS.selectLayout(stock/* default.state */.ZP.state.page.meta, this.constant);
      stock/* setter */.IS.selectComponent(null);
      this.updateView();
    },
    // 选中组件
    handleSelectComponent(component) {
      if (!this.isDesign) {
        return false;
      }
      stock/* setter */.IS.selectPropertyView(box_property/* default */.Z);
      setTimeout(() => {
        stock/* setter */.IS.selectComponent(component);
        this.updateView();
      }, 40);
    },
    // 获取元数据
    getMetaValue() {
      let list = this.components.map(item => {
        let node = Object.assign({}, item);
        delete node.component;
        delete node.propertyComponent;
        return node;
      });
      let meta = stock/* default.state */.ZP.state.page.meta;
      let cpr = this.cpr;
      return {
        name: cpr.name,
        version: cpr.version,
        properts: meta.properts,
        layout: meta.layout,
        events: meta.events,
        resource: meta.resource,
        theme: meta.theme,
        childComponents: list
      };
    },
    // 移除组件
    handleRemoveComponent(item) {
      stock/* setter */.IS.removeComponent(item);
      stock/* setter */.IS.selectLayout(null);
      stock/* setter */.IS.selectComponent(null);
      stock/* setter */.IS.selectPropertyView(null);
      this.updateView();
    },
    /**
     * @function 更新视图
    */
    updateView() {
      // 子项更新
      let items = this.$refs.layoutItem;
      if (items && items.length) {
        items.forEach(item => {
          setTimeout(() => {
            item.updateView();
          }, 40);
        });
      }
    }
  },
  mounted() {
    this.syncLayout();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/index.vue?vue&type=script&lang=js
 /* harmony default export */ var layout_absolutevue_type_script_lang_js = (absolutevue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/index.vue?vue&type=style&index=0&id=4446a4ba&prod&lang=less
var absolutevue_type_style_index_0_id_4446a4ba_prod_lang_less = __webpack_require__(3711);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/index.vue?vue&type=style&index=0&id=4446a4ba&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/index.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  layout_absolutevue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var absolute = (component.exports);

/***/ }),

/***/ 4597:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ item; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/item.vue?vue&type=template&id=596e64fa&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('DragResizer', {
    staticClass: "d-layout-render-item",
    style: _vm.style,
    attrs: {
      "isActive": _vm.isActive,
      "isDragAble": _vm.isDragAble,
      "isResizeAble": _vm.isResizeAble,
      "directions": _vm.directions
    },
    on: {
      "drag-start": _vm.handleDragStart,
      "drag-move": _vm.handleDragMove,
      "drag-end": _vm.handleDragEnd,
      "resize-move": _vm.handleResizeMove,
      "resize-end": _vm.handleResizeEnd
    }
  }, [_vm._t("default")], 2);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./src/views/dview/page/components/drag-resizer.vue + 4 modules
var drag_resizer = __webpack_require__(3394);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/item.vue?vue&type=script&lang=js

/* harmony default export */ var itemvue_type_script_lang_js = ({
  components: {
    DragResizer: drag_resizer/* default */.Z
  },
  inject: ['container'],
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return {
          x: 0,
          y: 0,
          w: 16,
          h: 16
        };
      }
    },
    // 是否可拖拽
    isDragAble: {
      type: Boolean,
      default: true
    },
    // 是否可缩放
    isResizeAble: {
      type: Boolean,
      default: true
    },
    // 方向
    directions: {
      type: Array,
      default() {
        return ['e', 's', 'se'];
      }
    },
    // 是否选中状态
    isActive: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      style: {}
    };
  },
  watch: {
    layout: {
      handler() {
        this.syncStyle();
      },
      deep: true
    }
  },
  methods: {
    // 同步样式
    syncStyle() {
      let style = {};
      if (this.container) {
        style = this.container.getItemStyle(this);
      }
      this.style = style;
    },
    // 拖拽开始
    handleDragStart() {},
    // 拖拽移动
    handleDragMove(args) {
      // console.log(args)
    },
    // 拖拽结束
    handleDragEnd(args) {
      let layout = this.container.getItemLayout(this, args);
      Object.assign(this.layout, layout);
    },
    // 拖拽移动
    handleResizeMove(args) {
      // console.log(args)
    },
    // 拖拽结束
    handleResizeEnd(args) {
      let layout = this.container.getItemLayout(this, args);
      Object.assign(this.layout, layout);
    },
    /**
     * @function 更新视图
    */
    updateView() {
      return this.syncStyle();
    }
  },
  mounted() {
    this.syncStyle();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/item.vue?vue&type=script&lang=js
 /* harmony default export */ var absolute_itemvue_type_script_lang_js = (itemvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/item.vue?vue&type=style&index=0&id=596e64fa&prod&lang=less&scoped=true
var itemvue_type_style_index_0_id_596e64fa_prod_lang_less_scoped_true = __webpack_require__(9919);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/item.vue?vue&type=style&index=0&id=596e64fa&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/item.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  absolute_itemvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "596e64fa",
  null
  
)

/* harmony default export */ var item = (component.exports);

/***/ }),

/***/ 8760:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/property.vue?vue&type=template&id=0a89ea9d&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('el-tabs', {
    staticClass: "layout-properts el-tabs-paner",
    model: {
      value: _vm.activeName,
      callback: function callback($$v) {
        _vm.activeName = $$v;
      },
      expression: "activeName"
    }
  }, [_c('el-tab-pane', {
    attrs: {
      "label": "配置",
      "name": "first"
    }
  }, [_c('div', {
    staticClass: "form-section"
  }, [_c('FormGroup', {
    attrs: {
      "label": "列数",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.columns,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "columns", $$v);
      },
      expression: "layout.columns"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "行数",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.rows,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "rows", $$v);
      },
      expression: "layout.rows"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "自适应高度",
      "col": 11
    }
  }, [_c('Switcher', {
    model: {
      value: _vm.layout.isFixHeight,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "isFixHeight", $$v);
      },
      expression: "layout.isFixHeight"
    }
  })], 1), !_vm.layout.isFixHeight ? _c('FormGroup', {
    attrs: {
      "label": "参考高度",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.referHeight,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "referHeight", $$v);
      },
      expression: "layout.referHeight"
    }
  })], 1) : _vm._e(), ((_vm.layout || {}).contrast || {}).isAble !== undefined ? _c('FormGroup', {
    attrs: {
      "label": "对比",
      "col": 11
    }
  }, [_c('Switcher', {
    model: {
      value: _vm.layout.contrast.isAble,
      callback: function callback($$v) {
        _vm.$set(_vm.layout.contrast, "isAble", $$v);
      },
      expression: "layout.contrast.isAble"
    }
  })], 1) : _vm._e(), ((_vm.layout || {}).contrast || {}).isAble ? _c('FormGroup', {
    attrs: {
      "label": "子项背景色",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "type": "color"
    },
    model: {
      value: _vm.layout.contrast.bgColor,
      callback: function callback($$v) {
        _vm.$set(_vm.layout.contrast, "bgColor", $$v);
      },
      expression: "layout.contrast.bgColor"
    }
  })], 1) : _vm._e()], 1)])], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/vdog/dist/components.js"
var components_js_ = __webpack_require__(4236);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/constant.js
var constant = __webpack_require__(5670);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/property.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }


/* harmony default export */ var propertyvue_type_script_lang_js = ({
  components: {
    FormGroup: components_js_.FormGroup,
    FormInput: components_js_.FormInput,
    Switcher: components_js_.Switcher
  },
  props: {
    properts: {
      default() {
        return {};
      }
    },
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    },
    events: {
      default() {
        return {};
      }
    },
    resource: {
      default() {
        return {};
      }
    },
    theme: {
      default() {
        return {};
      }
    },
    childComponents: {
      default() {
        return [];
      }
    },
    value: {
      default() {
        return {};
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z,
      activeName: 'first'
    };
  },
  methods: {
    // 同步
    syncProps() {
      // 布局
      let layout = this.layout || {};
      let contrast = layout.contrast || _objectSpread({}, this.constant.layout.contrast);
      this.$set(this.layout, 'contrast', contrast);
    }
  },
  mounted() {
    this.syncProps();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/property.vue?vue&type=script&lang=js
 /* harmony default export */ var absolute_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/absolute/property.vue?vue&type=style&index=0&id=0a89ea9d&prod&lang=less&scoped=true
var propertyvue_type_style_index_0_id_0a89ea9d_prod_lang_less_scoped_true = __webpack_require__(3046);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/property.vue?vue&type=style&index=0&id=0a89ea9d&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/absolute/property.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  absolute_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "0a89ea9d",
  null
  
)

/* harmony default export */ var property = (component.exports);

/***/ }),

/***/ 7494:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ fill; }
});

// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/index.vue + 4 modules
var absolute = __webpack_require__(9399);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/container.vue?vue&type=template&id=401a08ed
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    class: _vm.className,
    style: _vm.containerStyle
  }, [_c('div', {
    ref: "layout",
    staticClass: "d-layout-render",
    style: _vm.renderStyle
  }, [_c('div', {
    ref: "container",
    staticClass: "d-layout-container",
    attrs: {
      "id": _vm.id
    }
  }, [_vm.isRendered ? _vm._t("default") : _vm._e()], 2)])]);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/fill/constant.js
var constant = __webpack_require__(9293);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/container.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }


/* harmony default export */ var containervue_type_script_lang_js = ({
  provide() {
    return {
      container: this
    };
  },
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    },
    // 主题
    theme: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.theme);
      }
    }
  },
  watch: {
    layout: {
      handler() {
        this.syncLayoutStyle();
      },
      deep: true
    },
    theme: {
      handler() {
        this.syncLayoutStyle();
      },
      deep: true
    }
  },
  data() {
    return {
      constant: constant/* default */.Z,
      id: components_js_.strer.utid(),
      className: 'd-layout-fill',
      // 容器样式
      containerStyle: {},
      // 渲染区域
      renderStyle: {},
      // 是否渲染完成
      isRendered: false
    };
  },
  methods: {
    // 适配
    adapterLayout() {
      let constant = this.constant;
      let cl = this.layout;
      this.$set(cl, 'columns', Array.isArray(cl.columns) ? cl.columns : constant.layout.columns);
      this.$set(cl, 'rows', Array.isArray(cl.rows) ? cl.rows : constant.layout.rows);
      this.$set(cl, 'margin', cl.margin || constant.layout.margin);
      this.$set(cl, 'padding', cl.padding || constant.layout.padding);
      this.$set(cl, 'areas', Array.isArray(cl.areas) ? cl.areas : constant.layout.areas);
      this.$set(cl, 'rowGap', cl.rowGap || constant.layout.rowGap);
      this.$set(cl, 'colGap', cl.colGap || constant.layout.colGap);
    },
    // 同步布局样式
    syncLayoutStyle() {
      this.adapterLayout();
      let cl = this.layout;
      let m = cl.margin;
      let p = cl.padding;
      // 获取宽度
      const w = col => {
        // 直接单位
        if (/px/.test(col)) {
          return col;
        }
        return col + 'px';
      };
      let margin = "".concat(w(m.top), " ").concat(w(m.right), " ").concat(w(m.bottom), " ").concat(w(m.left));
      let padding = "".concat(w(p.top), " ").concat(w(p.right), " ").concat(w(p.bottom), " ").concat(w(p.left));
      this.renderStyle = {
        margin,
        padding
      };
      let theme = this.theme || {};
      let bgColor = theme.bgColor || '';
      let bgImage = theme.bgImage || '';
      let bgSize = theme.bgSize || '';
      let containerStyle = {};
      if (bgColor) {
        containerStyle.backgroundColor = bgColor;
      }
      if (bgImage) {
        containerStyle.backgroundImage = 'url(' + bgImage + ')';
      }
      if (bgSize) {
        containerStyle.backgroundSize = bgSize;
      }
      this.containerStyle = containerStyle;

      // 更新填充样式
      this.updateFillStyle();
    },
    // 获取容器样式
    getContainerStyle() {
      let container = this.$refs.container || document.body;
      let style = getComputedStyle(container);
      let width = parseFloat(style.width);
      return {
        width: width
      };
    },
    // 获取每项样式
    getItemStyle(layoutItem) {
      let constant = this.constant;
      let layout = layoutItem.layout || {};
      let l = parseInt(layout.x || 0);
      let t = parseInt(layout.y || 0);
      let w = parseInt(layout.w || 16);
      let h = parseInt(layout.h || 16);
      let rh = parseInt(layout.referHeight || constant.layout.referHeight);
      // 获取宽度
      let style = this.getContainerStyle();
      let width = style.width;
      let height = rh;
      this.adapterLayout();
      let cl = this.layout;
      const cw = (col, w) => {
        // 比例换算成单位
        return col * w / cl.columns + 'px';
      };
      let itemStyle = {
        left: '0',
        width: 'auto',
        top: '0',
        height: 'auto'
        // left: cw(l, width),
        // width: cw(w, width),
        // top: cw(t, height),
        // height: cw(h, height)
      };

      if (layout.zIndex) {
        itemStyle['z-index'] = layout.zIndex;
      }
      return itemStyle;
    },
    // 获取每项布局配置
    getItemLayout(layoutItem, args) {
      let constant = this.constant;
      let columns = this.layout.columns;
      let layout = layoutItem.layout || {};
      let drager = args.drager;
      let options = drager.options;
      let newTargetStyle = args.newTargetStyle;
      let style = this.getContainerStyle();
      let rh = parseInt(layout.referHeight || constant.layout.referHeight);
      let width = style.width;
      let height = rh;
      if (options.de === 'nesw') {
        let tc = Math.round(columns * newTargetStyle.top / height);
        let lc = Math.round(columns * newTargetStyle.left / width);
        lc = lc > columns - layout.w ? columns - layout.w : lc;
        // layout.y = tc
        // layout.x = lc
      } else if (options.de === 'se') {
        let wc = Math.round(columns * newTargetStyle.width / width);
        let hc = Math.round(columns * newTargetStyle.height / height);
        hc = hc > columns - layout.lc ? columns - layout.lc : hc;
        // layout.w = wc
        // layout.h = hc
      }

      layout.x = '0';
      layout.y = '0';
      layout.w = 'auto';
      layout.h = 'auto';
      return layout;
    },
    /**
     * @function 更新视图
    */
    updateView() {
      this.syncLayoutStyle();
      // 渲染完成
      setTimeout(() => {
        this.isRendered = true;
      }, 40);
    },
    // 更新容器样式
    updateFillStyle() {
      let style = {};
      let layout = this.layout || {};
      let rows = layout.rows;
      let columns = layout.columns;
      let rowsHeight = [];
      rows.forEach(item => {
        rowsHeight.push(item.rowHeight);
      });
      style['grid-template-rows'] = rowsHeight.join(' ');
      let colsWidth = [];
      columns.forEach(item => {
        colsWidth.push(item.colWidth);
      });
      style['grid-template-columns'] = colsWidth.join(' ');
      if (layout.rowGap) {
        style['grid-row-gap'] = layout.rowGap;
      }
      if (layout.colGap) {
        style['grid-column-gap'] = layout.colGap;
      }
      if (Array.isArray(layout.areas) && layout.areas.length) {
        let areas = [];
        layout.areas.forEach(item => {
          if (item.area && String(item.area.length)) {
            areas.push("'" + item.area + "'");
          }
        });
        if (areas.length) {
          style['grid-template-areas'] = areas.join('\n');
        }
      }
      // 样式文本
      let text = '#' + this.id + '{';
      for (let key in style) {
        text += key + ': ' + style[key] + ';';
      }
      text += '}';
      components_js_.domer.appendElement('style', {
        id: 'style' + this.id,
        innerHTML: text
      });
    }
  },
  mounted() {
    // 更新视图
    this.updateView();
  },
  beforeDestroy() {
    clearInterval(this.fitTimer);
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/container.vue?vue&type=script&lang=js
 /* harmony default export */ var fill_containervue_type_script_lang_js = (containervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/container.vue?vue&type=style&index=0&id=401a08ed&prod&lang=less
var containervue_type_style_index_0_id_401a08ed_prod_lang_less = __webpack_require__(1658);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/container.vue?vue&type=style&index=0&id=401a08ed&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/container.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  fill_containervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var container = (component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/item.vue?vue&type=template&id=f539f3ae&scoped=true
var itemvue_type_template_id_f539f3ae_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('DragResizer', {
    staticClass: "d-layout-render-item",
    style: _vm.style,
    attrs: {
      "isActive": _vm.isActive,
      "isDragAble": _vm.isDragAble,
      "isResizeAble": _vm.isResizeAble,
      "directions": _vm.directions
    },
    on: {
      "drag-start": _vm.handleDragStart,
      "drag-move": _vm.handleDragMove,
      "drag-end": _vm.handleDragEnd,
      "resize-move": _vm.handleResizeMove,
      "resize-end": _vm.handleResizeEnd
    }
  }, [_vm._t("default")], 2);
};
var itemvue_type_template_id_f539f3ae_scoped_true_staticRenderFns = [];

// EXTERNAL MODULE: ./src/views/dview/page/components/drag-resizer.vue + 4 modules
var drag_resizer = __webpack_require__(3394);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/item.vue?vue&type=script&lang=js

/* harmony default export */ var itemvue_type_script_lang_js = ({
  components: {
    DragResizer: drag_resizer/* default */.Z
  },
  inject: ['container'],
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return {
          x: 0,
          y: 0,
          w: 16,
          h: 16
        };
      }
    },
    // 是否可拖拽
    isDragAble: {
      type: Boolean,
      default: true
    },
    // 是否可缩放
    isResizeAble: {
      type: Boolean,
      default: true
    },
    // 方向
    directions: {
      type: Array,
      default() {
        return ['e', 's', 'se'];
      }
    },
    // 是否选中状态
    isActive: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      style: {}
    };
  },
  watch: {
    layout: {
      handler() {
        this.syncStyle();
      },
      deep: true
    }
  },
  methods: {
    // 同步样式
    syncStyle() {
      let style = {};
      if (this.container) {
        style = this.container.getItemStyle(this);
      }
      this.style = style;
      let $el = this.$el;
      if ($el) {
        $el.style.left = 0;
        $el.style.top = 0;
        $el.style.width = 'auto';
        $el.style.height = 'auto';
      }
    },
    // 拖拽开始
    handleDragStart() {},
    // 拖拽移动
    handleDragMove(args) {
      // console.log(args)
    },
    // 拖拽结束
    handleDragEnd(args) {
      let layout = this.container.getItemLayout(this, args);
      Object.assign(this.layout, layout);
    },
    // 拖拽移动
    handleResizeMove(args) {
      // console.log(args)
    },
    // 拖拽结束
    handleResizeEnd(args) {
      let layout = this.container.getItemLayout(this, args);
      Object.assign(this.layout, layout);
    },
    /**
     * @function 更新视图
    */
    updateView() {
      return this.syncStyle();
    }
  },
  mounted() {
    this.syncStyle();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/item.vue?vue&type=script&lang=js
 /* harmony default export */ var fill_itemvue_type_script_lang_js = (itemvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/item.vue?vue&type=style&index=0&id=f539f3ae&prod&lang=less&scoped=true
var itemvue_type_style_index_0_id_f539f3ae_prod_lang_less_scoped_true = __webpack_require__(4169);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/item.vue?vue&type=style&index=0&id=f539f3ae&prod&lang=less&scoped=true

;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/item.vue



;


/* normalize component */

var item_component = (0,componentNormalizer/* default */.Z)(
  fill_itemvue_type_script_lang_js,
  itemvue_type_template_id_f539f3ae_scoped_true_render,
  itemvue_type_template_id_f539f3ae_scoped_true_staticRenderFns,
  false,
  null,
  "f539f3ae",
  null
  
)

/* harmony default export */ var item = (item_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/index.vue?vue&type=script&lang=js
function fillvue_type_script_lang_js_ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function fillvue_type_script_lang_js_objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? fillvue_type_script_lang_js_ownKeys(Object(t), !0).forEach(function (r) { fillvue_type_script_lang_js_defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : fillvue_type_script_lang_js_ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function fillvue_type_script_lang_js_defineProperty(obj, key, value) { key = fillvue_type_script_lang_js_toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function fillvue_type_script_lang_js_toPropertyKey(arg) { var key = fillvue_type_script_lang_js_toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function fillvue_type_script_lang_js_toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }




/* harmony default export */ var fillvue_type_script_lang_js = ({
  extends: absolute["default"],
  components: {
    LayoutContainer: container,
    LayoutItem: item
  },
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return fillvue_type_script_lang_js_objectSpread({}, constant/* default */.Z.layout);
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z,
      isResizeAble: false
    };
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/index.vue?vue&type=script&lang=js
 /* harmony default export */ var layout_fillvue_type_script_lang_js = (fillvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/index.vue
var fill_render, fill_staticRenderFns
;



/* normalize component */
;
var fill_component = (0,componentNormalizer/* default */.Z)(
  layout_fillvue_type_script_lang_js,
  fill_render,
  fill_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var fill = (fill_component.exports);

/***/ }),

/***/ 6822:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ fill_property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/property.vue?vue&type=template&id=dfba1982
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('el-tabs', {
    staticClass: "layout-properts el-tabs-paner",
    model: {
      value: _vm.activeName,
      callback: function callback($$v) {
        _vm.activeName = $$v;
      },
      expression: "activeName"
    }
  }, [_c('el-tab-pane', {
    attrs: {
      "label": "配置",
      "name": "first"
    }
  }, [_c('div', {
    staticClass: "d-container"
  }, [_c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead d-layout-flex-between-center"
  }, [_c('span', [_vm._v("行配置")]), _c('span', {
    staticClass: "d-text-primary d-text-linker",
    attrs: {
      "title": "添加行"
    },
    on: {
      "click": _vm.handleAddRow
    }
  }, [_c('i', {
    staticClass: "el-icon-plus"
  })])]), _c('div', {
    staticClass: "d-section"
  }, [_c('Grid', {
    ref: "gridList",
    attrs: {
      "isPagerAble": false
    },
    model: {
      value: _vm.layout.rows,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "rows", $$v);
      },
      expression: "layout.rows"
    }
  }, [_c('GridColumn', {
    attrs: {
      "width": 60,
      "type": "index",
      "label": "序号",
      "className": "d-vm"
    }
  }), _c('GridColumn', {
    attrs: {
      "field": "rowHeight",
      "label": "行高"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('FormInput', {
          model: {
            value: scope.rowHeight,
            callback: function callback($$v) {
              _vm.$set(scope, "rowHeight", $$v);
            },
            expression: "scope.rowHeight"
          }
        })];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "action",
      "width": 80,
      "label": "操作",
      "className": "d-vm"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('div', {
          staticClass: "d-bar-tools d-bar-tools-condensed d-grid-actions"
        }, [_c('a', {
          staticClass: "d-tools-item d-text-theme",
          on: {
            "click": function click($event) {
              return _vm.handleDelRow(scope);
            }
          }
        }, [_c('DeleteIcon', {
          attrs: {
            "size": "sm"
          }
        })], 1)])];
      }
    }])
  })], 1)], 1)]), _c('div', {
    staticClass: "d-container"
  }, [_c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead d-layout-flex-between-center"
  }, [_c('span', [_vm._v("列配置")]), _c('span', {
    staticClass: "d-text-primary d-text-linker",
    attrs: {
      "title": "添加列"
    },
    on: {
      "click": _vm.handleAddCol
    }
  }, [_c('i', {
    staticClass: "el-icon-plus"
  })])]), _c('div', {
    staticClass: "d-section"
  }, [_c('Grid', {
    ref: "gridList",
    attrs: {
      "isPagerAble": false
    },
    model: {
      value: _vm.layout.columns,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "columns", $$v);
      },
      expression: "layout.columns"
    }
  }, [_c('GridColumn', {
    attrs: {
      "width": 60,
      "type": "index",
      "label": "序号",
      "className": "d-vm"
    }
  }), _c('GridColumn', {
    attrs: {
      "field": "colWidth",
      "label": "列宽"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('FormInput', {
          model: {
            value: scope.colWidth,
            callback: function callback($$v) {
              _vm.$set(scope, "colWidth", $$v);
            },
            expression: "scope.colWidth"
          }
        })];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "action",
      "width": 80,
      "label": "操作",
      "className": "d-vm"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('div', {
          staticClass: "d-bar-tools d-bar-tools-condensed d-grid-actions"
        }, [_c('a', {
          staticClass: "d-tools-item d-text-theme",
          on: {
            "click": function click($event) {
              return _vm.handleDelCol(scope);
            }
          }
        }, [_c('DeleteIcon', {
          attrs: {
            "size": "sm"
          }
        })], 1)])];
      }
    }])
  })], 1)], 1)]), _c('div', {
    staticClass: "d-container"
  }, [_c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead d-layout-flex-between-center"
  }, [_c('span', [_vm._v("区域配置")]), _c('span', {
    staticClass: "d-text-primary d-text-linker",
    attrs: {
      "title": "添加列"
    },
    on: {
      "click": _vm.handleAddArea
    }
  }, [_c('i', {
    staticClass: "el-icon-plus"
  })])]), _c('div', {
    staticClass: "d-section"
  }, [_c('Grid', {
    ref: "gridList",
    attrs: {
      "isPagerAble": false
    },
    model: {
      value: _vm.layout.areas,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "areas", $$v);
      },
      expression: "layout.areas"
    }
  }, [_c('GridColumn', {
    attrs: {
      "width": 60,
      "type": "index",
      "label": "序号",
      "className": "d-vm"
    }
  }), _c('GridColumn', {
    attrs: {
      "field": "colWidth",
      "label": "项目设置"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('FormInput', {
          model: {
            value: scope.area,
            callback: function callback($$v) {
              _vm.$set(scope, "area", $$v);
            },
            expression: "scope.area"
          }
        })];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "action",
      "width": 80,
      "label": "操作",
      "className": "d-vm"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('div', {
          staticClass: "d-bar-tools d-bar-tools-condensed d-grid-actions"
        }, [_c('a', {
          staticClass: "d-tools-item d-text-theme",
          on: {
            "click": function click($event) {
              return _vm.handleDelArea(scope);
            }
          }
        }, [_c('DeleteIcon', {
          attrs: {
            "size": "sm"
          }
        })], 1)])];
      }
    }])
  })], 1)], 1)]), _c('div', {
    staticClass: "d-container"
  }, [_c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead d-layout-flex-between-center"
  }, [_c('span', [_vm._v("公用配置")])])]), _c('div', {
    staticClass: "form-section"
  }, [_c('FormGroup', {
    attrs: {
      "label": "行距",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.rowGap,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "rowGap", $$v);
      },
      expression: "layout.rowGap"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "列距",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.colGap,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "colGap", $$v);
      },
      expression: "layout.colGap"
    }
  })], 1), ((_vm.layout || {}).contrast || {}).isAble !== undefined ? _c('FormGroup', {
    attrs: {
      "label": "对比",
      "col": 11
    }
  }, [_c('Switcher', {
    model: {
      value: _vm.layout.contrast.isAble,
      callback: function callback($$v) {
        _vm.$set(_vm.layout.contrast, "isAble", $$v);
      },
      expression: "layout.contrast.isAble"
    }
  })], 1) : _vm._e(), ((_vm.layout || {}).contrast || {}).isAble ? _c('FormGroup', {
    attrs: {
      "label": "子项背景色",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "type": "color"
    },
    model: {
      value: _vm.layout.contrast.bgColor,
      callback: function callback($$v) {
        _vm.$set(_vm.layout.contrast, "bgColor", $$v);
      },
      expression: "layout.contrast.bgColor"
    }
  })], 1) : _vm._e()], 1)])], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
// EXTERNAL MODULE: external "@daelui/vdog/dist/components.js"
var dist_components_js_ = __webpack_require__(4236);
// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/property.vue + 4 modules
var property = __webpack_require__(8760);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/fill/constant.js
var constant = __webpack_require__(9293);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/fill/property.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }





/* harmony default export */ var propertyvue_type_script_lang_js = ({
  extends: property["default"],
  components: {
    FormGroup: dist_components_js_.FormGroup,
    FormInput: dist_components_js_.FormInput,
    Grid: dist_components_js_.Grid,
    GridColumn: dist_components_js_.GridColumn,
    DeleteIcon: icons_js_.DeleteIcon
  },
  props: {
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z
    };
  },
  methods: {
    handleAddRow() {
      this.layout.rows.push({
        id: components_js_.strer.utid(),
        rowHeight: 'auto'
      });
    },
    handleDelRow(node) {
      let index = this.layout.rows.findIndex(function (item) {
        return item === node;
      });
      this.layout.rows.splice(index, 1);
      this.$set(this.layout, 'rows', this.layout.rows);
    },
    handleAddCol() {
      this.layout.columns.push({
        id: components_js_.strer.utid(),
        colWidth: 'auto'
      });
    },
    handleDelCol(node) {
      let index = this.layout.columns.findIndex(function (item) {
        return item === node;
      });
      this.layout.columns.splice(index, 1);
    },
    handleAddArea() {
      this.layout.areas.push({
        id: components_js_.strer.utid(),
        area: ''
      });
    },
    handleDelArea(node) {
      let index = this.layout.areas.findIndex(function (item) {
        return item === node;
      });
      this.layout.areas.splice(index, 1);
    }
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/property.vue?vue&type=script&lang=js
 /* harmony default export */ var fill_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/fill/property.vue





/* normalize component */
;
var component = (0,componentNormalizer/* default */.Z)(
  fill_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var fill_property = (component.exports);

/***/ }),

/***/ 9480:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ grid; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/index.vue?vue&type=template&id=8d937876
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('LayoutContainer', {
    ref: "container",
    attrs: {
      "items": _vm.items,
      "layout": _vm.layout,
      "theme": _vm.theme,
      "isDragAble": _vm.isDesign,
      "isResizeAble": _vm.isDesign
    },
    on: {
      "changeStyle": _vm.updateView
    },
    nativeOn: {
      "click": function click($event) {
        $event.stopPropagation();
        return _vm.handleSelectLayout.apply(null, arguments);
      }
    }
  }, _vm._l(_vm.components, function (item) {
    return _c('LayoutItem', {
      key: item.id,
      ref: "layoutItem",
      refInFor: true,
      class: {
        'd-layout-item-design': _vm.isDesign
      },
      style: _vm.isContrast ? _vm.contrastStyle : {},
      attrs: {
        "layout": item.layout,
        "isDragAble": _vm.isDesign,
        "isResizeAble": _vm.isDesign,
        "isActive": _vm.isDesign && _vm.state.selectedComponent === item
      },
      nativeOn: {
        "click": function click($event) {
          $event.stopPropagation();
          return _vm.handleSelectComponent(item);
        }
      }
    }, [_c('BoxRender', {
      attrs: {
        "cpr": item,
        "value": item.properts,
        "layout": item.layout,
        "events": item.events,
        "resource": item.resource,
        "theme": item.theme,
        "childComponents": item.childComponents
      }
    }), _vm.isDesign ? _c('div', {
      staticClass: "d-layout-item-remove",
      attrs: {
        "title": "移除"
      }
    }, [_c('CloseIcon', {
      nativeOn: {
        "click": function click($event) {
          $event.stopPropagation();
          return _vm.handleRemoveComponent(item);
        }
      }
    })], 1) : _vm._e()], 1);
  }), 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/container.vue?vue&type=template&id=01a8689e
var containervue_type_template_id_01a8689e_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "d-layout-grid",
    style: _vm.containerStyle
  }, [_c('div', {
    ref: "layout",
    staticClass: "d-layout-render",
    style: _vm.renderStyle
  }, [_c('div', {
    ref: "container",
    staticClass: "d-layout-container"
  }, [_c('grid-layout', {
    attrs: {
      "layout": _vm.items,
      "col-num": _vm.layout.columns,
      "row-height": _vm.rowHeight,
      "is-draggable": _vm.isDragAble,
      "is-resizable": _vm.isResizeAble,
      "is-mirrored": false,
      "vertical-compact": true,
      "margin": [8, 8],
      "use-css-transforms": true
    },
    on: {
      "update:layout": function updateLayout($event) {
        _vm.items = $event;
      }
    }
  }, [_vm.isRendered ? _vm._t("default") : _vm._e()], 2)], 1)])]);
};
var containervue_type_template_id_01a8689e_staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/vue-grid-layout/dist/vue-grid-layout.common.js
var vue_grid_layout_common = __webpack_require__(3533);
var vue_grid_layout_common_default = /*#__PURE__*/__webpack_require__.n(vue_grid_layout_common);
// EXTERNAL MODULE: ./node_modules/element-resize-detector/src/element-resize-detector.js
var element_resize_detector = __webpack_require__(6084);
var element_resize_detector_default = /*#__PURE__*/__webpack_require__.n(element_resize_detector);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/grid/constant.js
var constant = __webpack_require__(873);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/container.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }



/* harmony default export */ var containervue_type_script_lang_js = ({
  components: {
    GridLayout: (vue_grid_layout_common_default()).GridLayout
  },
  provide() {
    return {
      container: this
    };
  },
  props: {
    items: {
      type: Array,
      default() {
        return [];
      }
    },
    // 是否可拖拽
    isDragAble: {
      type: Boolean,
      default: true
    },
    // 是否可缩放
    isResizeAble: {
      type: Boolean,
      default: true
    },
    // 布局属性
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    },
    // 主题
    theme: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.theme);
      }
    }
  },
  watch: {
    layout: {
      handler() {
        this.syncLayoutStyle();
      },
      deep: true
    }
  },
  data() {
    return {
      // 容器样式
      containerStyle: {},
      rowHeight: 8,
      // 渲染区域
      renderStyle: {},
      // 是否渲染完成
      isRendered: false
    };
  },
  methods: {
    // 适配
    adapterLayout() {
      let cl = this.layout;
      let columns = parseInt(cl.columns);
      cl.columns = isNaN(columns) ? constant/* default */.Z.layout.columns : columns;
      let rows = parseInt(cl.rows);
      cl.rows = isNaN(rows) ? constant/* default */.Z.layout.rows : rows;
      let referHeight = parseInt(cl.referHeight);
      cl.referHeight = isNaN(referHeight) ? constant/* default */.Z.layout.referHeight : referHeight;
      this.rowHeight = cl.referHeight / cl.rows;
      cl.margin = cl.margin || constant/* default */.Z.layout.margin;
      cl.padding = cl.padding || constant/* default */.Z.layout.padding;
    },
    // 适应容器高度
    fitContainerStyle() {
      // let contaienr = this.$refs.container
      // let nodes = contaienr.childNodes || []
      // nodes = Array.from(nodes)
      // let max = 0
      // nodes.forEach(item => {
      //   let style = getComputedStyle(item)
      //   let top = parseFloat(style.top)
      //   let height = parseFloat(style.height)
      //   max = top + height > max ? top + height : max
      // })
      // if (max) {
      //   contaienr.style.height = max + 'px'
      // }
    },
    // 同步布局样式
    syncLayoutStyle() {
      this.adapterLayout();
      let cl = this.layout;
      let m = cl.margin;
      let p = cl.padding;
      // 获取宽度
      const w = col => {
        // 直接单位
        if (/px/.test(col)) {
          return col;
        }
        return col + 'px';
      };
      let margin = "".concat(w(m.top), " ").concat(w(m.right), " ").concat(w(m.bottom), " ").concat(w(m.left));
      let padding = "".concat(w(p.top), " ").concat(w(p.right), " ").concat(w(p.bottom), " ").concat(w(p.left));
      this.renderStyle = {
        margin,
        padding
      };
      let theme = this.theme || {};
      let bgColor = theme.bgColor || '';
      let bgImage = theme.bgImage || '';
      let bgSize = theme.bgSize || '';
      let containerStyle = {};
      if (bgColor) {
        containerStyle.backgroundColor = bgColor;
      }
      if (bgImage) {
        containerStyle.backgroundImage = 'url(' + bgImage + ')';
      }
      if (bgSize) {
        containerStyle.backgroundSize = bgSize;
      }
      this.containerStyle = containerStyle;
    },
    // 获取容器样式
    getContainerStyle() {
      let container = this.$refs.container || document.body;
      let style = getComputedStyle(container);
      let width = parseFloat(style.width);
      return {
        width: width
      };
    },
    // 获取每项样式
    getItemStyle(layoutItem) {
      let layout = layoutItem.layout || {};
      let l = parseInt(layout.x || 0);
      let t = parseInt(layout.y || 0);
      let w = parseInt(layout.w || 16);
      let h = parseInt(layout.h || 16);
      let rh = parseInt(layout.referHeight || 1080);
      // 获取宽度
      let style = this.getContainerStyle();
      let width = style.width;
      let height = rh;
      this.adapterLayout();
      let cl = this.layout;
      const cw = (col, w) => {
        // 比例换算成单位
        return col * w / cl.columns + 'px';
      };
      let itemStyle = {
        left: cw(l, width),
        width: cw(w, width),
        top: cw(t, height),
        height: cw(h, height)
      };
      if (layout.zIndex) {
        itemStyle['z-index'] = layout.zIndex;
      }
      return itemStyle;
    },
    // 获取每项布局配置
    getItemLayout(layoutItem, args) {
      let columns = this.layout.columns;
      let layout = layoutItem.layout || {};
      let drager = args.drager;
      let options = drager.options;
      let newTargetStyle = args.newTargetStyle;
      let style = this.getContainerStyle();
      let rh = parseInt(layout.referHeight || 1080);
      let width = style.width;
      let height = rh;
      if (options.de === 'nesw') {
        let tc = Math.round(columns * newTargetStyle.top / height);
        let lc = Math.round(columns * newTargetStyle.left / width);
        lc = lc > columns - layout.w ? columns - layout.w : lc;
        layout.y = tc;
        layout.x = lc;
      } else if (options.de === 'se') {
        let wc = Math.round(columns * newTargetStyle.width / width);
        let hc = Math.round(columns * newTargetStyle.height / height);
        hc = hc > columns - layout.lc ? columns - layout.lc : hc;
        layout.w = wc;
        layout.h = hc;
      }
      return layout;
    },
    /**
     * @function 更新视图
    */
    updateView() {
      this.syncLayoutStyle();
      // 渲染完成
      setTimeout(() => {
        this.isRendered = true;
      }, 40);
    }
  },
  mounted() {
    // 更新视图
    this.updateView();
    // 自适应容器高度
    this.fitTimer = setInterval(() => {
      this.fitContainerStyle();
    }, 1000);
    // 监听元素变化，编辑器自适应
    let erd = element_resize_detector_default()();
    erd.listenTo(this.$refs.container, () => {
      // 更新视图
      this.updateView();
      this.$emit('changeStyle');
    });
  },
  beforeDestroy() {
    clearInterval(this.fitTimer);
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/container.vue?vue&type=script&lang=js
 /* harmony default export */ var grid_containervue_type_script_lang_js = (containervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/container.vue?vue&type=style&index=0&id=01a8689e&prod&lang=less
var containervue_type_style_index_0_id_01a8689e_prod_lang_less = __webpack_require__(5563);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/container.vue?vue&type=style&index=0&id=01a8689e&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/container.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  grid_containervue_type_script_lang_js,
  containervue_type_template_id_01a8689e_render,
  containervue_type_template_id_01a8689e_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var container = (component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/item.vue?vue&type=template&id=627431db&scoped=true
var itemvue_type_template_id_627431db_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('grid-item', {
    key: _vm.layout.i,
    attrs: {
      "x": _vm.layout.x,
      "y": _vm.layout.y,
      "w": _vm.layout.w,
      "h": _vm.layout.h,
      "i": _vm.layout.i
    }
  }, [_vm._t("default")], 2);
};
var itemvue_type_template_id_627431db_scoped_true_staticRenderFns = [];

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/item.vue?vue&type=script&lang=js

/* harmony default export */ var itemvue_type_script_lang_js = ({
  components: {
    GridItem: (vue_grid_layout_common_default()).GridItem
  },
  inject: ['container'],
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return {
          i: 1,
          x: 0,
          y: 0,
          w: 16,
          h: 16
        };
      }
    },
    // 是否可拖拽
    isDragAble: {
      type: Boolean,
      default: true
    },
    // 是否可缩放
    isResizeAble: {
      type: Boolean,
      default: true
    },
    // 方向
    directions: {
      type: Array,
      default() {
        return ['e', 's', 'se'];
      }
    },
    // 是否选中状态
    isActive: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      style: {}
    };
  },
  watch: {
    layout: {
      handler() {
        this.syncStyle();
      },
      deep: true
    }
  },
  methods: {
    // 同步样式
    syncStyle() {
      let style = {};
      if (this.container) {
        style = this.container.getItemStyle(this);
      }
      this.style = style;
    },
    // 拖拽开始
    handleDragStart() {},
    // 拖拽移动
    handleDragMove(args) {
      // console.log(args)
    },
    // 拖拽结束
    handleDragEnd(args) {
      let layout = this.container.getItemLayout(this, args);
      Object.assign(this.layout, layout);
    },
    // 拖拽移动
    handleResizeMove(args) {
      // console.log(args)
    },
    // 拖拽结束
    handleResizeEnd(args) {
      let layout = this.container.getItemLayout(this, args);
      Object.assign(this.layout, layout);
    },
    /**
     * @function 更新视图
    */
    updateView() {
      return this.syncStyle();
    }
  },
  mounted() {
    this.syncStyle();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/item.vue?vue&type=script&lang=js
 /* harmony default export */ var grid_itemvue_type_script_lang_js = (itemvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/item.vue?vue&type=style&index=0&id=627431db&prod&lang=less&scoped=true
var itemvue_type_style_index_0_id_627431db_prod_lang_less_scoped_true = __webpack_require__(866);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/item.vue?vue&type=style&index=0&id=627431db&prod&lang=less&scoped=true

;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/item.vue



;


/* normalize component */

var item_component = (0,componentNormalizer/* default */.Z)(
  grid_itemvue_type_script_lang_js,
  itemvue_type_template_id_627431db_scoped_true_render,
  itemvue_type_template_id_627431db_scoped_true_staticRenderFns,
  false,
  null,
  "627431db",
  null
  
)

/* harmony default export */ var item = (item_component.exports);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/property.vue + 4 modules
var property = __webpack_require__(6431);
// EXTERNAL MODULE: ./src/views/dview/page/components/box/index.vue + 3 modules
var box = __webpack_require__(5377);
// EXTERNAL MODULE: ./src/views/dview/page/components/box/property.vue + 4 modules
var box_property = __webpack_require__(743);
// EXTERNAL MODULE: ./src/service/stock/index.js + 4 modules
var stock = __webpack_require__(3085);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/index.vue?vue&type=script&lang=js
function gridvue_type_script_lang_js_ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function gridvue_type_script_lang_js_objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? gridvue_type_script_lang_js_ownKeys(Object(t), !0).forEach(function (r) { gridvue_type_script_lang_js_defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : gridvue_type_script_lang_js_ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function gridvue_type_script_lang_js_defineProperty(obj, key, value) { key = gridvue_type_script_lang_js_toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function gridvue_type_script_lang_js_toPropertyKey(arg) { var key = gridvue_type_script_lang_js_toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function gridvue_type_script_lang_js_toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }








/* harmony default export */ var gridvue_type_script_lang_js = ({
  components: {
    LayoutContainer: container,
    LayoutItem: item,
    BoxRender: box/* default */.Z,
    CloseIcon: icons_js_.CloseIcon
  },
  props: {
    stock: {
      type: Object,
      default() {
        return {
          state: stock/* state */.SB,
          setter: stock/* setter */.IS
        };
      }
    },
    // 布局属性
    layout: {
      type: Object,
      default() {
        return gridvue_type_script_lang_js_objectSpread({}, constant/* default */.Z.layout);
      }
    },
    // 主题
    theme: {
      type: Object,
      default() {
        return {};
      }
    },
    // 事件
    events: {
      type: Object,
      default() {
        return {};
      }
    },
    // 资源
    resource: {
      type: Object,
      default() {
        return {};
      }
    },
    // 子组件
    childComponents: {
      default() {
        return [];
      }
    },
    cpr: {
      type: Object,
      default() {
        return {
          component: 'div',
          isProperty: false
        };
      }
    }
  },
  watch: {
    layout: {
      handler() {
        this.syncLayout();
      },
      deep: true
    },
    childComponents: {
      handler() {
        this.components = this.childComponents || [];
      },
      deep: true
    },
    components: function components() {
      this.syncItems();
    }
  },
  data() {
    return {
      state: stock/* state */.SB,
      isDesign: stock/* state */.SB.isDesign,
      // 设计态
      isContrast: false,
      // 对照态
      contrastStyle: {},
      // 对照样式
      components: ((stock/* state */.SB.page || {}).meta || {}).childComponents || [],
      items: []
    };
  },
  methods: {
    syncItems() {
      let components = this.components;
      let items = components.map((item, i) => {
        if (!item.layout.i && item.layout.i !== 0) {
          let prev = components[i - 1];
          if (i > 0) {
            item.layout.y = prev.layout.y + prev.layout.h;
          }
          item.layout.i = Date.now();
        }
        return item.layout;
      });
      this.items = items;
    },
    // 同步布局
    syncLayout() {
      let contrast = (this.layout || {}).contrast;
      this.isContrast = (contrast || {}).isAble;
      let contrastStyle = {
        'background-color': (contrast || {}).bgColor
      };
      this.contrastStyle = contrastStyle;
    },
    // 选中布局
    handleSelectLayout() {
      if (!this.isDesign) {
        return false;
      }
      stock/* setter */.IS.selectPropertyView(property/* default */.Z);
      stock/* setter */.IS.selectLayout(stock/* default.state */.ZP.state.page.meta, constant/* default */.Z);
      stock/* setter */.IS.selectComponent(null);
      this.updateView();
    },
    // 选中组件
    handleSelectComponent(component) {
      if (!this.isDesign) {
        return false;
      }
      stock/* setter */.IS.selectPropertyView(box_property/* default */.Z);
      setTimeout(() => {
        stock/* setter */.IS.selectComponent(component);
        this.updateView();
      }, 40);
    },
    // 获取元数据
    getMetaValue() {
      let list = this.components.map(item => {
        let node = Object.assign({}, item);
        delete node.component;
        delete node.propertyComponent;
        return node;
      });
      let meta = stock/* default.state */.ZP.state.page.meta;
      let cpr = this.cpr;
      return {
        name: cpr.name,
        version: cpr.version,
        properts: meta.properts,
        layout: meta.layout,
        events: meta.events,
        resource: meta.resource,
        theme: meta.theme,
        childComponents: list
      };
    },
    // 移除组件
    handleRemoveComponent(item) {
      stock/* setter */.IS.removeComponent(item);
      stock/* setter */.IS.selectLayout(null);
      stock/* setter */.IS.selectComponent(null);
      stock/* setter */.IS.selectPropertyView(null);
      this.updateView();
    },
    /**
     * @function 更新视图
    */
    updateView() {
      // 子项更新
      let items = this.$refs.layoutItem;
      if (items && items.length) {
        items.forEach(item => {
          setTimeout(() => {
            item.updateView();
          }, 40);
        });
      }
    }
  },
  mounted() {
    this.syncItems();
    this.syncLayout();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/index.vue?vue&type=script&lang=js
 /* harmony default export */ var layout_gridvue_type_script_lang_js = (gridvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/index.vue?vue&type=style&index=0&id=8d937876&prod&lang=less
var gridvue_type_style_index_0_id_8d937876_prod_lang_less = __webpack_require__(9350);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/index.vue?vue&type=style&index=0&id=8d937876&prod&lang=less

;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/index.vue



;


/* normalize component */

var grid_component = (0,componentNormalizer/* default */.Z)(
  layout_gridvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var grid = (grid_component.exports);

/***/ }),

/***/ 7032:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  constant: function() { return /* reexport */ constant/* default */.Z; },
  "default": function() { return /* binding */ grid_property; }
});

// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/property.vue + 4 modules
var property = __webpack_require__(8760);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/grid/constant.js
var constant = __webpack_require__(873);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/grid/property.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }


/* harmony default export */ var propertyvue_type_script_lang_js = ({
  extends: property["default"],
  props: {
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z
    };
  }
});

;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/property.vue?vue&type=script&lang=js
 /* harmony default export */ var grid_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/grid/property.vue
var render, staticRenderFns
;



/* normalize component */
;
var component = (0,componentNormalizer/* default */.Z)(
  grid_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var grid_property = (component.exports);

/***/ }),

/***/ 6431:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/property.vue?vue&type=template&id=6a865696&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('tabs', {
    attrs: {
      "type": "beforeline",
      "position": "top",
      "isLazyRender": true,
      "activeName": 'properts'
    }
  }, [_c('tabs-item', {
    attrs: {
      "title": "属性",
      "name": 'properts'
    }
  }, [_c('CPRender', {
    key: _vm.key,
    ref: "properts",
    attrs: {
      "cpr": _vm.property.component,
      "value": _vm.property.properts,
      "properts": _vm.property.properts,
      "layout": _vm.property.layout,
      "events": _vm.property.events,
      "resource": _vm.property.resource,
      "theme": _vm.property.theme,
      "childComponents": _vm.property.childComponents
    },
    on: {
      "update": _vm.handleUpdate
    }
  })], 1), _c('tabs-item', {
    attrs: {
      "title": "布局",
      "name": 'layout'
    }
  }, [_c('FormGroup', {
    attrs: {
      "label": "类型",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "value": _vm.getLayoutName(),
      "readonly": ""
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "外边距",
      "col": 11
    }
  }, [_c('div', {
    staticClass: "layout-gauge"
  }, [_c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "上"
    },
    model: {
      value: _vm.property.layout.margin.top,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.margin, "top", $$v);
      },
      expression: "property.layout.margin.top"
    }
  }), _c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "左"
    },
    model: {
      value: _vm.property.layout.margin.left,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.margin, "left", $$v);
      },
      expression: "property.layout.margin.left"
    }
  }), _c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "右"
    },
    model: {
      value: _vm.property.layout.margin.right,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.margin, "right", $$v);
      },
      expression: "property.layout.margin.right"
    }
  }), _c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "下"
    },
    model: {
      value: _vm.property.layout.margin.bottom,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.margin, "bottom", $$v);
      },
      expression: "property.layout.margin.bottom"
    }
  }), _c('div')], 1)]), _c('FormGroup', {
    attrs: {
      "label": "内边距",
      "col": 11
    }
  }, [_c('div', {
    staticClass: "layout-gauge"
  }, [_c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "上"
    },
    model: {
      value: _vm.property.layout.padding.top,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.padding, "top", $$v);
      },
      expression: "property.layout.padding.top"
    }
  }), _c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "左"
    },
    model: {
      value: _vm.property.layout.padding.left,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.padding, "left", $$v);
      },
      expression: "property.layout.padding.left"
    }
  }), _c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "右"
    },
    model: {
      value: _vm.property.layout.padding.right,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.padding, "right", $$v);
      },
      expression: "property.layout.padding.right"
    }
  }), _c('div'), _c('FormInput', {
    staticClass: "d-col-xs-12",
    attrs: {
      "placeholder": "下"
    },
    model: {
      value: _vm.property.layout.padding.bottom,
      callback: function callback($$v) {
        _vm.$set(_vm.property.layout.padding, "bottom", $$v);
      },
      expression: "property.layout.padding.bottom"
    }
  }), _c('div')], 1)])], 1), _c('tabs-item', {
    attrs: {
      "title": "事件",
      "name": 'events'
    }
  }, [_c('FormGroup', {
    attrs: {
      "label": "created",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.created,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "created", $$v);
      },
      expression: "property.events.created"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "beforeMount",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.beforeMount,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "beforeMount", $$v);
      },
      expression: "property.events.beforeMount"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "mounted",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.mounted,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "mounted", $$v);
      },
      expression: "property.events.mounted"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "beforeDestroy",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.beforeDestroy,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "beforeDestroy", $$v);
      },
      expression: "property.events.beforeDestroy"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "destroyed",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.events.destroyed,
      callback: function callback($$v) {
        _vm.$set(_vm.property.events, "destroyed", $$v);
      },
      expression: "property.events.destroyed"
    }
  })], 1)], 1), _c('tabs-item', {
    attrs: {
      "title": "主题",
      "name": 'theme'
    }
  }, [_c('FormGroup', {
    attrs: {
      "label": "背景色",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "placeholder": "如：#fafafa"
    },
    model: {
      value: _vm.property.theme.bgColor,
      callback: function callback($$v) {
        _vm.$set(_vm.property.theme, "bgColor", $$v);
      },
      expression: "property.theme.bgColor"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "背景图片",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "placeholder": "图片地址"
    },
    model: {
      value: _vm.property.theme.bgImage,
      callback: function callback($$v) {
        _vm.$set(_vm.property.theme, "bgImage", $$v);
      },
      expression: "property.theme.bgImage"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "背景尺寸",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "placeholder": "如：100% 50%"
    },
    model: {
      value: _vm.property.theme.bgSize,
      callback: function callback($$v) {
        _vm.$set(_vm.property.theme, "bgSize", $$v);
      },
      expression: "property.theme.bgSize"
    }
  })], 1)], 1), _c('tabs-item', {
    attrs: {
      "title": "资源",
      "name": 'resource',
      "className": "coder-group"
    }
  }, [_c('div', {
    staticClass: "d-container"
  }, [_c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead"
  }, [_vm._v("执行资源"), _c('span', {
    staticClass: "d-text-lightgray d-text-small"
  }, [_vm._v("(在组件初始后(created)执行)")])]), _c('FormGroup', {
    attrs: {
      "label": "JavaScript",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "height": "94vh"
    },
    model: {
      value: _vm.property.resource.js,
      callback: function callback($$v) {
        _vm.$set(_vm.property.resource, "js", $$v);
      },
      expression: "property.resource.js"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "CSS",
      "col": 12,
      "labelPosition": "left",
      "labelAlign": "right",
      "labelPadding": "0 12px",
      "labelWidth": "120px"
    }
  }, [_c('CoderEntry', {
    attrs: {
      "language": "css",
      "height": "94vh"
    },
    model: {
      value: _vm.property.resource.css,
      callback: function callback($$v) {
        _vm.$set(_vm.property.resource, "css", $$v);
      },
      expression: "property.resource.css"
    }
  })], 1), _c('div', {
    staticClass: "d-section d-pull-clear d-caption-lead d-layout-flex-between-center"
  }, [_c('span', [_vm._v("引用资源"), _c('span', {
    staticClass: "d-text-lightgray d-text-small"
  }, [_vm._v("(在组件初始后(created)执行)")])]), _c('span', {
    staticClass: "d-text-primary d-text-linker",
    attrs: {
      "title": "添加资源"
    },
    on: {
      "click": _vm.handleAddFile
    }
  }, [_c('i', {
    staticClass: "el-icon-plus"
  })])]), _c('div', {
    staticClass: "d-section"
  }, [_c('Grid', {
    ref: "gridList",
    attrs: {
      "isPagerAble": false
    },
    model: {
      value: _vm.files,
      callback: function callback($$v) {
        _vm.files = $$v;
      },
      expression: "files"
    }
  }, [_c('GridColumn', {
    attrs: {
      "width": 45,
      "type": "index",
      "label": "序号",
      "className": "d-vm"
    }
  }), _c('GridColumn', {
    attrs: {
      "field": "type",
      "label": "类型",
      "width": "120"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('select', {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: scope.type,
            expression: "scope.type"
          }],
          staticClass: "d-form-select",
          on: {
            "change": function change($event) {
              var $$selectedVal = Array.prototype.filter.call($event.target.options, function (o) {
                return o.selected;
              }).map(function (o) {
                var val = "_value" in o ? o._value : o.value;
                return val;
              });
              _vm.$set(scope, "type", $event.target.multiple ? $$selectedVal : $$selectedVal[0]);
            }
          }
        }, _vm._l(_vm.types, function (item) {
          return _c('option', {
            key: item.type,
            domProps: {
              "value": item.type
            }
          }, [_vm._v(_vm._s(item.text))]);
        }), 0)];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "name",
      "label": "名称",
      "width": 100
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('FormInput', {
          attrs: {
            "title": scope.name
          },
          model: {
            value: scope.name,
            callback: function callback($$v) {
              _vm.$set(scope, "name", $$v);
            },
            expression: "scope.name"
          }
        })];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "url",
      "label": "地址"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('FormInput', {
          attrs: {
            "title": scope.url
          },
          model: {
            value: scope.url,
            callback: function callback($$v) {
              _vm.$set(scope, "url", $$v);
            },
            expression: "scope.url"
          }
        })];
      }
    }])
  }), _c('GridColumn', {
    attrs: {
      "field": "action",
      "width": 60,
      "label": "操作",
      "className": "d-vm"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn(scope) {
        return [_c('div', {
          staticClass: "d-bar-tools d-bar-tools-condensed d-grid-actions"
        }, [_c('a', {
          staticClass: "d-tools-item d-text-theme",
          attrs: {
            "title": "注册"
          },
          on: {
            "click": function click($event) {
              return _vm.handleShowRegister(scope);
            }
          }
        }, [_c('PropIcon', {
          attrs: {
            "size": "sm"
          }
        })], 1), _c('a', {
          staticClass: "d-tools-item d-text-theme",
          attrs: {
            "title": "删除"
          },
          on: {
            "click": function click($event) {
              return _vm.handleDelFile(scope);
            }
          }
        }, [_c('DeleteIcon', {
          attrs: {
            "size": "sm"
          }
        })], 1)])];
      }
    }])
  })], 1)], 1)], 1), _c('Dialoger', {
    attrs: {
      "onConfirm": _vm.handleSubmitRegister,
      "dialogCls": 'd-paner-lg',
      "title": "资源注册"
    },
    scopedSlots: _vm._u([{
      key: "body",
      fn: function fn() {
        return [_c('FormGroup', {
          attrs: {
            "label": "注册名",
            "col": 12
          }
        }, [_c('FormInput', {
          model: {
            value: _vm.file.name,
            callback: function callback($$v) {
              _vm.$set(_vm.file, "name", $$v);
            },
            expression: "file.name"
          }
        })], 1), _c('FormGroup', {
          attrs: {
            "label": "地址",
            "col": 12
          }
        }, [_c('FormInput', {
          model: {
            value: _vm.file.url,
            callback: function callback($$v) {
              _vm.$set(_vm.file, "url", $$v);
            },
            expression: "file.url"
          }
        })], 1), _c('FormGroup', {
          attrs: {
            "label": "内容",
            "col": 12
          }
        }, [_c('Coder', {
          attrs: {
            "language": _vm.file.type === 'link' ? 'css' : 'javascript',
            "height": "60vh"
          },
          model: {
            value: _vm.file.content,
            callback: function callback($$v) {
              _vm.$set(_vm.file, "content", $$v);
            },
            expression: "file.content"
          }
        })], 1)];
      },
      proxy: true
    }]),
    model: {
      value: _vm.isShowRegister,
      callback: function callback($$v) {
        _vm.isShowRegister = $$v;
      },
      expression: "isShowRegister"
    }
  })], 1)], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/element-resize-detector/src/element-resize-detector.js
var element_resize_detector = __webpack_require__(6084);
var element_resize_detector_default = /*#__PURE__*/__webpack_require__.n(element_resize_detector);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
// EXTERNAL MODULE: external "@daelui/vdog/dist/components.js"
var dist_components_js_ = __webpack_require__(4236);
// EXTERNAL MODULE: external "@daelui/vdog/dist/coder.js"
var coder_js_ = __webpack_require__(7443);
// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
// EXTERNAL MODULE: ./src/service/stock/index.js + 4 modules
var stock = __webpack_require__(3085);
// EXTERNAL MODULE: ./src/views/dview/page/components/cprender.vue + 5 modules
var cprender = __webpack_require__(8735);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/property.vue?vue&type=script&lang=js








/* harmony default export */ var propertyvue_type_script_lang_js = ({
  components: {
    Paner: dist_components_js_.Paner,
    Tabs: dist_components_js_.Tabs,
    TabsItem: dist_components_js_.TabsItem,
    Coder: coder_js_.Coder,
    CoderEntry: coder_js_.CoderEntry,
    CPRender: cprender/* default */.Z,
    FormGroup: dist_components_js_.FormGroup,
    FormInput: dist_components_js_.FormInput,
    FormButton: dist_components_js_.FormButton,
    Grid: dist_components_js_.Grid,
    GridColumn: dist_components_js_.GridColumn,
    Dialoger: dist_components_js_.Dialoger,
    DeleteIcon: icons_js_.DeleteIcon,
    PropIcon: icons_js_.PropIcon
  },
  props: {
    stock: {
      type: Object,
      default() {
        return stock/* default */.ZP;
      }
    }
  },
  data() {
    return {
      key: '',
      // 属性面板状态
      property: {
        component: {
          component: 'div'
        },
        properts: {},
        events: {},
        resource: {},
        theme: {},
        layout: {
          margin: {
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
          },
          padding: {
            top: '8px',
            right: '8px',
            bottom: '8px',
            left: '8px'
          }
        }
      },
      types: [{
        type: 'script',
        text: 'JavaScript'
      }, {
        type: 'link',
        text: 'CSS'
      }],
      // 文件列表
      files: [
        // { id: strer.utid(), url : '', type: 'script' }
      ],
      isShowRegister: false,
      file: {}
    };
  },
  watch: {
    // 选择的布局更新
    'stock.state.selectedLayout': {
      handler() {
        this.handleUpdateSelectedLayout();
      },
      deep: true
    },
    // 布局
    'property.layout': {
      handler() {
        let selectedLayout = this.stock.state.selectedLayout;
        this.$set(selectedLayout, 'layout', this.property.layout || {});
      },
      deep: true
    },
    // 事件
    'property.events': {
      handler() {
        let selectedLayout = this.stock.state.selectedLayout;
        this.$set(selectedLayout, 'events', this.property.events || {});
      },
      deep: true
    },
    // 资源
    'property.resource': {
      handler() {
        let selectedLayout = this.stock.state.selectedLayout;
        this.$set(selectedLayout, 'resource', this.property.resource || {});
      },
      deep: true
    }
  },
  methods: {
    // 属性更新
    handleUpdate(data) {
      this.$emit('update', data);
    },
    // 更新属性
    handleUpdateSelectedLayout() {
      let sl = this.stock.state.selectedLayout;
      let defProps = this.stock.state.selectedLayoutDefProps;
      if (!sl.name) {
        return;
      }
      // 组件信息
      this.$set(this.property, 'component', {
        name: sl.name,
        version: sl.version,
        isProperty: true
      });
      // 属性
      this.$set(this.property, 'properts', sl.properts || {});
      // 布局
      let layout = sl.layout || {};
      layout.margin = layout.margin || defProps.layout.margin || {
        top: '0px',
        right: '0px',
        bottom: '0px',
        left: '0px'
      };
      layout.padding = layout.padding || defProps.layout.padding || {
        top: '8px',
        right: '8px',
        bottom: '8px',
        left: '8px'
      };
      this.$set(this.property, 'layout', layout);
      // 事件
      this.$set(this.property, 'events', sl.events || {});
      // 主题
      this.$set(this.property, 'theme', sl.theme || {});
      // 资源
      this.$set(this.property, 'resource', sl.resource || {});
      this.property.resource.files = Array.isArray(this.property.resource.files) ? this.property.resource.files : [];
      this.files = this.property.resource.files;
      this.key = sl.id || components_js_.strer.utid();
    },
    // 添加文件
    handleAddFile() {
      this.files.push({
        id: components_js_.strer.utid(),
        name: '',
        url: '',
        content: '',
        type: 'script'
      });
      this.$set(this.property.resource, 'files', this.files);
    },
    // 删除文件
    handleDelFile(item) {
      this.files = this.files.filter(node => {
        return item !== node;
      });
      this.$set(this.property.resource, 'files', this.files);
    },
    // 显示注册
    handleShowRegister(item) {
      this.file = components_js_.objecter.clone(item);
      this.currentfile = item;
      this.isShowRegister = true;
    },
    // 确认注册
    handleSubmitRegister() {
      Object.assign(this.currentfile, this.file);
    },
    // 获取布局名称
    getLayoutName() {
      let name = stock/* state */.SB.page.layout;
      let collection = stock/* state */.SB.layoutCollection;
      collection = Array.isArray(collection) ? collection : [];
      let node = collection.find(item => item.name === name);
      if (node) {
        name = node.text;
      }
      return name;
    }
  },
  mounted() {
    this.handleUpdateSelectedLayout();
    // 监听元素变化，编辑器自适应
    let erd = element_resize_detector_default()();
    erd.listenTo(this.$el, () => {
      for (let key in this.$refs) {
        if (/coder/.test(key)) {
          let monacoEditor = this.$refs[key].monacoEditor;
          monacoEditor && monacoEditor.layout();
        }
      }
    });
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/property.vue?vue&type=script&lang=js
 /* harmony default export */ var layout_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/property.vue?vue&type=style&index=0&id=6a865696&prod&lang=less&scoped=true
var propertyvue_type_style_index_0_id_6a865696_prod_lang_less_scoped_true = __webpack_require__(4196);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/property.vue?vue&type=style&index=0&id=6a865696&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/property.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  layout_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "6a865696",
  null
  
)

/* harmony default export */ var property = (component.exports);

/***/ }),

/***/ 2565:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ screen; }
});

// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/index.vue + 4 modules
var absolute = __webpack_require__(9399);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/container.vue + 4 modules
var container = __webpack_require__(9770);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/screen/constant.js
var constant = __webpack_require__(6674);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/screen/container.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }


/* harmony default export */ var containervue_type_script_lang_js = ({
  extends: container/* default */.Z,
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    },
    // 主题
    theme: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.theme);
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z,
      className: 'd-layout-screen'
    };
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/container.vue?vue&type=script&lang=js
 /* harmony default export */ var screen_containervue_type_script_lang_js = (containervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/screen/container.vue?vue&type=style&index=0&id=db9cece0&prod&lang=less
var containervue_type_style_index_0_id_db9cece0_prod_lang_less = __webpack_require__(4160);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/container.vue?vue&type=style&index=0&id=db9cece0&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/container.vue
var render, staticRenderFns
;

;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  screen_containervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var screen_container = (component.exports);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/item.vue + 4 modules
var item = __webpack_require__(4597);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/screen/item.vue?vue&type=script&lang=js

/* harmony default export */ var itemvue_type_script_lang_js = ({
  extends: item/* default */.Z
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/item.vue?vue&type=script&lang=js
 /* harmony default export */ var screen_itemvue_type_script_lang_js = (itemvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/item.vue
var item_render, item_staticRenderFns
;



/* normalize component */
;
var item_component = (0,componentNormalizer/* default */.Z)(
  screen_itemvue_type_script_lang_js,
  item_render,
  item_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var screen_item = (item_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/screen/index.vue?vue&type=script&lang=js
function screenvue_type_script_lang_js_ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function screenvue_type_script_lang_js_objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? screenvue_type_script_lang_js_ownKeys(Object(t), !0).forEach(function (r) { screenvue_type_script_lang_js_defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : screenvue_type_script_lang_js_ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function screenvue_type_script_lang_js_defineProperty(obj, key, value) { key = screenvue_type_script_lang_js_toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function screenvue_type_script_lang_js_toPropertyKey(arg) { var key = screenvue_type_script_lang_js_toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function screenvue_type_script_lang_js_toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }




/* harmony default export */ var screenvue_type_script_lang_js = ({
  extends: absolute["default"],
  components: {
    LayoutContainer: screen_container,
    LayoutItem: screen_item
  },
  props: {
    // 布局属性
    layout: {
      type: Object,
      default() {
        return screenvue_type_script_lang_js_objectSpread({}, constant/* default */.Z.layout);
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z
    };
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/index.vue?vue&type=script&lang=js
 /* harmony default export */ var layout_screenvue_type_script_lang_js = (screenvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/index.vue
var screen_render, screen_staticRenderFns
;



/* normalize component */
;
var screen_component = (0,componentNormalizer/* default */.Z)(
  layout_screenvue_type_script_lang_js,
  screen_render,
  screen_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var screen = (screen_component.exports);

/***/ }),

/***/ 6592:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ screen_property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/screen/property.vue?vue&type=template&id=9729ba9e
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('el-tabs', {
    staticClass: "layout-properts el-tabs-paner",
    model: {
      value: _vm.activeName,
      callback: function callback($$v) {
        _vm.activeName = $$v;
      },
      expression: "activeName"
    }
  }, [_c('el-tab-pane', {
    attrs: {
      "label": "配置",
      "name": "first"
    }
  }, [_c('div', {
    staticClass: "form-section"
  }, [_c('FormGroup', {
    attrs: {
      "label": "列数",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.columns,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "columns", $$v);
      },
      expression: "layout.columns"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "参考宽度",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.referWidth,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "referWidth", $$v);
      },
      expression: "layout.referWidth"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "行数",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.rows,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "rows", $$v);
      },
      expression: "layout.rows"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "参考高度",
      "col": 11
    }
  }, [_c('FormInput', {
    model: {
      value: _vm.layout.referHeight,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "referHeight", $$v);
      },
      expression: "layout.referHeight"
    }
  })], 1), _c('FormGroup', {
    attrs: {
      "label": "自适应高度",
      "col": 11
    }
  }, [_c('Switcher', {
    model: {
      value: _vm.layout.isFixHeight,
      callback: function callback($$v) {
        _vm.$set(_vm.layout, "isFixHeight", $$v);
      },
      expression: "layout.isFixHeight"
    }
  })], 1), ((_vm.layout || {}).contrast || {}).isAble !== undefined ? _c('FormGroup', {
    attrs: {
      "label": "对比",
      "col": 11
    }
  }, [_c('Switcher', {
    model: {
      value: _vm.layout.contrast.isAble,
      callback: function callback($$v) {
        _vm.$set(_vm.layout.contrast, "isAble", $$v);
      },
      expression: "layout.contrast.isAble"
    }
  })], 1) : _vm._e(), ((_vm.layout || {}).contrast || {}).isAble ? _c('FormGroup', {
    attrs: {
      "label": "子项背景色",
      "col": 11
    }
  }, [_c('FormInput', {
    attrs: {
      "type": "color"
    },
    model: {
      value: _vm.layout.contrast.bgColor,
      callback: function callback($$v) {
        _vm.$set(_vm.layout.contrast, "bgColor", $$v);
      },
      expression: "layout.contrast.bgColor"
    }
  })], 1) : _vm._e()], 1)])], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./src/views/dview/page/components/layout/absolute/property.vue + 4 modules
var property = __webpack_require__(8760);
// EXTERNAL MODULE: ./src/views/dview/page/components/layout/screen/constant.js
var constant = __webpack_require__(6674);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/layout/screen/property.vue?vue&type=script&lang=js
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }


/* harmony default export */ var propertyvue_type_script_lang_js = ({
  extends: property["default"],
  props: {
    layout: {
      type: Object,
      default() {
        return _objectSpread({}, constant/* default */.Z.layout);
      }
    }
  },
  data() {
    return {
      constant: constant/* default */.Z
    };
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/property.vue?vue&type=script&lang=js
 /* harmony default export */ var screen_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layout/screen/property.vue





/* normalize component */
;
var component = (0,componentNormalizer/* default */.Z)(
  screen_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var screen_property = (component.exports);

/***/ }),

/***/ 1001:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   Z: function() { return /* binding */ normalizeComponent; }
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent(
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */,
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options =
    typeof scriptExports === 'function' ? scriptExports.options : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) {
    // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
          injectStyles.call(
            this,
            (options.functional ? this.parent : this).$root.$options.shadowRoot
          )
        }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 4987:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".dicon-img[data-v-3b18a9b7]{max-width:100%;max-height:100%}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 5766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-file-chooser{display:inline-block;position:relative}.d-file-chooser .d-uploader-file{display:none}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 4448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".layout-gauge[data-v-50c1bb84]{display:grid;grid-template-areas:\". . .\";max-width:200px;grid-gap:4px}.layout-gauge[data-v-50c1bb84] .d-form-input{text-align:center}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 5976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-cprender-loading{position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);text-align:center;color:#3391ff}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 2694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-drag-resizer.d-dr-dragable{-webkit-user-select:none;-moz-user-select:none;user-select:none}.d-drag-resizer .d-dr-de-nesw,.d-drag-resizer.d-dr-active:before{position:absolute;top:0;right:0;bottom:0;left:0}.d-drag-resizer:before{content:\"\";outline:1px dashed #3391ff}.d-drag-resizer .d-dr-direction .d-dr-de{display:none}.d-drag-resizer.d-dr-active .d-dr-de{display:block}.d-drag-resizer .d-dr-de{position:absolute;background:#fff;border:1px solid #6c6c6c;z-index:9}.d-drag-resizer .d-dr-de-e,.d-drag-resizer .d-dr-de-n,.d-drag-resizer .d-dr-de-s,.d-drag-resizer .d-dr-de-w{border:none;background:transparent}.d-drag-resizer .d-dr-de-n{left:0;right:0;top:-4px;height:8px}.d-drag-resizer .d-dr-de-e{top:0;bottom:0;right:-4px;width:8px}.d-drag-resizer .d-dr-de-s{left:0;right:0;bottom:-4px;height:8px}.d-drag-resizer .d-dr-de-w{top:0;bottom:0;left:-4px;width:8px}.d-drag-resizer .d-dr-de-nw,.d-drag-resizer .d-dr-de-se{cursor:nwse-resize}.d-drag-resizer .d-dr-de-n,.d-drag-resizer .d-dr-de-no,.d-drag-resizer .d-dr-de-s,.d-drag-resizer .d-dr-de-so{cursor:ns-resize}.d-drag-resizer .d-dr-de-ne,.d-drag-resizer .d-dr-de-sw{cursor:nesw-resize}.d-drag-resizer .d-dr-de-e,.d-drag-resizer .d-dr-de-ea,.d-drag-resizer .d-dr-de-w,.d-drag-resizer .d-dr-de-we{cursor:ew-resize}.d-drag-resizer .d-dr-de-ea,.d-drag-resizer .d-dr-de-ne,.d-drag-resizer .d-dr-de-no,.d-drag-resizer .d-dr-de-nw,.d-drag-resizer .d-dr-de-se,.d-drag-resizer .d-dr-de-so,.d-drag-resizer .d-dr-de-sw,.d-drag-resizer .d-dr-de-we{width:8px;height:8px}.d-drag-resizer .d-dr-de-nw{top:-4px;left:-4px}.d-drag-resizer .d-dr-de-no{top:-4px;left:50%;margin-left:-4px}.d-drag-resizer .d-dr-de-ne{top:-4px;right:-4px}.d-drag-resizer .d-dr-de-ea{right:-4px;top:50%;margin-top:-4px}.d-drag-resizer .d-dr-de-se{right:-4px;bottom:-4px}.d-drag-resizer .d-dr-de-so{bottom:-4px;left:50%;margin-left:-4px}.d-drag-resizer .d-dr-de-sw{left:-4px;bottom:-4px}.d-drag-resizer .d-dr-de-we{left:-4px;top:50%;margin-top:-4px}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 6383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-absolute{overflow:auto;position:relative;height:100%}.d-layout-container,.d-layout-render{position:relative;min-height:60%}.d-layout-container{width:100%}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 2274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-item-design:hover .d-layout-item-remove{display:block}.d-layout-item-design .d-layout-item-remove{display:none;position:absolute;right:2px;top:0;cursor:pointer;color:#3391ff;z-index:3}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 7749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-render-item[data-v-596e64fa]{position:absolute}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 7067:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".layout-properts .form-section[data-v-0a89ea9d]{padding-top:12px}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-fill{overflow:auto;position:relative;height:100%}.d-layout-fill>.d-layout-render>.d-layout-container{width:100%;height:100%;display:grid;grid-template-columns:auto auto auto;grid-template-rows:auto auto auto;grid-column-gap:8px;grid-row-gap:8px}.d-layout-container,.d-layout-render{position:relative;height:100%}.d-layout-container{width:100%}.grid-fill{display:grid;grid-template-columns:auto auto auto;grid-template-rows:auto auto auto;grid-column-gap:16px;grid-row-gap:16px;grid-template-areas:\". . .\"}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 9533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-render-item[data-v-f539f3ae]{position:relative;width:auto;height:auto}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-grid{overflow:auto;position:relative;height:100%}.d-layout-container,.d-layout-render{position:relative;min-height:60%}.d-layout-container{width:100%}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 9842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-item-design:hover .d-layout-item-remove{display:block}.d-layout-item-design .d-layout-item-remove{display:none;position:absolute;right:2px;top:0;cursor:pointer;color:#3391ff;z-index:3}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 5844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-render-item[data-v-627431db]{position:absolute}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 3162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".layout-gauge[data-v-6a865696]{display:grid;grid-template-areas:\". . .\";max-width:200px;grid-gap:4px}.layout-gauge[data-v-6a865696] .d-form-input{text-align:center}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 627:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".d-layout-screen{overflow:auto;position:relative;height:100%;background:#171b22}.d-layout-container,.d-layout-render{position:relative;min-height:60%}.d-layout-container{width:100%}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 2937:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".components-paner[data-v-775d0abf]{overflow:auto;height:100%;border-radius:0}.components-paner[data-v-775d0abf] .el-collapse-item__header{line-height:16px}.component-type[data-v-775d0abf]{padding:0 12px;font-weight:700}.list-components[data-v-775d0abf]{padding:0 8px}.item-component[data-v-775d0abf]{margin:0;width:100%;overflow:hidden;border-radius:6px;cursor:pointer;border:1px solid rgba(0,0,0,.04);background-color:#fff;transition-duration:.4s}.item-component+.item-component[data-v-775d0abf]{margin-top:8px}.item-component[data-v-775d0abf]:hover{border-color:#439eff}.component-name[data-v-775d0abf]{display:flex;justify-content:space-between;align-items:center;padding:4px 8px;border-bottom:1px solid rgba(0,0,0,.04);color:#666}.component-name .name-text[data-v-775d0abf]{overflow:hidden;white-space:nowrap;text-overflow:ellipsis;width:90%}.component-name .name-desc[data-v-775d0abf]{color:#bbb}.component-icon[data-v-775d0abf]{display:flex;justify-content:center;align-items:center;height:96px;padding:8px;text-align:center}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 2299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".view-design[data-v-235f6774]{overflow:hidden;width:100%}.view-design.is-fullscreen .design-header[data-v-235f6774]{display:none}.view-design.is-fullscreen .design-body[data-v-235f6774]{height:100vh}.view-design .design-body-through[data-v-235f6774] .d-dr-de-nesw,.view-design .design-body-through[data-v-235f6774] .d-drag-resizer:before{display:none}.view-design .design-body[data-v-235f6774]{height:calc(100vh - 48px)}.view-design .design-body[data-v-235f6774]:after{content:\"\";display:block;clear:both}.view-design .design-side[data-v-235f6774]{float:left;position:relative;overflow:visible;width:168px;height:100%;z-index:11;border-right:1px solid #eee}.view-design .design-side .item-component[data-v-235f6774]{cursor:pointer}.view-design .design-side .component-name[data-v-235f6774]{text-align:center}.view-design .design-side-collapse[data-v-235f6774]{width:0}.view-design .design-side-collapse[data-v-235f6774] .components-paner{overflow:hidden;width:0}.view-design .design-render[data-v-235f6774]{overflow:auto;height:100%;background:#ddd;background-image:linear-gradient(#fff,transparent 0),linear-gradient(90deg,#fff,transparent 0),linear-gradient(hsla(0,0%,100%,.6) 1px,transparent 0),linear-gradient(90deg,hsla(0,0%,100%,.6) 1px,transparent 0);background-size:64px 64px,64px 64px,64px 16px,64px 16px}.view-design .design-property[data-v-235f6774]{float:right;position:relative;overflow:auto;width:512px;height:100%;z-index:11}.view-design .design-property-collapse[data-v-235f6774]{width:0!important}.view-design .design-property-collapse[data-v-235f6774] .property-paner{overflow:hidden;width:0!important}.view-design .design-collpase-bar[data-v-235f6774]{position:absolute;left:0;top:50%}.view-design .design-collpase-bar .bar-icon[data-v-235f6774]{font-size:24px;transform:rotate(90deg);color:#409eff;cursor:ew-resize}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 1841:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".design-menus{float:left;width:33%}.design-menus.el-menu--horizontal>.el-menu-item,.design-menus.el-menu--horizontal>.el-submenu .el-submenu__title{height:48px;line-height:48px}.el-tabs-paner .el-tabs__header{margin-bottom:0}.el-tabs-paner .el-tabs__nav{margin-left:16px}.el-tabs-paner .el-collapse-item__wrap{padding-top:8px;background:#f2f2f7}.el-tabs-paner .el-collapse-item__content{padding-bottom:8px}.design-header .design-name{float:left;display:flex;align-items:center;padding-left:8px;height:100%}.design-header .design-actions{float:right;display:flex;height:100%;align-items:center;padding-right:8px}.design-header .design-actions>.d-btn,.design-header .design-actions>.d-file-chooser,.design-header .design-actions>.d-form-switch,.design-header .design-actions>.d-switch-slider,.design-header .design-actions>.el-button,.design-header .design-actions>.el-dropdown{margin-left:8px}.design-header .design-actions .el-button--mini,.design-header .design-actions .el-button--mini.is-round{padding:7px 10px}.design-property-fullscreen .property-paner{overflow:hidden;position:fixed;top:0;right:0;bottom:0;left:0;z-index:3}.design-property-fullscreen .property-paner+.design-collapse-prop{display:none}.design-property-fullscreen .property-paner .d-editor-monaco{height:calc(100vh - 94px)!important}.design-property-fullscreen+.design-render{position:relative;z-index:2}.design-actions-switch{display:none}@media (max-width:980px){.design-header.is-actions-active{z-index:13}.design-header.is-actions-active .design-actions{display:flex}.design-header .design-actions{display:none;overflow:auto;position:absolute;right:0;top:48px;flex-direction:column;box-sizing:border-box;padding:8px;width:108px;height:calc(100vh - 48px);background:rgba(0,0,0,.5)}.design-header .design-actions .d-file-chooser,.design-header .design-actions>.el-button{flex-shrink:0;display:block;margin:4px 0;width:100%}.design-header .design-actions .d-form-switch{flex-shrink:0;display:block;margin:4px 0}.design-header .design-actions .d-file-chooser .el-button{display:block;width:100%}.design-header .design-actions .el-dropdown{flex-shrink:0;margin:4px 0}.design-header .design-actions-switch{display:block;float:right;display:flex;align-items:center;justify-content:center;width:36px;height:48px;font-size:20px;color:#fff;text-decoration:none}.design-header .design-actions-switch:active,.design-header .design-actions-switch:focus{text-decoration:none}.design-property-fullscreen .property-paner{top:48px}.view-design .design-body .design-property{width:50vw}}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 7114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".property-paner{position:relative;height:100%;border-radius:0;background:#fff}.paner-tools{position:absolute;top:0;right:0;z-index:3}.paner-tools .tool-item{float:left;padding:12px;cursor:pointer}.paner-tools .tool-item svg{font-size:18px}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 2737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".render-paner{height:100%}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 3645:
/***/ (function(module) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
module.exports = function (cssWithMappingToString) {
  var list = [];

  // return the list of modules as css string
  list.toString = function toString() {
    return this.map(function (item) {
      var content = "";
      var needLayer = typeof item[5] !== "undefined";
      if (item[4]) {
        content += "@supports (".concat(item[4], ") {");
      }
      if (item[2]) {
        content += "@media ".concat(item[2], " {");
      }
      if (needLayer) {
        content += "@layer".concat(item[5].length > 0 ? " ".concat(item[5]) : "", " {");
      }
      content += cssWithMappingToString(item);
      if (needLayer) {
        content += "}";
      }
      if (item[2]) {
        content += "}";
      }
      if (item[4]) {
        content += "}";
      }
      return content;
    }).join("");
  };

  // import a list of modules into the list
  list.i = function i(modules, media, dedupe, supports, layer) {
    if (typeof modules === "string") {
      modules = [[null, modules, undefined]];
    }
    var alreadyImportedModules = {};
    if (dedupe) {
      for (var k = 0; k < this.length; k++) {
        var id = this[k][0];
        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }
    for (var _k = 0; _k < modules.length; _k++) {
      var item = [].concat(modules[_k]);
      if (dedupe && alreadyImportedModules[item[0]]) {
        continue;
      }
      if (typeof layer !== "undefined") {
        if (typeof item[5] === "undefined") {
          item[5] = layer;
        } else {
          item[1] = "@layer".concat(item[5].length > 0 ? " ".concat(item[5]) : "", " {").concat(item[1], "}");
          item[5] = layer;
        }
      }
      if (media) {
        if (!item[2]) {
          item[2] = media;
        } else {
          item[1] = "@media ".concat(item[2], " {").concat(item[1], "}");
          item[2] = media;
        }
      }
      if (supports) {
        if (!item[4]) {
          item[4] = "".concat(supports);
        } else {
          item[1] = "@supports (".concat(item[4], ") {").concat(item[1], "}");
          item[4] = supports;
        }
      }
      list.push(item);
    }
  };
  return list;
};

/***/ }),

/***/ 8081:
/***/ (function(module) {

"use strict";


module.exports = function (i) {
  return i[1];
};

/***/ }),

/***/ 8845:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(1373);
module.exports = function batchProcessorMaker(options) {
  options = options || {};
  var reporter = options.reporter;
  var asyncProcess = utils.getOption(options, "async", true);
  var autoProcess = utils.getOption(options, "auto", true);
  if (autoProcess && !asyncProcess) {
    reporter && reporter.warn("Invalid options combination. auto=true and async=false is invalid. Setting async=true.");
    asyncProcess = true;
  }
  var batch = Batch();
  var asyncFrameHandler;
  var isProcessing = false;
  function addFunction(level, fn) {
    if (!isProcessing && autoProcess && asyncProcess && batch.size() === 0) {
      // Since this is async, it is guaranteed to be executed after that the fn is added to the batch.
      // This needs to be done before, since we're checking the size of the batch to be 0.
      processBatchAsync();
    }
    batch.add(level, fn);
  }
  function processBatch() {
    // Save the current batch, and create a new batch so that incoming functions are not added into the currently processing batch.
    // Continue processing until the top-level batch is empty (functions may be added to the new batch while processing, and so on).
    isProcessing = true;
    while (batch.size()) {
      var processingBatch = batch;
      batch = Batch();
      processingBatch.process();
    }
    isProcessing = false;
  }
  function forceProcessBatch(localAsyncProcess) {
    if (isProcessing) {
      return;
    }
    if (localAsyncProcess === undefined) {
      localAsyncProcess = asyncProcess;
    }
    if (asyncFrameHandler) {
      cancelFrame(asyncFrameHandler);
      asyncFrameHandler = null;
    }
    if (localAsyncProcess) {
      processBatchAsync();
    } else {
      processBatch();
    }
  }
  function processBatchAsync() {
    asyncFrameHandler = requestFrame(processBatch);
  }
  function clearBatch() {
    batch = {};
    batchSize = 0;
    topLevel = 0;
    bottomLevel = 0;
  }
  function cancelFrame(listener) {
    // var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.clearTimeout;
    var cancel = clearTimeout;
    return cancel(listener);
  }
  function requestFrame(callback) {
    // var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || function(fn) { return window.setTimeout(fn, 20); };
    var raf = function raf(fn) {
      return setTimeout(fn, 0);
    };
    return raf(callback);
  }
  return {
    add: addFunction,
    force: forceProcessBatch
  };
};
function Batch() {
  var batch = {};
  var size = 0;
  var topLevel = 0;
  var bottomLevel = 0;
  function add(level, fn) {
    if (!fn) {
      fn = level;
      level = 0;
    }
    if (level > topLevel) {
      topLevel = level;
    } else if (level < bottomLevel) {
      bottomLevel = level;
    }
    if (!batch[level]) {
      batch[level] = [];
    }
    batch[level].push(fn);
    size++;
  }
  function process() {
    for (var level = bottomLevel; level <= topLevel; level++) {
      var fns = batch[level];
      for (var i = 0; i < fns.length; i++) {
        var fn = fns[i];
        fn();
      }
    }
  }
  function getSize() {
    return size;
  }
  return {
    add: add,
    process: process,
    size: getSize
  };
}

/***/ }),

/***/ 1373:
/***/ (function(module) {

"use strict";


var utils = module.exports = {};
utils.getOption = getOption;
function getOption(options, name, defaultValue) {
  var value = options[name];
  if ((value === undefined || value === null) && defaultValue !== undefined) {
    return defaultValue;
  }
  return value;
}

/***/ }),

/***/ 2499:
/***/ (function(module) {

"use strict";


var detector = module.exports = {};
detector.isIE = function (version) {
  function isAnyIeVersion() {
    var agent = navigator.userAgent.toLowerCase();
    return agent.indexOf("msie") !== -1 || agent.indexOf("trident") !== -1 || agent.indexOf(" edge/") !== -1;
  }
  if (!isAnyIeVersion()) {
    return false;
  }
  if (!version) {
    return true;
  }

  //Shamelessly stolen from https://gist.github.com/padolsey/527683
  var ieVersion = function () {
    var undef,
      v = 3,
      div = document.createElement("div"),
      all = div.getElementsByTagName("i");
    do {
      div.innerHTML = "<!--[if gt IE " + ++v + "]><i></i><![endif]-->";
    } while (all[0]);
    return v > 4 ? v : undef;
  }();
  return version === ieVersion;
};
detector.isLegacyOpera = function () {
  return !!window.opera;
};

/***/ }),

/***/ 8485:
/***/ (function(module) {

"use strict";


var utils = module.exports = {};

/**
 * Loops through the collection and calls the callback for each element. if the callback returns truthy, the loop is broken and returns the same value.
 * @public
 * @param {*} collection The collection to loop through. Needs to have a length property set and have indices set from 0 to length - 1.
 * @param {function} callback The callback to be called for each element. The element will be given as a parameter to the callback. If this callback returns truthy, the loop is broken and the same value is returned.
 * @returns {*} The value that a callback has returned (if truthy). Otherwise nothing.
 */
utils.forEach = function (collection, callback) {
  for (var i = 0; i < collection.length; i++) {
    var result = callback(collection[i]);
    if (result) {
      return result;
    }
  }
};

/***/ }),

/***/ 8862:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

"use strict";
/**
 * Resize detection strategy that injects objects to elements in order to detect resize events.
 * Heavily inspired by: http://www.backalleycoder.com/2013/03/18/cross-browser-event-based-element-resize-detection/
 */



var browserDetector = __webpack_require__(2499);
module.exports = function (options) {
  options = options || {};
  var reporter = options.reporter;
  var batchProcessor = options.batchProcessor;
  var getState = options.stateHandler.getState;
  if (!reporter) {
    throw new Error("Missing required dependency: reporter.");
  }

  /**
   * Adds a resize event listener to the element.
   * @public
   * @param {element} element The element that should have the listener added.
   * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
   */
  function addListener(element, listener) {
    function listenerProxy() {
      listener(element);
    }
    if (browserDetector.isIE(8)) {
      //IE 8 does not support object, but supports the resize event directly on elements.
      getState(element).object = {
        proxy: listenerProxy
      };
      element.attachEvent("onresize", listenerProxy);
    } else {
      var object = getObject(element);
      if (!object) {
        throw new Error("Element is not detectable by this strategy.");
      }
      object.contentDocument.defaultView.addEventListener("resize", listenerProxy);
    }
  }
  function buildCssTextString(rules) {
    var seperator = options.important ? " !important; " : "; ";
    return (rules.join(seperator) + seperator).trim();
  }

  /**
   * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
   * @private
   * @param {object} options Optional options object.
   * @param {element} element The element to make detectable
   * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
   */
  function makeDetectable(options, element, callback) {
    if (!callback) {
      callback = element;
      element = options;
      options = null;
    }
    options = options || {};
    var debug = options.debug;
    function injectObject(element, callback) {
      var OBJECT_STYLE = buildCssTextString(["display: block", "position: absolute", "top: 0", "left: 0", "width: 100%", "height: 100%", "border: none", "padding: 0", "margin: 0", "opacity: 0", "z-index: -1000", "pointer-events: none"]);

      //The target element needs to be positioned (everything except static) so the absolute positioned object will be positioned relative to the target element.

      // Position altering may be performed directly or on object load, depending on if style resolution is possible directly or not.
      var positionCheckPerformed = false;

      // The element may not yet be attached to the DOM, and therefore the style object may be empty in some browsers.
      // Since the style object is a reference, it will be updated as soon as the element is attached to the DOM.
      var style = window.getComputedStyle(element);
      var width = element.offsetWidth;
      var height = element.offsetHeight;
      getState(element).startSize = {
        width: width,
        height: height
      };
      function mutateDom() {
        function alterPositionStyles() {
          if (style.position === "static") {
            element.style.setProperty("position", "relative", options.important ? "important" : "");
            var removeRelativeStyles = function removeRelativeStyles(reporter, element, style, property) {
              function getNumericalValue(value) {
                return value.replace(/[^-\d\.]/g, "");
              }
              var value = style[property];
              if (value !== "auto" && getNumericalValue(value) !== "0") {
                reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
                element.style.setProperty(property, "0", options.important ? "important" : "");
              }
            };

            //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
            //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
            removeRelativeStyles(reporter, element, style, "top");
            removeRelativeStyles(reporter, element, style, "right");
            removeRelativeStyles(reporter, element, style, "bottom");
            removeRelativeStyles(reporter, element, style, "left");
          }
        }
        function onObjectLoad() {
          // The object has been loaded, which means that the element now is guaranteed to be attached to the DOM.
          if (!positionCheckPerformed) {
            alterPositionStyles();
          }

          /*jshint validthis: true */

          function getDocument(element, callback) {
            //Opera 12 seem to call the object.onload before the actual document has been created.
            //So if it is not present, poll it with an timeout until it is present.
            //TODO: Could maybe be handled better with object.onreadystatechange or similar.
            if (!element.contentDocument) {
              var state = getState(element);
              if (state.checkForObjectDocumentTimeoutId) {
                window.clearTimeout(state.checkForObjectDocumentTimeoutId);
              }
              state.checkForObjectDocumentTimeoutId = setTimeout(function checkForObjectDocument() {
                state.checkForObjectDocumentTimeoutId = 0;
                getDocument(element, callback);
              }, 100);
              return;
            }
            callback(element.contentDocument);
          }

          //Mutating the object element here seems to fire another load event.
          //Mutating the inner document of the object element is fine though.
          var objectElement = this;

          //Create the style element to be added to the object.
          getDocument(objectElement, function onObjectDocumentReady(objectDocument) {
            //Notify that the element is ready to be listened to.
            callback(element);
          });
        }

        // The element may be detached from the DOM, and some browsers does not support style resolving of detached elements.
        // The alterPositionStyles needs to be delayed until we know the element has been attached to the DOM (which we are sure of when the onObjectLoad has been fired), if style resolution is not possible.
        if (style.position !== "") {
          alterPositionStyles(style);
          positionCheckPerformed = true;
        }

        //Add an object element as a child to the target element that will be listened to for resize events.
        var object = document.createElement("object");
        object.style.cssText = OBJECT_STYLE;
        object.tabIndex = -1;
        object.type = "text/html";
        object.setAttribute("aria-hidden", "true");
        object.onload = onObjectLoad;

        //Safari: This must occur before adding the object to the DOM.
        //IE: Does not like that this happens before, even if it is also added after.
        if (!browserDetector.isIE()) {
          object.data = "about:blank";
        }
        if (!getState(element)) {
          // The element has been uninstalled before the actual loading happened.
          return;
        }
        element.appendChild(object);
        getState(element).object = object;

        //IE: This must occur after adding the object to the DOM.
        if (browserDetector.isIE()) {
          object.data = "about:blank";
        }
      }
      if (batchProcessor) {
        batchProcessor.add(mutateDom);
      } else {
        mutateDom();
      }
    }
    if (browserDetector.isIE(8)) {
      //IE 8 does not support objects properly. Luckily they do support the resize event.
      //So do not inject the object and notify that the element is already ready to be listened to.
      //The event handler for the resize event is attached in the utils.addListener instead.
      callback(element);
    } else {
      injectObject(element, callback);
    }
  }

  /**
   * Returns the child object of the target element.
   * @private
   * @param {element} element The target element.
   * @returns The object element of the target.
   */
  function getObject(element) {
    return getState(element).object;
  }
  function uninstall(element) {
    if (!getState(element)) {
      return;
    }
    var object = getObject(element);
    if (!object) {
      return;
    }
    if (browserDetector.isIE(8)) {
      element.detachEvent("onresize", object.proxy);
    } else {
      element.removeChild(object);
    }
    if (getState(element).checkForObjectDocumentTimeoutId) {
      window.clearTimeout(getState(element).checkForObjectDocumentTimeoutId);
    }
    delete getState(element).object;
  }
  return {
    makeDetectable: makeDetectable,
    addListener: addListener,
    uninstall: uninstall
  };
};

/***/ }),

/***/ 8197:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

"use strict";
/**
 * Resize detection strategy that injects divs to elements in order to detect resize events on scroll events.
 * Heavily inspired by: https://github.com/marcj/css-element-queries/blob/master/src/ResizeSensor.js
 */



var forEach = (__webpack_require__(8485).forEach);
module.exports = function (options) {
  options = options || {};
  var reporter = options.reporter;
  var batchProcessor = options.batchProcessor;
  var getState = options.stateHandler.getState;
  var hasState = options.stateHandler.hasState;
  var idHandler = options.idHandler;
  if (!batchProcessor) {
    throw new Error("Missing required dependency: batchProcessor");
  }
  if (!reporter) {
    throw new Error("Missing required dependency: reporter.");
  }

  //TODO: Could this perhaps be done at installation time?
  var scrollbarSizes = getScrollbarSizes();
  var styleId = "erd_scroll_detection_scrollbar_style";
  var detectionContainerClass = "erd_scroll_detection_container";
  function initDocument(targetDocument) {
    // Inject the scrollbar styling that prevents them from appearing sometimes in Chrome.
    // The injected container needs to have a class, so that it may be styled with CSS (pseudo elements).
    injectScrollStyle(targetDocument, styleId, detectionContainerClass);
  }
  initDocument(window.document);
  function buildCssTextString(rules) {
    var seperator = options.important ? " !important; " : "; ";
    return (rules.join(seperator) + seperator).trim();
  }
  function getScrollbarSizes() {
    var width = 500;
    var height = 500;
    var child = document.createElement("div");
    child.style.cssText = buildCssTextString(["position: absolute", "width: " + width * 2 + "px", "height: " + height * 2 + "px", "visibility: hidden", "margin: 0", "padding: 0"]);
    var container = document.createElement("div");
    container.style.cssText = buildCssTextString(["position: absolute", "width: " + width + "px", "height: " + height + "px", "overflow: scroll", "visibility: none", "top: " + -width * 3 + "px", "left: " + -height * 3 + "px", "visibility: hidden", "margin: 0", "padding: 0"]);
    container.appendChild(child);
    document.body.insertBefore(container, document.body.firstChild);
    var widthSize = width - container.clientWidth;
    var heightSize = height - container.clientHeight;
    document.body.removeChild(container);
    return {
      width: widthSize,
      height: heightSize
    };
  }
  function injectScrollStyle(targetDocument, styleId, containerClass) {
    function injectStyle(style, method) {
      method = method || function (element) {
        targetDocument.head.appendChild(element);
      };
      var styleElement = targetDocument.createElement("style");
      styleElement.innerHTML = style;
      styleElement.id = styleId;
      method(styleElement);
      return styleElement;
    }
    if (!targetDocument.getElementById(styleId)) {
      var containerAnimationClass = containerClass + "_animation";
      var containerAnimationActiveClass = containerClass + "_animation_active";
      var style = "/* Created by the element-resize-detector library. */\n";
      style += "." + containerClass + " > div::-webkit-scrollbar { " + buildCssTextString(["display: none"]) + " }\n\n";
      style += "." + containerAnimationActiveClass + " { " + buildCssTextString(["-webkit-animation-duration: 0.1s", "animation-duration: 0.1s", "-webkit-animation-name: " + containerAnimationClass, "animation-name: " + containerAnimationClass]) + " }\n";
      style += "@-webkit-keyframes " + containerAnimationClass + " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }\n";
      style += "@keyframes " + containerAnimationClass + " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }";
      injectStyle(style);
    }
  }
  function addAnimationClass(element) {
    element.className += " " + detectionContainerClass + "_animation_active";
  }
  function addEvent(el, name, cb) {
    if (el.addEventListener) {
      el.addEventListener(name, cb);
    } else if (el.attachEvent) {
      el.attachEvent("on" + name, cb);
    } else {
      return reporter.error("[scroll] Don't know how to add event listeners.");
    }
  }
  function removeEvent(el, name, cb) {
    if (el.removeEventListener) {
      el.removeEventListener(name, cb);
    } else if (el.detachEvent) {
      el.detachEvent("on" + name, cb);
    } else {
      return reporter.error("[scroll] Don't know how to remove event listeners.");
    }
  }
  function getExpandElement(element) {
    return getState(element).container.childNodes[0].childNodes[0].childNodes[0];
  }
  function getShrinkElement(element) {
    return getState(element).container.childNodes[0].childNodes[0].childNodes[1];
  }

  /**
   * Adds a resize event listener to the element.
   * @public
   * @param {element} element The element that should have the listener added.
   * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
   */
  function addListener(element, listener) {
    var listeners = getState(element).listeners;
    if (!listeners.push) {
      throw new Error("Cannot add listener to an element that is not detectable.");
    }
    getState(element).listeners.push(listener);
  }

  /**
   * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
   * @private
   * @param {object} options Optional options object.
   * @param {element} element The element to make detectable
   * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
   */
  function makeDetectable(options, element, callback) {
    if (!callback) {
      callback = element;
      element = options;
      options = null;
    }
    options = options || {};
    function debug() {
      if (options.debug) {
        var args = Array.prototype.slice.call(arguments);
        args.unshift(idHandler.get(element), "Scroll: ");
        if (reporter.log.apply) {
          reporter.log.apply(null, args);
        } else {
          for (var i = 0; i < args.length; i++) {
            reporter.log(args[i]);
          }
        }
      }
    }
    function isDetached(element) {
      function isInDocument(element) {
        var isInShadowRoot = element.getRootNode && element.getRootNode().contains(element);
        return element === element.ownerDocument.body || element.ownerDocument.body.contains(element) || isInShadowRoot;
      }
      if (!isInDocument(element)) {
        return true;
      }

      // FireFox returns null style in hidden iframes. See https://github.com/wnr/element-resize-detector/issues/68 and https://bugzilla.mozilla.org/show_bug.cgi?id=795520
      if (window.getComputedStyle(element) === null) {
        return true;
      }
      return false;
    }
    function isUnrendered(element) {
      // Check the absolute positioned container since the top level container is display: inline.
      var container = getState(element).container.childNodes[0];
      var style = window.getComputedStyle(container);
      return !style.width || style.width.indexOf("px") === -1; //Can only compute pixel value when rendered.
    }

    function getStyle() {
      // Some browsers only force layouts when actually reading the style properties of the style object, so make sure that they are all read here,
      // so that the user of the function can be sure that it will perform the layout here, instead of later (important for batching).
      var elementStyle = window.getComputedStyle(element);
      var style = {};
      style.position = elementStyle.position;
      style.width = element.offsetWidth;
      style.height = element.offsetHeight;
      style.top = elementStyle.top;
      style.right = elementStyle.right;
      style.bottom = elementStyle.bottom;
      style.left = elementStyle.left;
      style.widthCSS = elementStyle.width;
      style.heightCSS = elementStyle.height;
      return style;
    }
    function storeStartSize() {
      var style = getStyle();
      getState(element).startSize = {
        width: style.width,
        height: style.height
      };
      debug("Element start size", getState(element).startSize);
    }
    function initListeners() {
      getState(element).listeners = [];
    }
    function storeStyle() {
      debug("storeStyle invoked.");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      var style = getStyle();
      getState(element).style = style;
    }
    function storeCurrentSize(element, width, height) {
      getState(element).lastWidth = width;
      getState(element).lastHeight = height;
    }
    function getExpandChildElement(element) {
      return getExpandElement(element).childNodes[0];
    }
    function getWidthOffset() {
      return 2 * scrollbarSizes.width + 1;
    }
    function getHeightOffset() {
      return 2 * scrollbarSizes.height + 1;
    }
    function getExpandWidth(width) {
      return width + 10 + getWidthOffset();
    }
    function getExpandHeight(height) {
      return height + 10 + getHeightOffset();
    }
    function getShrinkWidth(width) {
      return width * 2 + getWidthOffset();
    }
    function getShrinkHeight(height) {
      return height * 2 + getHeightOffset();
    }
    function positionScrollbars(element, width, height) {
      var expand = getExpandElement(element);
      var shrink = getShrinkElement(element);
      var expandWidth = getExpandWidth(width);
      var expandHeight = getExpandHeight(height);
      var shrinkWidth = getShrinkWidth(width);
      var shrinkHeight = getShrinkHeight(height);
      expand.scrollLeft = expandWidth;
      expand.scrollTop = expandHeight;
      shrink.scrollLeft = shrinkWidth;
      shrink.scrollTop = shrinkHeight;
    }
    function injectContainerElement() {
      var container = getState(element).container;
      if (!container) {
        container = document.createElement("div");
        container.className = detectionContainerClass;
        container.style.cssText = buildCssTextString(["visibility: hidden", "display: inline", "width: 0px", "height: 0px", "z-index: -1", "overflow: hidden", "margin: 0", "padding: 0"]);
        getState(element).container = container;
        addAnimationClass(container);
        element.appendChild(container);
        var onAnimationStart = function onAnimationStart() {
          getState(element).onRendered && getState(element).onRendered();
        };
        addEvent(container, "animationstart", onAnimationStart);

        // Store the event handler here so that they may be removed when uninstall is called.
        // See uninstall function for an explanation why it is needed.
        getState(element).onAnimationStart = onAnimationStart;
      }
      return container;
    }
    function injectScrollElements() {
      function alterPositionStyles() {
        var style = getState(element).style;
        if (style.position === "static") {
          element.style.setProperty("position", "relative", options.important ? "important" : "");
          var removeRelativeStyles = function removeRelativeStyles(reporter, element, style, property) {
            function getNumericalValue(value) {
              return value.replace(/[^-\d\.]/g, "");
            }
            var value = style[property];
            if (value !== "auto" && getNumericalValue(value) !== "0") {
              reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
              element.style[property] = 0;
            }
          };

          //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
          //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
          removeRelativeStyles(reporter, element, style, "top");
          removeRelativeStyles(reporter, element, style, "right");
          removeRelativeStyles(reporter, element, style, "bottom");
          removeRelativeStyles(reporter, element, style, "left");
        }
      }
      function getLeftTopBottomRightCssText(left, top, bottom, right) {
        left = !left ? "0" : left + "px";
        top = !top ? "0" : top + "px";
        bottom = !bottom ? "0" : bottom + "px";
        right = !right ? "0" : right + "px";
        return ["left: " + left, "top: " + top, "right: " + right, "bottom: " + bottom];
      }
      debug("Injecting elements");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      alterPositionStyles();
      var rootContainer = getState(element).container;
      if (!rootContainer) {
        rootContainer = injectContainerElement();
      }

      // Due to this WebKit bug https://bugs.webkit.org/show_bug.cgi?id=80808 (currently fixed in Blink, but still present in WebKit browsers such as Safari),
      // we need to inject two containers, one that is width/height 100% and another that is left/top -1px so that the final container always is 1x1 pixels bigger than
      // the targeted element.
      // When the bug is resolved, "containerContainer" may be removed.

      // The outer container can occasionally be less wide than the targeted when inside inline elements element in WebKit (see https://bugs.webkit.org/show_bug.cgi?id=152980).
      // This should be no problem since the inner container either way makes sure the injected scroll elements are at least 1x1 px.

      var scrollbarWidth = scrollbarSizes.width;
      var scrollbarHeight = scrollbarSizes.height;
      var containerContainerStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: hidden", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%", "left: 0px", "top: 0px"]);
      var containerStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: hidden", "z-index: -1", "visibility: hidden"].concat(getLeftTopBottomRightCssText(-(1 + scrollbarWidth), -(1 + scrollbarHeight), -scrollbarHeight, -scrollbarWidth)));
      var expandStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: scroll", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%"]);
      var shrinkStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: scroll", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%"]);
      var expandChildStyle = buildCssTextString(["position: absolute", "left: 0", "top: 0"]);
      var shrinkChildStyle = buildCssTextString(["position: absolute", "width: 200%", "height: 200%"]);
      var containerContainer = document.createElement("div");
      var container = document.createElement("div");
      var expand = document.createElement("div");
      var expandChild = document.createElement("div");
      var shrink = document.createElement("div");
      var shrinkChild = document.createElement("div");

      // Some browsers choke on the resize system being rtl, so force it to ltr. https://github.com/wnr/element-resize-detector/issues/56
      // However, dir should not be set on the top level container as it alters the dimensions of the target element in some browsers.
      containerContainer.dir = "ltr";
      containerContainer.style.cssText = containerContainerStyle;
      containerContainer.className = detectionContainerClass;
      container.className = detectionContainerClass;
      container.style.cssText = containerStyle;
      expand.style.cssText = expandStyle;
      expandChild.style.cssText = expandChildStyle;
      shrink.style.cssText = shrinkStyle;
      shrinkChild.style.cssText = shrinkChildStyle;
      expand.appendChild(expandChild);
      shrink.appendChild(shrinkChild);
      container.appendChild(expand);
      container.appendChild(shrink);
      containerContainer.appendChild(container);
      rootContainer.appendChild(containerContainer);
      function onExpandScroll() {
        var state = getState(element);
        if (state && state.onExpand) {
          state.onExpand();
        } else {
          debug("Aborting expand scroll handler: element has been uninstalled");
        }
      }
      function onShrinkScroll() {
        var state = getState(element);
        if (state && state.onShrink) {
          state.onShrink();
        } else {
          debug("Aborting shrink scroll handler: element has been uninstalled");
        }
      }
      addEvent(expand, "scroll", onExpandScroll);
      addEvent(shrink, "scroll", onShrinkScroll);

      // Store the event handlers here so that they may be removed when uninstall is called.
      // See uninstall function for an explanation why it is needed.
      getState(element).onExpandScroll = onExpandScroll;
      getState(element).onShrinkScroll = onShrinkScroll;
    }
    function registerListenersAndPositionElements() {
      function updateChildSizes(element, width, height) {
        var expandChild = getExpandChildElement(element);
        var expandWidth = getExpandWidth(width);
        var expandHeight = getExpandHeight(height);
        expandChild.style.setProperty("width", expandWidth + "px", options.important ? "important" : "");
        expandChild.style.setProperty("height", expandHeight + "px", options.important ? "important" : "");
      }
      function updateDetectorElements(done) {
        var width = element.offsetWidth;
        var height = element.offsetHeight;

        // Check whether the size has actually changed since last time the algorithm ran. If not, some steps may be skipped.
        var sizeChanged = width !== getState(element).lastWidth || height !== getState(element).lastHeight;
        debug("Storing current size", width, height);

        // Store the size of the element sync here, so that multiple scroll events may be ignored in the event listeners.
        // Otherwise the if-check in handleScroll is useless.
        storeCurrentSize(element, width, height);

        // Since we delay the processing of the batch, there is a risk that uninstall has been called before the batch gets to execute.
        // Since there is no way to cancel the fn executions, we need to add an uninstall guard to all fns of the batch.

        batchProcessor.add(0, function performUpdateChildSizes() {
          if (!sizeChanged) {
            return;
          }
          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          if (!areElementsInjected()) {
            debug("Aborting because element container has not been initialized");
            return;
          }
          if (options.debug) {
            var w = element.offsetWidth;
            var h = element.offsetHeight;
            if (w !== width || h !== height) {
              reporter.warn(idHandler.get(element), "Scroll: Size changed before updating detector elements.");
            }
          }
          updateChildSizes(element, width, height);
        });
        batchProcessor.add(1, function updateScrollbars() {
          // This function needs to be invoked event though the size is unchanged. The element could have been resized very quickly and then
          // been restored to the original size, which will have changed the scrollbar positions.

          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          if (!areElementsInjected()) {
            debug("Aborting because element container has not been initialized");
            return;
          }
          positionScrollbars(element, width, height);
        });
        if (sizeChanged && done) {
          batchProcessor.add(2, function () {
            if (!getState(element)) {
              debug("Aborting because element has been uninstalled");
              return;
            }
            if (!areElementsInjected()) {
              debug("Aborting because element container has not been initialized");
              return;
            }
            done();
          });
        }
      }
      function areElementsInjected() {
        return !!getState(element).container;
      }
      function notifyListenersIfNeeded() {
        function isFirstNotify() {
          return getState(element).lastNotifiedWidth === undefined;
        }
        debug("notifyListenersIfNeeded invoked");
        var state = getState(element);

        // Don't notify if the current size is the start size, and this is the first notification.
        if (isFirstNotify() && state.lastWidth === state.startSize.width && state.lastHeight === state.startSize.height) {
          return debug("Not notifying: Size is the same as the start size, and there has been no notification yet.");
        }

        // Don't notify if the size already has been notified.
        if (state.lastWidth === state.lastNotifiedWidth && state.lastHeight === state.lastNotifiedHeight) {
          return debug("Not notifying: Size already notified");
        }
        debug("Current size not notified, notifying...");
        state.lastNotifiedWidth = state.lastWidth;
        state.lastNotifiedHeight = state.lastHeight;
        forEach(getState(element).listeners, function (listener) {
          listener(element);
        });
      }
      function handleRender() {
        debug("startanimation triggered.");
        if (isUnrendered(element)) {
          debug("Ignoring since element is still unrendered...");
          return;
        }
        debug("Element rendered.");
        var expand = getExpandElement(element);
        var shrink = getShrinkElement(element);
        if (expand.scrollLeft === 0 || expand.scrollTop === 0 || shrink.scrollLeft === 0 || shrink.scrollTop === 0) {
          debug("Scrollbars out of sync. Updating detector elements...");
          updateDetectorElements(notifyListenersIfNeeded);
        }
      }
      function handleScroll() {
        debug("Scroll detected.");
        if (isUnrendered(element)) {
          // Element is still unrendered. Skip this scroll event.
          debug("Scroll event fired while unrendered. Ignoring...");
          return;
        }
        updateDetectorElements(notifyListenersIfNeeded);
      }
      debug("registerListenersAndPositionElements invoked.");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      getState(element).onRendered = handleRender;
      getState(element).onExpand = handleScroll;
      getState(element).onShrink = handleScroll;
      var style = getState(element).style;
      updateChildSizes(element, style.width, style.height);
    }
    function finalizeDomMutation() {
      debug("finalizeDomMutation invoked.");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      var style = getState(element).style;
      storeCurrentSize(element, style.width, style.height);
      positionScrollbars(element, style.width, style.height);
    }
    function ready() {
      callback(element);
    }
    function install() {
      debug("Installing...");
      initListeners();
      storeStartSize();
      batchProcessor.add(0, storeStyle);
      batchProcessor.add(1, injectScrollElements);
      batchProcessor.add(2, registerListenersAndPositionElements);
      batchProcessor.add(3, finalizeDomMutation);
      batchProcessor.add(4, ready);
    }
    debug("Making detectable...");
    if (isDetached(element)) {
      debug("Element is detached");
      injectContainerElement();
      debug("Waiting until element is attached...");
      getState(element).onRendered = function () {
        debug("Element is now attached");
        install();
      };
    } else {
      install();
    }
  }
  function uninstall(element) {
    var state = getState(element);
    if (!state) {
      // Uninstall has been called on a non-erd element.
      return;
    }

    // Uninstall may have been called in the following scenarios:
    // (1) Right between the sync code and async batch (here state.busy = true, but nothing have been registered or injected).
    // (2) In the ready callback of the last level of the batch by another element (here, state.busy = true, but all the stuff has been injected).
    // (3) After the installation process (here, state.busy = false and all the stuff has been injected).
    // So to be on the safe side, let's check for each thing before removing.

    // We need to remove the event listeners, because otherwise the event might fire on an uninstall element which results in an error when trying to get the state of the element.
    state.onExpandScroll && removeEvent(getExpandElement(element), "scroll", state.onExpandScroll);
    state.onShrinkScroll && removeEvent(getShrinkElement(element), "scroll", state.onShrinkScroll);
    state.onAnimationStart && removeEvent(state.container, "animationstart", state.onAnimationStart);
    state.container && element.removeChild(state.container);
  }
  return {
    makeDetectable: makeDetectable,
    addListener: addListener,
    uninstall: uninstall,
    initDocument: initDocument
  };
};

/***/ }),

/***/ 6084:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

"use strict";


var forEach = (__webpack_require__(8485).forEach);
var elementUtilsMaker = __webpack_require__(5048);
var listenerHandlerMaker = __webpack_require__(8655);
var idGeneratorMaker = __webpack_require__(5509);
var idHandlerMaker = __webpack_require__(6186);
var reporterMaker = __webpack_require__(9009);
var browserDetector = __webpack_require__(2499);
var batchProcessorMaker = __webpack_require__(8845);
var stateHandler = __webpack_require__(544);

//Detection strategies.
var objectStrategyMaker = __webpack_require__(8862);
var scrollStrategyMaker = __webpack_require__(8197);
function isCollection(obj) {
  return Array.isArray(obj) || obj.length !== undefined;
}
function toArray(collection) {
  if (!Array.isArray(collection)) {
    var array = [];
    forEach(collection, function (obj) {
      array.push(obj);
    });
    return array;
  } else {
    return collection;
  }
}
function isElement(obj) {
  return obj && obj.nodeType === 1;
}

/**
 * @typedef idHandler
 * @type {object}
 * @property {function} get Gets the resize detector id of the element.
 * @property {function} set Generate and sets the resize detector id of the element.
 */

/**
 * @typedef Options
 * @type {object}
 * @property {boolean} callOnAdd    Determines if listeners should be called when they are getting added.
                                    Default is true. If true, the listener is guaranteed to be called when it has been added.
                                    If false, the listener will not be guarenteed to be called when it has been added (does not prevent it from being called).
 * @property {idHandler} idHandler  A custom id handler that is responsible for generating, setting and retrieving id's for elements.
                                    If not provided, a default id handler will be used.
 * @property {reporter} reporter    A custom reporter that handles reporting logs, warnings and errors.
                                    If not provided, a default id handler will be used.
                                    If set to false, then nothing will be reported.
 * @property {boolean} debug        If set to true, the the system will report debug messages as default for the listenTo method.
 */

/**
 * Creates an element resize detector instance.
 * @public
 * @param {Options?} options Optional global options object that will decide how this instance will work.
 */
module.exports = function (options) {
  options = options || {};

  //idHandler is currently not an option to the listenTo function, so it should not be added to globalOptions.
  var idHandler;
  if (options.idHandler) {
    // To maintain compatability with idHandler.get(element, readonly), make sure to wrap the given idHandler
    // so that readonly flag always is true when it's used here. This may be removed next major version bump.
    idHandler = {
      get: function get(element) {
        return options.idHandler.get(element, true);
      },
      set: options.idHandler.set
    };
  } else {
    var idGenerator = idGeneratorMaker();
    var defaultIdHandler = idHandlerMaker({
      idGenerator: idGenerator,
      stateHandler: stateHandler
    });
    idHandler = defaultIdHandler;
  }

  //reporter is currently not an option to the listenTo function, so it should not be added to globalOptions.
  var reporter = options.reporter;
  if (!reporter) {
    //If options.reporter is false, then the reporter should be quiet.
    var quiet = reporter === false;
    reporter = reporterMaker(quiet);
  }

  //batchProcessor is currently not an option to the listenTo function, so it should not be added to globalOptions.
  var batchProcessor = getOption(options, "batchProcessor", batchProcessorMaker({
    reporter: reporter
  }));

  //Options to be used as default for the listenTo function.
  var globalOptions = {};
  globalOptions.callOnAdd = !!getOption(options, "callOnAdd", true);
  globalOptions.debug = !!getOption(options, "debug", false);
  var eventListenerHandler = listenerHandlerMaker(idHandler);
  var elementUtils = elementUtilsMaker({
    stateHandler: stateHandler
  });

  //The detection strategy to be used.
  var detectionStrategy;
  var desiredStrategy = getOption(options, "strategy", "object");
  var importantCssRules = getOption(options, "important", false);
  var strategyOptions = {
    reporter: reporter,
    batchProcessor: batchProcessor,
    stateHandler: stateHandler,
    idHandler: idHandler,
    important: importantCssRules
  };
  if (desiredStrategy === "scroll") {
    if (browserDetector.isLegacyOpera()) {
      reporter.warn("Scroll strategy is not supported on legacy Opera. Changing to object strategy.");
      desiredStrategy = "object";
    } else if (browserDetector.isIE(9)) {
      reporter.warn("Scroll strategy is not supported on IE9. Changing to object strategy.");
      desiredStrategy = "object";
    }
  }
  if (desiredStrategy === "scroll") {
    detectionStrategy = scrollStrategyMaker(strategyOptions);
  } else if (desiredStrategy === "object") {
    detectionStrategy = objectStrategyMaker(strategyOptions);
  } else {
    throw new Error("Invalid strategy name: " + desiredStrategy);
  }

  //Calls can be made to listenTo with elements that are still being installed.
  //Also, same elements can occur in the elements list in the listenTo function.
  //With this map, the ready callbacks can be synchronized between the calls
  //so that the ready callback can always be called when an element is ready - even if
  //it wasn't installed from the function itself.
  var onReadyCallbacks = {};

  /**
   * Makes the given elements resize-detectable and starts listening to resize events on the elements. Calls the event callback for each event for each element.
   * @public
   * @param {Options?} options Optional options object. These options will override the global options. Some options may not be overriden, such as idHandler.
   * @param {element[]|element} elements The given array of elements to detect resize events of. Single element is also valid.
   * @param {function} listener The callback to be executed for each resize event for each element.
   */
  function listenTo(options, elements, listener) {
    function onResizeCallback(element) {
      var listeners = eventListenerHandler.get(element);
      forEach(listeners, function callListenerProxy(listener) {
        listener(element);
      });
    }
    function addListener(callOnAdd, element, listener) {
      eventListenerHandler.add(element, listener);
      if (callOnAdd) {
        listener(element);
      }
    }

    //Options object may be omitted.
    if (!listener) {
      listener = elements;
      elements = options;
      options = {};
    }
    if (!elements) {
      throw new Error("At least one element required.");
    }
    if (!listener) {
      throw new Error("Listener required.");
    }
    if (isElement(elements)) {
      // A single element has been passed in.
      elements = [elements];
    } else if (isCollection(elements)) {
      // Convert collection to array for plugins.
      // TODO: May want to check so that all the elements in the collection are valid elements.
      elements = toArray(elements);
    } else {
      return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
    }
    var elementsReady = 0;
    var callOnAdd = getOption(options, "callOnAdd", globalOptions.callOnAdd);
    var onReadyCallback = getOption(options, "onReady", function noop() {});
    var debug = getOption(options, "debug", globalOptions.debug);
    forEach(elements, function attachListenerToElement(element) {
      if (!stateHandler.getState(element)) {
        stateHandler.initState(element);
        idHandler.set(element);
      }
      var id = idHandler.get(element);
      debug && reporter.log("Attaching listener to element", id, element);
      if (!elementUtils.isDetectable(element)) {
        debug && reporter.log(id, "Not detectable.");
        if (elementUtils.isBusy(element)) {
          debug && reporter.log(id, "System busy making it detectable");

          //The element is being prepared to be detectable. Do not make it detectable.
          //Just add the listener, because the element will soon be detectable.
          addListener(callOnAdd, element, listener);
          onReadyCallbacks[id] = onReadyCallbacks[id] || [];
          onReadyCallbacks[id].push(function onReady() {
            elementsReady++;
            if (elementsReady === elements.length) {
              onReadyCallback();
            }
          });
          return;
        }
        debug && reporter.log(id, "Making detectable...");
        //The element is not prepared to be detectable, so do prepare it and add a listener to it.
        elementUtils.markBusy(element, true);
        return detectionStrategy.makeDetectable({
          debug: debug,
          important: importantCssRules
        }, element, function onElementDetectable(element) {
          debug && reporter.log(id, "onElementDetectable");
          if (stateHandler.getState(element)) {
            elementUtils.markAsDetectable(element);
            elementUtils.markBusy(element, false);
            detectionStrategy.addListener(element, onResizeCallback);
            addListener(callOnAdd, element, listener);

            // Since the element size might have changed since the call to "listenTo", we need to check for this change,
            // so that a resize event may be emitted.
            // Having the startSize object is optional (since it does not make sense in some cases such as unrendered elements), so check for its existance before.
            // Also, check the state existance before since the element may have been uninstalled in the installation process.
            var state = stateHandler.getState(element);
            if (state && state.startSize) {
              var width = element.offsetWidth;
              var height = element.offsetHeight;
              if (state.startSize.width !== width || state.startSize.height !== height) {
                onResizeCallback(element);
              }
            }
            if (onReadyCallbacks[id]) {
              forEach(onReadyCallbacks[id], function (callback) {
                callback();
              });
            }
          } else {
            // The element has been unisntalled before being detectable.
            debug && reporter.log(id, "Element uninstalled before being detectable.");
          }
          delete onReadyCallbacks[id];
          elementsReady++;
          if (elementsReady === elements.length) {
            onReadyCallback();
          }
        });
      }
      debug && reporter.log(id, "Already detecable, adding listener.");

      //The element has been prepared to be detectable and is ready to be listened to.
      addListener(callOnAdd, element, listener);
      elementsReady++;
    });
    if (elementsReady === elements.length) {
      onReadyCallback();
    }
  }
  function uninstall(elements) {
    if (!elements) {
      return reporter.error("At least one element is required.");
    }
    if (isElement(elements)) {
      // A single element has been passed in.
      elements = [elements];
    } else if (isCollection(elements)) {
      // Convert collection to array for plugins.
      // TODO: May want to check so that all the elements in the collection are valid elements.
      elements = toArray(elements);
    } else {
      return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
    }
    forEach(elements, function (element) {
      eventListenerHandler.removeAllListeners(element);
      detectionStrategy.uninstall(element);
      stateHandler.cleanState(element);
    });
  }
  function initDocument(targetDocument) {
    detectionStrategy.initDocument && detectionStrategy.initDocument(targetDocument);
  }
  return {
    listenTo: listenTo,
    removeListener: eventListenerHandler.removeListener,
    removeAllListeners: eventListenerHandler.removeAllListeners,
    uninstall: uninstall,
    initDocument: initDocument
  };
};
function getOption(options, name, defaultValue) {
  var value = options[name];
  if ((value === undefined || value === null) && defaultValue !== undefined) {
    return defaultValue;
  }
  return value;
}

/***/ }),

/***/ 5048:
/***/ (function(module) {

"use strict";


module.exports = function (options) {
  var getState = options.stateHandler.getState;

  /**
   * Tells if the element has been made detectable and ready to be listened for resize events.
   * @public
   * @param {element} The element to check.
   * @returns {boolean} True or false depending on if the element is detectable or not.
   */
  function isDetectable(element) {
    var state = getState(element);
    return state && !!state.isDetectable;
  }

  /**
   * Marks the element that it has been made detectable and ready to be listened for resize events.
   * @public
   * @param {element} The element to mark.
   */
  function markAsDetectable(element) {
    getState(element).isDetectable = true;
  }

  /**
   * Tells if the element is busy or not.
   * @public
   * @param {element} The element to check.
   * @returns {boolean} True or false depending on if the element is busy or not.
   */
  function isBusy(element) {
    return !!getState(element).busy;
  }

  /**
   * Marks the object is busy and should not be made detectable.
   * @public
   * @param {element} element The element to mark.
   * @param {boolean} busy If the element is busy or not.
   */
  function markBusy(element, busy) {
    getState(element).busy = !!busy;
  }
  return {
    isDetectable: isDetectable,
    markAsDetectable: markAsDetectable,
    isBusy: isBusy,
    markBusy: markBusy
  };
};

/***/ }),

/***/ 5509:
/***/ (function(module) {

"use strict";


module.exports = function () {
  var idCount = 1;

  /**
   * Generates a new unique id in the context.
   * @public
   * @returns {number} A unique id in the context.
   */
  function generate() {
    return idCount++;
  }
  return {
    generate: generate
  };
};

/***/ }),

/***/ 6186:
/***/ (function(module) {

"use strict";


module.exports = function (options) {
  var idGenerator = options.idGenerator;
  var getState = options.stateHandler.getState;

  /**
   * Gets the resize detector id of the element.
   * @public
   * @param {element} element The target element to get the id of.
   * @returns {string|number|null} The id of the element. Null if it has no id.
   */
  function getId(element) {
    var state = getState(element);
    if (state && state.id !== undefined) {
      return state.id;
    }
    return null;
  }

  /**
   * Sets the resize detector id of the element. Requires the element to have a resize detector state initialized.
   * @public
   * @param {element} element The target element to set the id of.
   * @returns {string|number|null} The id of the element.
   */
  function setId(element) {
    var state = getState(element);
    if (!state) {
      throw new Error("setId required the element to have a resize detection state.");
    }
    var id = idGenerator.generate();
    state.id = id;
    return id;
  }
  return {
    get: getId,
    set: setId
  };
};

/***/ }),

/***/ 8655:
/***/ (function(module) {

"use strict";


module.exports = function (idHandler) {
  var eventListeners = {};

  /**
   * Gets all listeners for the given element.
   * @public
   * @param {element} element The element to get all listeners for.
   * @returns All listeners for the given element.
   */
  function getListeners(element) {
    var id = idHandler.get(element);
    if (id === undefined) {
      return [];
    }
    return eventListeners[id] || [];
  }

  /**
   * Stores the given listener for the given element. Will not actually add the listener to the element.
   * @public
   * @param {element} element The element that should have the listener added.
   * @param {function} listener The callback that the element has added.
   */
  function addListener(element, listener) {
    var id = idHandler.get(element);
    if (!eventListeners[id]) {
      eventListeners[id] = [];
    }
    eventListeners[id].push(listener);
  }
  function removeListener(element, listener) {
    var listeners = getListeners(element);
    for (var i = 0, len = listeners.length; i < len; ++i) {
      if (listeners[i] === listener) {
        listeners.splice(i, 1);
        break;
      }
    }
  }
  function removeAllListeners(element) {
    var listeners = getListeners(element);
    if (!listeners) {
      return;
    }
    listeners.length = 0;
  }
  return {
    get: getListeners,
    add: addListener,
    removeListener: removeListener,
    removeAllListeners: removeAllListeners
  };
};

/***/ }),

/***/ 9009:
/***/ (function(module) {

"use strict";


/* global console: false */

/**
 * Reporter that handles the reporting of logs, warnings and errors.
 * @public
 * @param {boolean} quiet Tells if the reporter should be quiet or not.
 */
module.exports = function (quiet) {
  function noop() {
    //Does nothing.
  }
  var reporter = {
    log: noop,
    warn: noop,
    error: noop
  };
  if (!quiet && window.console) {
    var attachFunction = function attachFunction(reporter, name) {
      //The proxy is needed to be able to call the method with the console context,
      //since we cannot use bind.
      reporter[name] = function reporterProxy() {
        var f = console[name];
        if (f.apply) {
          //IE9 does not support console.log.apply :)
          f.apply(console, arguments);
        } else {
          for (var i = 0; i < arguments.length; i++) {
            f(arguments[i]);
          }
        }
      };
    };
    attachFunction(reporter, "log");
    attachFunction(reporter, "warn");
    attachFunction(reporter, "error");
  }
  return reporter;
};

/***/ }),

/***/ 544:
/***/ (function(module) {

"use strict";


var prop = "_erd";
function initState(element) {
  element[prop] = {};
  return getState(element);
}
function getState(element) {
  return element[prop];
}
function cleanState(element) {
  delete element[prop];
}
module.exports = {
  initState: initState,
  getState: getState,
  cleanState: cleanState
};

/***/ }),

/***/ 3533:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread2(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty2(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty2(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
/*! vue-grid-layout - 2.4.0 | (c) 2015, 2022  Gustavo Santos (JBay Solutions) <gustavo.santos@jbaysolutions.com> (http://www.jbaysolutions.com) | https://github.com/jbaysolutions/vue-grid-layout */
module.exports = /******/function (modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/
  var installedModules = {};
  /******/
  /******/ // The require function
  /******/
  function __nested_webpack_require_1834__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/if (installedModules[moduleId]) {
      /******/return installedModules[moduleId].exports;
      /******/
    }
    /******/ // Create a new module (and put it into the cache)
    /******/
    var module = installedModules[moduleId] = {
      /******/i: moduleId,
      /******/l: false,
      /******/exports: {}
      /******/
    };
    /******/
    /******/ // Execute the module function
    /******/
    modules[moduleId].call(module.exports, module, module.exports, __nested_webpack_require_1834__);
    /******/
    /******/ // Flag the module as loaded
    /******/
    module.l = true;
    /******/
    /******/ // Return the exports of the module
    /******/
    return module.exports;
    /******/
  }
  /******/
  /******/
  /******/ // expose the modules object (__webpack_modules__)
  /******/
  __nested_webpack_require_1834__.m = modules;
  /******/
  /******/ // expose the module cache
  /******/
  __nested_webpack_require_1834__.c = installedModules;
  /******/
  /******/ // define getter function for harmony exports
  /******/
  __nested_webpack_require_1834__.d = function (exports, name, getter) {
    /******/if (!__nested_webpack_require_1834__.o(exports, name)) {
      /******/Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter
      });
      /******/
    }
    /******/
  };
  /******/
  /******/ // define __esModule on exports
  /******/
  __nested_webpack_require_1834__.r = function (exports) {
    /******/if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
      /******/Object.defineProperty(exports, Symbol.toStringTag, {
        value: 'Module'
      });
      /******/
    }
    /******/
    Object.defineProperty(exports, '__esModule', {
      value: true
    });
    /******/
  };
  /******/
  /******/ // create a fake namespace object
  /******/ // mode & 1: value is a module id, require it
  /******/ // mode & 2: merge all properties of value into the ns
  /******/ // mode & 4: return value when already ns object
  /******/ // mode & 8|1: behave like require
  /******/
  __nested_webpack_require_1834__.t = function (value, mode) {
    /******/if (mode & 1) value = __nested_webpack_require_1834__(value);
    /******/
    if (mode & 8) return value;
    /******/
    if (mode & 4 && typeof value === 'object' && value && value.__esModule) return value;
    /******/
    var ns = Object.create(null);
    /******/
    __nested_webpack_require_1834__.r(ns);
    /******/
    Object.defineProperty(ns, 'default', {
      enumerable: true,
      value: value
    });
    /******/
    if (mode & 2 && typeof value != 'string') for (var key in value) __nested_webpack_require_1834__.d(ns, key, function (key) {
      return value[key];
    }.bind(null, key));
    /******/
    return ns;
    /******/
  };
  /******/
  /******/ // getDefaultExport function for compatibility with non-harmony modules
  /******/
  __nested_webpack_require_1834__.n = function (module) {
    /******/var getter = module && module.__esModule ? /******/function getDefault() {
      return module['default'];
    } : /******/function getModuleExports() {
      return module;
    };
    /******/
    __nested_webpack_require_1834__.d(getter, 'a', getter);
    /******/
    return getter;
    /******/
  };
  /******/
  /******/ // Object.prototype.hasOwnProperty.call
  /******/
  __nested_webpack_require_1834__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  };
  /******/
  /******/ // __webpack_public_path__
  /******/
  __nested_webpack_require_1834__.p = "";
  /******/
  /******/
  /******/ // Load entry module and return exports
  /******/
  return __nested_webpack_require_1834__(__nested_webpack_require_1834__.s = "fb15");
  /******/
}
/************************************************************************/
/******/({
  /***/"01f9": /***/function f9(module, exports, __nested_webpack_require_5659__) {
    "use strict";

    var LIBRARY = __nested_webpack_require_5659__("2d00");
    var $export = __nested_webpack_require_5659__("5ca1");
    var redefine = __nested_webpack_require_5659__("2aba");
    var hide = __nested_webpack_require_5659__("32e9");
    var Iterators = __nested_webpack_require_5659__("84f2");
    var $iterCreate = __nested_webpack_require_5659__("41a0");
    var setToStringTag = __nested_webpack_require_5659__("7f20");
    var getPrototypeOf = __nested_webpack_require_5659__("38fd");
    var ITERATOR = __nested_webpack_require_5659__("2b4c")('iterator');
    var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
    var FF_ITERATOR = '@@iterator';
    var KEYS = 'keys';
    var VALUES = 'values';
    var returnThis = function returnThis() {
      return this;
    };
    module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
      $iterCreate(Constructor, NAME, next);
      var getMethod = function getMethod(kind) {
        if (!BUGGY && kind in proto) return proto[kind];
        switch (kind) {
          case KEYS:
            return function keys() {
              return new Constructor(this, kind);
            };
          case VALUES:
            return function values() {
              return new Constructor(this, kind);
            };
        }
        return function entries() {
          return new Constructor(this, kind);
        };
      };
      var TAG = NAME + ' Iterator';
      var DEF_VALUES = DEFAULT == VALUES;
      var VALUES_BUG = false;
      var proto = Base.prototype;
      var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
      var $default = $native || getMethod(DEFAULT);
      var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
      var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
      var methods, key, IteratorPrototype;
      // Fix native
      if ($anyNative) {
        IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
        if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
          // Set @@toStringTag to native iterators
          setToStringTag(IteratorPrototype, TAG, true);
          // fix for some old engines
          if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
        }
      }
      // fix Array#{values, @@iterator}.name in V8 / FF
      if (DEF_VALUES && $native && $native.name !== VALUES) {
        VALUES_BUG = true;
        $default = function values() {
          return $native.call(this);
        };
      }
      // Define iterator
      if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
        hide(proto, ITERATOR, $default);
      }
      // Plug for library
      Iterators[NAME] = $default;
      Iterators[TAG] = returnThis;
      if (DEFAULT) {
        methods = {
          values: DEF_VALUES ? $default : getMethod(VALUES),
          keys: IS_SET ? $default : getMethod(KEYS),
          entries: $entries
        };
        if (FORCED) for (key in methods) {
          if (!(key in proto)) redefine(proto, key, methods[key]);
        } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
      }
      return methods;
    };

    /***/
  },

  /***/"02f4": /***/function f4(module, exports, __nested_webpack_require_8985__) {
    var toInteger = __nested_webpack_require_8985__("4588");
    var defined = __nested_webpack_require_8985__("be13");
    // true  -> String#at
    // false -> String#codePointAt
    module.exports = function (TO_STRING) {
      return function (that, pos) {
        var s = String(defined(that));
        var i = toInteger(pos);
        var l = s.length;
        var a, b;
        if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
        a = s.charCodeAt(i);
        return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff ? TO_STRING ? s.charAt(i) : a : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
      };
    };

    /***/
  },

  /***/"0390": /***/function _(module, exports, __nested_webpack_require_9750__) {
    "use strict";

    var at = __nested_webpack_require_9750__("02f4")(true);

    // `AdvanceStringIndex` abstract operation
    // https://tc39.github.io/ecma262/#sec-advancestringindex
    module.exports = function (S, index, unicode) {
      return index + (unicode ? at(S, index).length : 1);
    };

    /***/
  },

  /***/"0bfb": /***/function bfb(module, exports, __nested_webpack_require_10134__) {
    "use strict";

    // 21.2.5.3 get RegExp.prototype.flags
    var anObject = __nested_webpack_require_10134__("cb7c");
    module.exports = function () {
      var that = anObject(this);
      var result = '';
      if (that.global) result += 'g';
      if (that.ignoreCase) result += 'i';
      if (that.multiline) result += 'm';
      if (that.unicode) result += 'u';
      if (that.sticky) result += 'y';
      return result;
    };

    /***/
  },

  /***/"0d58": /***/function d58(module, exports, __nested_webpack_require_10651__) {
    // 19.1.2.14 / 15.2.3.14 Object.keys(O)
    var $keys = __nested_webpack_require_10651__("ce10");
    var enumBugKeys = __nested_webpack_require_10651__("e11e");
    module.exports = Object.keys || function keys(O) {
      return $keys(O, enumBugKeys);
    };

    /***/
  },

  /***/"1156": /***/function _(module, exports, __nested_webpack_require_10977__) {
    // style-loader: Adds some css to the DOM by adding a <style> tag

    // load the styles
    var content = __nested_webpack_require_10977__("ad20");
    if (typeof content === 'string') content = [[module.i, content, '']];
    if (content.locals) module.exports = content.locals;
    // add the styles to the DOM
    var add = __nested_webpack_require_10977__("499e").default;
    var update = add("c1ec597e", content, true, {
      "sourceMap": false,
      "shadowMode": false
    });

    /***/
  },

  /***/"11e9": /***/function e9(module, exports, __nested_webpack_require_11532__) {
    var pIE = __nested_webpack_require_11532__("52a7");
    var createDesc = __nested_webpack_require_11532__("4630");
    var toIObject = __nested_webpack_require_11532__("6821");
    var toPrimitive = __nested_webpack_require_11532__("6a99");
    var has = __nested_webpack_require_11532__("69a8");
    var IE8_DOM_DEFINE = __nested_webpack_require_11532__("c69a");
    var gOPD = Object.getOwnPropertyDescriptor;
    exports.f = __nested_webpack_require_11532__("9e1e") ? gOPD : function getOwnPropertyDescriptor(O, P) {
      O = toIObject(O);
      P = toPrimitive(P, true);
      if (IE8_DOM_DEFINE) try {
        return gOPD(O, P);
      } catch (e) {/* empty */}
      if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
    };

    /***/
  },

  /***/"1495": /***/function _(module, exports, __nested_webpack_require_12272__) {
    var dP = __nested_webpack_require_12272__("86cc");
    var anObject = __nested_webpack_require_12272__("cb7c");
    var getKeys = __nested_webpack_require_12272__("0d58");
    module.exports = __nested_webpack_require_12272__("9e1e") ? Object.defineProperties : function defineProperties(O, Properties) {
      anObject(O);
      var keys = getKeys(Properties);
      var length = keys.length;
      var i = 0;
      var P;
      while (length > i) dP.f(O, P = keys[i++], Properties[P]);
      return O;
    };

    /***/
  },

  /***/"18d2": /***/function d2(module, exports, __nested_webpack_require_12824__) {
    "use strict";

    /**
     * Resize detection strategy that injects objects to elements in order to detect resize events.
     * Heavily inspired by: http://www.backalleycoder.com/2013/03/18/cross-browser-event-based-element-resize-detection/
     */
    var browserDetector = __nested_webpack_require_12824__("18e9");
    module.exports = function (options) {
      options = options || {};
      var reporter = options.reporter;
      var batchProcessor = options.batchProcessor;
      var getState = options.stateHandler.getState;
      if (!reporter) {
        throw new Error("Missing required dependency: reporter.");
      }

      /**
       * Adds a resize event listener to the element.
       * @public
       * @param {element} element The element that should have the listener added.
       * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
       */
      function addListener(element, listener) {
        function listenerProxy() {
          listener(element);
        }
        if (browserDetector.isIE(8)) {
          //IE 8 does not support object, but supports the resize event directly on elements.
          getState(element).object = {
            proxy: listenerProxy
          };
          element.attachEvent("onresize", listenerProxy);
        } else {
          var object = getObject(element);
          if (!object) {
            throw new Error("Element is not detectable by this strategy.");
          }
          object.contentDocument.defaultView.addEventListener("resize", listenerProxy);
        }
      }
      function buildCssTextString(rules) {
        var seperator = options.important ? " !important; " : "; ";
        return (rules.join(seperator) + seperator).trim();
      }

      /**
       * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
       * @private
       * @param {object} options Optional options object.
       * @param {element} element The element to make detectable
       * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
       */
      function makeDetectable(options, element, callback) {
        if (!callback) {
          callback = element;
          element = options;
          options = null;
        }
        options = options || {};
        var debug = options.debug;
        function injectObject(element, callback) {
          var OBJECT_STYLE = buildCssTextString(["display: block", "position: absolute", "top: 0", "left: 0", "width: 100%", "height: 100%", "border: none", "padding: 0", "margin: 0", "opacity: 0", "z-index: -1000", "pointer-events: none"]);

          //The target element needs to be positioned (everything except static) so the absolute positioned object will be positioned relative to the target element.

          // Position altering may be performed directly or on object load, depending on if style resolution is possible directly or not.
          var positionCheckPerformed = false;

          // The element may not yet be attached to the DOM, and therefore the style object may be empty in some browsers.
          // Since the style object is a reference, it will be updated as soon as the element is attached to the DOM.
          var style = window.getComputedStyle(element);
          var width = element.offsetWidth;
          var height = element.offsetHeight;
          getState(element).startSize = {
            width: width,
            height: height
          };
          function mutateDom() {
            function alterPositionStyles() {
              if (style.position === "static") {
                element.style.setProperty("position", "relative", options.important ? "important" : "");
                var removeRelativeStyles = function removeRelativeStyles(reporter, element, style, property) {
                  function getNumericalValue(value) {
                    return value.replace(/[^-\d\.]/g, "");
                  }
                  var value = style[property];
                  if (value !== "auto" && getNumericalValue(value) !== "0") {
                    reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
                    element.style.setProperty(property, "0", options.important ? "important" : "");
                  }
                };

                //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
                //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
                removeRelativeStyles(reporter, element, style, "top");
                removeRelativeStyles(reporter, element, style, "right");
                removeRelativeStyles(reporter, element, style, "bottom");
                removeRelativeStyles(reporter, element, style, "left");
              }
            }
            function onObjectLoad() {
              // The object has been loaded, which means that the element now is guaranteed to be attached to the DOM.
              if (!positionCheckPerformed) {
                alterPositionStyles();
              }

              /*jshint validthis: true */

              function getDocument(element, callback) {
                //Opera 12 seem to call the object.onload before the actual document has been created.
                //So if it is not present, poll it with an timeout until it is present.
                //TODO: Could maybe be handled better with object.onreadystatechange or similar.
                if (!element.contentDocument) {
                  var state = getState(element);
                  if (state.checkForObjectDocumentTimeoutId) {
                    window.clearTimeout(state.checkForObjectDocumentTimeoutId);
                  }
                  state.checkForObjectDocumentTimeoutId = setTimeout(function checkForObjectDocument() {
                    state.checkForObjectDocumentTimeoutId = 0;
                    getDocument(element, callback);
                  }, 100);
                  return;
                }
                callback(element.contentDocument);
              }

              //Mutating the object element here seems to fire another load event.
              //Mutating the inner document of the object element is fine though.
              var objectElement = this;

              //Create the style element to be added to the object.
              getDocument(objectElement, function onObjectDocumentReady(objectDocument) {
                //Notify that the element is ready to be listened to.
                callback(element);
              });
            }

            // The element may be detached from the DOM, and some browsers does not support style resolving of detached elements.
            // The alterPositionStyles needs to be delayed until we know the element has been attached to the DOM (which we are sure of when the onObjectLoad has been fired), if style resolution is not possible.
            if (style.position !== "") {
              alterPositionStyles(style);
              positionCheckPerformed = true;
            }

            //Add an object element as a child to the target element that will be listened to for resize events.
            var object = document.createElement("object");
            object.style.cssText = OBJECT_STYLE;
            object.tabIndex = -1;
            object.type = "text/html";
            object.setAttribute("aria-hidden", "true");
            object.onload = onObjectLoad;

            //Safari: This must occur before adding the object to the DOM.
            //IE: Does not like that this happens before, even if it is also added after.
            if (!browserDetector.isIE()) {
              object.data = "about:blank";
            }
            if (!getState(element)) {
              // The element has been uninstalled before the actual loading happened.
              return;
            }
            element.appendChild(object);
            getState(element).object = object;

            //IE: This must occur after adding the object to the DOM.
            if (browserDetector.isIE()) {
              object.data = "about:blank";
            }
          }
          if (batchProcessor) {
            batchProcessor.add(mutateDom);
          } else {
            mutateDom();
          }
        }
        if (browserDetector.isIE(8)) {
          //IE 8 does not support objects properly. Luckily they do support the resize event.
          //So do not inject the object and notify that the element is already ready to be listened to.
          //The event handler for the resize event is attached in the utils.addListener instead.
          callback(element);
        } else {
          injectObject(element, callback);
        }
      }

      /**
       * Returns the child object of the target element.
       * @private
       * @param {element} element The target element.
       * @returns The object element of the target.
       */
      function getObject(element) {
        return getState(element).object;
      }
      function uninstall(element) {
        if (!getState(element)) {
          return;
        }
        var object = getObject(element);
        if (!object) {
          return;
        }
        if (browserDetector.isIE(8)) {
          element.detachEvent("onresize", object.proxy);
        } else {
          element.removeChild(object);
        }
        if (getState(element).checkForObjectDocumentTimeoutId) {
          window.clearTimeout(getState(element).checkForObjectDocumentTimeoutId);
        }
        delete getState(element).object;
      }
      return {
        makeDetectable: makeDetectable,
        addListener: addListener,
        uninstall: uninstall
      };
    };

    /***/
  },

  /***/"18e9": /***/function e9(module, exports, __webpack_require__) {
    "use strict";

    var detector = module.exports = {};
    detector.isIE = function (version) {
      function isAnyIeVersion() {
        var agent = navigator.userAgent.toLowerCase();
        return agent.indexOf("msie") !== -1 || agent.indexOf("trident") !== -1 || agent.indexOf(" edge/") !== -1;
      }
      if (!isAnyIeVersion()) {
        return false;
      }
      if (!version) {
        return true;
      }

      //Shamelessly stolen from https://gist.github.com/padolsey/527683
      var ieVersion = function () {
        var undef,
          v = 3,
          div = document.createElement("div"),
          all = div.getElementsByTagName("i");
        do {
          div.innerHTML = "<!--[if gt IE " + ++v + "]><i></i><![endif]-->";
        } while (all[0]);
        return v > 4 ? v : undef;
      }();
      return version === ieVersion;
    };
    detector.isLegacyOpera = function () {
      return !!window.opera;
    };

    /***/
  },

  /***/"1ca7": /***/function ca7(module, __nested_webpack_exports__, __nested_webpack_require_24235__) {
    "use strict";

    /* harmony export (binding) */
    __nested_webpack_require_24235__.d(__nested_webpack_exports__, "b", function () {
      return getDocumentDir;
    });
    /* unused harmony export setDocumentDir */
    /* harmony export (binding) */
    __nested_webpack_require_24235__.d(__nested_webpack_exports__, "a", function () {
      return addWindowEventListener;
    });
    /* harmony export (binding) */
    __nested_webpack_require_24235__.d(__nested_webpack_exports__, "c", function () {
      return removeWindowEventListener;
    });
    var currentDir
    /*: "ltr" | "rtl" | "auto"*/ = "auto"; // let currentDir = "auto";

    function hasDocument() {
      return typeof document !== "undefined";
    }
    function hasWindow() {
      return typeof window !== "undefined";
    }
    function getDocumentDir() {
      if (!hasDocument()) {
        return currentDir;
      }
      var direction = typeof document.dir !== "undefined" ? document.dir : document.getElementsByTagName("html")[0].getAttribute("dir");
      return direction;
    }
    function setDocumentDir(dir
    /*: "ltr" | "rtl" | "auto"*/) {
      // export function setDocumentDir(dir){
      if (!hasDocument) {
        currentDir = dir;
        return;
      }
      var html = document.getElementsByTagName("html")[0];
      html.setAttribute("dir", dir);
    }
    function addWindowEventListener(event
    /*:string*/, callback
    /*: () => mixed*/) {
      if (!hasWindow) {
        callback();
        return;
      }
      window.addEventListener(event, callback);
    }
    function removeWindowEventListener(event
    /*:string*/, callback
    /*: () => mixed*/) {
      if (!hasWindow) {
        return;
      }
      window.removeEventListener(event, callback);
    }

    /***/
  },

  /***/"214f": /***/function f(module, exports, __nested_webpack_require_26041__) {
    "use strict";

    __nested_webpack_require_26041__("b0c5");
    var redefine = __nested_webpack_require_26041__("2aba");
    var hide = __nested_webpack_require_26041__("32e9");
    var fails = __nested_webpack_require_26041__("79e5");
    var defined = __nested_webpack_require_26041__("be13");
    var wks = __nested_webpack_require_26041__("2b4c");
    var regexpExec = __nested_webpack_require_26041__("520a");
    var SPECIES = wks('species');
    var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
      // #replace needs built-in support for named groups.
      // #match works fine because it just return the exec results, even if it has
      // a "grops" property.
      var re = /./;
      re.exec = function () {
        var result = [];
        result.groups = {
          a: '7'
        };
        return result;
      };
      return ''.replace(re, '$<a>') !== '7';
    });
    var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = function () {
      // Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
      var re = /(?:)/;
      var originalExec = re.exec;
      re.exec = function () {
        return originalExec.apply(this, arguments);
      };
      var result = 'ab'.split(re);
      return result.length === 2 && result[0] === 'a' && result[1] === 'b';
    }();
    module.exports = function (KEY, length, exec) {
      var SYMBOL = wks(KEY);
      var DELEGATES_TO_SYMBOL = !fails(function () {
        // String methods call symbol-named RegEp methods
        var O = {};
        O[SYMBOL] = function () {
          return 7;
        };
        return ''[KEY](O) != 7;
      });
      var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL ? !fails(function () {
        // Symbol-named RegExp methods call .exec
        var execCalled = false;
        var re = /a/;
        re.exec = function () {
          execCalled = true;
          return null;
        };
        if (KEY === 'split') {
          // RegExp[@@split] doesn't call the regex's exec method, but first creates
          // a new one. We need to return the patched regex when creating the new one.
          re.constructor = {};
          re.constructor[SPECIES] = function () {
            return re;
          };
        }
        re[SYMBOL]('');
        return !execCalled;
      }) : undefined;
      if (!DELEGATES_TO_SYMBOL || !DELEGATES_TO_EXEC || KEY === 'replace' && !REPLACE_SUPPORTS_NAMED_GROUPS || KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC) {
        var nativeRegExpMethod = /./[SYMBOL];
        var fns = exec(defined, SYMBOL, ''[KEY], function maybeCallNative(nativeMethod, regexp, str, arg2, forceStringMethod) {
          if (regexp.exec === regexpExec) {
            if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
              // The native String method already delegates to @@method (this
              // polyfilled function), leasing to infinite recursion.
              // We avoid it by directly calling the native @@method method.
              return {
                done: true,
                value: nativeRegExpMethod.call(regexp, str, arg2)
              };
            }
            return {
              done: true,
              value: nativeMethod.call(str, regexp, arg2)
            };
          }
          return {
            done: false
          };
        });
        var strfn = fns[0];
        var rxfn = fns[1];
        redefine(String.prototype, KEY, strfn);
        hide(RegExp.prototype, SYMBOL, length == 2
        // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
        // 21.2.5.11 RegExp.prototype[@@split](string, limit)
        ? function (string, arg) {
          return rxfn.call(string, this, arg);
        }
        // 21.2.5.6 RegExp.prototype[@@match](string)
        // 21.2.5.9 RegExp.prototype[@@search](string)
        : function (string) {
          return rxfn.call(string, this);
        });
      }
    };

    /***/
  },

  /***/"230e": /***/function e(module, exports, __nested_webpack_require_29951__) {
    var isObject = __nested_webpack_require_29951__("d3f4");
    var document = __nested_webpack_require_29951__("7726").document;
    // typeof document.createElement is 'object' in old IE
    var is = isObject(document) && isObject(document.createElement);
    module.exports = function (it) {
      return is ? document.createElement(it) : {};
    };

    /***/
  },

  /***/"2350": /***/function _(module, exports) {
    /*
    	MIT License http://www.opensource.org/licenses/mit-license.php
    	Author Tobias Koppers @sokra
    */
    // css base code, injected by the css-loader
    module.exports = function (useSourceMap) {
      var list = [];

      // return the list of modules as css string
      list.toString = function toString() {
        return this.map(function (item) {
          var content = cssWithMappingToString(item, useSourceMap);
          if (item[2]) {
            return "@media " + item[2] + "{" + content + "}";
          } else {
            return content;
          }
        }).join("");
      };

      // import a list of modules into the list
      list.i = function (modules, mediaQuery) {
        if (typeof modules === "string") modules = [[null, modules, ""]];
        var alreadyImportedModules = {};
        for (var i = 0; i < this.length; i++) {
          var id = this[i][0];
          if (typeof id === "number") alreadyImportedModules[id] = true;
        }
        for (i = 0; i < modules.length; i++) {
          var item = modules[i];
          // skip already imported module
          // this implementation is not 100% perfect for weird media query combinations
          //  when a module is imported multiple times with different media queries.
          //  I hope this will never occur (Hey this way we have smaller bundles)
          if (typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
            if (mediaQuery && !item[2]) {
              item[2] = mediaQuery;
            } else if (mediaQuery) {
              item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
            }
            list.push(item);
          }
        }
      };
      return list;
    };
    function cssWithMappingToString(item, useSourceMap) {
      var content = item[1] || '';
      var cssMapping = item[3];
      if (!cssMapping) {
        return content;
      }
      if (useSourceMap && typeof btoa === 'function') {
        var sourceMapping = toComment(cssMapping);
        var sourceURLs = cssMapping.sources.map(function (source) {
          return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
        });
        return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
      }
      return [content].join('\n');
    }

    // Adapted from convert-source-map (MIT)
    function toComment(sourceMap) {
      // eslint-disable-next-line no-undef
      var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
      var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
      return '/*# ' + data + ' */';
    }

    /***/
  },

  /***/"23c6": /***/function c6(module, exports, __nested_webpack_require_33079__) {
    // getting tag from 19.1.3.6 Object.prototype.toString()
    var cof = __nested_webpack_require_33079__("2d95");
    var TAG = __nested_webpack_require_33079__("2b4c")('toStringTag');
    // ES3 wrong here
    var ARG = cof(function () {
      return arguments;
    }()) == 'Arguments';

    // fallback for IE11 Script Access Denied error
    var tryGet = function tryGet(it, key) {
      try {
        return it[key];
      } catch (e) {/* empty */}
    };
    module.exports = function (it) {
      var O, T, B;
      return it === undefined ? 'Undefined' : it === null ? 'Null'
      // @@toStringTag case
      : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
      // builtinTag case
      : ARG ? cof(O)
      // ES3 arguments fallback
      : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
    };

    /***/
  },

  /***/"2621": /***/function _(module, exports) {
    exports.f = Object.getOwnPropertySymbols;

    /***/
  },

  /***/"2877": /***/function _(module, __nested_webpack_exports__, __nested_webpack_require_34115__) {
    "use strict";

    /* harmony export (binding) */
    __nested_webpack_require_34115__.d(__nested_webpack_exports__, "a", function () {
      return normalizeComponent;
    });
    /* globals __VUE_SSR_CONTEXT__ */

    // IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
    // This module is a runtime utility for cleaner component module output and will
    // be included in the final webpack user bundle.

    function normalizeComponent(scriptExports, render, staticRenderFns, functionalTemplate, injectStyles, scopeId, moduleIdentifier, /* server only */
    shadowMode /* vue-cli only */) {
      // Vue.extend constructor export interop
      var options = typeof scriptExports === 'function' ? scriptExports.options : scriptExports;

      // render functions
      if (render) {
        options.render = render;
        options.staticRenderFns = staticRenderFns;
        options._compiled = true;
      }

      // functional template
      if (functionalTemplate) {
        options.functional = true;
      }

      // scopedId
      if (scopeId) {
        options._scopeId = 'data-v-' + scopeId;
      }
      var hook;
      if (moduleIdentifier) {
        // server build
        hook = function hook(context) {
          // 2.3 injection
          context = context ||
          // cached call
          this.$vnode && this.$vnode.ssrContext ||
          // stateful
          this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
          // 2.2 with runInNewContext: true
          if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
            context = __VUE_SSR_CONTEXT__;
          }
          // inject component styles
          if (injectStyles) {
            injectStyles.call(this, context);
          }
          // register component module identifier for async chunk inferrence
          if (context && context._registeredComponents) {
            context._registeredComponents.add(moduleIdentifier);
          }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
      } else if (injectStyles) {
        hook = shadowMode ? function () {
          injectStyles.call(this, (options.functional ? this.parent : this).$root.$options.shadowRoot);
        } : injectStyles;
      }
      if (hook) {
        if (options.functional) {
          // for template-only hot-reload because in that case the render fn doesn't
          // go through the normalizer
          options._injectStyles = hook;
          // register for functional component in vue file
          var originalRender = options.render;
          options.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = options.beforeCreate;
          options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
      return {
        exports: scriptExports,
        options: options
      };
    }

    /***/
  },

  /***/"2aba": /***/function aba(module, exports, __nested_webpack_require_37356__) {
    var global = __nested_webpack_require_37356__("7726");
    var hide = __nested_webpack_require_37356__("32e9");
    var has = __nested_webpack_require_37356__("69a8");
    var SRC = __nested_webpack_require_37356__("ca5a")('src');
    var $toString = __nested_webpack_require_37356__("fa5b");
    var TO_STRING = 'toString';
    var TPL = ('' + $toString).split(TO_STRING);
    __nested_webpack_require_37356__("8378").inspectSource = function (it) {
      return $toString.call(it);
    };
    (module.exports = function (O, key, val, safe) {
      var isFunction = typeof val == 'function';
      if (isFunction) has(val, 'name') || hide(val, 'name', key);
      if (O[key] === val) return;
      if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
      if (O === global) {
        O[key] = val;
      } else if (!safe) {
        delete O[key];
        hide(O, key, val);
      } else if (O[key]) {
        O[key] = val;
      } else {
        hide(O, key, val);
      }
      // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
    })(Function.prototype, TO_STRING, function toString() {
      return typeof this == 'function' && this[SRC] || $toString.call(this);
    });

    /***/
  },

  /***/"2aeb": /***/function aeb(module, exports, __nested_webpack_require_38652__) {
    // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
    var anObject = __nested_webpack_require_38652__("cb7c");
    var dPs = __nested_webpack_require_38652__("1495");
    var enumBugKeys = __nested_webpack_require_38652__("e11e");
    var IE_PROTO = __nested_webpack_require_38652__("613b")('IE_PROTO');
    var Empty = function Empty() {/* empty */};
    var PROTOTYPE = 'prototype';

    // Create object with fake `null` prototype: use iframe Object with cleared prototype
    var _createDict = function createDict() {
      // Thrash, waste and sodomy: IE GC bug
      var iframe = __nested_webpack_require_38652__("230e")('iframe');
      var i = enumBugKeys.length;
      var lt = '<';
      var gt = '>';
      var iframeDocument;
      iframe.style.display = 'none';
      __nested_webpack_require_38652__("fab2").appendChild(iframe);
      iframe.src = 'javascript:'; // eslint-disable-line no-script-url
      // createDict = iframe.contentWindow.Object;
      // html.removeChild(iframe);
      iframeDocument = iframe.contentWindow.document;
      iframeDocument.open();
      iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
      iframeDocument.close();
      _createDict = iframeDocument.F;
      while (i--) delete _createDict[PROTOTYPE][enumBugKeys[i]];
      return _createDict();
    };
    module.exports = Object.create || function create(O, Properties) {
      var result;
      if (O !== null) {
        Empty[PROTOTYPE] = anObject(O);
        result = new Empty();
        Empty[PROTOTYPE] = null;
        // add "__proto__" for Object.getPrototypeOf polyfill
        result[IE_PROTO] = O;
      } else result = _createDict();
      return Properties === undefined ? result : dPs(result, Properties);
    };

    /***/
  },

  /***/"2af9": /***/function af9(module, __nested_webpack_exports__, __nested_webpack_require_40454__) {
    "use strict";

    /* WEBPACK VAR INJECTION */
    (function (global) {
      /* harmony export (binding) */__nested_webpack_require_40454__.d(__nested_webpack_exports__, "d", function () {
        return install;
      });
      /* harmony import */
      var core_js_modules_es6_function_name__WEBPACK_IMPORTED_MODULE_0__ = __nested_webpack_require_40454__("7f7f");
      /* harmony import */
      var core_js_modules_es6_function_name__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__nested_webpack_require_40454__.n(core_js_modules_es6_function_name__WEBPACK_IMPORTED_MODULE_0__);
      /* harmony import */
      var core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_1__ = __nested_webpack_require_40454__("cadf");
      /* harmony import */
      var core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__nested_webpack_require_40454__.n(core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */
      var core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_2__ = __nested_webpack_require_40454__("456d");
      /* harmony import */
      var core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__nested_webpack_require_40454__.n(core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */
      var core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_3__ = __nested_webpack_require_40454__("ac6a");
      /* harmony import */
      var core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__nested_webpack_require_40454__.n(core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */
      var _GridItem_vue__WEBPACK_IMPORTED_MODULE_4__ = __nested_webpack_require_40454__("bc21");
      /* harmony reexport (safe) */
      __nested_webpack_require_40454__.d(__nested_webpack_exports__, "a", function () {
        return _GridItem_vue__WEBPACK_IMPORTED_MODULE_4__["a"];
      });

      /* harmony import */
      var _GridLayout_vue__WEBPACK_IMPORTED_MODULE_5__ = __nested_webpack_require_40454__("37c8");
      /* harmony reexport (safe) */
      __nested_webpack_require_40454__.d(__nested_webpack_exports__, "b", function () {
        return _GridLayout_vue__WEBPACK_IMPORTED_MODULE_5__["a"];
      });

      // import ResponsiveGridLayout from './ResponsiveGridLayout.vue';

      var VueGridLayout = {
        // ResponsiveGridLayout,
        GridLayout: _GridLayout_vue__WEBPACK_IMPORTED_MODULE_5__[/* default */"a"],
        GridItem: _GridItem_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */"a"]
      };
      function install(Vue) {
        if (install.installed) return;
        install.installed = true;
        Object.keys(VueGridLayout).forEach(function (name) {
          Vue.component(name, VueGridLayout[name]);
        });
      }
      var plugin = {
        install: install
      };
      var GlobalVue = null;
      if (typeof window !== 'undefined') {
        GlobalVue = window.Vue;
      } else if (typeof global !== 'undefined') {
        GlobalVue = global.Vue;
      }
      if (GlobalVue) {
        GlobalVue.use(plugin);
      }

      /* harmony default export */
      __nested_webpack_exports__["c"] = VueGridLayout;

      /* WEBPACK VAR INJECTION */
    }).call(this, __nested_webpack_require_40454__("c8ba"));

    /***/
  },

  /***/"2b4c": /***/function b4c(module, exports, __nested_webpack_require_43713__) {
    var store = __nested_webpack_require_43713__("5537")('wks');
    var uid = __nested_webpack_require_43713__("ca5a");
    var Symbol = __nested_webpack_require_43713__("7726").Symbol;
    var USE_SYMBOL = typeof Symbol == 'function';
    var $exports = module.exports = function (name) {
      return store[name] || (store[name] = USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
    };
    $exports.store = store;

    /***/
  },

  /***/"2cef": /***/function cef(module, exports, __webpack_require__) {
    "use strict";

    module.exports = function () {
      var idCount = 1;

      /**
       * Generates a new unique id in the context.
       * @public
       * @returns {number} A unique id in the context.
       */
      function generate() {
        return idCount++;
      }
      return {
        generate: generate
      };
    };

    /***/
  },

  /***/"2d00": /***/function d00(module, exports) {
    module.exports = false;

    /***/
  },

  /***/"2d95": /***/function d95(module, exports) {
    var toString = {}.toString;
    module.exports = function (it) {
      return toString.call(it).slice(8, -1);
    };

    /***/
  },

  /***/"2f21": /***/function f21(module, exports, __nested_webpack_require_44929__) {
    "use strict";

    var fails = __nested_webpack_require_44929__("79e5");
    module.exports = function (method, arg) {
      return !!method && fails(function () {
        // eslint-disable-next-line no-useless-call
        arg ? method.call(null, function () {/* empty */}, 1) : method.call(null);
      });
    };

    /***/
  },

  /***/"32e9": /***/function e9(module, exports, __nested_webpack_require_45325__) {
    var dP = __nested_webpack_require_45325__("86cc");
    var createDesc = __nested_webpack_require_45325__("4630");
    module.exports = __nested_webpack_require_45325__("9e1e") ? function (object, key, value) {
      return dP.f(object, key, createDesc(1, value));
    } : function (object, key, value) {
      object[key] = value;
      return object;
    };

    /***/
  },

  /***/"37c8": /***/function c8(module, __nested_webpack_exports__, __nested_webpack_require_45750__) {
    "use strict";

    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"1705dc22-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/GridLayout.vue?vue&type=template&id=361da5e4&
    var render = function render() {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c('div', {
        ref: "item",
        staticClass: "vue-grid-layout",
        style: _vm.mergedStyle
      }, [_vm._t("default"), _c('grid-item', {
        directives: [{
          name: "show",
          rawName: "v-show",
          value: _vm.isDragging,
          expression: "isDragging"
        }],
        staticClass: "vue-grid-placeholder",
        attrs: {
          "x": _vm.placeholder.x,
          "y": _vm.placeholder.y,
          "w": _vm.placeholder.w,
          "h": _vm.placeholder.h,
          "i": _vm.placeholder.i
        }
      })], 2);
    };
    var staticRenderFns = [];

    // CONCATENATED MODULE: ./src/components/GridLayout.vue?vue&type=template&id=361da5e4&

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es7.object.get-own-property-descriptors.js
    var es7_object_get_own_property_descriptors = __nested_webpack_require_45750__("8e6e");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.iterator.js
    var es6_array_iterator = __nested_webpack_require_45750__("cadf");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.object.keys.js
    var es6_object_keys = __nested_webpack_require_45750__("456d");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.object.assign.js
    var es6_object_assign = __nested_webpack_require_45750__("f751");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.is-finite.js
    var es6_number_is_finite = __nested_webpack_require_45750__("fca0");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom.iterable.js
    var web_dom_iterable = __nested_webpack_require_45750__("ac6a");

    // EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/defineProperty.js
    var defineProperty = __nested_webpack_require_45750__("ade3");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.constructor.js
    var es6_number_constructor = __nested_webpack_require_45750__("c5f6");

    // EXTERNAL MODULE: external {"commonjs":"vue","commonjs2":"vue","root":"Vue"}
    var external_commonjs_vue_commonjs2_vue_root_Vue_ = __nested_webpack_require_45750__("8bbf");
    var external_commonjs_vue_commonjs2_vue_root_Vue_default = /*#__PURE__*/__nested_webpack_require_45750__.n(external_commonjs_vue_commonjs2_vue_root_Vue_);

    // EXTERNAL MODULE: ./src/helpers/utils.js
    var utils = __nested_webpack_require_45750__("a2b6");

    // EXTERNAL MODULE: ./src/helpers/responsiveUtils.js
    var responsiveUtils = __nested_webpack_require_45750__("97a7");

    // EXTERNAL MODULE: ./src/components/GridItem.vue + 71 modules
    var GridItem = __nested_webpack_require_45750__("bc21");

    // EXTERNAL MODULE: ./src/helpers/DOM.js
    var DOM = __nested_webpack_require_45750__("1ca7");

    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/GridLayout.vue?vue&type=script&lang=js&

    function ownKeys(object, enumerableOnly) {
      var keys = Object.keys(object);
      if (Object.getOwnPropertySymbols) {
        var symbols = Object.getOwnPropertySymbols(object);
        if (enumerableOnly) symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
        keys.push.apply(keys, symbols);
      }
      return keys;
    }
    function _objectSpread(target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i] != null ? arguments[i] : {};
        if (i % 2) {
          ownKeys(Object(source), true).forEach(function (key) {
            Object(defineProperty["a" /* default */])(target, key, source[key]);
          });
        } else if (Object.getOwnPropertyDescriptors) {
          Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
        } else {
          ownKeys(Object(source)).forEach(function (key) {
            Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
          });
        }
      }
      return target;
    }

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    var elementResizeDetectorMaker = __nested_webpack_require_45750__("eec4");

    //var eventBus = require('./eventBus');

    /* harmony default export */
    var GridLayoutvue_type_script_lang_js_ = {
      name: "GridLayout",
      provide: function provide() {
        return {
          eventBus: null,
          layout: this
        };
      },
      components: {
        GridItem: GridItem["a" /* default */]
      },

      props: {
        // If true, the container height swells and contracts to fit contents
        autoSize: {
          type: Boolean,
          default: true
        },
        colNum: {
          type: Number,
          default: 12
        },
        rowHeight: {
          type: Number,
          default: 150
        },
        maxRows: {
          type: Number,
          default: Infinity
        },
        margin: {
          type: Array,
          default: function _default() {
            return [10, 10];
          }
        },
        isDraggable: {
          type: Boolean,
          default: true
        },
        isResizable: {
          type: Boolean,
          default: true
        },
        isMirrored: {
          type: Boolean,
          default: false
        },
        isBounded: {
          type: Boolean,
          default: false
        },
        useCssTransforms: {
          type: Boolean,
          default: true
        },
        verticalCompact: {
          type: Boolean,
          default: true
        },
        restoreOnDrag: {
          type: Boolean,
          default: false
        },
        layout: {
          type: Array,
          required: true
        },
        responsive: {
          type: Boolean,
          default: false
        },
        responsiveLayouts: {
          type: Object,
          default: function _default() {
            return {};
          }
        },
        transformScale: {
          type: Number,
          default: 1
        },
        breakpoints: {
          type: Object,
          default: function _default() {
            return {
              lg: 1200,
              md: 996,
              sm: 768,
              xs: 480,
              xxs: 0
            };
          }
        },
        cols: {
          type: Object,
          default: function _default() {
            return {
              lg: 12,
              md: 10,
              sm: 6,
              xs: 4,
              xxs: 2
            };
          }
        },
        preventCollision: {
          type: Boolean,
          default: false
        },
        useStyleCursor: {
          type: Boolean,
          default: true
        }
      },
      data: function data() {
        return {
          width: null,
          mergedStyle: {},
          lastLayoutLength: 0,
          isDragging: false,
          placeholder: {
            x: 0,
            y: 0,
            w: 0,
            h: 0,
            i: -1
          },
          layouts: {},
          // array to store all layouts from different breakpoints
          lastBreakpoint: null,
          // store last active breakpoint
          originalLayout: null // store original Layout
        };
      },

      created: function created() {
        var self = this; // Accessible refernces of functions for removing in beforeDestroy

        self.resizeEventHandler = function (eventType, i, x, y, h, w) {
          self.resizeEvent(eventType, i, x, y, h, w);
        };
        self.dragEventHandler = function (eventType, i, x, y, h, w) {
          self.dragEvent(eventType, i, x, y, h, w);
        };
        self._provided.eventBus = new external_commonjs_vue_commonjs2_vue_root_Vue_default.a();
        self.eventBus = self._provided.eventBus;
        self.eventBus.$on('resizeEvent', self.resizeEventHandler);
        self.eventBus.$on('dragEvent', self.dragEventHandler);
        self.$emit('layout-created', self.layout);
      },
      beforeDestroy: function beforeDestroy() {
        //Remove listeners
        this.eventBus.$off('resizeEvent', this.resizeEventHandler);
        this.eventBus.$off('dragEvent', this.dragEventHandler);
        this.eventBus.$destroy();
        Object(DOM["c" /* removeWindowEventListener */])("resize", this.onWindowResize);
        if (this.erd) {
          this.erd.uninstall(this.$refs.item);
        }
      },
      beforeMount: function beforeMount() {
        this.$emit('layout-before-mount', this.layout);
      },
      mounted: function mounted() {
        this.$emit('layout-mounted', this.layout);
        this.$nextTick(function () {
          Object(utils["l" /* validateLayout */])(this.layout);
          this.originalLayout = this.layout;
          var self = this;
          this.$nextTick(function () {
            self.initResponsiveFeatures();
            self.onWindowResize(); //self.width = self.$el.offsetWidth;

            Object(DOM["a" /* addWindowEventListener */])('resize', self.onWindowResize);
            Object(utils["c" /* compact */])(self.layout, self.verticalCompact);
            self.$emit('layout-updated', self.layout);
            self.updateHeight();
            self.$nextTick(function () {
              this.erd = elementResizeDetectorMaker({
                strategy: "scroll",
                //<- For ultra performance.
                // See https://github.com/wnr/element-resize-detector/issues/110 about callOnAdd.
                callOnAdd: false
              });
              this.erd.listenTo(self.$refs.item, function () {
                self.onWindowResize();
              });
            });
          });
        });
      },
      watch: {
        width: function width(newval, oldval) {
          var self = this;
          this.$nextTick(function () {
            var _this = this;

            //this.$broadcast("updateWidth", this.width);
            this.eventBus.$emit("updateWidth", this.width);
            if (oldval === null) {
              /*
                  If oldval == null is when the width has never been
                  set before. That only occurs when mouting is
                  finished, and onWindowResize has been called and
                  this.width has been changed the first time after it
                  got set to null in the constructor. It is now time
                  to issue layout-ready events as the GridItems have
                  their sizes configured properly.
                    The reason for emitting the layout-ready events on
                  the next tick is to allow for the newly-emitted
                  updateWidth event (above) to have reached the
                  children GridItem-s and had their effect, so we're
                  sure that they have the final size before we emit
                  layout-ready (for this GridLayout) and
                  item-layout-ready (for the GridItem-s).
                    This way any client event handlers can reliably
                  invistigate stable sizes of GridItem-s.
              */
              this.$nextTick(function () {
                _this.$emit('layout-ready', self.layout);
              });
            }
            this.updateHeight();
          });
        },
        layout: function layout() {
          this.layoutUpdate();
        },
        colNum: function colNum(val) {
          this.eventBus.$emit("setColNum", val);
        },
        rowHeight: function rowHeight() {
          this.eventBus.$emit("setRowHeight", this.rowHeight);
        },
        isDraggable: function isDraggable() {
          this.eventBus.$emit("setDraggable", this.isDraggable);
        },
        isResizable: function isResizable() {
          this.eventBus.$emit("setResizable", this.isResizable);
        },
        isBounded: function isBounded() {
          this.eventBus.$emit("setBounded", this.isBounded);
        },
        transformScale: function transformScale() {
          this.eventBus.$emit("setTransformScale", this.transformScale);
        },
        responsive: function responsive() {
          if (!this.responsive) {
            this.$emit('update:layout', this.originalLayout);
            this.eventBus.$emit("setColNum", this.colNum);
          }
          this.onWindowResize();
        },
        maxRows: function maxRows() {
          this.eventBus.$emit("setMaxRows", this.maxRows);
        },
        margin: function margin() {
          this.updateHeight();
        }
      },
      methods: {
        layoutUpdate: function layoutUpdate() {
          if (this.layout !== undefined && this.originalLayout !== null) {
            if (this.layout.length !== this.originalLayout.length) {
              // console.log("### LAYOUT UPDATE!", this.layout.length, this.originalLayout.length);
              var diff = this.findDifference(this.layout, this.originalLayout);
              if (diff.length > 0) {
                // console.log(diff);
                if (this.layout.length > this.originalLayout.length) {
                  this.originalLayout = this.originalLayout.concat(diff);
                } else {
                  this.originalLayout = this.originalLayout.filter(function (obj) {
                    return !diff.some(function (obj2) {
                      return obj.i === obj2.i;
                    });
                  });
                }
              }
              this.lastLayoutLength = this.layout.length;
              this.initResponsiveFeatures();
            }
            Object(utils["c" /* compact */])(this.layout, this.verticalCompact);
            this.eventBus.$emit("updateWidth", this.width);
            this.updateHeight();
            this.$emit('layout-updated', this.layout);
          }
        },
        updateHeight: function updateHeight() {
          this.mergedStyle = {
            height: this.containerHeight()
          };
        },
        onWindowResize: function onWindowResize() {
          if (this.$refs !== null && this.$refs.item !== null && this.$refs.item !== undefined) {
            this.width = this.$refs.item.offsetWidth;
          }
          this.eventBus.$emit("resizeEvent");
        },
        containerHeight: function containerHeight() {
          if (!this.autoSize) return; // console.log("bottom: " + bottom(this.layout))
          // console.log("rowHeight + margins: " + (this.rowHeight + this.margin[1]) + this.margin[1])

          var containerHeight = Object(utils["a" /* bottom */])(this.layout) * (this.rowHeight + this.margin[1]) + this.margin[1] + 'px';
          return containerHeight;
        },
        dragEvent: function dragEvent(eventName, id, x, y, h, w) {
          //console.log(eventName + " id=" + id + ", x=" + x + ", y=" + y);
          var l = Object(utils["f" /* getLayoutItem */])(this.layout, id); //GetLayoutItem sometimes returns null object

          if (l === undefined || l === null) {
            l = {
              x: 0,
              y: 0
            };
          }
          if (eventName === "dragstart" && !this.verticalCompact) {
            this.positionsBeforeDrag = this.layout.reduce(function (result, _ref) {
              var i = _ref.i,
                x = _ref.x,
                y = _ref.y;
              return _objectSpread(_objectSpread({}, result), {}, Object(defineProperty["a" /* default */])({}, i, {
                x: x,
                y: y
              }));
            }, {});
          }
          if (eventName === "dragmove" || eventName === "dragstart") {
            this.placeholder.i = id;
            this.placeholder.x = l.x;
            this.placeholder.y = l.y;
            this.placeholder.w = w;
            this.placeholder.h = h;
            this.$nextTick(function () {
              this.isDragging = true;
            }); //this.$broadcast("updateWidth", this.width);

            this.eventBus.$emit("updateWidth", this.width);
          } else {
            this.$nextTick(function () {
              this.isDragging = false;
            });
          } // Move the element to the dragged location.

          this.layout = Object(utils["g" /* moveElement */])(this.layout, l, x, y, true, this.preventCollision);
          if (this.restoreOnDrag) {
            // Do not compact items more than in layout before drag
            // Set moved item as static to avoid to compact it
            l.static = true;
            Object(utils["c" /* compact */])(this.layout, this.verticalCompact, this.positionsBeforeDrag);
            l.static = false;
          } else {
            Object(utils["c" /* compact */])(this.layout, this.verticalCompact);
          } // needed because vue can't detect changes on array element properties

          this.eventBus.$emit("compact");
          this.updateHeight();
          if (eventName === 'dragend') {
            delete this.positionsBeforeDrag;
            this.$emit('layout-updated', this.layout);
          }
        },
        resizeEvent: function resizeEvent(eventName, id, x, y, h, w) {
          var l = Object(utils["f" /* getLayoutItem */])(this.layout, id); //GetLayoutItem sometimes return null object

          if (l === undefined || l === null) {
            l = {
              h: 0,
              w: 0
            };
          }
          var hasCollisions;
          if (this.preventCollision) {
            var collisions = Object(utils["e" /* getAllCollisions */])(this.layout, _objectSpread(_objectSpread({}, l), {}, {
              w: w,
              h: h
            })).filter(function (layoutItem) {
              return layoutItem.i !== l.i;
            });
            hasCollisions = collisions.length > 0; // If we're colliding, we need adjust the placeholder.

            if (hasCollisions) {
              // adjust w && h to maximum allowed space
              var leastX = Infinity,
                leastY = Infinity;
              collisions.forEach(function (layoutItem) {
                if (layoutItem.x > l.x) leastX = Math.min(leastX, layoutItem.x);
                if (layoutItem.y > l.y) leastY = Math.min(leastY, layoutItem.y);
              });
              if (Number.isFinite(leastX)) l.w = leastX - l.x;
              if (Number.isFinite(leastY)) l.h = leastY - l.y;
            }
          }
          if (!hasCollisions) {
            // Set new width and height.
            l.w = w;
            l.h = h;
          }
          if (eventName === "resizestart" || eventName === "resizemove") {
            this.placeholder.i = id;
            this.placeholder.x = x;
            this.placeholder.y = y;
            this.placeholder.w = l.w;
            this.placeholder.h = l.h;
            this.$nextTick(function () {
              this.isDragging = true;
            }); //this.$broadcast("updateWidth", this.width);

            this.eventBus.$emit("updateWidth", this.width);
          } else {
            this.$nextTick(function () {
              this.isDragging = false;
            });
          }
          if (this.responsive) this.responsiveGridLayout();
          Object(utils["c" /* compact */])(this.layout, this.verticalCompact);
          this.eventBus.$emit("compact");
          this.updateHeight();
          if (eventName === 'resizeend') this.$emit('layout-updated', this.layout);
        },
        // finds or generates new layouts for set breakpoints
        responsiveGridLayout: function responsiveGridLayout() {
          var newBreakpoint = Object(responsiveUtils["b" /* getBreakpointFromWidth */])(this.breakpoints, this.width);
          var newCols = Object(responsiveUtils["c" /* getColsFromBreakpoint */])(newBreakpoint, this.cols); // save actual layout in layouts

          if (this.lastBreakpoint != null && !this.layouts[this.lastBreakpoint]) this.layouts[this.lastBreakpoint] = Object(utils["b" /* cloneLayout */])(this.layout); // Find or generate a new layout.

          var layout = Object(responsiveUtils["a" /* findOrGenerateResponsiveLayout */])(this.originalLayout, this.layouts, this.breakpoints, newBreakpoint, this.lastBreakpoint, newCols, this.verticalCompact); // Store the new layout.

          this.layouts[newBreakpoint] = layout;
          if (this.lastBreakpoint !== newBreakpoint) {
            this.$emit('breakpoint-changed', newBreakpoint, layout);
          } // new prop sync

          this.$emit('update:layout', layout);
          this.lastBreakpoint = newBreakpoint;
          this.eventBus.$emit("setColNum", Object(responsiveUtils["c" /* getColsFromBreakpoint */])(newBreakpoint, this.cols));
        },
        // clear all responsive layouts
        initResponsiveFeatures: function initResponsiveFeatures() {
          // clear layouts
          this.layouts = Object.assign({}, this.responsiveLayouts);
        },
        // find difference in layouts
        findDifference: function findDifference(layout, originalLayout) {
          //Find values that are in result1 but not in result2
          var uniqueResultOne = layout.filter(function (obj) {
            return !originalLayout.some(function (obj2) {
              return obj.i === obj2.i;
            });
          }); //Find values that are in result2 but not in result1

          var uniqueResultTwo = originalLayout.filter(function (obj) {
            return !layout.some(function (obj2) {
              return obj.i === obj2.i;
            });
          }); //Combine the two arrays of unique entries#

          return uniqueResultOne.concat(uniqueResultTwo);
        }
      }
    };
    // CONCATENATED MODULE: ./src/components/GridLayout.vue?vue&type=script&lang=js&
    /* harmony default export */
    var components_GridLayoutvue_type_script_lang_js_ = GridLayoutvue_type_script_lang_js_;
    // EXTERNAL MODULE: ./src/components/GridLayout.vue?vue&type=style&index=0&lang=css&
    var GridLayoutvue_type_style_index_0_lang_css_ = __nested_webpack_require_45750__("e279");

    // EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
    var componentNormalizer = __nested_webpack_require_45750__("2877");

    // CONCATENATED MODULE: ./src/components/GridLayout.vue

    /* normalize component */

    var component = Object(componentNormalizer["a" /* default */])(components_GridLayoutvue_type_script_lang_js_, render, staticRenderFns, false, null, null, null);

    /* harmony default export */
    var GridLayout = __nested_webpack_exports__["a"] = component.exports;

    /***/
  },

  /***/"38fd": /***/function fd(module, exports, __nested_webpack_require_68824__) {
    // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
    var has = __nested_webpack_require_68824__("69a8");
    var toObject = __nested_webpack_require_68824__("4bf8");
    var IE_PROTO = __nested_webpack_require_68824__("613b")('IE_PROTO');
    var ObjectProto = Object.prototype;
    module.exports = Object.getPrototypeOf || function (O) {
      O = toObject(O);
      if (has(O, IE_PROTO)) return O[IE_PROTO];
      if (typeof O.constructor == 'function' && O instanceof O.constructor) {
        return O.constructor.prototype;
      }
      return O instanceof Object ? ObjectProto : null;
    };

    /***/
  },

  /***/"41a0": /***/function a0(module, exports, __nested_webpack_require_69476__) {
    "use strict";

    var create = __nested_webpack_require_69476__("2aeb");
    var descriptor = __nested_webpack_require_69476__("4630");
    var setToStringTag = __nested_webpack_require_69476__("7f20");
    var IteratorPrototype = {};

    // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
    __nested_webpack_require_69476__("32e9")(IteratorPrototype, __nested_webpack_require_69476__("2b4c")('iterator'), function () {
      return this;
    });
    module.exports = function (Constructor, NAME, next) {
      Constructor.prototype = create(IteratorPrototype, {
        next: descriptor(1, next)
      });
      setToStringTag(Constructor, NAME + ' Iterator');
    };

    /***/
  },

  /***/"456d": /***/function d(module, exports, __nested_webpack_require_70173__) {
    // 19.1.2.14 Object.keys(O)
    var toObject = __nested_webpack_require_70173__("4bf8");
    var $keys = __nested_webpack_require_70173__("0d58");
    __nested_webpack_require_70173__("5eda")('keys', function () {
      return function keys(it) {
        return $keys(toObject(it));
      };
    });

    /***/
  },

  /***/"4588": /***/function _(module, exports) {
    // 7.1.4 ToInteger
    var ceil = Math.ceil;
    var floor = Math.floor;
    module.exports = function (it) {
      return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
    };

    /***/
  },

  /***/"4630": /***/function _(module, exports) {
    module.exports = function (bitmap, value) {
      return {
        enumerable: !(bitmap & 1),
        configurable: !(bitmap & 2),
        writable: !(bitmap & 4),
        value: value
      };
    };

    /***/
  },

  /***/"4917": /***/function _(module, exports, __nested_webpack_require_71050__) {
    "use strict";

    var anObject = __nested_webpack_require_71050__("cb7c");
    var toLength = __nested_webpack_require_71050__("9def");
    var advanceStringIndex = __nested_webpack_require_71050__("0390");
    var regExpExec = __nested_webpack_require_71050__("5f1b");

    // @@match logic
    __nested_webpack_require_71050__("214f")('match', 1, function (defined, MATCH, $match, maybeCallNative) {
      return [
      // `String.prototype.match` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.match
      function match(regexp) {
        var O = defined(this);
        var fn = regexp == undefined ? undefined : regexp[MATCH];
        return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
      },
      // `RegExp.prototype[@@match]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
      function (regexp) {
        var res = maybeCallNative($match, regexp, this);
        if (res.done) return res.value;
        var rx = anObject(regexp);
        var S = String(this);
        if (!rx.global) return regExpExec(rx, S);
        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
        var A = [];
        var n = 0;
        var result;
        while ((result = regExpExec(rx, S)) !== null) {
          var matchStr = String(result[0]);
          A[n] = matchStr;
          if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
          n++;
        }
        return n === 0 ? null : A;
      }];
    });

    /***/
  },

  /***/"499e": /***/function e(module, __nested_webpack_exports__, __nested_webpack_require_72632__) {
    "use strict";

    // ESM COMPAT FLAG
    __nested_webpack_require_72632__.r(__nested_webpack_exports__);

    // EXPORTS
    __nested_webpack_require_72632__.d(__nested_webpack_exports__, "default", function () {
      return (/* binding */addStylesClient
      );
    });

    // CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
    /**
     * Translates the list format produced by css-loader into something
     * easier to manipulate.
     */
    function listToStyles(parentId, list) {
      var styles = [];
      var newStyles = {};
      for (var i = 0; i < list.length; i++) {
        var item = list[i];
        var id = item[0];
        var css = item[1];
        var media = item[2];
        var sourceMap = item[3];
        var part = {
          id: parentId + ':' + i,
          css: css,
          media: media,
          sourceMap: sourceMap
        };
        if (!newStyles[id]) {
          styles.push(newStyles[id] = {
            id: id,
            parts: [part]
          });
        } else {
          newStyles[id].parts.push(part);
        }
      }
      return styles;
    }

    // CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesClient.js
    /*
      MIT License http://www.opensource.org/licenses/mit-license.php
      Author Tobias Koppers @sokra
      Modified by Evan You @yyx990803
    */

    var hasDocument = typeof document !== 'undefined';
    if (typeof DEBUG !== 'undefined' && DEBUG) {
      if (!hasDocument) {
        throw new Error('vue-style-loader cannot be used in a non-browser environment. ' + "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment.");
      }
    }

    /*
    type StyleObject = {
      id: number;
      parts: Array<StyleObjectPart>
    }
    
    type StyleObjectPart = {
      css: string;
      media: string;
      sourceMap: ?string
    }
    */

    var stylesInDom = {/*
                       [id: number]: {
                       id: number,
                       refs: number,
                       parts: Array<(obj?: StyleObjectPart) => void>
                       }
                       */};
    var head = hasDocument && (document.head || document.getElementsByTagName('head')[0]);
    var singletonElement = null;
    var singletonCounter = 0;
    var isProduction = false;
    var noop = function noop() {};
    var options = null;
    var ssrIdKey = 'data-vue-ssr-id';

    // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
    // tags it will allow on a page
    var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase());
    function addStylesClient(parentId, list, _isProduction, _options) {
      isProduction = _isProduction;
      options = _options || {};
      var styles = listToStyles(parentId, list);
      addStylesToDom(styles);
      return function update(newList) {
        var mayRemove = [];
        for (var i = 0; i < styles.length; i++) {
          var item = styles[i];
          var domStyle = stylesInDom[item.id];
          domStyle.refs--;
          mayRemove.push(domStyle);
        }
        if (newList) {
          styles = listToStyles(parentId, newList);
          addStylesToDom(styles);
        } else {
          styles = [];
        }
        for (var i = 0; i < mayRemove.length; i++) {
          var domStyle = mayRemove[i];
          if (domStyle.refs === 0) {
            for (var j = 0; j < domStyle.parts.length; j++) {
              domStyle.parts[j]();
            }
            delete stylesInDom[domStyle.id];
          }
        }
      };
    }
    function addStylesToDom(styles /* Array<StyleObject> */) {
      for (var i = 0; i < styles.length; i++) {
        var item = styles[i];
        var domStyle = stylesInDom[item.id];
        if (domStyle) {
          domStyle.refs++;
          for (var j = 0; j < domStyle.parts.length; j++) {
            domStyle.parts[j](item.parts[j]);
          }
          for (; j < item.parts.length; j++) {
            domStyle.parts.push(addStyle(item.parts[j]));
          }
          if (domStyle.parts.length > item.parts.length) {
            domStyle.parts.length = item.parts.length;
          }
        } else {
          var parts = [];
          for (var j = 0; j < item.parts.length; j++) {
            parts.push(addStyle(item.parts[j]));
          }
          stylesInDom[item.id] = {
            id: item.id,
            refs: 1,
            parts: parts
          };
        }
      }
    }
    function createStyleElement() {
      var styleElement = document.createElement('style');
      styleElement.type = 'text/css';
      head.appendChild(styleElement);
      return styleElement;
    }
    function addStyle(obj /* StyleObjectPart */) {
      var update, remove;
      var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]');
      if (styleElement) {
        if (isProduction) {
          // has SSR styles and in production mode.
          // simply do nothing.
          return noop;
        } else {
          // has SSR styles but in dev mode.
          // for some reason Chrome can't handle source map in server-rendered
          // style tags - source maps in <style> only works if the style tag is
          // created and inserted dynamically. So we remove the server rendered
          // styles and inject new ones.
          styleElement.parentNode.removeChild(styleElement);
        }
      }
      if (isOldIE) {
        // use singleton mode for IE9.
        var styleIndex = singletonCounter++;
        styleElement = singletonElement || (singletonElement = createStyleElement());
        update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
        remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
      } else {
        // use multi-style-tag mode in all other cases
        styleElement = createStyleElement();
        update = applyToTag.bind(null, styleElement);
        remove = function remove() {
          styleElement.parentNode.removeChild(styleElement);
        };
      }
      update(obj);
      return function updateStyle(newObj /* StyleObjectPart */) {
        if (newObj) {
          if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
            return;
          }
          update(obj = newObj);
        } else {
          remove();
        }
      };
    }
    var replaceText = function () {
      var textStore = [];
      return function (index, replacement) {
        textStore[index] = replacement;
        return textStore.filter(Boolean).join('\n');
      };
    }();
    function applyToSingletonTag(styleElement, index, remove, obj) {
      var css = remove ? '' : obj.css;
      if (styleElement.styleSheet) {
        styleElement.styleSheet.cssText = replaceText(index, css);
      } else {
        var cssNode = document.createTextNode(css);
        var childNodes = styleElement.childNodes;
        if (childNodes[index]) styleElement.removeChild(childNodes[index]);
        if (childNodes.length) {
          styleElement.insertBefore(cssNode, childNodes[index]);
        } else {
          styleElement.appendChild(cssNode);
        }
      }
    }
    function applyToTag(styleElement, obj) {
      var css = obj.css;
      var media = obj.media;
      var sourceMap = obj.sourceMap;
      if (media) {
        styleElement.setAttribute('media', media);
      }
      if (options.ssrId) {
        styleElement.setAttribute(ssrIdKey, obj.id);
      }
      if (sourceMap) {
        // https://developer.chrome.com/devtools/docs/javascript-debugging
        // this makes source maps inside style tags work properly in Chrome
        css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */';
        // http://stackoverflow.com/a/26603875
        css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */';
      }
      if (styleElement.styleSheet) {
        styleElement.styleSheet.cssText = css;
      } else {
        while (styleElement.firstChild) {
          styleElement.removeChild(styleElement.firstChild);
        }
        styleElement.appendChild(document.createTextNode(css));
      }
    }

    /***/
  },

  /***/"49ad": /***/function ad(module, exports, __webpack_require__) {
    "use strict";

    module.exports = function (idHandler) {
      var eventListeners = {};

      /**
       * Gets all listeners for the given element.
       * @public
       * @param {element} element The element to get all listeners for.
       * @returns All listeners for the given element.
       */
      function getListeners(element) {
        var id = idHandler.get(element);
        if (id === undefined) {
          return [];
        }
        return eventListeners[id] || [];
      }

      /**
       * Stores the given listener for the given element. Will not actually add the listener to the element.
       * @public
       * @param {element} element The element that should have the listener added.
       * @param {function} listener The callback that the element has added.
       */
      function addListener(element, listener) {
        var id = idHandler.get(element);
        if (!eventListeners[id]) {
          eventListeners[id] = [];
        }
        eventListeners[id].push(listener);
      }
      function removeListener(element, listener) {
        var listeners = getListeners(element);
        for (var i = 0, len = listeners.length; i < len; ++i) {
          if (listeners[i] === listener) {
            listeners.splice(i, 1);
            break;
          }
        }
      }
      function removeAllListeners(element) {
        var listeners = getListeners(element);
        if (!listeners) {
          return;
        }
        listeners.length = 0;
      }
      return {
        get: getListeners,
        add: addListener,
        removeListener: removeListener,
        removeAllListeners: removeAllListeners
      };
    };

    /***/
  },

  /***/"4bf8": /***/function bf8(module, exports, __nested_webpack_require_82772__) {
    // 7.1.13 ToObject(argument)
    var defined = __nested_webpack_require_82772__("be13");
    module.exports = function (it) {
      return Object(defined(it));
    };

    /***/
  },

  /***/"5058": /***/function _(module, exports, __webpack_require__) {
    "use strict";

    module.exports = function (options) {
      var idGenerator = options.idGenerator;
      var getState = options.stateHandler.getState;

      /**
       * Gets the resize detector id of the element.
       * @public
       * @param {element} element The target element to get the id of.
       * @returns {string|number|null} The id of the element. Null if it has no id.
       */
      function getId(element) {
        var state = getState(element);
        if (state && state.id !== undefined) {
          return state.id;
        }
        return null;
      }

      /**
       * Sets the resize detector id of the element. Requires the element to have a resize detector state initialized.
       * @public
       * @param {element} element The target element to set the id of.
       * @returns {string|number|null} The id of the element.
       */
      function setId(element) {
        var state = getState(element);
        if (!state) {
          throw new Error("setId required the element to have a resize detection state.");
        }
        var id = idGenerator.generate();
        state.id = id;
        return id;
      }
      return {
        get: getId,
        set: setId
      };
    };

    /***/
  },

  /***/"50bf": /***/function bf(module, exports, __webpack_require__) {
    "use strict";

    var utils = module.exports = {};
    utils.getOption = getOption;
    function getOption(options, name, defaultValue) {
      var value = options[name];
      if ((value === undefined || value === null) && defaultValue !== undefined) {
        return defaultValue;
      }
      return value;
    }

    /***/
  },

  /***/"520a": /***/function a(module, exports, __nested_webpack_require_84750__) {
    "use strict";

    var regexpFlags = __nested_webpack_require_84750__("0bfb");
    var nativeExec = RegExp.prototype.exec;
    // This always refers to the native implementation, because the
    // String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
    // which loads this file before patching the method.
    var nativeReplace = String.prototype.replace;
    var patchedExec = nativeExec;
    var LAST_INDEX = 'lastIndex';
    var UPDATES_LAST_INDEX_WRONG = function () {
      var re1 = /a/,
        re2 = /b*/g;
      nativeExec.call(re1, 'a');
      nativeExec.call(re2, 'a');
      return re1[LAST_INDEX] !== 0 || re2[LAST_INDEX] !== 0;
    }();

    // nonparticipating capturing group, copied from es5-shim's String#split patch.
    var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;
    var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED;
    if (PATCH) {
      patchedExec = function exec(str) {
        var re = this;
        var lastIndex, reCopy, match, i;
        if (NPCG_INCLUDED) {
          reCopy = new RegExp('^' + re.source + '$(?!\\s)', regexpFlags.call(re));
        }
        if (UPDATES_LAST_INDEX_WRONG) lastIndex = re[LAST_INDEX];
        match = nativeExec.call(re, str);
        if (UPDATES_LAST_INDEX_WRONG && match) {
          re[LAST_INDEX] = re.global ? match.index + match[0].length : lastIndex;
        }
        if (NPCG_INCLUDED && match && match.length > 1) {
          // Fix browsers whose `exec` methods don't consistently return `undefined`
          // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
          // eslint-disable-next-line no-loop-func
          nativeReplace.call(match[0], reCopy, function () {
            for (i = 1; i < arguments.length - 2; i++) {
              if (arguments[i] === undefined) match[i] = undefined;
            }
          });
        }
        return match;
      };
    }
    module.exports = patchedExec;

    /***/
  },

  /***/"52a7": /***/function a7(module, exports) {
    exports.f = {}.propertyIsEnumerable;

    /***/
  },

  /***/"5537": /***/function _(module, exports, __nested_webpack_require_86860__) {
    var core = __nested_webpack_require_86860__("8378");
    var global = __nested_webpack_require_86860__("7726");
    var SHARED = '__core-js_shared__';
    var store = global[SHARED] || (global[SHARED] = {});
    (module.exports = function (key, value) {
      return store[key] || (store[key] = value !== undefined ? value : {});
    })('versions', []).push({
      version: core.version,
      mode: __nested_webpack_require_86860__("2d00") ? 'pure' : 'global',
      copyright: '© 2020 Denis Pushkarev (zloirock.ru)'
    });

    /***/
  },

  /***/"55dd": /***/function dd(module, exports, __nested_webpack_require_87441__) {
    "use strict";

    var $export = __nested_webpack_require_87441__("5ca1");
    var aFunction = __nested_webpack_require_87441__("d8e8");
    var toObject = __nested_webpack_require_87441__("4bf8");
    var fails = __nested_webpack_require_87441__("79e5");
    var $sort = [].sort;
    var test = [1, 2, 3];
    $export($export.P + $export.F * (fails(function () {
      // IE8-
      test.sort(undefined);
    }) || !fails(function () {
      // V8 bug
      test.sort(null);
      // Old WebKit
    }) || !__nested_webpack_require_87441__("2f21")($sort)), 'Array', {
      // 22.1.3.25 Array.prototype.sort(comparefn)
      sort: function sort(comparefn) {
        return comparefn === undefined ? $sort.call(toObject(this)) : $sort.call(toObject(this), aFunction(comparefn));
      }
    });

    /***/
  },

  /***/"5be5": /***/function be5(module, exports, __webpack_require__) {
    "use strict";

    module.exports = function (options) {
      var getState = options.stateHandler.getState;

      /**
       * Tells if the element has been made detectable and ready to be listened for resize events.
       * @public
       * @param {element} The element to check.
       * @returns {boolean} True or false depending on if the element is detectable or not.
       */
      function isDetectable(element) {
        var state = getState(element);
        return state && !!state.isDetectable;
      }

      /**
       * Marks the element that it has been made detectable and ready to be listened for resize events.
       * @public
       * @param {element} The element to mark.
       */
      function markAsDetectable(element) {
        getState(element).isDetectable = true;
      }

      /**
       * Tells if the element is busy or not.
       * @public
       * @param {element} The element to check.
       * @returns {boolean} True or false depending on if the element is busy or not.
       */
      function isBusy(element) {
        return !!getState(element).busy;
      }

      /**
       * Marks the object is busy and should not be made detectable.
       * @public
       * @param {element} element The element to mark.
       * @param {boolean} busy If the element is busy or not.
       */
      function markBusy(element, busy) {
        getState(element).busy = !!busy;
      }
      return {
        isDetectable: isDetectable,
        markAsDetectable: markAsDetectable,
        isBusy: isBusy,
        markBusy: markBusy
      };
    };

    /***/
  },

  /***/"5ca1": /***/function ca1(module, exports, __nested_webpack_require_89939__) {
    var global = __nested_webpack_require_89939__("7726");
    var core = __nested_webpack_require_89939__("8378");
    var hide = __nested_webpack_require_89939__("32e9");
    var redefine = __nested_webpack_require_89939__("2aba");
    var ctx = __nested_webpack_require_89939__("9b43");
    var PROTOTYPE = 'prototype';
    var $export = function $export(type, name, source) {
      var IS_FORCED = type & $export.F;
      var IS_GLOBAL = type & $export.G;
      var IS_STATIC = type & $export.S;
      var IS_PROTO = type & $export.P;
      var IS_BIND = type & $export.B;
      var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
      var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
      var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
      var key, own, out, exp;
      if (IS_GLOBAL) source = name;
      for (key in source) {
        // contains in native
        own = !IS_FORCED && target && target[key] !== undefined;
        // export native or passed
        out = (own ? target : source)[key];
        // bind timers to global for call from export context
        exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
        // extend global
        if (target) redefine(target, key, out, type & $export.U);
        // export
        if (exports[key] != out) hide(exports, key, exp);
        if (IS_PROTO && expProto[key] != out) expProto[key] = out;
      }
    };
    global.core = core;
    // type bitmap
    $export.F = 1; // forced
    $export.G = 2; // global
    $export.S = 4; // static
    $export.P = 8; // proto
    $export.B = 16; // bind
    $export.W = 32; // wrap
    $export.U = 64; // safe
    $export.R = 128; // real proto method for `library`
    module.exports = $export;

    /***/
  },

  /***/"5dbc": /***/function dbc(module, exports, __nested_webpack_require_91833__) {
    var isObject = __nested_webpack_require_91833__("d3f4");
    var setPrototypeOf = __nested_webpack_require_91833__("8b97").set;
    module.exports = function (that, target, C) {
      var S = target.constructor;
      var P;
      if (S !== C && typeof S == 'function' && (P = S.prototype) !== C.prototype && isObject(P) && setPrototypeOf) {
        setPrototypeOf(that, P);
      }
      return that;
    };

    /***/
  },

  /***/"5ed4": /***/function ed4(module, __webpack_exports__, __nested_webpack_require_92322__) {
    "use strict";

    /* harmony import */
    var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GridItem_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __nested_webpack_require_92322__("6e21");
    /* harmony import */
    var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GridItem_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__nested_webpack_require_92322__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GridItem_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /***/
  },

  /***/"5eda": /***/function eda(module, exports, __nested_webpack_require_93964__) {
    // most Object methods by ES6 should accept primitives
    var $export = __nested_webpack_require_93964__("5ca1");
    var core = __nested_webpack_require_93964__("8378");
    var fails = __nested_webpack_require_93964__("79e5");
    module.exports = function (KEY, exec) {
      var fn = (core.Object || {})[KEY] || Object[KEY];
      var exp = {};
      exp[KEY] = exec(fn);
      $export($export.S + $export.F * fails(function () {
        fn(1);
      }), 'Object', exp);
    };

    /***/
  },

  /***/"5f1b": /***/function f1b(module, exports, __nested_webpack_require_94502__) {
    "use strict";

    var classof = __nested_webpack_require_94502__("23c6");
    var builtinExec = RegExp.prototype.exec;

    // `RegExpExec` abstract operation
    // https://tc39.github.io/ecma262/#sec-regexpexec
    module.exports = function (R, S) {
      var exec = R.exec;
      if (typeof exec === 'function') {
        var result = exec.call(R, S);
        if (typeof result !== 'object') {
          throw new TypeError('RegExp exec method returned something other than an Object or null');
        }
        return result;
      }
      if (classof(R) !== 'RegExp') {
        throw new TypeError('RegExp#exec called on incompatible receiver');
      }
      return builtinExec.call(R, S);
    };

    /***/
  },

  /***/"613b": /***/function b(module, exports, __nested_webpack_require_95286__) {
    var shared = __nested_webpack_require_95286__("5537")('keys');
    var uid = __nested_webpack_require_95286__("ca5a");
    module.exports = function (key) {
      return shared[key] || (shared[key] = uid(key));
    };

    /***/
  },

  /***/"626a": /***/function a(module, exports, __nested_webpack_require_95570__) {
    // fallback for non-array-like ES3 and non-enumerable old V8 strings
    var cof = __nested_webpack_require_95570__("2d95");
    // eslint-disable-next-line no-prototype-builtins
    module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
      return cof(it) == 'String' ? it.split('') : Object(it);
    };

    /***/
  },

  /***/"6821": /***/function _(module, exports, __nested_webpack_require_95981__) {
    // to indexed object, toObject with fallback for non-array-like ES3 strings
    var IObject = __nested_webpack_require_95981__("626a");
    var defined = __nested_webpack_require_95981__("be13");
    module.exports = function (it) {
      return IObject(defined(it));
    };

    /***/
  },

  /***/"69a8": /***/function a8(module, exports) {
    var hasOwnProperty = {}.hasOwnProperty;
    module.exports = function (it, key) {
      return hasOwnProperty.call(it, key);
    };

    /***/
  },

  /***/"6a99": /***/function a99(module, exports, __nested_webpack_require_96528__) {
    // 7.1.1 ToPrimitive(input [, PreferredType])
    var isObject = __nested_webpack_require_96528__("d3f4");
    // instead of the ES6 spec version, we didn't implement @@toPrimitive case
    // and the second argument - flag - preferred type is a string
    module.exports = function (it, S) {
      if (!isObject(it)) return it;
      var fn, val;
      if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
      if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      throw TypeError("Can't convert object to primitive value");
    };

    /***/
  },

  /***/"6e21": /***/function e21(module, exports, __nested_webpack_require_97325__) {
    // style-loader: Adds some css to the DOM by adding a <style> tag

    // load the styles
    var content = __nested_webpack_require_97325__("9cbe");
    if (typeof content === 'string') content = [[module.i, content, '']];
    if (content.locals) module.exports = content.locals;
    // add the styles to the DOM
    var add = __nested_webpack_require_97325__("499e").default;
    var update = add("3cbd0c21", content, true, {
      "sourceMap": false,
      "shadowMode": false
    });

    /***/
  },

  /***/"7333": /***/function _(module, exports, __nested_webpack_require_97879__) {
    "use strict";

    // 19.1.2.1 Object.assign(target, source, ...)
    var DESCRIPTORS = __nested_webpack_require_97879__("9e1e");
    var getKeys = __nested_webpack_require_97879__("0d58");
    var gOPS = __nested_webpack_require_97879__("2621");
    var pIE = __nested_webpack_require_97879__("52a7");
    var toObject = __nested_webpack_require_97879__("4bf8");
    var IObject = __nested_webpack_require_97879__("626a");
    var $assign = Object.assign;

    // should work with symbols and should have deterministic property order (V8 bug)
    module.exports = !$assign || __nested_webpack_require_97879__("79e5")(function () {
      var A = {};
      var B = {};
      // eslint-disable-next-line no-undef
      var S = Symbol();
      var K = 'abcdefghijklmnopqrst';
      A[S] = 7;
      K.split('').forEach(function (k) {
        B[k] = k;
      });
      return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
    }) ? function assign(target, source) {
      // eslint-disable-line no-unused-vars
      var T = toObject(target);
      var aLen = arguments.length;
      var index = 1;
      var getSymbols = gOPS.f;
      var isEnum = pIE.f;
      while (aLen > index) {
        var S = IObject(arguments[index++]);
        var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
        var length = keys.length;
        var j = 0;
        var key;
        while (length > j) {
          key = keys[j++];
          if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
        }
      }
      return T;
    } : $assign;

    /***/
  },

  /***/"7726": /***/function _(module, exports) {
    // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
    var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self
    // eslint-disable-next-line no-new-func
    : Function('return this')();
    if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

    /***/
  },

  /***/"77f1": /***/function f1(module, exports, __nested_webpack_require_99901__) {
    var toInteger = __nested_webpack_require_99901__("4588");
    var max = Math.max;
    var min = Math.min;
    module.exports = function (index, length) {
      index = toInteger(index);
      return index < 0 ? max(index + length, 0) : min(index, length);
    };

    /***/
  },

  /***/"79e5": /***/function e5(module, exports) {
    module.exports = function (exec) {
      try {
        return !!exec();
      } catch (e) {
        return true;
      }
    };

    /***/
  },

  /***/"7f20": /***/function f20(module, exports, __nested_webpack_require_100445__) {
    var def = __nested_webpack_require_100445__("86cc").f;
    var has = __nested_webpack_require_100445__("69a8");
    var TAG = __nested_webpack_require_100445__("2b4c")('toStringTag');
    module.exports = function (it, tag, stat) {
      if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, {
        configurable: true,
        value: tag
      });
    };

    /***/
  },

  /***/"7f7f": /***/function f7f(module, exports, __nested_webpack_require_100866__) {
    var dP = __nested_webpack_require_100866__("86cc").f;
    var FProto = Function.prototype;
    var nameRE = /^\s*function ([^ (]*)/;
    var NAME = 'name';

    // 19.2.4.2 name
    NAME in FProto || __nested_webpack_require_100866__("9e1e") && dP(FProto, NAME, {
      configurable: true,
      get: function get() {
        try {
          return ('' + this).match(nameRE)[1];
        } catch (e) {
          return '';
        }
      }
    });

    /***/
  },

  /***/"8378": /***/function _(module, exports) {
    var core = module.exports = {
      version: '2.6.12'
    };
    if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

    /***/
  },

  /***/"84f2": /***/function f2(module, exports) {
    module.exports = {};

    /***/
  },

  /***/"86cc": /***/function cc(module, exports, __nested_webpack_require_101680__) {
    var anObject = __nested_webpack_require_101680__("cb7c");
    var IE8_DOM_DEFINE = __nested_webpack_require_101680__("c69a");
    var toPrimitive = __nested_webpack_require_101680__("6a99");
    var dP = Object.defineProperty;
    exports.f = __nested_webpack_require_101680__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
      anObject(O);
      P = toPrimitive(P, true);
      anObject(Attributes);
      if (IE8_DOM_DEFINE) try {
        return dP(O, P, Attributes);
      } catch (e) {/* empty */}
      if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
      if ('value' in Attributes) O[P] = Attributes.value;
      return O;
    };

    /***/
  },

  /***/"8b97": /***/function b97(module, exports, __nested_webpack_require_102433__) {
    // Works with __proto__ only. Old v8 can't work with null proto objects.
    /* eslint-disable no-proto */
    var isObject = __nested_webpack_require_102433__("d3f4");
    var anObject = __nested_webpack_require_102433__("cb7c");
    var check = function check(O, proto) {
      anObject(O);
      if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
    };
    module.exports = {
      set: Object.setPrototypeOf || ('__proto__' in {} ?
      // eslint-disable-line
      function (test, buggy, set) {
        try {
          set = __nested_webpack_require_102433__("9b43")(Function.call, __nested_webpack_require_102433__("11e9").f(Object.prototype, '__proto__').set, 2);
          set(test, []);
          buggy = !(test instanceof Array);
        } catch (e) {
          buggy = true;
        }
        return function setPrototypeOf(O, proto) {
          check(O, proto);
          if (buggy) O.__proto__ = proto;else set(O, proto);
          return O;
        };
      }({}, false) : undefined),
      check: check
    };

    /***/
  },

  /***/"8bbf": /***/function bbf(module, exports) {
    module.exports = __webpack_require__(748);

    /***/
  },

  /***/"8e6e": /***/function e6e(module, exports, __nested_webpack_require_103647__) {
    // https://github.com/tc39/proposal-object-getownpropertydescriptors
    var $export = __nested_webpack_require_103647__("5ca1");
    var ownKeys = __nested_webpack_require_103647__("990b");
    var toIObject = __nested_webpack_require_103647__("6821");
    var gOPD = __nested_webpack_require_103647__("11e9");
    var createProperty = __nested_webpack_require_103647__("f1ae");
    $export($export.S, 'Object', {
      getOwnPropertyDescriptors: function getOwnPropertyDescriptors(object) {
        var O = toIObject(object);
        var getDesc = gOPD.f;
        var keys = ownKeys(O);
        var result = {};
        var i = 0;
        var key, desc;
        while (keys.length > i) {
          desc = getDesc(O, key = keys[i++]);
          if (desc !== undefined) createProperty(result, key, desc);
        }
        return result;
      }
    });

    /***/
  },

  /***/"9093": /***/function _(module, exports, __nested_webpack_require_104523__) {
    // 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
    var $keys = __nested_webpack_require_104523__("ce10");
    var hiddenKeys = __nested_webpack_require_104523__("e11e").concat('length', 'prototype');
    exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
      return $keys(O, hiddenKeys);
    };

    /***/
  },

  /***/"97a7": /***/function a7(module, __nested_webpack_exports__, __nested_webpack_require_104928__) {
    "use strict";

    /* harmony export (binding) */
    __nested_webpack_require_104928__.d(__nested_webpack_exports__, "b", function () {
      return getBreakpointFromWidth;
    });
    /* harmony export (binding) */
    __nested_webpack_require_104928__.d(__nested_webpack_exports__, "c", function () {
      return getColsFromBreakpoint;
    });
    /* harmony export (binding) */
    __nested_webpack_require_104928__.d(__nested_webpack_exports__, "a", function () {
      return findOrGenerateResponsiveLayout;
    });
    /* unused harmony export generateResponsiveLayout */
    /* unused harmony export sortBreakpoints */
    /* harmony import */
    var core_js_modules_es6_array_sort__WEBPACK_IMPORTED_MODULE_0__ = __nested_webpack_require_104928__("55dd");
    /* harmony import */
    var core_js_modules_es6_array_sort__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__nested_webpack_require_104928__.n(core_js_modules_es6_array_sort__WEBPACK_IMPORTED_MODULE_0__);
    /* harmony import */
    var core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_1__ = __nested_webpack_require_104928__("ac6a");
    /* harmony import */
    var core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__nested_webpack_require_104928__.n(core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */
    var core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_2__ = __nested_webpack_require_104928__("cadf");
    /* harmony import */
    var core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__nested_webpack_require_104928__.n(core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */
    var core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_3__ = __nested_webpack_require_104928__("456d");
    /* harmony import */
    var core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__nested_webpack_require_104928__.n(core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */
    var _utils__WEBPACK_IMPORTED_MODULE_4__ = __nested_webpack_require_104928__("a2b6");

    // @flow

    /*:: import type {Layout} from './utils';*/

    /*:: export type ResponsiveLayout = {lg?: Layout, md?: Layout, sm?: Layout, xs?: Layout, xxs?: Layout};*/

    /*:: type Breakpoint = string;*/

    /**
     * Given a width, find the highest breakpoint that matches is valid for it (width > breakpoint).
     *
     * @param  {Object} breakpoints Breakpoints object (e.g. {lg: 1200, md: 960, ...})
     * @param  {Number} width Screen width.
     * @return {String}       Highest breakpoint that is less than width.
     */

    /*:: type Breakpoints = {lg?: number, md?: number, sm?: number, xs?: number, xxs?: number};*/

    function getBreakpointFromWidth(breakpoints
    /*: Breakpoints*/, width
    /*: number*/) /*: Breakpoint*/
    {
      var sorted = sortBreakpoints(breakpoints);
      var matching = sorted[0];
      for (var i = 1, len = sorted.length; i < len; i++) {
        var breakpointName = sorted[i];
        if (width > breakpoints[breakpointName]) matching = breakpointName;
      }
      return matching;
    }
    /**
     * Given a breakpoint, get the # of cols set for it.
     * @param  {String} breakpoint Breakpoint name.
     * @param  {Object} cols       Map of breakpoints to cols.
     * @return {Number}            Number of cols.
     */

    function getColsFromBreakpoint(breakpoint
    /*: Breakpoint*/, cols
    /*: Breakpoints*/) /*: number*/
    {
      if (!cols[breakpoint]) {
        throw new Error("ResponsiveGridLayout: `cols` entry for breakpoint " + breakpoint + " is missing!");
      }
      return cols[breakpoint];
    }
    /**
     * Given existing layouts and a new breakpoint, find or generate a new layout.
     *
     * This finds the layout above the new one and generates from it, if it exists.
     *
     * @param  {Array} orgLayout     Original layout.
     * @param  {Object} layouts     Existing layouts.
     * @param  {Array} breakpoints All breakpoints.
     * @param  {String} breakpoint New breakpoint.
     * @param  {String} breakpoint Last breakpoint (for fallback).
     * @param  {Number} cols       Column count at new breakpoint.
     * @param  {Boolean} verticalCompact Whether or not to compact the layout
     *   vertically.
     * @return {Array}             New layout.
     */

    function findOrGenerateResponsiveLayout(orgLayout
    /*: Layout*/, layouts
    /*: ResponsiveLayout*/, breakpoints
    /*: Breakpoints*/, breakpoint
    /*: Breakpoint*/, lastBreakpoint
    /*: Breakpoint*/, cols
    /*: number*/, verticalCompact
    /*: boolean*/) /*: Layout*/
    {
      // If it already exists, just return it.
      if (layouts[breakpoint]) return Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* cloneLayout */"b"])(layouts[breakpoint]); // Find or generate the next layout

      var layout = orgLayout;
      var breakpointsSorted = sortBreakpoints(breakpoints);
      var breakpointsAbove = breakpointsSorted.slice(breakpointsSorted.indexOf(breakpoint));
      for (var i = 0, len = breakpointsAbove.length; i < len; i++) {
        var b = breakpointsAbove[i];
        if (layouts[b]) {
          layout = layouts[b];
          break;
        }
      }
      layout = Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* cloneLayout */"b"])(layout || []); // clone layout so we don't modify existing items

      return Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* compact */"c"])(Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* correctBounds */"d"])(layout, {
        cols: cols
      }), verticalCompact);
    }
    function generateResponsiveLayout(layout
    /*: Layout*/, breakpoints
    /*: Breakpoints*/, breakpoint
    /*: Breakpoint*/, lastBreakpoint
    /*: Breakpoint*/, cols
    /*: number*/, verticalCompact
    /*: boolean*/) /*: Layout*/
    {
      // If it already exists, just return it.

      /*if (layouts[breakpoint]) return cloneLayout(layouts[breakpoint]);
      // Find or generate the next layout
      let layout = layouts[lastBreakpoint];*/

      /*const breakpointsSorted = sortBreakpoints(breakpoints);
      const breakpointsAbove = breakpointsSorted.slice(breakpointsSorted.indexOf(breakpoint));
      for (let i = 0, len = breakpointsAbove.length; i < len; i++) {
      const b = breakpointsAbove[i];
      if (layouts[b]) {
        layout = layouts[b];
        break;
      }
      }*/
      layout = Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* cloneLayout */"b"])(layout || []); // clone layout so we don't modify existing items

      return Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* compact */"c"])(Object(_utils__WEBPACK_IMPORTED_MODULE_4__[/* correctBounds */"d"])(layout, {
        cols: cols
      }), verticalCompact);
    }
    /**
     * Given breakpoints, return an array of breakpoints sorted by width. This is usually
     * e.g. ['xxs', 'xs', 'sm', ...]
     *
     * @param  {Object} breakpoints Key/value pair of breakpoint names to widths.
     * @return {Array}              Sorted breakpoints.
     */

    function sortBreakpoints(breakpoints
    /*: Breakpoints*/) /*: Array<Breakpoint>*/
    {
      var keys
      /*: Array<string>*/ = Object.keys(breakpoints);
      return keys.sort(function (a, b) {
        return breakpoints[a] - breakpoints[b];
      });
    }

    /***/
  },

  /***/"990b": /***/function b(module, exports, __nested_webpack_require_112276__) {
    // all object keys, includes non-enumerable and symbols
    var gOPN = __nested_webpack_require_112276__("9093");
    var gOPS = __nested_webpack_require_112276__("2621");
    var anObject = __nested_webpack_require_112276__("cb7c");
    var Reflect = __nested_webpack_require_112276__("7726").Reflect;
    module.exports = Reflect && Reflect.ownKeys || function ownKeys(it) {
      var keys = gOPN.f(anObject(it));
      var getSymbols = gOPS.f;
      return getSymbols ? keys.concat(getSymbols(it)) : keys;
    };

    /***/
  },

  /***/"9b43": /***/function b43(module, exports, __nested_webpack_require_112830__) {
    // optional / simple context binding
    var aFunction = __nested_webpack_require_112830__("d8e8");
    module.exports = function (fn, that, length) {
      aFunction(fn);
      if (that === undefined) return fn;
      switch (length) {
        case 1:
          return function (a) {
            return fn.call(that, a);
          };
        case 2:
          return function (a, b) {
            return fn.call(that, a, b);
          };
        case 3:
          return function (a, b, c) {
            return fn.call(that, a, b, c);
          };
      }
      return function /* ...args */
      () {
        return fn.apply(that, arguments);
      };
    };

    /***/
  },

  /***/"9c6c": /***/function c6c(module, exports, __nested_webpack_require_113572__) {
    // 22.1.3.31 Array.prototype[@@unscopables]
    var UNSCOPABLES = __nested_webpack_require_113572__("2b4c")('unscopables');
    var ArrayProto = Array.prototype;
    if (ArrayProto[UNSCOPABLES] == undefined) __nested_webpack_require_113572__("32e9")(ArrayProto, UNSCOPABLES, {});
    module.exports = function (key) {
      ArrayProto[UNSCOPABLES][key] = true;
    };

    /***/
  },

  /***/"9cbe": /***/function cbe(module, exports, __nested_webpack_require_114006__) {
    exports = module.exports = __nested_webpack_require_114006__("2350")(false);
    // imports

    // module
    exports.push([module.i, ".vue-grid-item{-webkit-transition:all .2s ease;transition:all .2s ease;-webkit-transition-property:left,top,right;transition-property:left,top,right}.vue-grid-item.no-touch{-ms-touch-action:none;touch-action:none}.vue-grid-item.cssTransforms{-webkit-transition-property:-webkit-transform;transition-property:-webkit-transform;transition-property:transform;transition-property:transform,-webkit-transform;left:0;right:auto}.vue-grid-item.cssTransforms.render-rtl{left:auto;right:0}.vue-grid-item.resizing{opacity:.6;z-index:3}.vue-grid-item.vue-draggable-dragging{-webkit-transition:none;transition:none;z-index:3}.vue-grid-item.vue-grid-placeholder{background:red;opacity:.2;-webkit-transition-duration:.1s;transition-duration:.1s;z-index:2;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-o-user-select:none;user-select:none}.vue-grid-item>.vue-resizable-handle{position:absolute;width:20px;height:20px;bottom:0;right:0;background:url(\"data:image/svg+xml;base64,PHN2ZyBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjojZmZmZmZmMDAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjYiIGhlaWdodD0iNiI+PHBhdGggZD0iTTYgNkgwVjQuMmg0LjJWMEg2djZ6IiBvcGFjaXR5PSIuMzAyIi8+PC9zdmc+\");background-position:100% 100%;padding:0 3px 3px 0;background-repeat:no-repeat;background-origin:content-box;-webkit-box-sizing:border-box;box-sizing:border-box;cursor:se-resize}.vue-grid-item>.vue-rtl-resizable-handle{bottom:0;left:0;background:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAiIGhlaWdodD0iMTAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZmlsbD0ibm9uZSIgZD0iTS0xLTFoMTJ2MTJILTF6Ii8+PGc+PHBhdGggc3Ryb2tlLWxpbmVjYXA9InVuZGVmaW5lZCIgc3Ryb2tlLWxpbmVqb2luPSJ1bmRlZmluZWQiIHN0cm9rZS13aWR0aD0iMS41IiBzdHJva2U9IiMwMDAiIGZpbGw9Im5vbmUiIGQ9Ik0xNDQuODIxLTM4LjM5M2wtMjAuMzU3LTMxLjc4NSIvPjxwYXRoIHN0cm9rZT0iIzY2NiIgc3Ryb2tlLWxpbmVjYXA9InVuZGVmaW5lZCIgc3Ryb2tlLWxpbmVqb2luPSJ1bmRlZmluZWQiIHN0cm9rZS13aWR0aD0iMiIgZmlsbD0ibm9uZSIgZD0iTS45NDctLjAxOHY5LjEyNU0tLjY1NiA5aDEwLjczIi8+PC9nPjwvc3ZnPg==);background-position:0 100%;padding-left:3px;background-repeat:no-repeat;background-origin:content-box;cursor:sw-resize;right:auto}.vue-grid-item.disable-userselect{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}", ""]);

    // exports

    /***/
  },

  /***/"9def": /***/function def(module, exports, __nested_webpack_require_116493__) {
    // 7.1.15 ToLength
    var toInteger = __nested_webpack_require_116493__("4588");
    var min = Math.min;
    module.exports = function (it) {
      return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
    };

    /***/
  },

  /***/"9e1e": /***/function e1e(module, exports, __nested_webpack_require_116825__) {
    // Thank's IE8 for his funny defineProperty
    module.exports = !__nested_webpack_require_116825__("79e5")(function () {
      return Object.defineProperty({}, 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });

    /***/
  },

  /***/"a2b6": /***/function a2b6(module, __nested_webpack_exports__, __nested_webpack_require_117171__) {
    "use strict";

    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "a", function () {
      return bottom;
    });
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "b", function () {
      return cloneLayout;
    });
    /* unused harmony export cloneLayoutItem */
    /* unused harmony export collides */
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "c", function () {
      return compact;
    });
    /* unused harmony export compactItem */
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "d", function () {
      return correctBounds;
    });
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "f", function () {
      return getLayoutItem;
    });
    /* unused harmony export getFirstCollision */
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "e", function () {
      return getAllCollisions;
    });
    /* unused harmony export getStatics */
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "g", function () {
      return moveElement;
    });
    /* unused harmony export moveElementAwayFromCollision */
    /* unused harmony export perc */
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "j", function () {
      return setTransform;
    });
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "k", function () {
      return setTransformRtl;
    });
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "h", function () {
      return setTopLeft;
    });
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "i", function () {
      return setTopRight;
    });
    /* unused harmony export sortLayoutItemsByRowCol */
    /* harmony export (binding) */
    __nested_webpack_require_117171__.d(__nested_webpack_exports__, "l", function () {
      return validateLayout;
    });
    /* unused harmony export autoBindHandlers */
    /* unused harmony export createMarkup */
    /* unused harmony export IS_UNITLESS */
    /* unused harmony export addPx */
    /* unused harmony export hyphenateRE */
    /* unused harmony export hyphenate */
    /* unused harmony export findItemInArray */
    /* unused harmony export findAndRemove */
    /* harmony import */
    var core_js_modules_es6_regexp_replace__WEBPACK_IMPORTED_MODULE_0__ = __nested_webpack_require_117171__("a481");
    /* harmony import */
    var core_js_modules_es6_regexp_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__nested_webpack_require_117171__.n(core_js_modules_es6_regexp_replace__WEBPACK_IMPORTED_MODULE_0__);
    /* harmony import */
    var core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_1__ = __nested_webpack_require_117171__("cadf");
    /* harmony import */
    var core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__nested_webpack_require_117171__.n(core_js_modules_es6_array_iterator__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */
    var core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_2__ = __nested_webpack_require_117171__("456d");
    /* harmony import */
    var core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__nested_webpack_require_117171__.n(core_js_modules_es6_object_keys__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */
    var core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_3__ = __nested_webpack_require_117171__("ac6a");
    /* harmony import */
    var core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__nested_webpack_require_117171__.n(core_js_modules_web_dom_iterable__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */
    var core_js_modules_es6_array_sort__WEBPACK_IMPORTED_MODULE_4__ = __nested_webpack_require_117171__("55dd");
    /* harmony import */
    var core_js_modules_es6_array_sort__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__nested_webpack_require_117171__.n(core_js_modules_es6_array_sort__WEBPACK_IMPORTED_MODULE_4__);

    // @flow

    /*:: export type LayoutItemRequired = {w: number, h: number, x: number, y: number, i: string};*/

    /*:: export type LayoutItem = LayoutItemRequired &
                             {minW?: number, minH?: number, maxW?: number, maxH?: number,
                              moved?: boolean, static?: boolean,
                              isDraggable?: ?boolean, isResizable?: ?boolean};*/

    // export type Position = {left: number, top: number, width: number, height: number};

    /*
    export type DragCallbackData = {
      node: HTMLElement,
      x: number, y: number,
      deltaX: number, deltaY: number,
      lastX: number, lastY: number
    };
    */
    // export type DragEvent = {e: Event} & DragCallbackData;

    /*:: export type Layout = Array<LayoutItem>;*/

    // export type ResizeEvent = {e: Event, node: HTMLElement, size: Size};
    // const isProduction = process.env.NODE_ENV === 'production';

    /**
     * Return the bottom coordinate of the layout.
     *
     * @param  {Array} layout Layout array.
     * @return {Number}       Bottom coordinate.
     */

    /*:: export type Size = {width: number, height: number};*/

    function bottom(layout
    /*: Layout*/) /*: number*/
    {
      var max = 0,
        bottomY;
      for (var i = 0, len = layout.length; i < len; i++) {
        bottomY = layout[i].y + layout[i].h;
        if (bottomY > max) max = bottomY;
      }
      return max;
    }
    function cloneLayout(layout
    /*: Layout*/) /*: Layout*/
    {
      var newLayout = Array(layout.length);
      for (var i = 0, len = layout.length; i < len; i++) {
        newLayout[i] = cloneLayoutItem(layout[i]);
      }
      return newLayout;
    } // Fast path to cloning, since this is monomorphic

    function cloneLayoutItem(layoutItem
    /*: LayoutItem*/) /*: LayoutItem*/
    {
      /*return {
        w: layoutItem.w, h: layoutItem.h, x: layoutItem.x, y: layoutItem.y, i: layoutItem.i,
        minW: layoutItem.minW, maxW: layoutItem.maxW, minH: layoutItem.minH, maxH: layoutItem.maxH,
        moved: Boolean(layoutItem.moved), static: Boolean(layoutItem.static),
        // These can be null
        isDraggable: layoutItem.isDraggable, isResizable: layoutItem.isResizable
      };*/
      return JSON.parse(JSON.stringify(layoutItem));
    }
    /**
     * Given two layoutitems, check if they collide.
     *
     * @return {Boolean}   True if colliding.
     */

    function collides(l1
    /*: LayoutItem*/, l2
    /*: LayoutItem*/) /*: boolean*/
    {
      if (l1 === l2) return false; // same element

      if (l1.x + l1.w <= l2.x) return false; // l1 is left of l2

      if (l1.x >= l2.x + l2.w) return false; // l1 is right of l2

      if (l1.y + l1.h <= l2.y) return false; // l1 is above l2

      if (l1.y >= l2.y + l2.h) return false; // l1 is below l2

      return true; // boxes overlap
    }
    /**
     * Given a layout, compact it. This involves going down each y coordinate and removing gaps
     * between items.
     *
     * @param  {Array} layout Layout.
     * @param  {Boolean} verticalCompact Whether or not to compact the layout
     *   vertically.
     * @param {Object} minPositions
     * @return {Array}       Compacted Layout.
     */

    function compact(layout
    /*: Layout*/, verticalCompact
    /*: Boolean*/, minPositions) /*: Layout*/
    {
      // Statics go in the compareWith array right away so items flow around them.
      var compareWith = getStatics(layout); // We go through the items by row and column.

      var sorted = sortLayoutItemsByRowCol(layout); // Holding for new items.

      var out = Array(layout.length);
      for (var i = 0, len = sorted.length; i < len; i++) {
        var l = sorted[i]; // Don't move static elements

        if (!l.static) {
          l = compactItem(compareWith, l, verticalCompact, minPositions); // Add to comparison array. We only collide with items before this one.
          // Statics are already in this array.

          compareWith.push(l);
        } // Add to output array to make sure they still come out in the right order.

        out[layout.indexOf(l)] = l; // Clear moved flag, if it exists.

        l.moved = false;
      }
      return out;
    }
    /**
     * Compact an item in the layout.
     */

    function compactItem(compareWith
    /*: Layout*/, l
    /*: LayoutItem*/, verticalCompact
    /*: boolean*/, minPositions) /*: LayoutItem*/
    {
      if (verticalCompact) {
        // Move the element up as far as it can go without colliding.
        while (l.y > 0 && !getFirstCollision(compareWith, l)) {
          l.y--;
        }
      } else if (minPositions) {
        var minY = minPositions[l.i].y;
        while (l.y > minY && !getFirstCollision(compareWith, l)) {
          l.y--;
        }
      } // Move it down, and keep moving it down if it's colliding.

      var collides;
      while (collides = getFirstCollision(compareWith, l)) {
        l.y = collides.y + collides.h;
      }
      return l;
    }
    /**
     * Given a layout, make sure all elements fit within its bounds.
     *
     * @param  {Array} layout Layout array.
     * @param  {Number} bounds Number of columns.
     */

    function correctBounds(layout
    /*: Layout*/, bounds
    /*: {cols: number}*/) /*: Layout*/
    {
      var collidesWith = getStatics(layout);
      for (var i = 0, len = layout.length; i < len; i++) {
        var l = layout[i]; // Overflows right

        if (l.x + l.w > bounds.cols) l.x = bounds.cols - l.w; // Overflows left

        if (l.x < 0) {
          l.x = 0;
          l.w = bounds.cols;
        }
        if (!l.static) collidesWith.push(l);else {
          // If this is static and collides with other statics, we must move it down.
          // We have to do something nicer than just letting them overlap.
          while (getFirstCollision(collidesWith, l)) {
            l.y++;
          }
        }
      }
      return layout;
    }
    /**
     * Get a layout item by ID. Used so we can override later on if necessary.
     *
     * @param  {Array}  layout Layout array.
     * @param  {String} id     ID
     * @return {LayoutItem}    Item at ID.
     */

    function getLayoutItem(layout
    /*: Layout*/, id
    /*: string*/) /*: ?LayoutItem*/
    {
      for (var i = 0, len = layout.length; i < len; i++) {
        if (layout[i].i === id) return layout[i];
      }
    }
    /**
     * Returns the first item this layout collides with.
     * It doesn't appear to matter which order we approach this from, although
     * perhaps that is the wrong thing to do.
     *
     * @param  {Object} layoutItem Layout item.
     * @return {Object|undefined}  A colliding layout item, or undefined.
     */

    function getFirstCollision(layout
    /*: Layout*/, layoutItem
    /*: LayoutItem*/) /*: ?LayoutItem*/
    {
      for (var i = 0, len = layout.length; i < len; i++) {
        if (collides(layout[i], layoutItem)) return layout[i];
      }
    }
    function getAllCollisions(layout
    /*: Layout*/, layoutItem
    /*: LayoutItem*/) /*: Array<LayoutItem>*/
    {
      return layout.filter(function (l) {
        return collides(l, layoutItem);
      });
    }
    /**
     * Get all static elements.
     * @param  {Array} layout Array of layout objects.
     * @return {Array}        Array of static layout items..
     */

    function getStatics(layout
    /*: Layout*/) /*: Array<LayoutItem>*/
    {
      //return [];
      return layout.filter(function (l) {
        return l.static;
      });
    }
    /**
     * Move an element. Responsible for doing cascading movements of other elements.
     *
     * @param  {Array}      layout Full layout to modify.
     * @param  {LayoutItem} l      element to move.
     * @param  {Number}     [x]    X position in grid units.
     * @param  {Number}     [y]    Y position in grid units.
     * @param  {Boolean}    [isUserAction] If true, designates that the item we're moving is
     *                                     being dragged/resized by th euser.
     */

    function moveElement(layout
    /*: Layout*/, l
    /*: LayoutItem*/, x
    /*: Number*/, y
    /*: Number*/, isUserAction
    /*: Boolean*/, preventCollision
    /*: Boolean*/) /*: Layout*/
    {
      if (l.static) return layout; // Short-circuit if nothing to do.
      //if (l.y === y && l.x === x) return layout;

      var oldX = l.x;
      var oldY = l.y;
      var movingUp = y && l.y > y; // This is quite a bit faster than extending the object

      if (typeof x === 'number') l.x = x;
      if (typeof y === 'number') l.y = y;
      l.moved = true; // If this collides with anything, move it.
      // When doing this comparison, we have to sort the items we compare with
      // to ensure, in the case of multiple collisions, that we're getting the
      // nearest collision.

      var sorted = sortLayoutItemsByRowCol(layout);
      if (movingUp) sorted = sorted.reverse();
      var collisions = getAllCollisions(sorted, l);
      if (preventCollision && collisions.length) {
        l.x = oldX;
        l.y = oldY;
        l.moved = false;
        return layout;
      } // Move each item that collides away from this element.

      for (var i = 0, len = collisions.length; i < len; i++) {
        var collision = collisions[i]; // console.log('resolving collision between', l.i, 'at', l.y, 'and', collision.i, 'at', collision.y);
        // Short circuit so we can't infinite loop

        if (collision.moved) continue; // This makes it feel a bit more precise by waiting to swap for just a bit when moving up.

        if (l.y > collision.y && l.y - collision.y > collision.h / 4) continue; // Don't move static items - we have to move *this* element away

        if (collision.static) {
          layout = moveElementAwayFromCollision(layout, collision, l, isUserAction);
        } else {
          layout = moveElementAwayFromCollision(layout, l, collision, isUserAction);
        }
      }
      return layout;
    }
    /**
     * This is where the magic needs to happen - given a collision, move an element away from the collision.
     * We attempt to move it up if there's room, otherwise it goes below.
     *
     * @param  {Array} layout            Full layout to modify.
     * @param  {LayoutItem} collidesWith Layout item we're colliding with.
     * @param  {LayoutItem} itemToMove   Layout item we're moving.
     * @param  {Boolean} [isUserAction]  If true, designates that the item we're moving is being dragged/resized
     *                                   by the user.
     */

    function moveElementAwayFromCollision(layout
    /*: Layout*/, collidesWith
    /*: LayoutItem*/, itemToMove
    /*: LayoutItem*/, isUserAction
    /*: ?boolean*/) /*: Layout*/
    {
      var preventCollision = false; // we're already colliding
      // If there is enough space above the collision to put this element, move it there.
      // We only do this on the main collision as this can get funky in cascades and cause
      // unwanted swapping behavior.

      if (isUserAction) {
        // Make a mock item so we don't modify the item here, only modify in moveElement.
        var fakeItem
        /*: LayoutItem*/ = {
          x: itemToMove.x,
          y: itemToMove.y,
          w: itemToMove.w,
          h: itemToMove.h,
          i: '-1'
        };
        fakeItem.y = Math.max(collidesWith.y - itemToMove.h, 0);
        if (!getFirstCollision(layout, fakeItem)) {
          return moveElement(layout, itemToMove, undefined, fakeItem.y, preventCollision);
        }
      } // Previously this was optimized to move below the collision directly, but this can cause problems
      // with cascading moves, as an item may actually leapflog a collision and cause a reversal in order.

      return moveElement(layout, itemToMove, undefined, itemToMove.y + 1, preventCollision);
    }
    /**
     * Helper to convert a number to a percentage string.
     *
     * @param  {Number} num Any number
     * @return {String}     That number as a percentage.
     */

    function perc(num
    /*: number*/) /*: string*/
    {
      return num * 100 + '%';
    }
    function setTransform(top, left, width, height) /*: Object*/
    {
      // Replace unitless items with px
      var translate = "translate3d(" + left + "px," + top + "px, 0)";
      return {
        transform: translate,
        WebkitTransform: translate,
        MozTransform: translate,
        msTransform: translate,
        OTransform: translate,
        width: width + "px",
        height: height + "px",
        position: 'absolute'
      };
    }
    /**
     * Just like the setTransform method, but instead it will return a negative value of right.
     *
     * @param top
     * @param right
     * @param width
     * @param height
     * @returns {{transform: string, WebkitTransform: string, MozTransform: string, msTransform: string, OTransform: string, width: string, height: string, position: string}}
     */

    function setTransformRtl(top, right, width, height) /*: Object*/
    {
      // Replace unitless items with px
      var translate = "translate3d(" + right * -1 + "px," + top + "px, 0)";
      return {
        transform: translate,
        WebkitTransform: translate,
        MozTransform: translate,
        msTransform: translate,
        OTransform: translate,
        width: width + "px",
        height: height + "px",
        position: 'absolute'
      };
    }
    function setTopLeft(top, left, width, height) /*: Object*/
    {
      return {
        top: top + "px",
        left: left + "px",
        width: width + "px",
        height: height + "px",
        position: 'absolute'
      };
    }
    /**
     * Just like the setTopLeft method, but instead, it will return a right property instead of left.
     *
     * @param top
     * @param right
     * @param width
     * @param height
     * @returns {{top: string, right: string, width: string, height: string, position: string}}
     */

    function setTopRight(top, right, width, height) /*: Object*/
    {
      return {
        top: top + "px",
        right: right + "px",
        width: width + "px",
        height: height + "px",
        position: 'absolute'
      };
    }
    /**
     * Get layout items sorted from top left to right and down.
     *
     * @return {Array} Array of layout objects.
     * @return {Array}        Layout, sorted static items first.
     */

    function sortLayoutItemsByRowCol(layout
    /*: Layout*/) /*: Layout*/
    {
      return [].concat(layout).sort(function (a, b) {
        if (a.y === b.y && a.x === b.x) {
          return 0;
        }
        if (a.y > b.y || a.y === b.y && a.x > b.x) {
          return 1;
        }
        return -1;
      });
    }
    /**
     * Generate a layout using the initialLayout and children as a template.
     * Missing entries will be added, extraneous ones will be truncated.
     *
     * @param  {Array}  initialLayout Layout passed in through props.
     * @param  {String} breakpoint    Current responsive breakpoint.
     * @param  {Boolean} verticalCompact Whether or not to compact the layout vertically.
     * @return {Array}                Working layout.
     */

    /*
    export function synchronizeLayoutWithChildren(initialLayout: Layout, children: Array<React.Element>|React.Element,
                                                  cols: number, verticalCompact: boolean): Layout {
      // ensure 'children' is always an array
      if (!Array.isArray(children)) {
        children = [children];
      }
      initialLayout = initialLayout || [];
    
      // Generate one layout item per child.
      let layout: Layout = [];
      for (let i = 0, len = children.length; i < len; i++) {
        let newItem;
        const child = children[i];
    
        // Don't overwrite if it already exists.
        const exists = getLayoutItem(initialLayout, child.key || "1" /!* FIXME satisfies Flow *!/);
        if (exists) {
          newItem = exists;
        } else {
          const g = child.props._grid;
    
          // Hey, this item has a _grid property, use it.
          if (g) {
            if (!isProduction) {
              validateLayout([g], 'ReactGridLayout.children');
            }
            // Validated; add it to the layout. Bottom 'y' possible is the bottom of the layout.
            // This allows you to do nice stuff like specify {y: Infinity}
            if (verticalCompact) {
              newItem = cloneLayoutItem({...g, y: Math.min(bottom(layout), g.y), i: child.key});
            } else {
              newItem = cloneLayoutItem({...g, y: g.y, i: child.key});
            }
          }
          // Nothing provided: ensure this is added to the bottom
          else {
            newItem = cloneLayoutItem({w: 1, h: 1, x: 0, y: bottom(layout), i: child.key || "1"});
          }
        }
        layout[i] = newItem;
      }
    
      // Correct the layout.
      layout = correctBounds(layout, {cols: cols});
      layout = compact(layout, verticalCompact);
    
      return layout;
    }
    */

    /**
     * Validate a layout. Throws errors.
     *
     * @param  {Array}  layout        Array of layout items.
     * @param  {String} [contextName] Context name for errors.
     * @throw  {Error}                Validation error.
     */

    function validateLayout(layout
    /*: Layout*/, contextName
    /*: string*/) /*: void*/
    {
      contextName = contextName || "Layout";
      var subProps = ['x', 'y', 'w', 'h'];
      var keyArr = [];
      if (!Array.isArray(layout)) throw new Error(contextName + " must be an array!");
      for (var i = 0, len = layout.length; i < len; i++) {
        var item = layout[i];
        for (var j = 0; j < subProps.length; j++) {
          if (typeof item[subProps[j]] !== 'number') {
            throw new Error('VueGridLayout: ' + contextName + '[' + i + '].' + subProps[j] + ' must be a number!');
          }
        }
        if (item.i === undefined || item.i === null) {
          throw new Error('VueGridLayout: ' + contextName + '[' + i + '].i cannot be null!');
        }
        if (typeof item.i !== 'number' && typeof item.i !== 'string') {
          throw new Error('VueGridLayout: ' + contextName + '[' + i + '].i must be a string or number!');
        }
        if (keyArr.indexOf(item.i) >= 0) {
          throw new Error('VueGridLayout: ' + contextName + '[' + i + '].i must be unique!');
        }
        keyArr.push(item.i);
        if (item.static !== undefined && typeof item.static !== 'boolean') {
          throw new Error('VueGridLayout: ' + contextName + '[' + i + '].static must be a boolean!');
        }
      }
    } // Flow can't really figure this out, so we just use Object

    function autoBindHandlers(el
    /*: Object*/, fns
    /*: Array<string>*/) /*: void*/
    {
      fns.forEach(function (key) {
        return el[key] = el[key].bind(el);
      });
    }
    /**
     * Convert a JS object to CSS string. Similar to React's output of CSS.
     * @param obj
     * @returns {string}
     */

    function createMarkup(obj) {
      var keys = Object.keys(obj);
      if (!keys.length) return '';
      var i,
        len = keys.length;
      var result = '';
      for (i = 0; i < len; i++) {
        var key = keys[i];
        var val = obj[key];
        result += hyphenate(key) + ':' + addPx(key, val) + ';';
      }
      return result;
    }
    /* The following list is defined in React's core */

    var IS_UNITLESS = {
      animationIterationCount: true,
      boxFlex: true,
      boxFlexGroup: true,
      boxOrdinalGroup: true,
      columnCount: true,
      flex: true,
      flexGrow: true,
      flexPositive: true,
      flexShrink: true,
      flexNegative: true,
      flexOrder: true,
      gridRow: true,
      gridColumn: true,
      fontWeight: true,
      lineClamp: true,
      lineHeight: true,
      opacity: true,
      order: true,
      orphans: true,
      tabSize: true,
      widows: true,
      zIndex: true,
      zoom: true,
      // SVG-related properties
      fillOpacity: true,
      stopOpacity: true,
      strokeDashoffset: true,
      strokeOpacity: true,
      strokeWidth: true
    };
    /**
     * Will add px to the end of style values which are Numbers.
     * @param name
     * @param value
     * @returns {*}
     */

    function addPx(name, value) {
      if (typeof value === 'number' && !IS_UNITLESS[name]) {
        return value + 'px';
      } else {
        return value;
      }
    }
    /**
     * Hyphenate a camelCase string.
     *
     * @param {String} str
     * @return {String}
     */

    var hyphenateRE = /([a-z\d])([A-Z])/g;
    function hyphenate(str) {
      return str.replace(hyphenateRE, '$1-$2').toLowerCase();
    }
    function findItemInArray(array, property, value) {
      for (var i = 0; i < array.length; i++) {
        if (array[i][property] == value) return true;
      }
      return false;
    }
    function findAndRemove(array, property, value) {
      array.forEach(function (result, index) {
        if (result[property] === value) {
          //Remove from array
          array.splice(index, 1);
        }
      });
    }

    /***/
  },

  /***/"a481": /***/function a481(module, exports, __nested_webpack_require_142722__) {
    "use strict";

    var anObject = __nested_webpack_require_142722__("cb7c");
    var toObject = __nested_webpack_require_142722__("4bf8");
    var toLength = __nested_webpack_require_142722__("9def");
    var toInteger = __nested_webpack_require_142722__("4588");
    var advanceStringIndex = __nested_webpack_require_142722__("0390");
    var regExpExec = __nested_webpack_require_142722__("5f1b");
    var max = Math.max;
    var min = Math.min;
    var floor = Math.floor;
    var SUBSTITUTION_SYMBOLS = /\$([$&`']|\d\d?|<[^>]*>)/g;
    var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&`']|\d\d?)/g;
    var maybeToString = function maybeToString(it) {
      return it === undefined ? it : String(it);
    };

    // @@replace logic
    __nested_webpack_require_142722__("214f")('replace', 2, function (defined, REPLACE, $replace, maybeCallNative) {
      return [
      // `String.prototype.replace` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.replace
      function replace(searchValue, replaceValue) {
        var O = defined(this);
        var fn = searchValue == undefined ? undefined : searchValue[REPLACE];
        return fn !== undefined ? fn.call(searchValue, O, replaceValue) : $replace.call(String(O), searchValue, replaceValue);
      },
      // `RegExp.prototype[@@replace]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@replace
      function (regexp, replaceValue) {
        var res = maybeCallNative($replace, regexp, this, replaceValue);
        if (res.done) return res.value;
        var rx = anObject(regexp);
        var S = String(this);
        var functionalReplace = typeof replaceValue === 'function';
        if (!functionalReplace) replaceValue = String(replaceValue);
        var global = rx.global;
        if (global) {
          var fullUnicode = rx.unicode;
          rx.lastIndex = 0;
        }
        var results = [];
        while (true) {
          var result = regExpExec(rx, S);
          if (result === null) break;
          results.push(result);
          if (!global) break;
          var matchStr = String(result[0]);
          if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        }
        var accumulatedResult = '';
        var nextSourcePosition = 0;
        for (var i = 0; i < results.length; i++) {
          result = results[i];
          var matched = String(result[0]);
          var position = max(min(toInteger(result.index), S.length), 0);
          var captures = [];
          // NOTE: This is equivalent to
          //   captures = result.slice(1).map(maybeToString)
          // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
          // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
          // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
          for (var j = 1; j < result.length; j++) captures.push(maybeToString(result[j]));
          var namedCaptures = result.groups;
          if (functionalReplace) {
            var replacerArgs = [matched].concat(captures, position, S);
            if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
            var replacement = String(replaceValue.apply(undefined, replacerArgs));
          } else {
            replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
          }
          if (position >= nextSourcePosition) {
            accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
            nextSourcePosition = position + matched.length;
          }
        }
        return accumulatedResult + S.slice(nextSourcePosition);
      }];

      // https://tc39.github.io/ecma262/#sec-getsubstitution
      function getSubstitution(matched, str, position, captures, namedCaptures, replacement) {
        var tailPos = position + matched.length;
        var m = captures.length;
        var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
        if (namedCaptures !== undefined) {
          namedCaptures = toObject(namedCaptures);
          symbols = SUBSTITUTION_SYMBOLS;
        }
        return $replace.call(replacement, symbols, function (match, ch) {
          var capture;
          switch (ch.charAt(0)) {
            case '$':
              return '$';
            case '&':
              return matched;
            case '`':
              return str.slice(0, position);
            case "'":
              return str.slice(tailPos);
            case '<':
              capture = namedCaptures[ch.slice(1, -1)];
              break;
            default:
              // \d\d?
              var n = +ch;
              if (n === 0) return match;
              if (n > m) {
                var f = floor(n / 10);
                if (f === 0) return match;
                if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
                return match;
              }
              capture = captures[n - 1];
          }
          return capture === undefined ? '' : capture;
        });
      }
    });

    /***/
  },

  /***/"aa77": /***/function aa77(module, exports, __nested_webpack_require_147858__) {
    var $export = __nested_webpack_require_147858__("5ca1");
    var defined = __nested_webpack_require_147858__("be13");
    var fails = __nested_webpack_require_147858__("79e5");
    var spaces = __nested_webpack_require_147858__("fdef");
    var space = '[' + spaces + ']';
    var non = '\u200b\u0085';
    var ltrim = RegExp('^' + space + space + '*');
    var rtrim = RegExp(space + space + '*$');
    var exporter = function exporter(KEY, exec, ALIAS) {
      var exp = {};
      var FORCE = fails(function () {
        return !!spaces[KEY]() || non[KEY]() != non;
      });
      var fn = exp[KEY] = FORCE ? exec(trim) : spaces[KEY];
      if (ALIAS) exp[ALIAS] = fn;
      $export($export.P + $export.F * FORCE, 'String', exp);
    };

    // 1 -> String#trimLeft
    // 2 -> String#trimRight
    // 3 -> String#trim
    var trim = exporter.trim = function (string, TYPE) {
      string = String(defined(string));
      if (TYPE & 1) string = string.replace(ltrim, '');
      if (TYPE & 2) string = string.replace(rtrim, '');
      return string;
    };
    module.exports = exporter;

    /***/
  },

  /***/"abb4": /***/function abb4(module, exports, __webpack_require__) {
    "use strict";

    /* global console: false */

    /**
     * Reporter that handles the reporting of logs, warnings and errors.
     * @public
     * @param {boolean} quiet Tells if the reporter should be quiet or not.
     */
    module.exports = function (quiet) {
      function noop() {
        //Does nothing.
      }
      var reporter = {
        log: noop,
        warn: noop,
        error: noop
      };
      if (!quiet && window.console) {
        var attachFunction = function attachFunction(reporter, name) {
          //The proxy is needed to be able to call the method with the console context,
          //since we cannot use bind.
          reporter[name] = function reporterProxy() {
            var f = console[name];
            if (f.apply) {
              //IE9 does not support console.log.apply :)
              f.apply(console, arguments);
            } else {
              for (var i = 0; i < arguments.length; i++) {
                f(arguments[i]);
              }
            }
          };
        };
        attachFunction(reporter, "log");
        attachFunction(reporter, "warn");
        attachFunction(reporter, "error");
      }
      return reporter;
    };

    /***/
  },

  /***/"ac6a": /***/function ac6a(module, exports, __nested_webpack_require_150280__) {
    var $iterators = __nested_webpack_require_150280__("cadf");
    var getKeys = __nested_webpack_require_150280__("0d58");
    var redefine = __nested_webpack_require_150280__("2aba");
    var global = __nested_webpack_require_150280__("7726");
    var hide = __nested_webpack_require_150280__("32e9");
    var Iterators = __nested_webpack_require_150280__("84f2");
    var wks = __nested_webpack_require_150280__("2b4c");
    var ITERATOR = wks('iterator');
    var TO_STRING_TAG = wks('toStringTag');
    var ArrayValues = Iterators.Array;
    var DOMIterables = {
      CSSRuleList: true,
      // TODO: Not spec compliant, should be false.
      CSSStyleDeclaration: false,
      CSSValueList: false,
      ClientRectList: false,
      DOMRectList: false,
      DOMStringList: false,
      DOMTokenList: true,
      DataTransferItemList: false,
      FileList: false,
      HTMLAllCollection: false,
      HTMLCollection: false,
      HTMLFormElement: false,
      HTMLSelectElement: false,
      MediaList: true,
      // TODO: Not spec compliant, should be false.
      MimeTypeArray: false,
      NamedNodeMap: false,
      NodeList: true,
      PaintRequestList: false,
      Plugin: false,
      PluginArray: false,
      SVGLengthList: false,
      SVGNumberList: false,
      SVGPathSegList: false,
      SVGPointList: false,
      SVGStringList: false,
      SVGTransformList: false,
      SourceBufferList: false,
      StyleSheetList: true,
      // TODO: Not spec compliant, should be false.
      TextTrackCueList: false,
      TextTrackList: false,
      TouchList: false
    };
    for (var collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
      var NAME = collections[i];
      var explicit = DOMIterables[NAME];
      var Collection = global[NAME];
      var proto = Collection && Collection.prototype;
      var key;
      if (proto) {
        if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues);
        if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
        Iterators[NAME] = ArrayValues;
        if (explicit) for (key in $iterators) if (!proto[key]) redefine(proto, key, $iterators[key], true);
      }
    }

    /***/
  },

  /***/"ad20": /***/function ad20(module, exports, __nested_webpack_require_152454__) {
    exports = module.exports = __nested_webpack_require_152454__("2350")(false);
    // imports

    // module
    exports.push([module.i, ".vue-grid-layout{position:relative;-webkit-transition:height .2s ease;transition:height .2s ease}", ""]);

    // exports

    /***/
  },

  /***/"ade3": /***/function ade3(module, __nested_webpack_exports__, __nested_webpack_require_152805__) {
    "use strict";

    /* harmony export (binding) */
    __nested_webpack_require_152805__.d(__nested_webpack_exports__, "a", function () {
      return _defineProperty;
    });
    function _defineProperty(obj, key, value) {
      if (key in obj) {
        Object.defineProperty(obj, key, {
          value: value,
          enumerable: true,
          configurable: true,
          writable: true
        });
      } else {
        obj[key] = value;
      }
      return obj;
    }

    /***/
  },

  /***/"b0c5": /***/function b0c5(module, exports, __nested_webpack_require_153360__) {
    "use strict";

    var regexpExec = __nested_webpack_require_153360__("520a");
    __nested_webpack_require_153360__("5ca1")({
      target: 'RegExp',
      proto: true,
      forced: regexpExec !== /./.exec
    }, {
      exec: regexpExec
    });

    /***/
  },

  /***/"b770": /***/function b770(module, exports, __webpack_require__) {
    "use strict";

    var utils = module.exports = {};

    /**
     * Loops through the collection and calls the callback for each element. if the callback returns truthy, the loop is broken and returns the same value.
     * @public
     * @param {*} collection The collection to loop through. Needs to have a length property set and have indices set from 0 to length - 1.
     * @param {function} callback The callback to be called for each element. The element will be given as a parameter to the callback. If this callback returns truthy, the loop is broken and the same value is returned.
     * @returns {*} The value that a callback has returned (if truthy). Otherwise nothing.
     */
    utils.forEach = function (collection, callback) {
      for (var i = 0; i < collection.length; i++) {
        var result = callback(collection[i]);
        if (result) {
          return result;
        }
      }
    };

    /***/
  },

  /***/"bc21": /***/function bc21(module, __nested_webpack_exports__, __nested_webpack_require_154697__) {
    "use strict";

    // NAMESPACE OBJECT: ./node_modules/@interactjs/snappers/all.js
    var all_namespaceObject = {};
    __nested_webpack_require_154697__.r(all_namespaceObject);
    __nested_webpack_require_154697__.d(all_namespaceObject, "edgeTarget", function () {
      return edgeTarget;
    });
    __nested_webpack_require_154697__.d(all_namespaceObject, "elements", function () {
      return snappers_elements;
    });
    __nested_webpack_require_154697__.d(all_namespaceObject, "grid", function () {
      return grid;
    });

    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"1705dc22-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/GridItem.vue?vue&type=template&id=e7489122&
    var render = function render() {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c('div', {
        ref: "item",
        staticClass: "vue-grid-item",
        class: _vm.classObj,
        style: _vm.style
      }, [_vm._t("default"), _vm.resizableAndNotStatic ? _c('span', {
        ref: "handle",
        class: _vm.resizableHandleClass
      }) : _vm._e()], 2);
    };
    var staticRenderFns = [];

    // CONCATENATED MODULE: ./src/components/GridItem.vue?vue&type=template&id=e7489122&

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es7.object.get-own-property-descriptors.js
    var es7_object_get_own_property_descriptors = __nested_webpack_require_154697__("8e6e");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom.iterable.js
    var web_dom_iterable = __nested_webpack_require_154697__("ac6a");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.iterator.js
    var es6_array_iterator = __nested_webpack_require_154697__("cadf");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.object.keys.js
    var es6_object_keys = __nested_webpack_require_154697__("456d");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.regexp.replace.js
    var es6_regexp_replace = __nested_webpack_require_154697__("a481");

    // EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/defineProperty.js
    var defineProperty = __nested_webpack_require_154697__("ade3");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.is-finite.js
    var es6_number_is_finite = __nested_webpack_require_154697__("fca0");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.regexp.match.js
    var es6_regexp_match = __nested_webpack_require_154697__("4917");

    // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.constructor.js
    var es6_number_constructor = __nested_webpack_require_154697__("c5f6");

    // EXTERNAL MODULE: ./src/helpers/utils.js
    var utils = __nested_webpack_require_154697__("a2b6");

    // CONCATENATED MODULE: ./src/helpers/draggableUtils.js
    // Get {x, y} positions from event.
    function getControlPosition(e) {
      return offsetXYFromParentOf(e);
    } // Get from offsetParent

    function offsetXYFromParentOf(evt) {
      var offsetParent = evt.target.offsetParent || document.body;
      var offsetParentRect = evt.offsetParent === document.body ? {
        left: 0,
        top: 0
      } : offsetParent.getBoundingClientRect();
      var x = evt.clientX + offsetParent.scrollLeft - offsetParentRect.left;
      var y = evt.clientY + offsetParent.scrollTop - offsetParentRect.top;
      /*const x = Math.round(evt.clientX + offsetParent.scrollLeft - offsetParentRect.left);
      const y = Math.round(evt.clientY + offsetParent.scrollTop - offsetParentRect.top);*/

      return {
        x: x,
        y: y
      };
    } // Create an data object exposed by <DraggableCore>'s events

    function createCoreData(lastX, lastY, x, y) {
      // State changes are often (but not always!) async. We want the latest value.
      var isStart = !isNum(lastX);
      if (isStart) {
        // If this is our first move, use the x and y as last coords.
        return {
          deltaX: 0,
          deltaY: 0,
          lastX: x,
          lastY: y,
          x: x,
          y: y
        };
      } else {
        // Otherwise calculate proper values.
        return {
          deltaX: x - lastX,
          deltaY: y - lastY,
          lastX: lastX,
          lastY: lastY,
          x: x,
          y: y
        };
      }
    }
    function isNum(num) {
      return typeof num === 'number' && !isNaN(num);
    }
    // EXTERNAL MODULE: ./src/helpers/responsiveUtils.js
    var responsiveUtils = __nested_webpack_require_154697__("97a7");

    // EXTERNAL MODULE: ./src/helpers/DOM.js
    var DOM = __nested_webpack_require_154697__("1ca7");

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/domObjects.js
    const domObjects = {
      init,
      document: null,
      DocumentFragment: null,
      SVGElement: null,
      SVGSVGElement: null,
      SVGElementInstance: null,
      Element: null,
      HTMLElement: null,
      Event: null,
      Touch: null,
      PointerEvent: null
    };
    function blank() {}

    /* harmony default export */
    var utils_domObjects = domObjects;
    function init(window) {
      const win = window;
      domObjects.document = win.document;
      domObjects.DocumentFragment = win.DocumentFragment || blank;
      domObjects.SVGElement = win.SVGElement || blank;
      domObjects.SVGSVGElement = win.SVGSVGElement || blank;
      domObjects.SVGElementInstance = win.SVGElementInstance || blank;
      domObjects.Element = win.Element || blank;
      domObjects.HTMLElement = win.HTMLElement || domObjects.Element;
      domObjects.Event = win.Event;
      domObjects.Touch = win.Touch || blank;
      domObjects.PointerEvent = win.PointerEvent || win.MSPointerEvent;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/isWindow.js
    /* harmony default export */
    var isWindow = thing => !!(thing && thing.Window) && thing instanceof thing.Window;

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/window.js

    let realWindow = undefined;
    let win = undefined;
    function window_init(window) {
      // get wrapped window if using Shadow DOM polyfill
      realWindow = window; // create a TextNode

      const el = window.document.createTextNode(''); // check if it's wrapped by a polyfill

      if (el.ownerDocument !== window.document && typeof window.wrap === 'function' && window.wrap(el) === el) {
        // use wrapped window
        window = window.wrap(window);
      }
      win = window;
    }
    if (typeof window !== 'undefined' && !!window) {
      window_init(window);
    }
    function getWindow(node) {
      if (isWindow(node)) {
        return node;
      }
      const rootNode = node.ownerDocument || node;
      return rootNode.defaultView || win.window;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/is.js

    const is_window = thing => thing === win || isWindow(thing);
    const docFrag = thing => object(thing) && thing.nodeType === 11;
    const object = thing => !!thing && typeof thing === 'object';
    const func = thing => typeof thing === 'function';
    const number = thing => typeof thing === 'number';
    const bool = thing => typeof thing === 'boolean';
    const string = thing => typeof thing === 'string';
    const is_element = thing => {
      if (!thing || typeof thing !== 'object') {
        return false;
      } // eslint-disable-next-line import/no-named-as-default-member

      const _window = getWindow(thing) || win;
      return /object|function/.test(typeof _window.Element) ? thing instanceof _window.Element // DOM2
      : thing.nodeType === 1 && typeof thing.nodeName === 'string';
    };
    const plainObject = thing => object(thing) && !!thing.constructor && /function Object\b/.test(thing.constructor.toString());
    const array = thing => object(thing) && typeof thing.length !== 'undefined' && func(thing.splice);

    /* harmony default export */
    var is = {
      window: is_window,
      docFrag,
      object,
      func,
      number,
      bool,
      string,
      element: is_element,
      plainObject,
      array
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/browser.js

    const browser = {
      init: browser_init,
      supportsTouch: null,
      supportsPointerEvent: null,
      isIOS7: null,
      isIOS: null,
      isIe9: null,
      isOperaMobile: null,
      prefixedMatchesSelector: null,
      pEventTypes: null,
      wheelEvent: null
    };
    function browser_init(window) {
      const Element = utils_domObjects.Element;
      const navigator = win.navigator; // Does the browser support touch input?

      browser.supportsTouch = 'ontouchstart' in window || is.func(window.DocumentTouch) && utils_domObjects.document instanceof window.DocumentTouch; // Does the browser support PointerEvents

      browser.supportsPointerEvent = navigator.pointerEnabled !== false && !!utils_domObjects.PointerEvent;
      browser.isIOS = /iP(hone|od|ad)/.test(navigator.platform); // scrolling doesn't change the result of getClientRects on iOS 7

      browser.isIOS7 = /iP(hone|od|ad)/.test(navigator.platform) && /OS 7[^\d]/.test(navigator.appVersion);
      browser.isIe9 = /MSIE 9/.test(navigator.userAgent); // Opera Mobile must be handled differently

      browser.isOperaMobile = navigator.appName === 'Opera' && browser.supportsTouch && /Presto/.test(navigator.userAgent); // prefix matchesSelector

      browser.prefixedMatchesSelector = 'matches' in Element.prototype ? 'matches' : 'webkitMatchesSelector' in Element.prototype ? 'webkitMatchesSelector' : 'mozMatchesSelector' in Element.prototype ? 'mozMatchesSelector' : 'oMatchesSelector' in Element.prototype ? 'oMatchesSelector' : 'msMatchesSelector';
      browser.pEventTypes = browser.supportsPointerEvent ? utils_domObjects.PointerEvent === window.MSPointerEvent ? {
        up: 'MSPointerUp',
        down: 'MSPointerDown',
        over: 'mouseover',
        out: 'mouseout',
        move: 'MSPointerMove',
        cancel: 'MSPointerCancel'
      } : {
        up: 'pointerup',
        down: 'pointerdown',
        over: 'pointerover',
        out: 'pointerout',
        move: 'pointermove',
        cancel: 'pointercancel'
      } : null; // because Webkit and Opera still use 'mousewheel' event type

      browser.wheelEvent = 'onmousewheel' in utils_domObjects.document ? 'mousewheel' : 'wheel';
    }

    /* harmony default export */
    var utils_browser = browser;

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/arr.js
    const contains = (array, target) => array.indexOf(target) !== -1;
    const arr_remove = (array, target) => array.splice(array.indexOf(target), 1);
    const merge = (target, source) => {
      for (const item of source) {
        target.push(item);
      }
      return target;
    };
    const from = source => merge([], source);
    const findIndex = (array, func) => {
      for (let i = 0; i < array.length; i++) {
        if (func(array[i], i, array)) {
          return i;
        }
      }
      return -1;
    };
    const find = (array, func) => array[findIndex(array, func)];

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/clone.js

    // tslint:disable-next-line ban-types

    function clone(source) {
      const dest = {};
      for (const prop in source) {
        const value = source[prop];
        if (is.plainObject(value)) {
          dest[prop] = clone(value);
        } else if (is.array(value)) {
          dest[prop] = from(value);
        } else {
          dest[prop] = value;
        }
      }
      return dest;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/extend.js
    function extend(dest, source) {
      for (const prop in source) {
        dest[prop] = source[prop];
      }
      const ret = dest;
      return ret;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/raf.js
    let lastTime = 0;
    let _request;
    let _cancel;
    function raf_init(window) {
      _request = window.requestAnimationFrame;
      _cancel = window.cancelAnimationFrame;
      if (!_request) {
        const vendors = ['ms', 'moz', 'webkit', 'o'];
        for (const vendor of vendors) {
          _request = window["".concat(vendor, "RequestAnimationFrame")];
          _cancel = window["".concat(vendor, "CancelAnimationFrame")] || window["".concat(vendor, "CancelRequestAnimationFrame")];
        }
      }
      _request = _request && _request.bind(window);
      _cancel = _cancel && _cancel.bind(window);
      if (!_request) {
        _request = callback => {
          const currTime = Date.now();
          const timeToCall = Math.max(0, 16 - (currTime - lastTime)); // eslint-disable-next-line node/no-callback-literal

          const token = window.setTimeout(() => {
            callback(currTime + timeToCall);
          }, timeToCall);
          lastTime = currTime + timeToCall;
          return token;
        };
        _cancel = token => clearTimeout(token);
      }
    }

    /* harmony default export */
    var raf = {
      request: callback => _request(callback),
      cancel: token => _cancel(token),
      init: raf_init
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/normalizeListeners.js

    function normalize(type, listeners, result) {
      result = result || {};
      if (is.string(type) && type.search(' ') !== -1) {
        type = split(type);
      }
      if (is.array(type)) {
        return type.reduce((acc, t) => extend(acc, normalize(t, listeners, result)), result);
      } // ({ type: fn }) -> ('', { type: fn })

      if (is.object(type)) {
        listeners = type;
        type = '';
      }
      if (is.func(listeners)) {
        result[type] = result[type] || [];
        result[type].push(listeners);
      } else if (is.array(listeners)) {
        for (const l of listeners) {
          normalize(type, l, result);
        }
      } else if (is.object(listeners)) {
        for (const prefix in listeners) {
          const combinedTypes = split(prefix).map(p => "".concat(type).concat(p));
          normalize(combinedTypes, listeners[prefix], result);
        }
      }
      return result;
    }
    function split(type) {
      return type.trim().split(/ +/);
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/Eventable.js

    function fireUntilImmediateStopped(event, listeners) {
      for (const listener of listeners) {
        if (event.immediatePropagationStopped) {
          break;
        }
        listener(event);
      }
    }
    class Eventable_Eventable {
      constructor(options) {
        this.options = void 0;
        this.types = {};
        this.propagationStopped = false;
        this.immediatePropagationStopped = false;
        this.global = void 0;
        this.options = extend({}, options || {});
      }
      fire(event) {
        let listeners;
        const global = this.global; // Interactable#on() listeners
        // tslint:disable no-conditional-assignment

        if (listeners = this.types[event.type]) {
          fireUntilImmediateStopped(event, listeners);
        } // interact.on() listeners

        if (!event.propagationStopped && global && (listeners = global[event.type])) {
          fireUntilImmediateStopped(event, listeners);
        }
      }
      on(type, listener) {
        const listeners = normalize(type, listener);
        for (type in listeners) {
          this.types[type] = merge(this.types[type] || [], listeners[type]);
        }
      }
      off(type, listener) {
        const listeners = normalize(type, listener);
        for (type in listeners) {
          const eventList = this.types[type];
          if (!eventList || !eventList.length) {
            continue;
          }
          for (const subListener of listeners[type]) {
            const index = eventList.indexOf(subListener);
            if (index !== -1) {
              eventList.splice(index, 1);
            }
          }
        }
      }
      getRect(_element) {
        return null;
      }
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/domUtils.js

    function nodeContains(parent, child) {
      if (parent.contains) {
        return parent.contains(child);
      }
      while (child) {
        if (child === parent) {
          return true;
        }
        child = child.parentNode;
      }
      return false;
    }
    function domUtils_closest(element, selector) {
      while (is.element(element)) {
        if (matchesSelector(element, selector)) {
          return element;
        }
        element = parentNode(element);
      }
      return null;
    }
    function parentNode(node) {
      let parent = node.parentNode;
      if (is.docFrag(parent)) {
        // skip past #shado-root fragments
        // tslint:disable-next-line
        while ((parent = parent.host) && is.docFrag(parent)) {
          continue;
        }
        return parent;
      }
      return parent;
    }
    function matchesSelector(element, selector) {
      // remove /deep/ from selectors if shadowDOM polyfill is used
      if (win !== realWindow) {
        selector = selector.replace(/\/deep\//g, ' ');
      }
      return element[utils_browser.prefixedMatchesSelector](selector);
    }
    const getParent = el => el.parentNode || el.host; // Test for the element that's "above" all other qualifiers

    function indexOfDeepestElement(elements) {
      let deepestNodeParents = [];
      let deepestNodeIndex;
      for (let i = 0; i < elements.length; i++) {
        const currentNode = elements[i];
        const deepestNode = elements[deepestNodeIndex]; // node may appear in elements array multiple times

        if (!currentNode || i === deepestNodeIndex) {
          continue;
        }
        if (!deepestNode) {
          deepestNodeIndex = i;
          continue;
        }
        const currentNodeParent = getParent(currentNode);
        const deepestNodeParent = getParent(deepestNode); // check if the deepest or current are document.documentElement/rootElement
        // - if the current node is, do nothing and continue

        if (currentNodeParent === currentNode.ownerDocument) {
          continue;
        } // - if deepest is, update with the current node and continue to next
        else if (deepestNodeParent === currentNode.ownerDocument) {
          deepestNodeIndex = i;
          continue;
        } // compare zIndex of siblings

        if (currentNodeParent === deepestNodeParent) {
          if (zIndexIsHigherThan(currentNode, deepestNode)) {
            deepestNodeIndex = i;
          }
          continue;
        } // populate the ancestry array for the latest deepest node

        deepestNodeParents = deepestNodeParents.length ? deepestNodeParents : getNodeParents(deepestNode);
        let ancestryStart; // if the deepest node is an HTMLElement and the current node is a non root svg element

        if (deepestNode instanceof utils_domObjects.HTMLElement && currentNode instanceof utils_domObjects.SVGElement && !(currentNode instanceof utils_domObjects.SVGSVGElement)) {
          // TODO: is this check necessary? Was this for HTML elements embedded in SVG?
          if (currentNode === deepestNodeParent) {
            continue;
          }
          ancestryStart = currentNode.ownerSVGElement;
        } else {
          ancestryStart = currentNode;
        }
        const currentNodeParents = getNodeParents(ancestryStart, deepestNode.ownerDocument);
        let commonIndex = 0; // get (position of closest common ancestor) + 1

        while (currentNodeParents[commonIndex] && currentNodeParents[commonIndex] === deepestNodeParents[commonIndex]) {
          commonIndex++;
        }
        const parents = [currentNodeParents[commonIndex - 1], currentNodeParents[commonIndex], deepestNodeParents[commonIndex]];
        let child = parents[0].lastChild;
        while (child) {
          if (child === parents[1]) {
            deepestNodeIndex = i;
            deepestNodeParents = currentNodeParents;
            break;
          } else if (child === parents[2]) {
            break;
          }
          child = child.previousSibling;
        }
      }
      return deepestNodeIndex;
    }
    function getNodeParents(node, limit) {
      const parents = [];
      let parent = node;
      let parentParent;
      while ((parentParent = getParent(parent)) && parent !== limit && parentParent !== parent.ownerDocument) {
        parents.unshift(parent);
        parent = parentParent;
      }
      return parents;
    }
    function zIndexIsHigherThan(higherNode, lowerNode) {
      const higherIndex = parseInt(getWindow(higherNode).getComputedStyle(higherNode).zIndex, 10) || 0;
      const lowerIndex = parseInt(getWindow(lowerNode).getComputedStyle(lowerNode).zIndex, 10) || 0;
      return higherIndex >= lowerIndex;
    }
    function matchesUpTo(element, selector, limit) {
      while (is.element(element)) {
        if (matchesSelector(element, selector)) {
          return true;
        }
        element = parentNode(element);
        if (element === limit) {
          return matchesSelector(element, selector);
        }
      }
      return false;
    }
    function getActualElement(element) {
      return element.correspondingUseElement || element;
    }
    function getScrollXY(relevantWindow) {
      relevantWindow = relevantWindow || win;
      return {
        x: relevantWindow.scrollX || relevantWindow.document.documentElement.scrollLeft,
        y: relevantWindow.scrollY || relevantWindow.document.documentElement.scrollTop
      };
    }
    function getElementClientRect(element) {
      const clientRect = element instanceof utils_domObjects.SVGElement ? element.getBoundingClientRect() : element.getClientRects()[0];
      return clientRect && {
        left: clientRect.left,
        right: clientRect.right,
        top: clientRect.top,
        bottom: clientRect.bottom,
        width: clientRect.width || clientRect.right - clientRect.left,
        height: clientRect.height || clientRect.bottom - clientRect.top
      };
    }
    function getElementRect(element) {
      const clientRect = getElementClientRect(element);
      if (!utils_browser.isIOS7 && clientRect) {
        const scroll = getScrollXY(getWindow(element));
        clientRect.left += scroll.x;
        clientRect.right += scroll.x;
        clientRect.top += scroll.y;
        clientRect.bottom += scroll.y;
      }
      return clientRect;
    }
    function getPath(node) {
      const path = [];
      while (node) {
        path.push(node);
        node = parentNode(node);
      }
      return path;
    }
    function trySelector(value) {
      if (!is.string(value)) {
        return false;
      } // an exception will be raised if it is invalid

      utils_domObjects.document.querySelector(value);
      return true;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/rect.js

    function getStringOptionResult(value, target, element) {
      if (value === 'parent') {
        return parentNode(element);
      }
      if (value === 'self') {
        return target.getRect(element);
      }
      return domUtils_closest(element, value);
    }
    function resolveRectLike(value, target, element, functionArgs) {
      let returnValue = value;
      if (is.string(returnValue)) {
        returnValue = getStringOptionResult(returnValue, target, element);
      } else if (is.func(returnValue)) {
        returnValue = returnValue(...functionArgs);
      }
      if (is.element(returnValue)) {
        returnValue = getElementRect(returnValue);
      }
      return returnValue;
    }
    function rectToXY(rect) {
      return rect && {
        x: 'x' in rect ? rect.x : rect.left,
        y: 'y' in rect ? rect.y : rect.top
      };
    }
    function xywhToTlbr(rect) {
      if (rect && !('left' in rect && 'top' in rect)) {
        rect = extend({}, rect);
        rect.left = rect.x || 0;
        rect.top = rect.y || 0;
        rect.right = rect.right || rect.left + rect.width;
        rect.bottom = rect.bottom || rect.top + rect.height;
      }
      return rect;
    }
    function tlbrToXywh(rect) {
      if (rect && !('x' in rect && 'y' in rect)) {
        rect = extend({}, rect);
        rect.x = rect.left || 0;
        rect.y = rect.top || 0;
        rect.width = rect.width || (rect.right || 0) - rect.x;
        rect.height = rect.height || (rect.bottom || 0) - rect.y;
      }
      return rect;
    }
    function addEdges(edges, rect, delta) {
      if (edges.left) {
        rect.left += delta.x;
      }
      if (edges.right) {
        rect.right += delta.x;
      }
      if (edges.top) {
        rect.top += delta.y;
      }
      if (edges.bottom) {
        rect.bottom += delta.y;
      }
      rect.width = rect.right - rect.left;
      rect.height = rect.bottom - rect.top;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/getOriginXY.js

    /* harmony default export */
    var getOriginXY = function getOriginXY(target, element, actionName) {
      const actionOptions = target.options[actionName];
      const actionOrigin = actionOptions && actionOptions.origin;
      const origin = actionOrigin || target.options.origin;
      const originRect = resolveRectLike(origin, target, element, [target && element]);
      return rectToXY(originRect) || {
        x: 0,
        y: 0
      };
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/hypot.js
    /* harmony default export */
    var hypot = (x, y) => Math.sqrt(x * x + y * y);

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/BaseEvent.js
    class BaseEvent {
      constructor(interaction) {
        this.type = void 0;
        this.target = void 0;
        this.currentTarget = void 0;
        this.interactable = void 0;
        this._interaction = void 0;
        this.timeStamp = void 0;
        this.immediatePropagationStopped = false;
        this.propagationStopped = false;
        this._interaction = interaction;
      }
      preventDefault() {}
      /**
       * Don't call any other listeners (even on the current target)
       */

      stopPropagation() {
        this.propagationStopped = true;
      }
      /**
       * Don't call listeners on the remaining targets
       */

      stopImmediatePropagation() {
        this.immediatePropagationStopped = this.propagationStopped = true;
      }
    } // defined outside of class definition to avoid assignment of undefined during
    // construction

    // getters and setters defined here to support typescript 3.6 and below which
    // don't support getter and setters in .d.ts files
    Object.defineProperty(BaseEvent.prototype, 'interaction', {
      get() {
        return this._interaction._proxy;
      },
      set() {}
    });

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/defaultOptions.js
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    // export interface Options extends BaseDefaults, PerActionDefaults {}
    const defaultOptions_defaults = {
      base: {
        preventDefault: 'auto',
        deltaSource: 'page'
      },
      perAction: {
        enabled: false,
        origin: {
          x: 0,
          y: 0
        }
      },
      actions: {}
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/InteractEvent.js

    class InteractEvent_InteractEvent extends BaseEvent {
      // resize

      /** */
      constructor(interaction, event, actionName, phase, element, preEnd, type) {
        super(interaction);
        this.target = void 0;
        this.currentTarget = void 0;
        this.relatedTarget = null;
        this.screenX = void 0;
        this.screenY = void 0;
        this.button = void 0;
        this.buttons = void 0;
        this.ctrlKey = void 0;
        this.shiftKey = void 0;
        this.altKey = void 0;
        this.metaKey = void 0;
        this.page = void 0;
        this.client = void 0;
        this.delta = void 0;
        this.rect = void 0;
        this.x0 = void 0;
        this.y0 = void 0;
        this.t0 = void 0;
        this.dt = void 0;
        this.duration = void 0;
        this.clientX0 = void 0;
        this.clientY0 = void 0;
        this.velocity = void 0;
        this.speed = void 0;
        this.swipe = void 0;
        this.timeStamp = void 0;
        this.axes = void 0;
        this.preEnd = void 0;
        element = element || interaction.element;
        const target = interaction.interactable;
        const deltaSource = (target && target.options || defaultOptions_defaults).deltaSource;
        const origin = getOriginXY(target, element, actionName);
        const starting = phase === 'start';
        const ending = phase === 'end';
        const prevEvent = starting ? this : interaction.prevEvent;
        const coords = starting ? interaction.coords.start : ending ? {
          page: prevEvent.page,
          client: prevEvent.client,
          timeStamp: interaction.coords.cur.timeStamp
        } : interaction.coords.cur;
        this.page = extend({}, coords.page);
        this.client = extend({}, coords.client);
        this.rect = extend({}, interaction.rect);
        this.timeStamp = coords.timeStamp;
        if (!ending) {
          this.page.x -= origin.x;
          this.page.y -= origin.y;
          this.client.x -= origin.x;
          this.client.y -= origin.y;
        }
        this.ctrlKey = event.ctrlKey;
        this.altKey = event.altKey;
        this.shiftKey = event.shiftKey;
        this.metaKey = event.metaKey;
        this.button = event.button;
        this.buttons = event.buttons;
        this.target = element;
        this.currentTarget = element;
        this.preEnd = preEnd;
        this.type = type || actionName + (phase || '');
        this.interactable = target;
        this.t0 = starting ? interaction.pointers[interaction.pointers.length - 1].downTime : prevEvent.t0;
        this.x0 = interaction.coords.start.page.x - origin.x;
        this.y0 = interaction.coords.start.page.y - origin.y;
        this.clientX0 = interaction.coords.start.client.x - origin.x;
        this.clientY0 = interaction.coords.start.client.y - origin.y;
        if (starting || ending) {
          this.delta = {
            x: 0,
            y: 0
          };
        } else {
          this.delta = {
            x: this[deltaSource].x - prevEvent[deltaSource].x,
            y: this[deltaSource].y - prevEvent[deltaSource].y
          };
        }
        this.dt = interaction.coords.delta.timeStamp;
        this.duration = this.timeStamp - this.t0; // velocity and speed in pixels per second

        this.velocity = extend({}, interaction.coords.velocity[deltaSource]);
        this.speed = hypot(this.velocity.x, this.velocity.y);
        this.swipe = ending || phase === 'inertiastart' ? this.getSwipe() : null;
      }
      getSwipe() {
        const interaction = this._interaction;
        if (interaction.prevEvent.speed < 600 || this.timeStamp - interaction.prevEvent.timeStamp > 150) {
          return null;
        }
        let angle = 180 * Math.atan2(interaction.prevEvent.velocityY, interaction.prevEvent.velocityX) / Math.PI;
        const overlap = 22.5;
        if (angle < 0) {
          angle += 360;
        }
        const left = 135 - overlap <= angle && angle < 225 + overlap;
        const up = 225 - overlap <= angle && angle < 315 + overlap;
        const right = !left && (315 - overlap <= angle || angle < 45 + overlap);
        const down = !up && 45 - overlap <= angle && angle < 135 + overlap;
        return {
          up,
          down,
          left,
          right,
          angle,
          speed: interaction.prevEvent.speed,
          velocity: {
            x: interaction.prevEvent.velocityX,
            y: interaction.prevEvent.velocityY
          }
        };
      }
      preventDefault() {}
      /**
       * Don't call listeners on the remaining targets
       */

      stopImmediatePropagation() {
        this.immediatePropagationStopped = this.propagationStopped = true;
      }
      /**
       * Don't call any other listeners (even on the current target)
       */

      stopPropagation() {
        this.propagationStopped = true;
      }
    } // getters and setters defined here to support typescript 3.6 and below which
    // don't support getter and setters in .d.ts files

    Object.defineProperties(InteractEvent_InteractEvent.prototype, {
      pageX: {
        get() {
          return this.page.x;
        },
        set(value) {
          this.page.x = value;
        }
      },
      pageY: {
        get() {
          return this.page.y;
        },
        set(value) {
          this.page.y = value;
        }
      },
      clientX: {
        get() {
          return this.client.x;
        },
        set(value) {
          this.client.x = value;
        }
      },
      clientY: {
        get() {
          return this.client.y;
        },
        set(value) {
          this.client.y = value;
        }
      },
      dx: {
        get() {
          return this.delta.x;
        },
        set(value) {
          this.delta.x = value;
        }
      },
      dy: {
        get() {
          return this.delta.y;
        },
        set(value) {
          this.delta.y = value;
        }
      },
      velocityX: {
        get() {
          return this.velocity.x;
        },
        set(value) {
          this.velocity.x = value;
        }
      },
      velocityY: {
        get() {
          return this.velocity.y;
        },
        set(value) {
          this.velocity.y = value;
        }
      }
    });

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/isNonNativeEvent.js
    function isNonNativeEvent(type, actions) {
      if (actions.phaselessTypes[type]) {
        return true;
      }
      for (const name in actions.map) {
        if (type.indexOf(name) === 0 && type.substr(name.length) in actions.phases) {
          return true;
        }
      }
      return false;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/Interactable.js
    /* eslint-disable no-dupe-class-members */

    /** */
    class Interactable_Interactable {
      /** @internal */
      get _defaults() {
        return {
          base: {},
          perAction: {},
          actions: {}
        };
      }

      /** */
      constructor(target, options, defaultContext, scopeEvents) {
        this.options = void 0;
        this._actions = void 0;
        this.target = void 0;
        this.events = new Eventable_Eventable();
        this._context = void 0;
        this._win = void 0;
        this._doc = void 0;
        this._scopeEvents = void 0;
        this._rectChecker = void 0;
        this._actions = options.actions;
        this.target = target;
        this._context = options.context || defaultContext;
        this._win = getWindow(trySelector(target) ? this._context : target);
        this._doc = this._win.document;
        this._scopeEvents = scopeEvents;
        this.set(options);
      }
      setOnEvents(actionName, phases) {
        if (is.func(phases.onstart)) {
          this.on("".concat(actionName, "start"), phases.onstart);
        }
        if (is.func(phases.onmove)) {
          this.on("".concat(actionName, "move"), phases.onmove);
        }
        if (is.func(phases.onend)) {
          this.on("".concat(actionName, "end"), phases.onend);
        }
        if (is.func(phases.oninertiastart)) {
          this.on("".concat(actionName, "inertiastart"), phases.oninertiastart);
        }
        return this;
      }
      updatePerActionListeners(actionName, prev, cur) {
        if (is.array(prev) || is.object(prev)) {
          this.off(actionName, prev);
        }
        if (is.array(cur) || is.object(cur)) {
          this.on(actionName, cur);
        }
      }
      setPerAction(actionName, options) {
        const defaults = this._defaults; // for all the default per-action options

        for (const optionName_ in options) {
          const optionName = optionName_;
          const actionOptions = this.options[actionName];
          const optionValue = options[optionName]; // remove old event listeners and add new ones

          if (optionName === 'listeners') {
            this.updatePerActionListeners(actionName, actionOptions.listeners, optionValue);
          } // if the option value is an array

          if (is.array(optionValue)) {
            actionOptions[optionName] = from(optionValue);
          } // if the option value is an object
          else if (is.plainObject(optionValue)) {
            // copy the object
            actionOptions[optionName] = extend(actionOptions[optionName] || {}, clone(optionValue)); // set anabled field to true if it exists in the defaults

            if (is.object(defaults.perAction[optionName]) && 'enabled' in defaults.perAction[optionName]) {
              actionOptions[optionName].enabled = optionValue.enabled !== false;
            }
          } // if the option value is a boolean and the default is an object
          else if (is.bool(optionValue) && is.object(defaults.perAction[optionName])) {
            actionOptions[optionName].enabled = optionValue;
          } // if it's anything else, do a plain assignment
          else {
            actionOptions[optionName] = optionValue;
          }
        }
      }
      /**
       * The default function to get an Interactables bounding rect. Can be
       * overridden using {@link Interactable.rectChecker}.
       *
       * @param {Element} [element] The element to measure.
       * @return {Rect} The object's bounding rectangle.
       */

      getRect(element) {
        element = element || (is.element(this.target) ? this.target : null);
        if (is.string(this.target)) {
          element = element || this._context.querySelector(this.target);
        }
        return getElementRect(element);
      }
      /**
       * Returns or sets the function used to calculate the interactable's
       * element's rectangle
       *
       * @param {function} [checker] A function which returns this Interactable's
       * bounding rectangle. See {@link Interactable.getRect}
       * @return {function | object} The checker function or this Interactable
       */

      rectChecker(checker) {
        if (is.func(checker)) {
          this._rectChecker = checker;
          this.getRect = element => {
            const rect = extend({}, this._rectChecker(element));
            if (!('width' in rect)) {
              rect.width = rect.right - rect.left;
              rect.height = rect.bottom - rect.top;
            }
            return rect;
          };
          return this;
        }
        if (checker === null) {
          delete this.getRect;
          delete this._rectChecker;
          return this;
        }
        return this.getRect;
      }
      _backCompatOption(optionName, newValue) {
        if (trySelector(newValue) || is.object(newValue)) {
          this.options[optionName] = newValue;
          for (const action in this._actions.map) {
            this.options[action][optionName] = newValue;
          }
          return this;
        }
        return this.options[optionName];
      }
      /**
       * Gets or sets the origin of the Interactable's element.  The x and y
       * of the origin will be subtracted from action event coordinates.
       *
       * @param {Element | object | string} [origin] An HTML or SVG Element whose
       * rect will be used, an object eg. { x: 0, y: 0 } or string 'parent', 'self'
       * or any CSS selector
       *
       * @return {object} The current origin or this Interactable
       */

      origin(newValue) {
        return this._backCompatOption('origin', newValue);
      }
      /**
       * Returns or sets the mouse coordinate types used to calculate the
       * movement of the pointer.
       *
       * @param {string} [newValue] Use 'client' if you will be scrolling while
       * interacting; Use 'page' if you want autoScroll to work
       * @return {string | object} The current deltaSource or this Interactable
       */

      deltaSource(newValue) {
        if (newValue === 'page' || newValue === 'client') {
          this.options.deltaSource = newValue;
          return this;
        }
        return this.options.deltaSource;
      }
      /**
       * Gets the selector context Node of the Interactable. The default is
       * `window.document`.
       *
       * @return {Node} The context Node of this Interactable
       */

      context() {
        return this._context;
      }
      inContext(element) {
        return this._context === element.ownerDocument || nodeContains(this._context, element);
      }
      testIgnoreAllow(options, targetNode, eventTarget) {
        return !this.testIgnore(options.ignoreFrom, targetNode, eventTarget) && this.testAllow(options.allowFrom, targetNode, eventTarget);
      }
      testAllow(allowFrom, targetNode, element) {
        if (!allowFrom) {
          return true;
        }
        if (!is.element(element)) {
          return false;
        }
        if (is.string(allowFrom)) {
          return matchesUpTo(element, allowFrom, targetNode);
        } else if (is.element(allowFrom)) {
          return nodeContains(allowFrom, element);
        }
        return false;
      }
      testIgnore(ignoreFrom, targetNode, element) {
        if (!ignoreFrom || !is.element(element)) {
          return false;
        }
        if (is.string(ignoreFrom)) {
          return matchesUpTo(element, ignoreFrom, targetNode);
        } else if (is.element(ignoreFrom)) {
          return nodeContains(ignoreFrom, element);
        }
        return false;
      }
      /**
       * Calls listeners for the given InteractEvent type bound globally
       * and directly to this Interactable
       *
       * @param {InteractEvent} iEvent The InteractEvent object to be fired on this
       * Interactable
       * @return {Interactable} this Interactable
       */

      fire(iEvent) {
        this.events.fire(iEvent);
        return this;
      }
      _onOff(method, typeArg, listenerArg, options) {
        if (is.object(typeArg) && !is.array(typeArg)) {
          options = listenerArg;
          listenerArg = null;
        }
        const addRemove = method === 'on' ? 'add' : 'remove';
        const listeners = normalize(typeArg, listenerArg);
        for (let type in listeners) {
          if (type === 'wheel') {
            type = utils_browser.wheelEvent;
          }
          for (const listener of listeners[type]) {
            // if it is an action event type
            if (isNonNativeEvent(type, this._actions)) {
              this.events[method](type, listener);
            } // delegated event
            else if (is.string(this.target)) {
              this._scopeEvents["".concat(addRemove, "Delegate")](this.target, this._context, type, listener, options);
            } // remove listener from this Interactable's element
            else {
              this._scopeEvents[addRemove](this.target, type, listener, options);
            }
          }
        }
        return this;
      }
      /**
       * Binds a listener for an InteractEvent, pointerEvent or DOM event.
       *
       * @param {string | array | object} types The types of events to listen
       * for
       * @param {function | array | object} [listener] The event listener function(s)
       * @param {object | boolean} [options] options object or useCapture flag for
       * addEventListener
       * @return {Interactable} This Interactable
       */

      on(types, listener, options) {
        return this._onOff('on', types, listener, options);
      }
      /**
       * Removes an InteractEvent, pointerEvent or DOM event listener.
       *
       * @param {string | array | object} types The types of events that were
       * listened for
       * @param {function | array | object} [listener] The event listener function(s)
       * @param {object | boolean} [options] options object or useCapture flag for
       * removeEventListener
       * @return {Interactable} This Interactable
       */

      off(types, listener, options) {
        return this._onOff('off', types, listener, options);
      }
      /**
       * Reset the options of this Interactable
       *
       * @param {object} options The new settings to apply
       * @return {object} This Interactable
       */

      set(options) {
        const defaults = this._defaults;
        if (!is.object(options)) {
          options = {};
        }
        this.options = clone(defaults.base);
        for (const actionName_ in this._actions.methodDict) {
          const actionName = actionName_;
          const methodName = this._actions.methodDict[actionName];
          this.options[actionName] = {};
          this.setPerAction(actionName, extend(extend({}, defaults.perAction), defaults.actions[actionName]));
          this[methodName](options[actionName]);
        }
        for (const setting in options) {
          if (is.func(this[setting])) {
            this[setting](options[setting]);
          }
        }
        return this;
      }
      /**
       * Remove this interactable from the list of interactables and remove it's
       * action capabilities and event listeners
       */

      unset() {
        if (is.string(this.target)) {
          // remove delegated events
          for (const type in this._scopeEvents.delegatedEvents) {
            const delegated = this._scopeEvents.delegatedEvents[type];
            for (let i = delegated.length - 1; i >= 0; i--) {
              const {
                selector,
                context,
                listeners
              } = delegated[i];
              if (selector === this.target && context === this._context) {
                delegated.splice(i, 1);
              }
              for (let l = listeners.length - 1; l >= 0; l--) {
                this._scopeEvents.removeDelegate(this.target, this._context, type, listeners[l][0], listeners[l][1]);
              }
            }
          }
        } else {
          this._scopeEvents.remove(this.target, 'all');
        }
      }
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/InteractableSet.js

    class InteractableSet_InteractableSet {
      // all set interactables
      constructor(scope) {
        this.list = [];
        this.selectorMap = {};
        this.scope = void 0;
        this.scope = scope;
        scope.addListeners({
          'interactable:unset': _ref2 => {
            let {
              interactable
            } = _ref2;
            const {
              target,
              _context: context
            } = interactable;
            const targetMappings = is.string(target) ? this.selectorMap[target] : target[this.scope.id];
            const targetIndex = findIndex(targetMappings, m => m.context === context);
            if (targetMappings[targetIndex]) {
              // Destroying mappingInfo's context and interactable
              targetMappings[targetIndex].context = null;
              targetMappings[targetIndex].interactable = null;
            }
            targetMappings.splice(targetIndex, 1);
          }
        });
      }
      new(target, options) {
        options = extend(options || {}, {
          actions: this.scope.actions
        });
        const interactable = new this.scope.Interactable(target, options, this.scope.document, this.scope.events);
        const mappingInfo = {
          context: interactable._context,
          interactable
        };
        this.scope.addDocument(interactable._doc);
        this.list.push(interactable);
        if (is.string(target)) {
          if (!this.selectorMap[target]) {
            this.selectorMap[target] = [];
          }
          this.selectorMap[target].push(mappingInfo);
        } else {
          if (!interactable.target[this.scope.id]) {
            Object.defineProperty(target, this.scope.id, {
              value: [],
              configurable: true
            });
          }
          target[this.scope.id].push(mappingInfo);
        }
        this.scope.fire('interactable:new', {
          target,
          options,
          interactable,
          win: this.scope._win
        });
        return interactable;
      }
      get(target, options) {
        const context = options && options.context || this.scope.document;
        const isSelector = is.string(target);
        const targetMappings = isSelector ? this.selectorMap[target] : target[this.scope.id];
        if (!targetMappings) {
          return null;
        }
        const found = find(targetMappings, m => m.context === context && (isSelector || m.interactable.inContext(target)));
        return found && found.interactable;
      }
      forEachMatch(node, callback) {
        for (const interactable of this.list) {
          let ret;
          if ((is.string(interactable.target) // target is a selector and the element matches
          ? is.element(node) && matchesSelector(node, interactable.target) :
          // target is the element
          node === interactable.target) &&
          // the element is in context
          interactable.inContext(node)) {
            ret = callback(interactable);
          }
          if (ret !== undefined) {
            return ret;
          }
        }
      }
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/pointerExtend.js
    function pointerExtend(dest, source) {
      for (const prop in source) {
        const prefixedPropREs = pointerExtend.prefixedPropREs;
        let deprecated = false; // skip deprecated prefixed properties

        for (const vendor in prefixedPropREs) {
          if (prop.indexOf(vendor) === 0 && prefixedPropREs[vendor].test(prop)) {
            deprecated = true;
            break;
          }
        }
        if (!deprecated && typeof source[prop] !== 'function') {
          dest[prop] = source[prop];
        }
      }
      return dest;
    }
    pointerExtend.prefixedPropREs = {
      webkit: /(Movement[XY]|Radius[XY]|RotationAngle|Force)$/,
      moz: /(Pressure)$/
    };
    /* harmony default export */
    var utils_pointerExtend = pointerExtend;

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/pointerUtils.js

    function copyCoords(dest, src) {
      dest.page = dest.page || {};
      dest.page.x = src.page.x;
      dest.page.y = src.page.y;
      dest.client = dest.client || {};
      dest.client.x = src.client.x;
      dest.client.y = src.client.y;
      dest.timeStamp = src.timeStamp;
    }
    function setCoordDeltas(targetObj, prev, cur) {
      targetObj.page.x = cur.page.x - prev.page.x;
      targetObj.page.y = cur.page.y - prev.page.y;
      targetObj.client.x = cur.client.x - prev.client.x;
      targetObj.client.y = cur.client.y - prev.client.y;
      targetObj.timeStamp = cur.timeStamp - prev.timeStamp;
    }
    function setCoordVelocity(targetObj, delta) {
      const dt = Math.max(delta.timeStamp / 1000, 0.001);
      targetObj.page.x = delta.page.x / dt;
      targetObj.page.y = delta.page.y / dt;
      targetObj.client.x = delta.client.x / dt;
      targetObj.client.y = delta.client.y / dt;
      targetObj.timeStamp = dt;
    }
    function setZeroCoords(targetObj) {
      targetObj.page.x = 0;
      targetObj.page.y = 0;
      targetObj.client.x = 0;
      targetObj.client.y = 0;
    }
    function isNativePointer(pointer) {
      return pointer instanceof utils_domObjects.Event || pointer instanceof utils_domObjects.Touch;
    } // Get specified X/Y coords for mouse or event.touches[0]

    function getXY(type, pointer, xy) {
      xy = xy || {};
      type = type || 'page';
      xy.x = pointer[type + 'X'];
      xy.y = pointer[type + 'Y'];
      return xy;
    }
    function getPageXY(pointer, page) {
      page = page || {
        x: 0,
        y: 0
      }; // Opera Mobile handles the viewport and scrolling oddly

      if (utils_browser.isOperaMobile && isNativePointer(pointer)) {
        getXY('screen', pointer, page);
        page.x += window.scrollX;
        page.y += window.scrollY;
      } else {
        getXY('page', pointer, page);
      }
      return page;
    }
    function getClientXY(pointer, client) {
      client = client || {};
      if (utils_browser.isOperaMobile && isNativePointer(pointer)) {
        // Opera Mobile handles the viewport and scrolling oddly
        getXY('screen', pointer, client);
      } else {
        getXY('client', pointer, client);
      }
      return client;
    }
    function getPointerId(pointer) {
      return is.number(pointer.pointerId) ? pointer.pointerId : pointer.identifier;
    }
    function setCoords(dest, pointers, timeStamp) {
      const pointer = pointers.length > 1 ? pointerAverage(pointers) : pointers[0];
      getPageXY(pointer, dest.page);
      getClientXY(pointer, dest.client);
      dest.timeStamp = timeStamp;
    }
    function getTouchPair(event) {
      const touches = []; // array of touches is supplied

      if (is.array(event)) {
        touches[0] = event[0];
        touches[1] = event[1];
      } // an event
      else {
        if (event.type === 'touchend') {
          if (event.touches.length === 1) {
            touches[0] = event.touches[0];
            touches[1] = event.changedTouches[0];
          } else if (event.touches.length === 0) {
            touches[0] = event.changedTouches[0];
            touches[1] = event.changedTouches[1];
          }
        } else {
          touches[0] = event.touches[0];
          touches[1] = event.touches[1];
        }
      }
      return touches;
    }
    function pointerAverage(pointers) {
      const average = {
        pageX: 0,
        pageY: 0,
        clientX: 0,
        clientY: 0,
        screenX: 0,
        screenY: 0
      };
      for (const pointer of pointers) {
        for (const prop in average) {
          average[prop] += pointer[prop];
        }
      }
      for (const prop in average) {
        average[prop] /= pointers.length;
      }
      return average;
    }
    function touchBBox(event) {
      if (!event.length) {
        return null;
      }
      const touches = getTouchPair(event);
      const minX = Math.min(touches[0].pageX, touches[1].pageX);
      const minY = Math.min(touches[0].pageY, touches[1].pageY);
      const maxX = Math.max(touches[0].pageX, touches[1].pageX);
      const maxY = Math.max(touches[0].pageY, touches[1].pageY);
      return {
        x: minX,
        y: minY,
        left: minX,
        top: minY,
        right: maxX,
        bottom: maxY,
        width: maxX - minX,
        height: maxY - minY
      };
    }
    function touchDistance(event, deltaSource) {
      const sourceX = deltaSource + 'X';
      const sourceY = deltaSource + 'Y';
      const touches = getTouchPair(event);
      const dx = touches[0][sourceX] - touches[1][sourceX];
      const dy = touches[0][sourceY] - touches[1][sourceY];
      return hypot(dx, dy);
    }
    function touchAngle(event, deltaSource) {
      const sourceX = deltaSource + 'X';
      const sourceY = deltaSource + 'Y';
      const touches = getTouchPair(event);
      const dx = touches[1][sourceX] - touches[0][sourceX];
      const dy = touches[1][sourceY] - touches[0][sourceY];
      const angle = 180 * Math.atan2(dy, dx) / Math.PI;
      return angle;
    }
    function getPointerType(pointer) {
      return is.string(pointer.pointerType) ? pointer.pointerType : is.number(pointer.pointerType) ? [undefined, undefined, 'touch', 'pen', 'mouse'][pointer.pointerType] // if the PointerEvent API isn't available, then the "pointer" must
      // be either a MouseEvent, TouchEvent, or Touch object
      : /touch/.test(pointer.type) || pointer instanceof utils_domObjects.Touch ? 'touch' : 'mouse';
    } // [ event.target, event.currentTarget ]

    function getEventTargets(event) {
      const path = is.func(event.composedPath) ? event.composedPath() : event.path;
      return [getActualElement(path ? path[0] : event.target), getActualElement(event.currentTarget)];
    }
    function newCoords() {
      return {
        page: {
          x: 0,
          y: 0
        },
        client: {
          x: 0,
          y: 0
        },
        timeStamp: 0
      };
    }
    function coordsToEvent(coords) {
      const event = {
        coords,
        get page() {
          return this.coords.page;
        },
        get client() {
          return this.coords.client;
        },
        get timeStamp() {
          return this.coords.timeStamp;
        },
        get pageX() {
          return this.coords.page.x;
        },
        get pageY() {
          return this.coords.page.y;
        },
        get clientX() {
          return this.coords.client.x;
        },
        get clientY() {
          return this.coords.client.y;
        },
        get pointerId() {
          return this.coords.pointerId;
        },
        get target() {
          return this.coords.target;
        },
        get type() {
          return this.coords.type;
        },
        get pointerType() {
          return this.coords.pointerType;
        },
        get buttons() {
          return this.coords.buttons;
        },
        preventDefault() {}
      };
      return event;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/events.js

    function install(scope) {
      const targets = [];
      const delegatedEvents = {};
      const documents = [];
      const eventsMethods = {
        add,
        remove,
        addDelegate,
        removeDelegate,
        delegateListener,
        delegateUseCapture,
        delegatedEvents,
        documents,
        targets,
        supportsOptions: false,
        supportsPassive: false
      }; // check if browser supports passive events and options arg

      scope.document.createElement('div').addEventListener('test', null, {
        get capture() {
          return eventsMethods.supportsOptions = true;
        },
        get passive() {
          return eventsMethods.supportsPassive = true;
        }
      });
      scope.events = eventsMethods;
      function add(eventTarget, type, listener, optionalArg) {
        const options = getOptions(optionalArg);
        let target = find(targets, t => t.eventTarget === eventTarget);
        if (!target) {
          target = {
            eventTarget,
            events: {}
          };
          targets.push(target);
        }
        if (!target.events[type]) {
          target.events[type] = [];
        }
        if (eventTarget.addEventListener && !contains(target.events[type], listener)) {
          eventTarget.addEventListener(type, listener, eventsMethods.supportsOptions ? options : options.capture);
          target.events[type].push(listener);
        }
      }
      function remove(eventTarget, type, listener, optionalArg) {
        const options = getOptions(optionalArg);
        const targetIndex = findIndex(targets, t => t.eventTarget === eventTarget);
        const target = targets[targetIndex];
        if (!target || !target.events) {
          return;
        }
        if (type === 'all') {
          for (type in target.events) {
            if (target.events.hasOwnProperty(type)) {
              remove(eventTarget, type, 'all');
            }
          }
          return;
        }
        let typeIsEmpty = false;
        const typeListeners = target.events[type];
        if (typeListeners) {
          if (listener === 'all') {
            for (let i = typeListeners.length - 1; i >= 0; i--) {
              remove(eventTarget, type, typeListeners[i], options);
            }
            return;
          } else {
            for (let i = 0; i < typeListeners.length; i++) {
              if (typeListeners[i] === listener) {
                eventTarget.removeEventListener(type, listener, eventsMethods.supportsOptions ? options : options.capture);
                typeListeners.splice(i, 1);
                if (typeListeners.length === 0) {
                  delete target.events[type];
                  typeIsEmpty = true;
                }
                break;
              }
            }
          }
        }
        if (typeIsEmpty && !Object.keys(target.events).length) {
          targets.splice(targetIndex, 1);
        }
      }
      function addDelegate(selector, context, type, listener, optionalArg) {
        const options = getOptions(optionalArg);
        if (!delegatedEvents[type]) {
          delegatedEvents[type] = []; // add delegate listener functions

          for (const doc of documents) {
            add(doc, type, delegateListener);
            add(doc, type, delegateUseCapture, true);
          }
        }
        const delegates = delegatedEvents[type];
        let delegate = find(delegates, d => d.selector === selector && d.context === context);
        if (!delegate) {
          delegate = {
            selector,
            context,
            listeners: []
          };
          delegates.push(delegate);
        }
        delegate.listeners.push([listener, options]);
      }
      function removeDelegate(selector, context, type, listener, optionalArg) {
        const options = getOptions(optionalArg);
        const delegates = delegatedEvents[type];
        let matchFound = false;
        let index;
        if (!delegates) {
          return;
        } // count from last index of delegated to 0

        for (index = delegates.length - 1; index >= 0; index--) {
          const cur = delegates[index]; // look for matching selector and context Node

          if (cur.selector === selector && cur.context === context) {
            const {
              listeners
            } = cur; // each item of the listeners array is an array: [function, capture, passive]

            for (let i = listeners.length - 1; i >= 0; i--) {
              const [fn, {
                capture,
                passive
              }] = listeners[i]; // check if the listener functions and capture and passive flags match

              if (fn === listener && capture === options.capture && passive === options.passive) {
                // remove the listener from the array of listeners
                listeners.splice(i, 1); // if all listeners for this target have been removed
                // remove the target from the delegates array

                if (!listeners.length) {
                  delegates.splice(index, 1); // remove delegate function from context

                  remove(context, type, delegateListener);
                  remove(context, type, delegateUseCapture, true);
                } // only remove one listener

                matchFound = true;
                break;
              }
            }
            if (matchFound) {
              break;
            }
          }
        }
      } // bound to the interactable context when a DOM event
      // listener is added to a selector interactable

      function delegateListener(event, optionalArg) {
        const options = getOptions(optionalArg);
        const fakeEvent = new events_FakeEvent(event);
        const delegates = delegatedEvents[event.type];
        const [eventTarget] = getEventTargets(event);
        let element = eventTarget; // climb up document tree looking for selector matches

        while (is.element(element)) {
          for (let i = 0; i < delegates.length; i++) {
            const cur = delegates[i];
            const {
              selector,
              context
            } = cur;
            if (matchesSelector(element, selector) && nodeContains(context, eventTarget) && nodeContains(context, element)) {
              const {
                listeners
              } = cur;
              fakeEvent.currentTarget = element;
              for (const [fn, {
                capture,
                passive
              }] of listeners) {
                if (capture === options.capture && passive === options.passive) {
                  fn(fakeEvent);
                }
              }
            }
          }
          element = parentNode(element);
        }
      }
      function delegateUseCapture(event) {
        return delegateListener.call(this, event, true);
      } // for type inferrence

      return eventsMethods;
    }
    class events_FakeEvent {
      constructor(originalEvent) {
        this.currentTarget = void 0;
        this.originalEvent = void 0;
        this.type = void 0;
        this.originalEvent = originalEvent; // duplicate the event so that currentTarget can be changed

        utils_pointerExtend(this, originalEvent);
      }
      preventOriginalDefault() {
        this.originalEvent.preventDefault();
      }
      stopPropagation() {
        this.originalEvent.stopPropagation();
      }
      stopImmediatePropagation() {
        this.originalEvent.stopImmediatePropagation();
      }
    }
    function getOptions(param) {
      if (!is.object(param)) {
        return {
          capture: !!param,
          passive: false
        };
      }
      const options = extend({}, param);
      options.capture = !!param.capture;
      options.passive = !!param.passive;
      return options;
    }

    /* harmony default export */
    var events = {
      id: 'events',
      install
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/utils/misc.js

    function warnOnce(method, message) {
      let warned = false;
      return function () {
        if (!warned) {
          win.console.warn(message);
          warned = true;
        }
        return method.apply(this, arguments);
      };
    }
    function copyAction(dest, src) {
      dest.name = src.name;
      dest.axis = src.axis;
      dest.edges = src.edges;
      return dest;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/interactStatic.js
    /** @module interact */

    function createInteractStatic(scope) {
      /**
       * ```js
       * interact('#draggable').draggable(true)
       *
       * var rectables = interact('rect')
       * rectables
       *   .gesturable(true)
       *   .on('gesturemove', function (event) {
       *       // ...
       *   })
       * ```
       *
       * The methods of this variable can be used to set elements as interactables
       * and also to change various default settings.
       *
       * Calling it as a function and passing an element or a valid CSS selector
       * string returns an Interactable object which has various methods to configure
       * it.
       *
       * @global
       *
       * @param {Element | string} target The HTML or SVG Element to interact with
       * or CSS selector
       * @return {Interactable}
       */
      const interact = (target, options) => {
        let interactable = scope.interactables.get(target, options);
        if (!interactable) {
          interactable = scope.interactables.new(target, options);
          interactable.events.global = interact.globalEvents;
        }
        return interactable;
      }; // expose the functions used to calculate multi-touch properties

      interact.getPointerAverage = pointerAverage;
      interact.getTouchBBox = touchBBox;
      interact.getTouchDistance = touchDistance;
      interact.getTouchAngle = touchAngle;
      interact.getElementRect = getElementRect;
      interact.getElementClientRect = getElementClientRect;
      interact.matchesSelector = matchesSelector;
      interact.closest = domUtils_closest;
      interact.globalEvents = {}; // eslint-disable-next-line no-undef

      interact.version = "1.10.2";
      interact.scope = scope;
      /**
      * Use a plugin
      *
      * @alias module:interact.use
      *
       */

      interact.use = function (plugin, options) {
        this.scope.usePlugin(plugin, options);
        return this;
      };
      /**
       * Check if an element or selector has been set with the {@link interact}
       * function
       *
       * @alias module:interact.isSet
       *
       * @param {Target} target The Element or string being searched for
       * @param {object} options
       * @return {boolean} Indicates if the element or CSS selector was previously
       * passed to interact
       */

      interact.isSet = function (target, options) {
        return !!this.scope.interactables.get(target, options && options.context);
      };
      /**
       * @deprecated
       * Add a global listener for an InteractEvent or adds a DOM event to `document`
       *
       * @alias module:interact.on
       *
       * @param {string | array | object} type The types of events to listen for
       * @param {function} listener The function event (s)
       * @param {object | boolean} [options] object or useCapture flag for
       * addEventListener
       * @return {object} interact
       */

      interact.on = warnOnce(function on(type, listener, options) {
        if (is.string(type) && type.search(' ') !== -1) {
          type = type.trim().split(/ +/);
        }
        if (is.array(type)) {
          for (const eventType of type) {
            this.on(eventType, listener, options);
          }
          return this;
        }
        if (is.object(type)) {
          for (const prop in type) {
            this.on(prop, type[prop], listener);
          }
          return this;
        } // if it is an InteractEvent type, add listener to globalEvents

        if (isNonNativeEvent(type, this.scope.actions)) {
          // if this type of event was never bound
          if (!this.globalEvents[type]) {
            this.globalEvents[type] = [listener];
          } else {
            this.globalEvents[type].push(listener);
          }
        } // If non InteractEvent type, addEventListener to document
        else {
          this.scope.events.add(this.scope.document, type, listener, {
            options
          });
        }
        return this;
      }, 'The interact.on() method is being deprecated');
      /**
       * @deprecated
       * Removes a global InteractEvent listener or DOM event from `document`
       *
       * @alias module:interact.off
       *
       * @param {string | array | object} type The types of events that were listened
       * for
       * @param {function} listener The listener function to be removed
       * @param {object | boolean} options [options] object or useCapture flag for
       * removeEventListener
       * @return {object} interact
       */

      interact.off = warnOnce(function off(type, listener, options) {
        if (is.string(type) && type.search(' ') !== -1) {
          type = type.trim().split(/ +/);
        }
        if (is.array(type)) {
          for (const eventType of type) {
            this.off(eventType, listener, options);
          }
          return this;
        }
        if (is.object(type)) {
          for (const prop in type) {
            this.off(prop, type[prop], listener);
          }
          return this;
        }
        if (isNonNativeEvent(type, this.scope.actions)) {
          let index;
          if (type in this.globalEvents && (index = this.globalEvents[type].indexOf(listener)) !== -1) {
            this.globalEvents[type].splice(index, 1);
          }
        } else {
          this.scope.events.remove(this.scope.document, type, listener, options);
        }
        return this;
      }, 'The interact.off() method is being deprecated');
      interact.debug = function () {
        return this.scope;
      };
      /**
       * @alias module:interact.supportsTouch
       *
       * @return {boolean} Whether or not the browser supports touch input
       */

      interact.supportsTouch = function () {
        return utils_browser.supportsTouch;
      };
      /**
       * @alias module:interact.supportsPointerEvent
       *
       * @return {boolean} Whether or not the browser supports PointerEvents
       */

      interact.supportsPointerEvent = function () {
        return utils_browser.supportsPointerEvent;
      };
      /**
       * Cancels all interactions (end events are not fired)
       *
       * @alias module:interact.stop
       *
       * @return {object} interact
       */

      interact.stop = function () {
        for (const interaction of this.scope.interactions.list) {
          interaction.stop();
        }
        return this;
      };
      /**
       * Returns or sets the distance the pointer must be moved before an action
       * sequence occurs. This also affects tolerance for tap events.
       *
       * @alias module:interact.pointerMoveTolerance
       *
       * @param {number} [newValue] The movement from the start position must be greater than this value
       * @return {interact | number}
       */

      interact.pointerMoveTolerance = function (newValue) {
        if (is.number(newValue)) {
          this.scope.interactions.pointerMoveTolerance = newValue;
          return this;
        }
        return this.scope.interactions.pointerMoveTolerance;
      };
      interact.addDocument = function (doc, options) {
        this.scope.addDocument(doc, options);
      };
      interact.removeDocument = function (doc) {
        this.scope.removeDocument(doc);
      };
      return interact;
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/PointerInfo.js
    class PointerInfo {
      constructor(id, pointer, event, downTime, downTarget) {
        this.id = void 0;
        this.pointer = void 0;
        this.event = void 0;
        this.downTime = void 0;
        this.downTarget = void 0;
        this.id = id;
        this.pointer = pointer;
        this.event = event;
        this.downTime = downTime;
        this.downTarget = downTarget;
      }
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/Interaction.js

    let _ProxyValues;
    (function (_ProxyValues) {
      _ProxyValues["interactable"] = "";
      _ProxyValues["element"] = "";
      _ProxyValues["prepared"] = "";
      _ProxyValues["pointerIsDown"] = "";
      _ProxyValues["pointerWasMoved"] = "";
      _ProxyValues["_proxy"] = "";
    })(_ProxyValues || (_ProxyValues = {}));
    let _ProxyMethods;
    (function (_ProxyMethods) {
      _ProxyMethods["start"] = "";
      _ProxyMethods["move"] = "";
      _ProxyMethods["end"] = "";
      _ProxyMethods["stop"] = "";
      _ProxyMethods["interacting"] = "";
    })(_ProxyMethods || (_ProxyMethods = {}));
    let idCounter = 0;
    class Interaction_Interaction {
      // current interactable being interacted with
      // the target element of the interactable
      // action that's ready to be fired on next move event
      // keep track of added pointers
      // pointerdown/mousedown/touchstart event
      // previous action event

      /** @internal */
      get pointerMoveTolerance() {
        return 1;
      }
      /**
       * @alias Interaction.prototype.move
       */

      /** */
      constructor(_ref3) {
        let {
          pointerType,
          scopeFire
        } = _ref3;
        this.interactable = null;
        this.element = null;
        this.rect = void 0;
        this._rects = void 0;
        this.edges = void 0;
        this._scopeFire = void 0;
        this.prepared = {
          name: null,
          axis: null,
          edges: null
        };
        this.pointerType = void 0;
        this.pointers = [];
        this.downEvent = null;
        this.downPointer = {};
        this._latestPointer = {
          pointer: null,
          event: null,
          eventTarget: null
        };
        this.prevEvent = null;
        this.pointerIsDown = false;
        this.pointerWasMoved = false;
        this._interacting = false;
        this._ending = false;
        this._stopped = true;
        this._proxy = null;
        this.simulation = null;
        this.doMove = warnOnce(function (signalArg) {
          this.move(signalArg);
        }, 'The interaction.doMove() method has been renamed to interaction.move()');
        this.coords = {
          // Starting InteractEvent pointer coordinates
          start: newCoords(),
          // Previous native pointer move event coordinates
          prev: newCoords(),
          // current native pointer move event coordinates
          cur: newCoords(),
          // Change in coordinates and time of the pointer
          delta: newCoords(),
          // pointer velocity
          velocity: newCoords()
        };
        this._id = idCounter++;
        this._scopeFire = scopeFire;
        this.pointerType = pointerType;
        const that = this;
        this._proxy = {};
        for (const key in _ProxyValues) {
          Object.defineProperty(this._proxy, key, {
            get() {
              return that[key];
            }
          });
        }
        for (const key in _ProxyMethods) {
          Object.defineProperty(this._proxy, key, {
            value: function value() {
              return that[key](...arguments);
            }
          });
        }
        this._scopeFire('interactions:new', {
          interaction: this
        });
      }
      pointerDown(pointer, event, eventTarget) {
        const pointerIndex = this.updatePointer(pointer, event, eventTarget, true);
        const pointerInfo = this.pointers[pointerIndex];
        this._scopeFire('interactions:down', {
          pointer,
          event,
          eventTarget,
          pointerIndex,
          pointerInfo,
          type: 'down',
          interaction: this
        });
      }
      /**
       * ```js
       * interact(target)
       *   .draggable({
       *     // disable the default drag start by down->move
       *     manualStart: true
       *   })
       *   // start dragging after the user holds the pointer down
       *   .on('hold', function (event) {
       *     var interaction = event.interaction
       *
       *     if (!interaction.interacting()) {
       *       interaction.start({ name: 'drag' },
       *                         event.interactable,
       *                         event.currentTarget)
       *     }
       * })
       * ```
       *
       * Start an action with the given Interactable and Element as tartgets. The
       * action must be enabled for the target Interactable and an appropriate
       * number of pointers must be held down - 1 for drag/resize, 2 for gesture.
       *
       * Use it with `interactable.<action>able({ manualStart: false })` to always
       * [start actions manually](https://github.com/taye/interact.js/issues/114)
       *
       * @param {object} action   The action to be performed - drag, resize, etc.
       * @param {Interactable} target  The Interactable to target
       * @param {Element} element The DOM Element to target
       * @return {Boolean} Whether the interaction was successfully started
       */

      start(action, interactable, element) {
        if (this.interacting() || !this.pointerIsDown || this.pointers.length < (action.name === 'gesture' ? 2 : 1) || !interactable.options[action.name].enabled) {
          return false;
        }
        copyAction(this.prepared, action);
        this.interactable = interactable;
        this.element = element;
        this.rect = interactable.getRect(element);
        this.edges = this.prepared.edges ? extend({}, this.prepared.edges) : {
          left: true,
          right: true,
          top: true,
          bottom: true
        };
        this._stopped = false;
        this._interacting = this._doPhase({
          interaction: this,
          event: this.downEvent,
          phase: 'start'
        }) && !this._stopped;
        return this._interacting;
      }
      pointerMove(pointer, event, eventTarget) {
        if (!this.simulation && !(this.modification && this.modification.endResult)) {
          this.updatePointer(pointer, event, eventTarget, false);
        }
        const duplicateMove = this.coords.cur.page.x === this.coords.prev.page.x && this.coords.cur.page.y === this.coords.prev.page.y && this.coords.cur.client.x === this.coords.prev.client.x && this.coords.cur.client.y === this.coords.prev.client.y;
        let dx;
        let dy; // register movement greater than pointerMoveTolerance

        if (this.pointerIsDown && !this.pointerWasMoved) {
          dx = this.coords.cur.client.x - this.coords.start.client.x;
          dy = this.coords.cur.client.y - this.coords.start.client.y;
          this.pointerWasMoved = hypot(dx, dy) > this.pointerMoveTolerance;
        }
        const pointerIndex = this.getPointerIndex(pointer);
        const signalArg = {
          pointer,
          pointerIndex,
          pointerInfo: this.pointers[pointerIndex],
          event,
          type: 'move',
          eventTarget,
          dx,
          dy,
          duplicate: duplicateMove,
          interaction: this
        };
        if (!duplicateMove) {
          // set pointer coordinate, time changes and velocity
          setCoordVelocity(this.coords.velocity, this.coords.delta);
        }
        this._scopeFire('interactions:move', signalArg);
        if (!duplicateMove && !this.simulation) {
          // if interacting, fire an 'action-move' signal etc
          if (this.interacting()) {
            signalArg.type = null;
            this.move(signalArg);
          }
          if (this.pointerWasMoved) {
            copyCoords(this.coords.prev, this.coords.cur);
          }
        }
      }
      /**
       * ```js
       * interact(target)
       *   .draggable(true)
       *   .on('dragmove', function (event) {
       *     if (someCondition) {
       *       // change the snap settings
       *       event.interactable.draggable({ snap: { targets: [] }})
       *       // fire another move event with re-calculated snap
       *       event.interaction.move()
       *     }
       *   })
       * ```
       *
       * Force a move of the current action at the same coordinates. Useful if
       * snap/restrict has been changed and you want a movement with the new
       * settings.
       */

      move(signalArg) {
        if (!signalArg || !signalArg.event) {
          setZeroCoords(this.coords.delta);
        }
        signalArg = extend({
          pointer: this._latestPointer.pointer,
          event: this._latestPointer.event,
          eventTarget: this._latestPointer.eventTarget,
          interaction: this
        }, signalArg || {});
        signalArg.phase = 'move';
        this._doPhase(signalArg);
      } // End interact move events and stop auto-scroll unless simulation is running

      pointerUp(pointer, event, eventTarget, curEventTarget) {
        let pointerIndex = this.getPointerIndex(pointer);
        if (pointerIndex === -1) {
          pointerIndex = this.updatePointer(pointer, event, eventTarget, false);
        }
        const type = /cancel$/i.test(event.type) ? 'cancel' : 'up';
        this._scopeFire("interactions:".concat(type), {
          pointer,
          pointerIndex,
          pointerInfo: this.pointers[pointerIndex],
          event,
          eventTarget,
          type: type,
          curEventTarget,
          interaction: this
        });
        if (!this.simulation) {
          this.end(event);
        }
        this.removePointer(pointer, event);
      }
      documentBlur(event) {
        this.end(event);
        this._scopeFire('interactions:blur', {
          event,
          type: 'blur',
          interaction: this
        });
      }
      /**
       * ```js
       * interact(target)
       *   .draggable(true)
       *   .on('move', function (event) {
       *     if (event.pageX > 1000) {
       *       // end the current action
       *       event.interaction.end()
       *       // stop all further listeners from being called
       *       event.stopImmediatePropagation()
       *     }
       *   })
       * ```
       *
       * @param {PointerEvent} [event]
       */

      end(event) {
        this._ending = true;
        event = event || this._latestPointer.event;
        let endPhaseResult;
        if (this.interacting()) {
          endPhaseResult = this._doPhase({
            event,
            interaction: this,
            phase: 'end'
          });
        }
        this._ending = false;
        if (endPhaseResult === true) {
          this.stop();
        }
      }
      currentAction() {
        return this._interacting ? this.prepared.name : null;
      }
      interacting() {
        return this._interacting;
      }
      /** */

      stop() {
        this._scopeFire('interactions:stop', {
          interaction: this
        });
        this.interactable = this.element = null;
        this._interacting = false;
        this._stopped = true;
        this.prepared.name = this.prevEvent = null;
      }
      getPointerIndex(pointer) {
        const pointerId = getPointerId(pointer); // mouse and pen interactions may have only one pointer

        return this.pointerType === 'mouse' || this.pointerType === 'pen' ? this.pointers.length - 1 : findIndex(this.pointers, curPointer => curPointer.id === pointerId);
      }
      getPointerInfo(pointer) {
        return this.pointers[this.getPointerIndex(pointer)];
      }
      updatePointer(pointer, event, eventTarget, down) {
        const id = getPointerId(pointer);
        let pointerIndex = this.getPointerIndex(pointer);
        let pointerInfo = this.pointers[pointerIndex];
        down = down === false ? false : down || /(down|start)$/i.test(event.type);
        if (!pointerInfo) {
          pointerInfo = new PointerInfo(id, pointer, event, null, null);
          pointerIndex = this.pointers.length;
          this.pointers.push(pointerInfo);
        } else {
          pointerInfo.pointer = pointer;
        }
        setCoords(this.coords.cur, this.pointers.map(p => p.pointer), this._now());
        setCoordDeltas(this.coords.delta, this.coords.prev, this.coords.cur);
        if (down) {
          this.pointerIsDown = true;
          pointerInfo.downTime = this.coords.cur.timeStamp;
          pointerInfo.downTarget = eventTarget;
          utils_pointerExtend(this.downPointer, pointer);
          if (!this.interacting()) {
            copyCoords(this.coords.start, this.coords.cur);
            copyCoords(this.coords.prev, this.coords.cur);
            this.downEvent = event;
            this.pointerWasMoved = false;
          }
        }
        this._updateLatestPointer(pointer, event, eventTarget);
        this._scopeFire('interactions:update-pointer', {
          pointer,
          event,
          eventTarget,
          down,
          pointerInfo,
          pointerIndex,
          interaction: this
        });
        return pointerIndex;
      }
      removePointer(pointer, event) {
        const pointerIndex = this.getPointerIndex(pointer);
        if (pointerIndex === -1) {
          return;
        }
        const pointerInfo = this.pointers[pointerIndex];
        this._scopeFire('interactions:remove-pointer', {
          pointer,
          event,
          eventTarget: null,
          pointerIndex,
          pointerInfo,
          interaction: this
        });
        this.pointers.splice(pointerIndex, 1);
        this.pointerIsDown = false;
      }
      _updateLatestPointer(pointer, event, eventTarget) {
        this._latestPointer.pointer = pointer;
        this._latestPointer.event = event;
        this._latestPointer.eventTarget = eventTarget;
      }
      destroy() {
        this._latestPointer.pointer = null;
        this._latestPointer.event = null;
        this._latestPointer.eventTarget = null;
      }
      _createPreparedEvent(event, phase, preEnd, type) {
        return new InteractEvent_InteractEvent(this, event, this.prepared.name, phase, this.element, preEnd, type);
      }
      _fireEvent(iEvent) {
        this.interactable.fire(iEvent);
        if (!this.prevEvent || iEvent.timeStamp >= this.prevEvent.timeStamp) {
          this.prevEvent = iEvent;
        }
      }
      _doPhase(signalArg) {
        const {
          event,
          phase,
          preEnd,
          type
        } = signalArg;
        const {
          rect
        } = this;
        if (rect && phase === 'move') {
          // update the rect changes due to pointer move
          addEdges(this.edges, rect, this.coords.delta[this.interactable.options.deltaSource]);
          rect.width = rect.right - rect.left;
          rect.height = rect.bottom - rect.top;
        }
        const beforeResult = this._scopeFire("interactions:before-action-".concat(phase), signalArg);
        if (beforeResult === false) {
          return false;
        }
        const iEvent = signalArg.iEvent = this._createPreparedEvent(event, phase, preEnd, type);
        this._scopeFire("interactions:action-".concat(phase), signalArg);
        if (phase === 'start') {
          this.prevEvent = iEvent;
        }
        this._fireEvent(iEvent);
        this._scopeFire("interactions:after-action-".concat(phase), signalArg);
        return true;
      }
      _now() {
        return Date.now();
      }
    }
    /* harmony default export */
    var core_Interaction = Interaction_Interaction;

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/interactablePreventDefault.js

    function preventDefault(newValue) {
      if (/^(always|never|auto)$/.test(newValue)) {
        this.options.preventDefault = newValue;
        return this;
      }
      if (is.bool(newValue)) {
        this.options.preventDefault = newValue ? 'always' : 'never';
        return this;
      }
      return this.options.preventDefault;
    }
    function checkAndPreventDefault(interactable, scope, event) {
      const setting = interactable.options.preventDefault;
      if (setting === 'never') {
        return;
      }
      if (setting === 'always') {
        event.preventDefault();
        return;
      } // setting === 'auto'
      // if the browser supports passive event listeners and isn't running on iOS,
      // don't preventDefault of touch{start,move} events. CSS touch-action and
      // user-select should be used instead of calling event.preventDefault().

      if (scope.events.supportsPassive && /^touch(start|move)$/.test(event.type)) {
        const doc = getWindow(event.target).document;
        const docOptions = scope.getDocOptions(doc);
        if (!(docOptions && docOptions.events) || docOptions.events.passive !== false) {
          return;
        }
      } // don't preventDefault of pointerdown events

      if (/^(mouse|pointer|touch)*(down|start)/i.test(event.type)) {
        return;
      } // don't preventDefault on editable elements

      if (is.element(event.target) && matchesSelector(event.target, 'input,select,textarea,[contenteditable=true],[contenteditable=true] *')) {
        return;
      }
      event.preventDefault();
    }
    function onInteractionEvent(_ref4) {
      let {
        interaction,
        event
      } = _ref4;
      if (interaction.interactable) {
        interaction.interactable.checkAndPreventDefault(event);
      }
    }
    function interactablePreventDefault_install(scope) {
      /** @lends Interactable */
      const {
        Interactable
      } = scope;
      /**
       * Returns or sets whether to prevent the browser's default behaviour in
       * response to pointer events. Can be set to:
       *  - `'always'` to always prevent
       *  - `'never'` to never prevent
       *  - `'auto'` to let interact.js try to determine what would be best
       *
       * @param {string} [newValue] `'always'`, `'never'` or `'auto'`
       * @return {string | Interactable} The current setting or this Interactable
       */

      Interactable.prototype.preventDefault = preventDefault;
      Interactable.prototype.checkAndPreventDefault = function (event) {
        return checkAndPreventDefault(this, scope, event);
      }; // prevent native HTML5 drag on interact.js target elements

      scope.interactions.docEvents.push({
        type: 'dragstart',
        listener(event) {
          for (const interaction of scope.interactions.list) {
            if (interaction.element && (interaction.element === event.target || nodeContains(interaction.element, event.target))) {
              interaction.interactable.checkAndPreventDefault(event);
              return;
            }
          }
        }
      });
    }
    /* harmony default export */
    var interactablePreventDefault = {
      id: 'core/interactablePreventDefault',
      install: interactablePreventDefault_install,
      listeners: ['down', 'move', 'up', 'cancel'].reduce((acc, eventType) => {
        acc["interactions:".concat(eventType)] = onInteractionEvent;
        return acc;
      }, {})
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/interactionFinder.js

    const finder = {
      methodOrder: ['simulationResume', 'mouseOrPen', 'hasPointer', 'idle'],
      search(details) {
        for (const method of finder.methodOrder) {
          const interaction = finder[method](details);
          if (interaction) {
            return interaction;
          }
        }
        return null;
      },
      // try to resume simulation with a new pointer
      simulationResume(_ref5) {
        let {
          pointerType,
          eventType,
          eventTarget,
          scope
        } = _ref5;
        if (!/down|start/i.test(eventType)) {
          return null;
        }
        for (const interaction of scope.interactions.list) {
          let element = eventTarget;
          if (interaction.simulation && interaction.simulation.allowResume && interaction.pointerType === pointerType) {
            while (element) {
              // if the element is the interaction element
              if (element === interaction.element) {
                return interaction;
              }
              element = parentNode(element);
            }
          }
        }
        return null;
      },
      // if it's a mouse or pen interaction
      mouseOrPen(_ref6) {
        let {
          pointerId,
          pointerType,
          eventType,
          scope
        } = _ref6;
        if (pointerType !== 'mouse' && pointerType !== 'pen') {
          return null;
        }
        let firstNonActive;
        for (const interaction of scope.interactions.list) {
          if (interaction.pointerType === pointerType) {
            // if it's a down event, skip interactions with running simulations
            if (interaction.simulation && !hasPointerId(interaction, pointerId)) {
              continue;
            } // if the interaction is active, return it immediately

            if (interaction.interacting()) {
              return interaction;
            } // otherwise save it and look for another active interaction
            else if (!firstNonActive) {
              firstNonActive = interaction;
            }
          }
        } // if no active mouse interaction was found use the first inactive mouse
        // interaction

        if (firstNonActive) {
          return firstNonActive;
        } // find any mouse or pen interaction.
        // ignore the interaction if the eventType is a *down, and a simulation
        // is active

        for (const interaction of scope.interactions.list) {
          if (interaction.pointerType === pointerType && !(/down/i.test(eventType) && interaction.simulation)) {
            return interaction;
          }
        }
        return null;
      },
      // get interaction that has this pointer
      hasPointer(_ref7) {
        let {
          pointerId,
          scope
        } = _ref7;
        for (const interaction of scope.interactions.list) {
          if (hasPointerId(interaction, pointerId)) {
            return interaction;
          }
        }
        return null;
      },
      // get first idle interaction with a matching pointerType
      idle(_ref8) {
        let {
          pointerType,
          scope
        } = _ref8;
        for (const interaction of scope.interactions.list) {
          // if there's already a pointer held down
          if (interaction.pointers.length === 1) {
            const target = interaction.interactable; // don't add this pointer if there is a target interactable and it
            // isn't gesturable

            if (target && !(target.options.gesture && target.options.gesture.enabled)) {
              continue;
            }
          } // maximum of 2 pointers per interaction
          else if (interaction.pointers.length >= 2) {
            continue;
          }
          if (!interaction.interacting() && pointerType === interaction.pointerType) {
            return interaction;
          }
        }
        return null;
      }
    };
    function hasPointerId(interaction, pointerId) {
      return interaction.pointers.some(_ref9 => {
        let {
          id
        } = _ref9;
        return id === pointerId;
      });
    }

    /* harmony default export */
    var interactionFinder = finder;

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/interactions.js

    const methodNames = ['pointerDown', 'pointerMove', 'pointerUp', 'updatePointer', 'removePointer', 'windowBlur'];
    function interactions_install(scope) {
      const listeners = {};
      for (const method of methodNames) {
        listeners[method] = doOnInteractions(method, scope);
      }
      const pEventTypes = utils_browser.pEventTypes;
      let docEvents;
      if (utils_domObjects.PointerEvent) {
        docEvents = [{
          type: pEventTypes.down,
          listener: releasePointersOnRemovedEls
        }, {
          type: pEventTypes.down,
          listener: listeners.pointerDown
        }, {
          type: pEventTypes.move,
          listener: listeners.pointerMove
        }, {
          type: pEventTypes.up,
          listener: listeners.pointerUp
        }, {
          type: pEventTypes.cancel,
          listener: listeners.pointerUp
        }];
      } else {
        docEvents = [{
          type: 'mousedown',
          listener: listeners.pointerDown
        }, {
          type: 'mousemove',
          listener: listeners.pointerMove
        }, {
          type: 'mouseup',
          listener: listeners.pointerUp
        }, {
          type: 'touchstart',
          listener: releasePointersOnRemovedEls
        }, {
          type: 'touchstart',
          listener: listeners.pointerDown
        }, {
          type: 'touchmove',
          listener: listeners.pointerMove
        }, {
          type: 'touchend',
          listener: listeners.pointerUp
        }, {
          type: 'touchcancel',
          listener: listeners.pointerUp
        }];
      }
      docEvents.push({
        type: 'blur',
        listener(event) {
          for (const interaction of scope.interactions.list) {
            interaction.documentBlur(event);
          }
        }
      }); // for ignoring browser's simulated mouse events

      scope.prevTouchTime = 0;
      scope.Interaction = class extends core_Interaction {
        get pointerMoveTolerance() {
          return scope.interactions.pointerMoveTolerance;
        }
        set pointerMoveTolerance(value) {
          scope.interactions.pointerMoveTolerance = value;
        }
        _now() {
          return scope.now();
        }
      };
      scope.interactions = {
        // all active and idle interactions
        list: [],
        new(options) {
          options.scopeFire = (name, arg) => scope.fire(name, arg);
          const interaction = new scope.Interaction(options);
          scope.interactions.list.push(interaction);
          return interaction;
        },
        listeners,
        docEvents,
        pointerMoveTolerance: 1
      };
      function releasePointersOnRemovedEls() {
        // for all inactive touch interactions with pointers down
        for (const interaction of scope.interactions.list) {
          if (!interaction.pointerIsDown || interaction.pointerType !== 'touch' || interaction._interacting) {
            continue;
          } // if a pointer is down on an element that is no longer in the DOM tree

          for (const pointer of interaction.pointers) {
            if (!scope.documents.some(_ref10 => {
              let {
                doc
              } = _ref10;
              return nodeContains(doc, pointer.downTarget);
            })) {
              // remove the pointer from the interaction
              interaction.removePointer(pointer.pointer, pointer.event);
            }
          }
        }
      }
      scope.usePlugin(interactablePreventDefault);
    }
    function doOnInteractions(method, scope) {
      return function (event) {
        const interactions = scope.interactions.list;
        const pointerType = getPointerType(event);
        const [eventTarget, curEventTarget] = getEventTargets(event);
        const matches = []; // [ [pointer, interaction], ...]

        if (/^touch/.test(event.type)) {
          scope.prevTouchTime = scope.now(); // @ts-expect-error

          for (const changedTouch of event.changedTouches) {
            const pointer = changedTouch;
            const pointerId = getPointerId(pointer);
            const searchDetails = {
              pointer,
              pointerId,
              pointerType,
              eventType: event.type,
              eventTarget,
              curEventTarget,
              scope
            };
            const interaction = getInteraction(searchDetails);
            matches.push([searchDetails.pointer, searchDetails.eventTarget, searchDetails.curEventTarget, interaction]);
          }
        } else {
          let invalidPointer = false;
          if (!utils_browser.supportsPointerEvent && /mouse/.test(event.type)) {
            // ignore mouse events while touch interactions are active
            for (let i = 0; i < interactions.length && !invalidPointer; i++) {
              invalidPointer = interactions[i].pointerType !== 'mouse' && interactions[i].pointerIsDown;
            } // try to ignore mouse events that are simulated by the browser
            // after a touch event

            invalidPointer = invalidPointer || scope.now() - scope.prevTouchTime < 500 ||
            // on iOS and Firefox Mobile, MouseEvent.timeStamp is zero if simulated
            event.timeStamp === 0;
          }
          if (!invalidPointer) {
            const searchDetails = {
              pointer: event,
              pointerId: getPointerId(event),
              pointerType,
              eventType: event.type,
              curEventTarget,
              eventTarget,
              scope
            };
            const interaction = getInteraction(searchDetails);
            matches.push([searchDetails.pointer, searchDetails.eventTarget, searchDetails.curEventTarget, interaction]);
          }
        } // eslint-disable-next-line no-shadow

        for (const [pointer, eventTarget, curEventTarget, interaction] of matches) {
          interaction[method](pointer, event, eventTarget, curEventTarget);
        }
      };
    }
    function getInteraction(searchDetails) {
      const {
        pointerType,
        scope
      } = searchDetails;
      const foundInteraction = interactionFinder.search(searchDetails);
      const signalArg = {
        interaction: foundInteraction,
        searchDetails
      };
      scope.fire('interactions:find', signalArg);
      return signalArg.interaction || scope.interactions.new({
        pointerType
      });
    }
    function onDocSignal(_ref11, eventMethodName) {
      let {
        doc,
        scope,
        options
      } = _ref11;
      const {
        interactions: {
          docEvents
        },
        events
      } = scope;
      const eventMethod = events[eventMethodName];
      if (scope.browser.isIOS && !options.events) {
        options.events = {
          passive: false
        };
      } // delegate event listener

      for (const eventType in events.delegatedEvents) {
        eventMethod(doc, eventType, events.delegateListener);
        eventMethod(doc, eventType, events.delegateUseCapture, true);
      }
      const eventOptions = options && options.events;
      for (const {
        type,
        listener
      } of docEvents) {
        eventMethod(doc, type, listener, eventOptions);
      }
    }
    const interactions_interactions = {
      id: 'core/interactions',
      install: interactions_install,
      listeners: {
        'scope:add-document': arg => onDocSignal(arg, 'add'),
        'scope:remove-document': arg => onDocSignal(arg, 'remove'),
        'interactable:unset': (_ref12, scope) => {
          let {
            interactable
          } = _ref12;
          // Stop and destroy related interactions when an Interactable is unset
          for (let i = scope.interactions.list.length - 1; i >= 0; i--) {
            const interaction = scope.interactions.list[i];
            if (interaction.interactable !== interactable) {
              continue;
            }
            interaction.stop();
            scope.fire('interactions:destroy', {
              interaction
            });
            interaction.destroy();
            if (scope.interactions.list.length > 2) {
              scope.interactions.list.splice(i, 1);
            }
          }
        }
      },
      onDocSignal,
      doOnInteractions,
      methodNames
    };
    /* harmony default export */
    var core_interactions = interactions_interactions;

    // CONCATENATED MODULE: ./node_modules/@interactjs/core/scope.js

    class scope_Scope {
      // main window
      // main document
      // main window
      // all documents being listened to
      constructor() {
        this.id = "__interact_scope_".concat(Math.floor(Math.random() * 100));
        this.isInitialized = false;
        this.listenerMaps = [];
        this.browser = utils_browser;
        this.defaults = clone(defaultOptions_defaults);
        this.Eventable = Eventable_Eventable;
        this.actions = {
          map: {},
          phases: {
            start: true,
            move: true,
            end: true
          },
          methodDict: {},
          phaselessTypes: {}
        };
        this.interactStatic = createInteractStatic(this);
        this.InteractEvent = InteractEvent_InteractEvent;
        this.Interactable = void 0;
        this.interactables = new InteractableSet_InteractableSet(this);
        this._win = void 0;
        this.document = void 0;
        this.window = void 0;
        this.documents = [];
        this._plugins = {
          list: [],
          map: {}
        };
        this.onWindowUnload = event => this.removeDocument(event.target);
        const scope = this;
        this.Interactable = class extends Interactable_Interactable {
          get _defaults() {
            return scope.defaults;
          }
          set(options) {
            super.set(options);
            scope.fire('interactable:set', {
              options,
              interactable: this
            });
            return this;
          }
          unset() {
            super.unset();
            scope.interactables.list.splice(scope.interactables.list.indexOf(this), 1);
            scope.fire('interactable:unset', {
              interactable: this
            });
          }
        };
      }
      addListeners(map, id) {
        this.listenerMaps.push({
          id,
          map
        });
      }
      fire(name, arg) {
        for (const {
          map: {
            [name]: listener
          }
        } of this.listenerMaps) {
          if (!!listener && listener(arg, this, name) === false) {
            return false;
          }
        }
      }
      init(window) {
        return this.isInitialized ? this : initScope(this, window);
      }
      pluginIsInstalled(plugin) {
        return this._plugins.map[plugin.id] || this._plugins.list.indexOf(plugin) !== -1;
      }
      usePlugin(plugin, options) {
        if (!this.isInitialized) {
          return this;
        }
        if (this.pluginIsInstalled(plugin)) {
          return this;
        }
        if (plugin.id) {
          this._plugins.map[plugin.id] = plugin;
        }
        this._plugins.list.push(plugin);
        if (plugin.install) {
          plugin.install(this, options);
        }
        if (plugin.listeners && plugin.before) {
          let index = 0;
          const len = this.listenerMaps.length;
          const before = plugin.before.reduce((acc, id) => {
            acc[id] = true;
            acc[pluginIdRoot(id)] = true;
            return acc;
          }, {});
          for (; index < len; index++) {
            const otherId = this.listenerMaps[index].id;
            if (before[otherId] || before[pluginIdRoot(otherId)]) {
              break;
            }
          }
          this.listenerMaps.splice(index, 0, {
            id: plugin.id,
            map: plugin.listeners
          });
        } else if (plugin.listeners) {
          this.listenerMaps.push({
            id: plugin.id,
            map: plugin.listeners
          });
        }
        return this;
      }
      addDocument(doc, options) {
        // do nothing if document is already known
        if (this.getDocIndex(doc) !== -1) {
          return false;
        }
        const window = getWindow(doc);
        options = options ? extend({}, options) : {};
        this.documents.push({
          doc,
          options
        });
        this.events.documents.push(doc); // don't add an unload event for the main document
        // so that the page may be cached in browser history

        if (doc !== this.document) {
          this.events.add(window, 'unload', this.onWindowUnload);
        }
        this.fire('scope:add-document', {
          doc,
          window,
          scope: this,
          options
        });
      }
      removeDocument(doc) {
        const index = this.getDocIndex(doc);
        const window = getWindow(doc);
        const options = this.documents[index].options;
        this.events.remove(window, 'unload', this.onWindowUnload);
        this.documents.splice(index, 1);
        this.events.documents.splice(index, 1);
        this.fire('scope:remove-document', {
          doc,
          window,
          scope: this,
          options
        });
      }
      getDocIndex(doc) {
        for (let i = 0; i < this.documents.length; i++) {
          if (this.documents[i].doc === doc) {
            return i;
          }
        }
        return -1;
      }
      getDocOptions(doc) {
        const docIndex = this.getDocIndex(doc);
        return docIndex === -1 ? null : this.documents[docIndex].options;
      }
      now() {
        return (this.window.Date || Date).now();
      }
    }
    function initScope(scope, window) {
      scope.isInitialized = true;
      window_init(window);
      utils_domObjects.init(window);
      utils_browser.init(window);
      raf.init(window);
      scope.window = window;
      scope.document = window.document;
      scope.usePlugin(core_interactions);
      scope.usePlugin(events);
      return scope;
    }
    function pluginIdRoot(id) {
      return id && id.replace(/\/.*$/, '');
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/interact/index.js

    const interact_scope = new scope_Scope();
    const interact_interact = interact_scope.interactStatic;
    /* harmony default export */
    var _interactjs_interact = interact_interact;
    const interact_init = win => interact_scope.init(win);
    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-start/InteractableMethods.js

    function InteractableMethods_install(scope) {
      const {
        /** @lends Interactable */
        Interactable // tslint:disable-line no-shadowed-variable
      } = scope;
      Interactable.prototype.getAction = function getAction(pointer, event, interaction, element) {
        const action = defaultActionChecker(this, event, interaction, element, scope);
        if (this.options.actionChecker) {
          return this.options.actionChecker(pointer, event, action, this, element, interaction);
        }
        return action;
      };
      /**
       * If the target of the `mousedown`, `pointerdown` or `touchstart` event or any
       * of it's parents match the given CSS selector or Element, no
       * drag/resize/gesture is started.
       *
       * @deprecated
       * Don't use this method. Instead set the `ignoreFrom` option for each action
       * or for `pointerEvents`
       *
       * ```js
       * interact(targett)
       *   .draggable({
       *     ignoreFrom: 'input, textarea, a[href]'',
       *   })
       *   .pointerEvents({
       *     ignoreFrom: '[no-pointer]',
       *   })
       * ```
       *
       * @param {string | Element | null} [newValue] a CSS selector string, an
       * Element or `null` to not ignore any elements
       * @return {string | Element | object} The current ignoreFrom value or this
       * Interactable
       */

      Interactable.prototype.ignoreFrom = warnOnce(function (newValue) {
        return this._backCompatOption('ignoreFrom', newValue);
      }, 'Interactable.ignoreFrom() has been deprecated. Use Interactble.draggable({ignoreFrom: newValue}).');
      /**
       *
       * A drag/resize/gesture is started only If the target of the `mousedown`,
       * `pointerdown` or `touchstart` event or any of it's parents match the given
       * CSS selector or Element.
       *
       * @deprecated
       * Don't use this method. Instead set the `allowFrom` option for each action
       * or for `pointerEvents`
       *
       * ```js
       * interact(targett)
       *   .resizable({
       *     allowFrom: '.resize-handle',
       *   .pointerEvents({
       *     allowFrom: '.handle',,
       *   })
       * ```
       *
       * @param {string | Element | null} [newValue] a CSS selector string, an
       * Element or `null` to allow from any element
       * @return {string | Element | object} The current allowFrom value or this
       * Interactable
       */

      Interactable.prototype.allowFrom = warnOnce(function (newValue) {
        return this._backCompatOption('allowFrom', newValue);
      }, 'Interactable.allowFrom() has been deprecated. Use Interactble.draggable({allowFrom: newValue}).');
      /**
       * ```js
       * interact('.resize-drag')
       *   .resizable(true)
       *   .draggable(true)
       *   .actionChecker(function (pointer, event, action, interactable, element, interaction) {
       *
       *     if (interact.matchesSelector(event.target, '.drag-handle')) {
       *       // force drag with handle target
       *       action.name = drag
       *     }
       *     else {
       *       // resize from the top and right edges
       *       action.name  = 'resize'
       *       action.edges = { top: true, right: true }
       *     }
       *
       *     return action
       * })
       * ```
       *
       * Returns or sets the function used to check action to be performed on
       * pointerDown
       *
       * @param {function | null} [checker] A function which takes a pointer event,
       * defaultAction string, interactable, element and interaction as parameters
       * and returns an object with name property 'drag' 'resize' or 'gesture' and
       * optionally an `edges` object with boolean 'top', 'left', 'bottom' and right
       * props.
       * @return {Function | Interactable} The checker function or this Interactable
       */

      Interactable.prototype.actionChecker = actionChecker;
      /**
       * Returns or sets whether the the cursor should be changed depending on the
       * action that would be performed if the mouse were pressed and dragged.
       *
       * @param {boolean} [newValue]
       * @return {boolean | Interactable} The current setting or this Interactable
       */

      Interactable.prototype.styleCursor = styleCursor;
    }
    function defaultActionChecker(interactable, event, interaction, element, scope) {
      const rect = interactable.getRect(element);
      const buttons = event.buttons || {
        0: 1,
        1: 4,
        3: 8,
        4: 16
      }[event.button];
      const arg = {
        action: null,
        interactable,
        interaction,
        element,
        rect,
        buttons
      };
      scope.fire('auto-start:check', arg);
      return arg.action;
    }
    function styleCursor(newValue) {
      if (is.bool(newValue)) {
        this.options.styleCursor = newValue;
        return this;
      }
      if (newValue === null) {
        delete this.options.styleCursor;
        return this;
      }
      return this.options.styleCursor;
    }
    function actionChecker(checker) {
      if (is.func(checker)) {
        this.options.actionChecker = checker;
        return this;
      }
      if (checker === null) {
        delete this.options.actionChecker;
        return this;
      }
      return this.options.actionChecker;
    }

    /* harmony default export */
    var InteractableMethods = {
      id: 'auto-start/interactableMethods',
      install: InteractableMethods_install
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-start/base.js

    function base_install(scope) {
      const {
        interactStatic: interact,
        defaults
      } = scope;
      scope.usePlugin(InteractableMethods);
      defaults.base.actionChecker = null;
      defaults.base.styleCursor = true;
      extend(defaults.perAction, {
        manualStart: false,
        max: Infinity,
        maxPerElement: 1,
        allowFrom: null,
        ignoreFrom: null,
        // only allow left button by default
        // see https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons#Return_value
        mouseButtons: 1
      });
      /**
       * Returns or sets the maximum number of concurrent interactions allowed.  By
       * default only 1 interaction is allowed at a time (for backwards
       * compatibility). To allow multiple interactions on the same Interactables and
       * elements, you need to enable it in the draggable, resizable and gesturable
       * `'max'` and `'maxPerElement'` options.
       *
       * @alias module:interact.maxInteractions
       *
       * @param {number} [newValue] Any number. newValue <= 0 means no interactions.
       */

      interact.maxInteractions = newValue => maxInteractions(newValue, scope);
      scope.autoStart = {
        // Allow this many interactions to happen simultaneously
        maxInteractions: Infinity,
        withinInteractionLimit,
        cursorElement: null
      };
    }
    function prepareOnDown(_ref13, scope) {
      let {
        interaction,
        pointer,
        event,
        eventTarget
      } = _ref13;
      if (interaction.interacting()) {
        return;
      }
      const actionInfo = getActionInfo(interaction, pointer, event, eventTarget, scope);
      prepare(interaction, actionInfo, scope);
    }
    function prepareOnMove(_ref14, scope) {
      let {
        interaction,
        pointer,
        event,
        eventTarget
      } = _ref14;
      if (interaction.pointerType !== 'mouse' || interaction.pointerIsDown || interaction.interacting()) {
        return;
      }
      const actionInfo = getActionInfo(interaction, pointer, event, eventTarget, scope);
      prepare(interaction, actionInfo, scope);
    }
    function startOnMove(arg, scope) {
      const {
        interaction
      } = arg;
      if (!interaction.pointerIsDown || interaction.interacting() || !interaction.pointerWasMoved || !interaction.prepared.name) {
        return;
      }
      scope.fire('autoStart:before-start', arg);
      const {
        interactable
      } = interaction;
      const actionName = interaction.prepared.name;
      if (actionName && interactable) {
        // check manualStart and interaction limit
        if (interactable.options[actionName].manualStart || !withinInteractionLimit(interactable, interaction.element, interaction.prepared, scope)) {
          interaction.stop();
        } else {
          interaction.start(interaction.prepared, interactable, interaction.element);
          setInteractionCursor(interaction, scope);
        }
      }
    }
    function clearCursorOnStop(_ref15, scope) {
      let {
        interaction
      } = _ref15;
      const {
        interactable
      } = interaction;
      if (interactable && interactable.options.styleCursor) {
        setCursor(interaction.element, '', scope);
      }
    } // Check if the current interactable supports the action.
    // If so, return the validated action. Otherwise, return null

    function validateAction(action, interactable, element, eventTarget, scope) {
      if (interactable.testIgnoreAllow(interactable.options[action.name], element, eventTarget) && interactable.options[action.name].enabled && withinInteractionLimit(interactable, element, action, scope)) {
        return action;
      }
      return null;
    }
    function validateMatches(interaction, pointer, event, matches, matchElements, eventTarget, scope) {
      for (let i = 0, len = matches.length; i < len; i++) {
        const match = matches[i];
        const matchElement = matchElements[i];
        const matchAction = match.getAction(pointer, event, interaction, matchElement);
        if (!matchAction) {
          continue;
        }
        const action = validateAction(matchAction, match, matchElement, eventTarget, scope);
        if (action) {
          return {
            action,
            interactable: match,
            element: matchElement
          };
        }
      }
      return {
        action: null,
        interactable: null,
        element: null
      };
    }
    function getActionInfo(interaction, pointer, event, eventTarget, scope) {
      let matches = [];
      let matchElements = [];
      let element = eventTarget;
      function pushMatches(interactable) {
        matches.push(interactable);
        matchElements.push(element);
      }
      while (is.element(element)) {
        matches = [];
        matchElements = [];
        scope.interactables.forEachMatch(element, pushMatches);
        const actionInfo = validateMatches(interaction, pointer, event, matches, matchElements, eventTarget, scope);
        if (actionInfo.action && !actionInfo.interactable.options[actionInfo.action.name].manualStart) {
          return actionInfo;
        }
        element = parentNode(element);
      }
      return {
        action: null,
        interactable: null,
        element: null
      };
    }
    function prepare(interaction, _ref16, scope) {
      let {
        action,
        interactable,
        element
      } = _ref16;
      action = action || {
        name: null
      };
      interaction.interactable = interactable;
      interaction.element = element;
      copyAction(interaction.prepared, action);
      interaction.rect = interactable && action.name ? interactable.getRect(element) : null;
      setInteractionCursor(interaction, scope);
      scope.fire('autoStart:prepared', {
        interaction
      });
    }
    function withinInteractionLimit(interactable, element, action, scope) {
      const options = interactable.options;
      const maxActions = options[action.name].max;
      const maxPerElement = options[action.name].maxPerElement;
      const autoStartMax = scope.autoStart.maxInteractions;
      let activeInteractions = 0;
      let interactableCount = 0;
      let elementCount = 0; // no actions if any of these values == 0

      if (!(maxActions && maxPerElement && autoStartMax)) {
        return false;
      }
      for (const interaction of scope.interactions.list) {
        const otherAction = interaction.prepared.name;
        if (!interaction.interacting()) {
          continue;
        }
        activeInteractions++;
        if (activeInteractions >= autoStartMax) {
          return false;
        }
        if (interaction.interactable !== interactable) {
          continue;
        }
        interactableCount += otherAction === action.name ? 1 : 0;
        if (interactableCount >= maxActions) {
          return false;
        }
        if (interaction.element === element) {
          elementCount++;
          if (otherAction === action.name && elementCount >= maxPerElement) {
            return false;
          }
        }
      }
      return autoStartMax > 0;
    }
    function maxInteractions(newValue, scope) {
      if (is.number(newValue)) {
        scope.autoStart.maxInteractions = newValue;
        return this;
      }
      return scope.autoStart.maxInteractions;
    }
    function setCursor(element, cursor, scope) {
      const {
        cursorElement: prevCursorElement
      } = scope.autoStart;
      if (prevCursorElement && prevCursorElement !== element) {
        prevCursorElement.style.cursor = '';
      }
      element.ownerDocument.documentElement.style.cursor = cursor;
      element.style.cursor = cursor;
      scope.autoStart.cursorElement = cursor ? element : null;
    }
    function setInteractionCursor(interaction, scope) {
      const {
        interactable,
        element,
        prepared
      } = interaction;
      if (!(interaction.pointerType === 'mouse' && interactable && interactable.options.styleCursor)) {
        // clear previous target element cursor
        if (scope.autoStart.cursorElement) {
          setCursor(scope.autoStart.cursorElement, '', scope);
        }
        return;
      }
      let cursor = '';
      if (prepared.name) {
        const cursorChecker = interactable.options[prepared.name].cursorChecker;
        if (is.func(cursorChecker)) {
          cursor = cursorChecker(prepared, interactable, element, interaction._interacting);
        } else {
          cursor = scope.actions.map[prepared.name].getCursor(prepared);
        }
      }
      setCursor(interaction.element, cursor || '', scope);
    }
    const autoStart = {
      id: 'auto-start/base',
      before: ['actions'],
      install: base_install,
      listeners: {
        'interactions:down': prepareOnDown,
        'interactions:move': (arg, scope) => {
          prepareOnMove(arg, scope);
          startOnMove(arg, scope);
        },
        'interactions:stop': clearCursorOnStop
      },
      maxInteractions,
      withinInteractionLimit,
      validateAction
    };
    /* harmony default export */
    var base = autoStart;

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-start/dragAxis.js

    function beforeStart(_ref17, scope) {
      let {
        interaction,
        eventTarget,
        dx,
        dy
      } = _ref17;
      if (interaction.prepared.name !== 'drag') {
        return;
      } // check if a drag is in the correct axis

      const absX = Math.abs(dx);
      const absY = Math.abs(dy);
      const targetOptions = interaction.interactable.options.drag;
      const startAxis = targetOptions.startAxis;
      const currentAxis = absX > absY ? 'x' : absX < absY ? 'y' : 'xy';
      interaction.prepared.axis = targetOptions.lockAxis === 'start' ? currentAxis[0] // always lock to one axis even if currentAxis === 'xy'
      : targetOptions.lockAxis; // if the movement isn't in the startAxis of the interactable

      if (currentAxis !== 'xy' && startAxis !== 'xy' && startAxis !== currentAxis) {
        // cancel the prepared action
        interaction.prepared.name = null; // then try to get a drag from another ineractable

        let element = eventTarget;
        const getDraggable = function getDraggable(interactable) {
          if (interactable === interaction.interactable) {
            return;
          }
          const options = interaction.interactable.options.drag;
          if (!options.manualStart && interactable.testIgnoreAllow(options, element, eventTarget)) {
            const action = interactable.getAction(interaction.downPointer, interaction.downEvent, interaction, element);
            if (action && action.name === 'drag' && checkStartAxis(currentAxis, interactable) && base.validateAction(action, interactable, element, eventTarget, scope)) {
              return interactable;
            }
          }
        }; // check all interactables

        while (is.element(element)) {
          const interactable = scope.interactables.forEachMatch(element, getDraggable);
          if (interactable) {
            interaction.prepared.name = 'drag';
            interaction.interactable = interactable;
            interaction.element = element;
            break;
          }
          element = parentNode(element);
        }
      }
    }
    function checkStartAxis(startAxis, interactable) {
      if (!interactable) {
        return false;
      }
      const thisAxis = interactable.options.drag.startAxis;
      return startAxis === 'xy' || thisAxis === 'xy' || thisAxis === startAxis;
    }

    /* harmony default export */
    var dragAxis = {
      id: 'auto-start/dragAxis',
      listeners: {
        'autoStart:before-start': beforeStart
      }
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-start/hold.js

    function hold_install(scope) {
      const {
        defaults
      } = scope;
      scope.usePlugin(base);
      defaults.perAction.hold = 0;
      defaults.perAction.delay = 0;
    }
    function getHoldDuration(interaction) {
      const actionName = interaction.prepared && interaction.prepared.name;
      if (!actionName) {
        return null;
      }
      const options = interaction.interactable.options;
      return options[actionName].hold || options[actionName].delay;
    }
    const hold = {
      id: 'auto-start/hold',
      install: hold_install,
      listeners: {
        'interactions:new': _ref18 => {
          let {
            interaction
          } = _ref18;
          interaction.autoStartHoldTimer = null;
        },
        'autoStart:prepared': _ref19 => {
          let {
            interaction
          } = _ref19;
          const hold = getHoldDuration(interaction);
          if (hold > 0) {
            interaction.autoStartHoldTimer = setTimeout(() => {
              interaction.start(interaction.prepared, interaction.interactable, interaction.element);
            }, hold);
          }
        },
        'interactions:move': _ref20 => {
          let {
            interaction,
            duplicate
          } = _ref20;
          if (interaction.autoStartHoldTimer && interaction.pointerWasMoved && !duplicate) {
            clearTimeout(interaction.autoStartHoldTimer);
            interaction.autoStartHoldTimer = null;
          }
        },
        // prevent regular down->move autoStart
        'autoStart:before-start': _ref21 => {
          let {
            interaction
          } = _ref21;
          const holdDuration = getHoldDuration(interaction);
          if (holdDuration > 0) {
            interaction.prepared.name = null;
          }
        }
      },
      getHoldDuration
    };
    /* harmony default export */
    var auto_start_hold = hold;

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-start/plugin.js

    /* harmony default export */
    var auto_start_plugin = {
      id: 'auto-start',
      install(scope) {
        scope.usePlugin(base);
        scope.usePlugin(auto_start_hold);
        scope.usePlugin(dragAxis);
      }
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-start/index.js
    /* eslint-disable import/order, no-console, eol-last */

    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }
    _interactjs_interact.use(auto_start_plugin);

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-scroll/plugin.js

    function plugin_install(scope) {
      const {
        defaults,
        actions
      } = scope;
      scope.autoScroll = autoScroll;
      autoScroll.now = () => scope.now();
      actions.phaselessTypes.autoscroll = true;
      defaults.perAction.autoScroll = autoScroll.defaults;
    }
    const autoScroll = {
      defaults: {
        enabled: false,
        margin: 60,
        // the item that is scrolled (Window or HTMLElement)
        container: null,
        // the scroll speed in pixels per second
        speed: 300
      },
      now: Date.now,
      interaction: null,
      i: 0,
      // the handle returned by window.setInterval
      // Direction each pulse is to scroll in
      x: 0,
      y: 0,
      isScrolling: false,
      prevTime: 0,
      margin: 0,
      speed: 0,
      start(interaction) {
        autoScroll.isScrolling = true;
        raf.cancel(autoScroll.i);
        interaction.autoScroll = autoScroll;
        autoScroll.interaction = interaction;
        autoScroll.prevTime = autoScroll.now();
        autoScroll.i = raf.request(autoScroll.scroll);
      },
      stop() {
        autoScroll.isScrolling = false;
        if (autoScroll.interaction) {
          autoScroll.interaction.autoScroll = null;
        }
        raf.cancel(autoScroll.i);
      },
      // scroll the window by the values in scroll.x/y
      scroll() {
        const {
          interaction
        } = autoScroll;
        const {
          interactable,
          element
        } = interaction;
        const actionName = interaction.prepared.name;
        const options = interactable.options[actionName].autoScroll;
        const container = getContainer(options.container, interactable, element);
        const now = autoScroll.now(); // change in time in seconds

        const dt = (now - autoScroll.prevTime) / 1000; // displacement

        const s = options.speed * dt;
        if (s >= 1) {
          const scrollBy = {
            x: autoScroll.x * s,
            y: autoScroll.y * s
          };
          if (scrollBy.x || scrollBy.y) {
            const prevScroll = getScroll(container);
            if (is.window(container)) {
              container.scrollBy(scrollBy.x, scrollBy.y);
            } else if (container) {
              container.scrollLeft += scrollBy.x;
              container.scrollTop += scrollBy.y;
            }
            const curScroll = getScroll(container);
            const delta = {
              x: curScroll.x - prevScroll.x,
              y: curScroll.y - prevScroll.y
            };
            if (delta.x || delta.y) {
              interactable.fire({
                type: 'autoscroll',
                target: element,
                interactable,
                delta,
                interaction,
                container
              });
            }
          }
          autoScroll.prevTime = now;
        }
        if (autoScroll.isScrolling) {
          raf.cancel(autoScroll.i);
          autoScroll.i = raf.request(autoScroll.scroll);
        }
      },
      check(interactable, actionName) {
        var _options$actionName$a;
        const options = interactable.options;
        return (_options$actionName$a = options[actionName].autoScroll) == null ? void 0 : _options$actionName$a.enabled;
      },
      onInteractionMove(_ref22) {
        let {
          interaction,
          pointer
        } = _ref22;
        if (!(interaction.interacting() && autoScroll.check(interaction.interactable, interaction.prepared.name))) {
          return;
        }
        if (interaction.simulation) {
          autoScroll.x = autoScroll.y = 0;
          return;
        }
        let top;
        let right;
        let bottom;
        let left;
        const {
          interactable,
          element
        } = interaction;
        const actionName = interaction.prepared.name;
        const options = interactable.options[actionName].autoScroll;
        const container = getContainer(options.container, interactable, element);
        if (is.window(container)) {
          left = pointer.clientX < autoScroll.margin;
          top = pointer.clientY < autoScroll.margin;
          right = pointer.clientX > container.innerWidth - autoScroll.margin;
          bottom = pointer.clientY > container.innerHeight - autoScroll.margin;
        } else {
          const rect = getElementClientRect(container);
          left = pointer.clientX < rect.left + autoScroll.margin;
          top = pointer.clientY < rect.top + autoScroll.margin;
          right = pointer.clientX > rect.right - autoScroll.margin;
          bottom = pointer.clientY > rect.bottom - autoScroll.margin;
        }
        autoScroll.x = right ? 1 : left ? -1 : 0;
        autoScroll.y = bottom ? 1 : top ? -1 : 0;
        if (!autoScroll.isScrolling) {
          // set the autoScroll properties to those of the target
          autoScroll.margin = options.margin;
          autoScroll.speed = options.speed;
          autoScroll.start(interaction);
        }
      }
    };
    function getContainer(value, interactable, element) {
      return (is.string(value) ? getStringOptionResult(value, interactable, element) : value) || getWindow(element);
    }
    function getScroll(container) {
      if (is.window(container)) {
        container = window.document.body;
      }
      return {
        x: container.scrollLeft,
        y: container.scrollTop
      };
    }
    function getScrollSize(container) {
      if (is.window(container)) {
        container = window.document.body;
      }
      return {
        x: container.scrollWidth,
        y: container.scrollHeight
      };
    }
    function getScrollSizeDelta(_ref23, func) {
      let {
        interaction,
        element
      } = _ref23;
      const scrollOptions = interaction && interaction.interactable.options[interaction.prepared.name].autoScroll;
      if (!scrollOptions || !scrollOptions.enabled) {
        func();
        return {
          x: 0,
          y: 0
        };
      }
      const scrollContainer = getContainer(scrollOptions.container, interaction.interactable, element);
      const prevSize = getScroll(scrollContainer);
      func();
      const curSize = getScroll(scrollContainer);
      return {
        x: curSize.x - prevSize.x,
        y: curSize.y - prevSize.y
      };
    }
    const autoScrollPlugin = {
      id: 'auto-scroll',
      install: plugin_install,
      listeners: {
        'interactions:new': _ref24 => {
          let {
            interaction
          } = _ref24;
          interaction.autoScroll = null;
        },
        'interactions:destroy': _ref25 => {
          let {
            interaction
          } = _ref25;
          interaction.autoScroll = null;
          autoScroll.stop();
          if (autoScroll.interaction) {
            autoScroll.interaction = null;
          }
        },
        'interactions:stop': autoScroll.stop,
        'interactions:action-move': arg => autoScroll.onInteractionMove(arg)
      }
    };
    /* harmony default export */
    var auto_scroll_plugin = autoScrollPlugin;

    // CONCATENATED MODULE: ./node_modules/@interactjs/auto-scroll/index.js
    /* eslint-disable import/order, no-console, eol-last */

    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }
    _interactjs_interact.use(auto_scroll_plugin);

    // CONCATENATED MODULE: ./node_modules/@interactjs/actions/drag/plugin.js

    function drag_plugin_install(scope) {
      const {
        actions,
        Interactable,
        defaults
      } = scope;
      Interactable.prototype.draggable = drag.draggable;
      actions.map.drag = drag;
      actions.methodDict.drag = 'draggable';
      defaults.actions.drag = drag.defaults;
    }
    function beforeMove(_ref26) {
      let {
        interaction
      } = _ref26;
      if (interaction.prepared.name !== 'drag') {
        return;
      }
      const axis = interaction.prepared.axis;
      if (axis === 'x') {
        interaction.coords.cur.page.y = interaction.coords.start.page.y;
        interaction.coords.cur.client.y = interaction.coords.start.client.y;
        interaction.coords.velocity.client.y = 0;
        interaction.coords.velocity.page.y = 0;
      } else if (axis === 'y') {
        interaction.coords.cur.page.x = interaction.coords.start.page.x;
        interaction.coords.cur.client.x = interaction.coords.start.client.x;
        interaction.coords.velocity.client.x = 0;
        interaction.coords.velocity.page.x = 0;
      }
    }
    function move(_ref27) {
      let {
        iEvent,
        interaction
      } = _ref27;
      if (interaction.prepared.name !== 'drag') {
        return;
      }
      const axis = interaction.prepared.axis;
      if (axis === 'x' || axis === 'y') {
        const opposite = axis === 'x' ? 'y' : 'x';
        iEvent.page[opposite] = interaction.coords.start.page[opposite];
        iEvent.client[opposite] = interaction.coords.start.client[opposite];
        iEvent.delta[opposite] = 0;
      }
    }
    /**
     * ```js
     * interact(element).draggable({
     *     onstart: function (event) {},
     *     onmove : function (event) {},
     *     onend  : function (event) {},
     *
     *     // the axis in which the first movement must be
     *     // for the drag sequence to start
     *     // 'xy' by default - any direction
     *     startAxis: 'x' || 'y' || 'xy',
     *
     *     // 'xy' by default - don't restrict to one axis (move in any direction)
     *     // 'x' or 'y' to restrict movement to either axis
     *     // 'start' to restrict movement to the axis the drag started in
     *     lockAxis: 'x' || 'y' || 'xy' || 'start',
     *
     *     // max number of drags that can happen concurrently
     *     // with elements of this Interactable. Infinity by default
     *     max: Infinity,
     *
     *     // max number of drags that can target the same element+Interactable
     *     // 1 by default
     *     maxPerElement: 2
     * })
     *
     * var isDraggable = interact('element').draggable(); // true
     * ```
     *
     * Get or set whether drag actions can be performed on the target
     *
     * @alias Interactable.prototype.draggable
     *
     * @param {boolean | object} [options] true/false or An object with event
     * listeners to be fired on drag events (object makes the Interactable
     * draggable)
     * @return {boolean | Interactable} boolean indicating if this can be the
     * target of drag events, or this Interctable
     */

    const plugin_draggable = function draggable(options) {
      if (is.object(options)) {
        this.options.drag.enabled = options.enabled !== false;
        this.setPerAction('drag', options);
        this.setOnEvents('drag', options);
        if (/^(xy|x|y|start)$/.test(options.lockAxis)) {
          this.options.drag.lockAxis = options.lockAxis;
        }
        if (/^(xy|x|y)$/.test(options.startAxis)) {
          this.options.drag.startAxis = options.startAxis;
        }
        return this;
      }
      if (is.bool(options)) {
        this.options.drag.enabled = options;
        return this;
      }
      return this.options.drag;
    };
    const drag = {
      id: 'actions/drag',
      install: drag_plugin_install,
      listeners: {
        'interactions:before-action-move': beforeMove,
        'interactions:action-resume': beforeMove,
        // dragmove
        'interactions:action-move': move,
        'auto-start:check': arg => {
          const {
            interaction,
            interactable,
            buttons
          } = arg;
          const dragOptions = interactable.options.drag;
          if (!(dragOptions && dragOptions.enabled) ||
          // check mouseButton setting if the pointer is down
          interaction.pointerIsDown && /mouse|pointer/.test(interaction.pointerType) && (buttons & interactable.options.drag.mouseButtons) === 0) {
            return undefined;
          }
          arg.action = {
            name: 'drag',
            axis: dragOptions.lockAxis === 'start' ? dragOptions.startAxis : dragOptions.lockAxis
          };
          return false;
        }
      },
      draggable: plugin_draggable,
      beforeMove,
      move,
      defaults: {
        startAxis: 'xy',
        lockAxis: 'xy'
      },
      getCursor() {
        return 'move';
      }
    };
    /* harmony default export */
    var drag_plugin = drag;

    // CONCATENATED MODULE: ./node_modules/@interactjs/actions/drag/index.js
    /* eslint-disable import/order, no-console, eol-last */

    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }
    _interactjs_interact.use(drag_plugin);

    // CONCATENATED MODULE: ./node_modules/@interactjs/actions/resize/plugin.js

    function resize_plugin_install(scope) {
      const {
        actions,
        browser,
        /** @lends Interactable */
        Interactable,
        // tslint:disable-line no-shadowed-variable
        defaults
      } = scope; // Less Precision with touch input

      resize.cursors = initCursors(browser);
      resize.defaultMargin = browser.supportsTouch || browser.supportsPointerEvent ? 20 : 10;
      /**
       * ```js
       * interact(element).resizable({
       *   onstart: function (event) {},
       *   onmove : function (event) {},
       *   onend  : function (event) {},
       *
       *   edges: {
       *     top   : true,       // Use pointer coords to check for resize.
       *     left  : false,      // Disable resizing from left edge.
       *     bottom: '.resize-s',// Resize if pointer target matches selector
       *     right : handleEl    // Resize if pointer target is the given Element
       *   },
       *
       *     // Width and height can be adjusted independently. When `true`, width and
       *     // height are adjusted at a 1:1 ratio.
       *     square: false,
       *
       *     // Width and height can be adjusted independently. When `true`, width and
       *     // height maintain the aspect ratio they had when resizing started.
       *     preserveAspectRatio: false,
       *
       *   // a value of 'none' will limit the resize rect to a minimum of 0x0
       *   // 'negate' will allow the rect to have negative width/height
       *   // 'reposition' will keep the width/height positive by swapping
       *   // the top and bottom edges and/or swapping the left and right edges
       *   invert: 'none' || 'negate' || 'reposition'
       *
       *   // limit multiple resizes.
       *   // See the explanation in the {@link Interactable.draggable} example
       *   max: Infinity,
       *   maxPerElement: 1,
       * })
       *
       * var isResizeable = interact(element).resizable()
       * ```
       *
       * Gets or sets whether resize actions can be performed on the target
       *
       * @param {boolean | object} [options] true/false or An object with event
       * listeners to be fired on resize events (object makes the Interactable
       * resizable)
       * @return {boolean | Interactable} A boolean indicating if this can be the
       * target of resize elements, or this Interactable
       */

      Interactable.prototype.resizable = function (options) {
        return resizable(this, options, scope);
      };
      actions.map.resize = resize;
      actions.methodDict.resize = 'resizable';
      defaults.actions.resize = resize.defaults;
    }
    function resizeChecker(arg) {
      const {
        interaction,
        interactable,
        element,
        rect,
        buttons
      } = arg;
      if (!rect) {
        return undefined;
      }
      const page = extend({}, interaction.coords.cur.page);
      const resizeOptions = interactable.options.resize;
      if (!(resizeOptions && resizeOptions.enabled) ||
      // check mouseButton setting if the pointer is down
      interaction.pointerIsDown && /mouse|pointer/.test(interaction.pointerType) && (buttons & resizeOptions.mouseButtons) === 0) {
        return undefined;
      } // if using resize.edges

      if (is.object(resizeOptions.edges)) {
        const resizeEdges = {
          left: false,
          right: false,
          top: false,
          bottom: false
        };
        for (const edge in resizeEdges) {
          resizeEdges[edge] = checkResizeEdge(edge, resizeOptions.edges[edge], page, interaction._latestPointer.eventTarget, element, rect, resizeOptions.margin || resize.defaultMargin);
        }
        resizeEdges.left = resizeEdges.left && !resizeEdges.right;
        resizeEdges.top = resizeEdges.top && !resizeEdges.bottom;
        if (resizeEdges.left || resizeEdges.right || resizeEdges.top || resizeEdges.bottom) {
          arg.action = {
            name: 'resize',
            edges: resizeEdges
          };
        }
      } else {
        const right = resizeOptions.axis !== 'y' && page.x > rect.right - resize.defaultMargin;
        const bottom = resizeOptions.axis !== 'x' && page.y > rect.bottom - resize.defaultMargin;
        if (right || bottom) {
          arg.action = {
            name: 'resize',
            axes: (right ? 'x' : '') + (bottom ? 'y' : '')
          };
        }
      }
      return arg.action ? false : undefined;
    }
    function resizable(interactable, options, scope) {
      if (is.object(options)) {
        interactable.options.resize.enabled = options.enabled !== false;
        interactable.setPerAction('resize', options);
        interactable.setOnEvents('resize', options);
        if (is.string(options.axis) && /^x$|^y$|^xy$/.test(options.axis)) {
          interactable.options.resize.axis = options.axis;
        } else if (options.axis === null) {
          interactable.options.resize.axis = scope.defaults.actions.resize.axis;
        }
        if (is.bool(options.preserveAspectRatio)) {
          interactable.options.resize.preserveAspectRatio = options.preserveAspectRatio;
        } else if (is.bool(options.square)) {
          interactable.options.resize.square = options.square;
        }
        return interactable;
      }
      if (is.bool(options)) {
        interactable.options.resize.enabled = options;
        return interactable;
      }
      return interactable.options.resize;
    }
    function checkResizeEdge(name, value, page, element, interactableElement, rect, margin) {
      // false, '', undefined, null
      if (!value) {
        return false;
      } // true value, use pointer coords and element rect

      if (value === true) {
        // if dimensions are negative, "switch" edges
        const width = is.number(rect.width) ? rect.width : rect.right - rect.left;
        const height = is.number(rect.height) ? rect.height : rect.bottom - rect.top; // don't use margin greater than half the relevent dimension

        margin = Math.min(margin, Math.abs((name === 'left' || name === 'right' ? width : height) / 2));
        if (width < 0) {
          if (name === 'left') {
            name = 'right';
          } else if (name === 'right') {
            name = 'left';
          }
        }
        if (height < 0) {
          if (name === 'top') {
            name = 'bottom';
          } else if (name === 'bottom') {
            name = 'top';
          }
        }
        if (name === 'left') {
          return page.x < (width >= 0 ? rect.left : rect.right) + margin;
        }
        if (name === 'top') {
          return page.y < (height >= 0 ? rect.top : rect.bottom) + margin;
        }
        if (name === 'right') {
          return page.x > (width >= 0 ? rect.right : rect.left) - margin;
        }
        if (name === 'bottom') {
          return page.y > (height >= 0 ? rect.bottom : rect.top) - margin;
        }
      } // the remaining checks require an element

      if (!is.element(element)) {
        return false;
      }
      return is.element(value) // the value is an element to use as a resize handle
      ? value === element // otherwise check if element matches value as selector
      : matchesUpTo(element, value, interactableElement);
    }
    /* eslint-disable multiline-ternary */

    function initCursors(browser) {
      return browser.isIe9 ? {
        x: 'e-resize',
        y: 's-resize',
        xy: 'se-resize',
        top: 'n-resize',
        left: 'w-resize',
        bottom: 's-resize',
        right: 'e-resize',
        topleft: 'se-resize',
        bottomright: 'se-resize',
        topright: 'ne-resize',
        bottomleft: 'ne-resize'
      } : {
        x: 'ew-resize',
        y: 'ns-resize',
        xy: 'nwse-resize',
        top: 'ns-resize',
        left: 'ew-resize',
        bottom: 'ns-resize',
        right: 'ew-resize',
        topleft: 'nwse-resize',
        bottomright: 'nwse-resize',
        topright: 'nesw-resize',
        bottomleft: 'nesw-resize'
      };
    }
    /* eslint-enable multiline-ternary */

    function start(_ref28) {
      let {
        iEvent,
        interaction
      } = _ref28;
      if (interaction.prepared.name !== 'resize' || !interaction.prepared.edges) {
        return;
      }
      const resizeEvent = iEvent;
      const rect = interaction.rect;
      interaction._rects = {
        start: extend({}, rect),
        corrected: extend({}, rect),
        previous: extend({}, rect),
        delta: {
          left: 0,
          right: 0,
          width: 0,
          top: 0,
          bottom: 0,
          height: 0
        }
      };
      resizeEvent.edges = interaction.prepared.edges;
      resizeEvent.rect = interaction._rects.corrected;
      resizeEvent.deltaRect = interaction._rects.delta;
    }
    function plugin_move(_ref29) {
      let {
        iEvent,
        interaction
      } = _ref29;
      if (interaction.prepared.name !== 'resize' || !interaction.prepared.edges) {
        return;
      }
      const resizeEvent = iEvent;
      const resizeOptions = interaction.interactable.options.resize;
      const invert = resizeOptions.invert;
      const invertible = invert === 'reposition' || invert === 'negate';
      const current = interaction.rect;
      const {
        start: startRect,
        corrected,
        delta: deltaRect,
        previous
      } = interaction._rects;
      extend(previous, corrected);
      if (invertible) {
        // if invertible, copy the current rect
        extend(corrected, current);
        if (invert === 'reposition') {
          // swap edge values if necessary to keep width/height positive
          if (corrected.top > corrected.bottom) {
            const swap = corrected.top;
            corrected.top = corrected.bottom;
            corrected.bottom = swap;
          }
          if (corrected.left > corrected.right) {
            const swap = corrected.left;
            corrected.left = corrected.right;
            corrected.right = swap;
          }
        }
      } else {
        // if not invertible, restrict to minimum of 0x0 rect
        corrected.top = Math.min(current.top, startRect.bottom);
        corrected.bottom = Math.max(current.bottom, startRect.top);
        corrected.left = Math.min(current.left, startRect.right);
        corrected.right = Math.max(current.right, startRect.left);
      }
      corrected.width = corrected.right - corrected.left;
      corrected.height = corrected.bottom - corrected.top;
      for (const edge in corrected) {
        deltaRect[edge] = corrected[edge] - previous[edge];
      }
      resizeEvent.edges = interaction.prepared.edges;
      resizeEvent.rect = corrected;
      resizeEvent.deltaRect = deltaRect;
    }
    function end(_ref30) {
      let {
        iEvent,
        interaction
      } = _ref30;
      if (interaction.prepared.name !== 'resize' || !interaction.prepared.edges) {
        return;
      }
      const resizeEvent = iEvent;
      resizeEvent.edges = interaction.prepared.edges;
      resizeEvent.rect = interaction._rects.corrected;
      resizeEvent.deltaRect = interaction._rects.delta;
    }
    function updateEventAxes(_ref31) {
      let {
        iEvent,
        interaction
      } = _ref31;
      if (interaction.prepared.name !== 'resize' || !interaction.resizeAxes) {
        return;
      }
      const options = interaction.interactable.options;
      const resizeEvent = iEvent;
      if (options.resize.square) {
        if (interaction.resizeAxes === 'y') {
          resizeEvent.delta.x = resizeEvent.delta.y;
        } else {
          resizeEvent.delta.y = resizeEvent.delta.x;
        }
        resizeEvent.axes = 'xy';
      } else {
        resizeEvent.axes = interaction.resizeAxes;
        if (interaction.resizeAxes === 'x') {
          resizeEvent.delta.y = 0;
        } else if (interaction.resizeAxes === 'y') {
          resizeEvent.delta.x = 0;
        }
      }
    }
    const resize = {
      id: 'actions/resize',
      before: ['actions/drag'],
      install: resize_plugin_install,
      listeners: {
        'interactions:new': _ref32 => {
          let {
            interaction
          } = _ref32;
          interaction.resizeAxes = 'xy';
        },
        'interactions:action-start': arg => {
          start(arg);
          updateEventAxes(arg);
        },
        'interactions:action-move': arg => {
          plugin_move(arg);
          updateEventAxes(arg);
        },
        'interactions:action-end': end,
        'auto-start:check': resizeChecker
      },
      defaults: {
        square: false,
        preserveAspectRatio: false,
        axis: 'xy',
        // use default margin
        margin: NaN,
        // object with props left, right, top, bottom which are
        // true/false values to resize when the pointer is over that edge,
        // CSS selectors to match the handles for each direction
        // or the Elements for each handle
        edges: null,
        // a value of 'none' will limit the resize rect to a minimum of 0x0
        // 'negate' will alow the rect to have negative width/height
        // 'reposition' will keep the width/height positive by swapping
        // the top and bottom edges and/or swapping the left and right edges
        invert: 'none'
      },
      cursors: null,
      getCursor(_ref33) {
        let {
          edges,
          axis,
          name
        } = _ref33;
        const cursors = resize.cursors;
        let result = null;
        if (axis) {
          result = cursors[name + axis];
        } else if (edges) {
          let cursorKey = '';
          for (const edge of ['top', 'bottom', 'left', 'right']) {
            if (edges[edge]) {
              cursorKey += edge;
            }
          }
          result = cursors[cursorKey];
        }
        return result;
      },
      defaultMargin: null
    };
    /* harmony default export */
    var resize_plugin = resize;

    // CONCATENATED MODULE: ./node_modules/@interactjs/actions/resize/index.js
    /* eslint-disable import/order, no-console, eol-last */

    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }
    _interactjs_interact.use(resize_plugin);

    // CONCATENATED MODULE: ./node_modules/@interactjs/snappers/edgeTarget.js
    /* harmony default export */
    var edgeTarget = () => {};

    // CONCATENATED MODULE: ./node_modules/@interactjs/snappers/elements.js
    /* harmony default export */
    var snappers_elements = () => {};

    // CONCATENATED MODULE: ./node_modules/@interactjs/snappers/grid.js
    /* harmony default export */
    var grid = grid => {
      const coordFields = [['x', 'y'], ['left', 'top'], ['right', 'bottom'], ['width', 'height']].filter(_ref34 => {
        let [xField, yField] = _ref34;
        return xField in grid || yField in grid;
      });
      const gridFunc = (x, y) => {
        const {
          range,
          limits = {
            left: -Infinity,
            right: Infinity,
            top: -Infinity,
            bottom: Infinity
          },
          offset = {
            x: 0,
            y: 0
          }
        } = grid;
        const result = {
          range,
          grid,
          x: null,
          y: null
        };
        for (const [xField, yField] of coordFields) {
          const gridx = Math.round((x - offset.x) / grid[xField]);
          const gridy = Math.round((y - offset.y) / grid[yField]);
          result[xField] = Math.max(limits.left, Math.min(limits.right, gridx * grid[xField] + offset.x));
          result[yField] = Math.max(limits.top, Math.min(limits.bottom, gridy * grid[yField] + offset.y));
        }
        return result;
      };
      gridFunc.grid = grid;
      gridFunc.coordFields = coordFields;
      return gridFunc;
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/snappers/all.js

    // CONCATENATED MODULE: ./node_modules/@interactjs/snappers/plugin.js

    const snappersPlugin = {
      id: 'snappers',
      install(scope) {
        const {
          interactStatic: interact
        } = scope;
        interact.snappers = extend(interact.snappers || {}, all_namespaceObject);
        interact.createSnapGrid = interact.snappers.grid;
      }
    };
    /* harmony default export */
    var snappers_plugin = snappersPlugin;

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/Modification.js

    class Modification_Modification {
      constructor(interaction) {
        this.states = [];
        this.startOffset = {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        };
        this.startDelta = null;
        this.result = null;
        this.endResult = null;
        this.edges = void 0;
        this.interaction = void 0;
        this.interaction = interaction;
        this.result = createResult();
      }
      start(_ref35, pageCoords) {
        let {
          phase
        } = _ref35;
        const {
          interaction
        } = this;
        const modifierList = getModifierList(interaction);
        this.prepareStates(modifierList);
        this.edges = extend({}, interaction.edges);
        this.startOffset = getRectOffset(interaction.rect, pageCoords);
        this.startDelta = {
          x: 0,
          y: 0
        };
        const arg = {
          phase,
          pageCoords,
          preEnd: false
        };
        this.result = createResult();
        this.startAll(arg);
        const result = this.result = this.setAll(arg);
        return result;
      }
      fillArg(arg) {
        const {
          interaction
        } = this;
        arg.interaction = interaction;
        arg.interactable = interaction.interactable;
        arg.element = interaction.element;
        arg.rect = arg.rect || interaction.rect;
        arg.edges = this.edges;
        arg.startOffset = this.startOffset;
      }
      startAll(arg) {
        this.fillArg(arg);
        for (const state of this.states) {
          if (state.methods.start) {
            arg.state = state;
            state.methods.start(arg);
          }
        }
      }
      setAll(arg) {
        this.fillArg(arg);
        const {
          phase,
          preEnd,
          skipModifiers,
          rect: unmodifiedRect
        } = arg;
        arg.coords = extend({}, arg.pageCoords);
        arg.rect = extend({}, unmodifiedRect);
        const states = skipModifiers ? this.states.slice(skipModifiers) : this.states;
        const newResult = createResult(arg.coords, arg.rect);
        for (const state of states) {
          const {
            options
          } = state;
          const lastModifierCoords = extend({}, arg.coords);
          let returnValue = null;
          if (state.methods.set && this.shouldDo(options, preEnd, phase)) {
            arg.state = state;
            returnValue = state.methods.set(arg);
            addEdges(this.interaction.edges, arg.rect, {
              x: arg.coords.x - lastModifierCoords.x,
              y: arg.coords.y - lastModifierCoords.y
            });
          }
          newResult.eventProps.push(returnValue);
        }
        newResult.delta.x = arg.coords.x - arg.pageCoords.x;
        newResult.delta.y = arg.coords.y - arg.pageCoords.y;
        newResult.rectDelta.left = arg.rect.left - unmodifiedRect.left;
        newResult.rectDelta.right = arg.rect.right - unmodifiedRect.right;
        newResult.rectDelta.top = arg.rect.top - unmodifiedRect.top;
        newResult.rectDelta.bottom = arg.rect.bottom - unmodifiedRect.bottom;
        const prevCoords = this.result.coords;
        const prevRect = this.result.rect;
        if (prevCoords && prevRect) {
          const rectChanged = newResult.rect.left !== prevRect.left || newResult.rect.right !== prevRect.right || newResult.rect.top !== prevRect.top || newResult.rect.bottom !== prevRect.bottom;
          newResult.changed = rectChanged || prevCoords.x !== newResult.coords.x || prevCoords.y !== newResult.coords.y;
        }
        return newResult;
      }
      applyToInteraction(arg) {
        const {
          interaction
        } = this;
        const {
          phase
        } = arg;
        const curCoords = interaction.coords.cur;
        const startCoords = interaction.coords.start;
        const {
          result,
          startDelta
        } = this;
        const curDelta = result.delta;
        if (phase === 'start') {
          extend(this.startDelta, result.delta);
        }
        for (const [coordsSet, delta] of [[startCoords, startDelta], [curCoords, curDelta]]) {
          coordsSet.page.x += delta.x;
          coordsSet.page.y += delta.y;
          coordsSet.client.x += delta.x;
          coordsSet.client.y += delta.y;
        }
        const {
          rectDelta
        } = this.result;
        const rect = arg.rect || interaction.rect;
        rect.left += rectDelta.left;
        rect.right += rectDelta.right;
        rect.top += rectDelta.top;
        rect.bottom += rectDelta.bottom;
        rect.width = rect.right - rect.left;
        rect.height = rect.bottom - rect.top;
      }
      setAndApply(arg) {
        const {
          interaction
        } = this;
        const {
          phase,
          preEnd,
          skipModifiers
        } = arg;
        const result = this.setAll({
          preEnd,
          phase,
          pageCoords: arg.modifiedCoords || interaction.coords.cur.page
        });
        this.result = result; // don't fire an action move if a modifier would keep the event in the same
        // cordinates as before

        if (!result.changed && (!skipModifiers || skipModifiers < this.states.length) && interaction.interacting()) {
          return false;
        }
        if (arg.modifiedCoords) {
          const {
            page
          } = interaction.coords.cur;
          const adjustment = {
            x: arg.modifiedCoords.x - page.x,
            y: arg.modifiedCoords.y - page.y
          };
          result.coords.x += adjustment.x;
          result.coords.y += adjustment.y;
          result.delta.x += adjustment.x;
          result.delta.y += adjustment.y;
        }
        this.applyToInteraction(arg);
      }
      beforeEnd(arg) {
        const {
          interaction,
          event
        } = arg;
        const states = this.states;
        if (!states || !states.length) {
          return;
        }
        let doPreend = false;
        for (const state of states) {
          arg.state = state;
          const {
            options,
            methods
          } = state;
          const endPosition = methods.beforeEnd && methods.beforeEnd(arg);
          if (endPosition) {
            this.endResult = endPosition;
            return false;
          }
          doPreend = doPreend || !doPreend && this.shouldDo(options, true, arg.phase, true);
        }
        if (doPreend) {
          // trigger a final modified move before ending
          interaction.move({
            event,
            preEnd: true
          });
        }
      }
      stop(arg) {
        const {
          interaction
        } = arg;
        if (!this.states || !this.states.length) {
          return;
        }
        const modifierArg = extend({
          states: this.states,
          interactable: interaction.interactable,
          element: interaction.element,
          rect: null
        }, arg);
        this.fillArg(modifierArg);
        for (const state of this.states) {
          modifierArg.state = state;
          if (state.methods.stop) {
            state.methods.stop(modifierArg);
          }
        }
        this.states = null;
        this.endResult = null;
      }
      prepareStates(modifierList) {
        this.states = [];
        for (let index = 0; index < modifierList.length; index++) {
          const {
            options,
            methods,
            name
          } = modifierList[index];
          this.states.push({
            options,
            methods,
            index,
            name
          });
        }
        return this.states;
      }
      restoreInteractionCoords(_ref36) {
        let {
          interaction: {
            coords,
            rect,
            modification
          }
        } = _ref36;
        if (!modification.result) {
          return;
        }
        const {
          startDelta
        } = modification;
        const {
          delta: curDelta,
          rectDelta
        } = modification.result;
        const coordsAndDeltas = [[coords.start, startDelta], [coords.cur, curDelta]];
        for (const [coordsSet, delta] of coordsAndDeltas) {
          coordsSet.page.x -= delta.x;
          coordsSet.page.y -= delta.y;
          coordsSet.client.x -= delta.x;
          coordsSet.client.y -= delta.y;
        }
        rect.left -= rectDelta.left;
        rect.right -= rectDelta.right;
        rect.top -= rectDelta.top;
        rect.bottom -= rectDelta.bottom;
      }
      shouldDo(options, preEnd, phase, requireEndOnly) {
        if (
        // ignore disabled modifiers
        !options || options.enabled === false ||
        // check if we require endOnly option to fire move before end
        requireEndOnly && !options.endOnly ||
        // don't apply endOnly modifiers when not ending
        options.endOnly && !preEnd ||
        // check if modifier should run be applied on start
        phase === 'start' && !options.setStart) {
          return false;
        }
        return true;
      }
      copyFrom(other) {
        this.startOffset = other.startOffset;
        this.startDelta = other.startDelta;
        this.edges = other.edges;
        this.states = other.states.map(s => clone(s));
        this.result = createResult(extend({}, other.result.coords), extend({}, other.result.rect));
      }
      destroy() {
        for (const prop in this) {
          this[prop] = null;
        }
      }
    }
    function createResult(coords, rect) {
      return {
        rect,
        coords,
        delta: {
          x: 0,
          y: 0
        },
        rectDelta: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        },
        eventProps: [],
        changed: true
      };
    }
    function getModifierList(interaction) {
      const actionOptions = interaction.interactable.options[interaction.prepared.name];
      const actionModifiers = actionOptions.modifiers;
      if (actionModifiers && actionModifiers.length) {
        return actionModifiers;
      }
      return ['snap', 'snapSize', 'snapEdges', 'restrict', 'restrictEdges', 'restrictSize'].map(type => {
        const options = actionOptions[type];
        return options && options.enabled && {
          options,
          methods: options._methods
        };
      }).filter(m => !!m);
    }
    function getRectOffset(rect, coords) {
      return rect ? {
        left: coords.x - rect.left,
        top: coords.y - rect.top,
        right: rect.right - coords.x,
        bottom: rect.bottom - coords.y
      } : {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
      };
    }

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/base.js

    function makeModifier(module, name) {
      const {
        defaults
      } = module;
      const methods = {
        start: module.start,
        set: module.set,
        beforeEnd: module.beforeEnd,
        stop: module.stop
      };
      const modifier = _options => {
        const options = _options || {};
        options.enabled = options.enabled !== false; // add missing defaults to options

        for (const prop in defaults) {
          if (!(prop in options)) {
            options[prop] = defaults[prop];
          }
        }
        const m = {
          options,
          methods,
          name,
          enable: () => {
            options.enabled = true;
            return m;
          },
          disable: () => {
            options.enabled = false;
            return m;
          }
        };
        return m;
      };
      if (name && typeof name === 'string') {
        // for backwrads compatibility
        modifier._defaults = defaults;
        modifier._methods = methods;
      }
      return modifier;
    }
    function addEventModifiers(_ref37) {
      let {
        iEvent,
        interaction: {
          modification: {
            result
          }
        }
      } = _ref37;
      if (result) {
        iEvent.modifiers = result.eventProps;
      }
    }
    const modifiersBase = {
      id: 'modifiers/base',
      before: ['actions'],
      install: scope => {
        scope.defaults.perAction.modifiers = [];
      },
      listeners: {
        'interactions:new': _ref38 => {
          let {
            interaction
          } = _ref38;
          interaction.modification = new Modification_Modification(interaction);
        },
        'interactions:before-action-start': arg => {
          const {
            modification
          } = arg.interaction;
          modification.start(arg, arg.interaction.coords.start.page);
          arg.interaction.edges = modification.edges;
          modification.applyToInteraction(arg);
        },
        'interactions:before-action-move': arg => arg.interaction.modification.setAndApply(arg),
        'interactions:before-action-end': arg => arg.interaction.modification.beforeEnd(arg),
        'interactions:action-start': addEventModifiers,
        'interactions:action-move': addEventModifiers,
        'interactions:action-end': addEventModifiers,
        'interactions:after-action-start': arg => arg.interaction.modification.restoreInteractionCoords(arg),
        'interactions:after-action-move': arg => arg.interaction.modification.restoreInteractionCoords(arg),
        'interactions:stop': arg => arg.interaction.modification.stop(arg)
      }
    };
    /* harmony default export */
    var modifiers_base = modifiersBase;

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/aspectRatio.js
    /**
     * @module modifiers/aspectRatio
     *
     * @description
     * This module forces elements to be resized with a specified dx/dy ratio.
     *
     * ```js
     * interact(target).resizable({
     *   modifiers: [
     *     interact.modifiers.snapSize({
     *       targets: [ interact.snappers.grid({ x: 20, y: 20 }) ],
     *     }),
     *     interact.aspectRatio({ ratio: 'preserve' }),
     *   ],
     * });
     * ```
     */

    const aspectRatio = {
      start(arg) {
        const {
          state,
          rect,
          edges: originalEdges,
          pageCoords: coords
        } = arg;
        let {
          ratio
        } = state.options;
        const {
          equalDelta,
          modifiers
        } = state.options;
        if (ratio === 'preserve') {
          ratio = rect.width / rect.height;
        }
        state.startCoords = extend({}, coords);
        state.startRect = extend({}, rect);
        state.ratio = ratio;
        state.equalDelta = equalDelta;
        const linkedEdges = state.linkedEdges = {
          top: originalEdges.top || originalEdges.left && !originalEdges.bottom,
          left: originalEdges.left || originalEdges.top && !originalEdges.right,
          bottom: originalEdges.bottom || originalEdges.right && !originalEdges.top,
          right: originalEdges.right || originalEdges.bottom && !originalEdges.left
        };
        state.xIsPrimaryAxis = !!(originalEdges.left || originalEdges.right);
        if (state.equalDelta) {
          state.edgeSign = (linkedEdges.left ? 1 : -1) * (linkedEdges.top ? 1 : -1);
        } else {
          const negativeSecondaryEdge = state.xIsPrimaryAxis ? linkedEdges.top : linkedEdges.left;
          state.edgeSign = negativeSecondaryEdge ? -1 : 1;
        }
        extend(arg.edges, linkedEdges);
        if (!modifiers || !modifiers.length) {
          return;
        }
        const subModification = new Modification_Modification(arg.interaction);
        subModification.copyFrom(arg.interaction.modification);
        subModification.prepareStates(modifiers);
        state.subModification = subModification;
        subModification.startAll(_objectSpread2({}, arg));
      },
      set(arg) {
        const {
          state,
          rect,
          coords
        } = arg;
        const initialCoords = extend({}, coords);
        const aspectMethod = state.equalDelta ? setEqualDelta : setRatio;
        aspectMethod(state, state.xIsPrimaryAxis, coords, rect);
        if (!state.subModification) {
          return null;
        }
        const correctedRect = extend({}, rect);
        addEdges(state.linkedEdges, correctedRect, {
          x: coords.x - initialCoords.x,
          y: coords.y - initialCoords.y
        });
        const result = state.subModification.setAll(_objectSpread2(_objectSpread2({}, arg), {}, {
          rect: correctedRect,
          edges: state.linkedEdges,
          pageCoords: coords,
          prevCoords: coords,
          prevRect: correctedRect
        }));
        const {
          delta
        } = result;
        if (result.changed) {
          const xIsCriticalAxis = Math.abs(delta.x) > Math.abs(delta.y); // do aspect modification again with critical edge axis as primary

          aspectMethod(state, xIsCriticalAxis, result.coords, result.rect);
          extend(coords, result.coords);
        }
        return result.eventProps;
      },
      defaults: {
        ratio: 'preserve',
        equalDelta: false,
        modifiers: [],
        enabled: false
      }
    };
    function setEqualDelta(_ref39, xIsPrimaryAxis, coords) {
      let {
        startCoords,
        edgeSign
      } = _ref39;
      if (xIsPrimaryAxis) {
        coords.y = startCoords.y + (coords.x - startCoords.x) * edgeSign;
      } else {
        coords.x = startCoords.x + (coords.y - startCoords.y) * edgeSign;
      }
    }
    function setRatio(_ref40, xIsPrimaryAxis, coords, rect) {
      let {
        startRect,
        startCoords,
        ratio,
        edgeSign
      } = _ref40;
      if (xIsPrimaryAxis) {
        const newHeight = rect.width / ratio;
        coords.y = startCoords.y + (newHeight - startRect.height) * edgeSign;
      } else {
        const newWidth = rect.height * ratio;
        coords.x = startCoords.x + (newWidth - startRect.width) * edgeSign;
      }
    }

    /* harmony default export */
    var modifiers_aspectRatio = makeModifier(aspectRatio, 'aspectRatio');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/noop.js
    const noop = () => {};
    noop._defaults = {};
    /* harmony default export */
    var modifiers_noop = noop;

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/restrict/pointer.js

    function pointer_start(_ref41) {
      let {
        rect,
        startOffset,
        state,
        interaction,
        pageCoords
      } = _ref41;
      const {
        options
      } = state;
      const {
        elementRect
      } = options;
      const offset = extend({
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
      }, options.offset || {});
      if (rect && elementRect) {
        const restriction = getRestrictionRect(options.restriction, interaction, pageCoords);
        if (restriction) {
          const widthDiff = restriction.right - restriction.left - rect.width;
          const heightDiff = restriction.bottom - restriction.top - rect.height;
          if (widthDiff < 0) {
            offset.left += widthDiff;
            offset.right += widthDiff;
          }
          if (heightDiff < 0) {
            offset.top += heightDiff;
            offset.bottom += heightDiff;
          }
        }
        offset.left += startOffset.left - rect.width * elementRect.left;
        offset.top += startOffset.top - rect.height * elementRect.top;
        offset.right += startOffset.right - rect.width * (1 - elementRect.right);
        offset.bottom += startOffset.bottom - rect.height * (1 - elementRect.bottom);
      }
      state.offset = offset;
    }
    function set(_ref42) {
      let {
        coords,
        interaction,
        state
      } = _ref42;
      const {
        options,
        offset
      } = state;
      const restriction = getRestrictionRect(options.restriction, interaction, coords);
      if (!restriction) {
        return;
      }
      const rect = xywhToTlbr(restriction);
      coords.x = Math.max(Math.min(rect.right - offset.right, coords.x), rect.left + offset.left);
      coords.y = Math.max(Math.min(rect.bottom - offset.bottom, coords.y), rect.top + offset.top);
    }
    function getRestrictionRect(value, interaction, coords) {
      if (is.func(value)) {
        return resolveRectLike(value, interaction.interactable, interaction.element, [coords.x, coords.y, interaction]);
      } else {
        return resolveRectLike(value, interaction.interactable, interaction.element);
      }
    }
    const pointer_defaults = {
      restriction: null,
      elementRect: null,
      offset: null,
      endOnly: false,
      enabled: false
    };
    const restrict = {
      start: pointer_start,
      set,
      defaults: pointer_defaults
    };
    /* harmony default export */
    var restrict_pointer = makeModifier(restrict, 'restrict');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/restrict/edges.js
    // This module adds the options.resize.restrictEdges setting which sets min and
    // max for the top, left, bottom and right edges of the target being resized.
    //
    // interact(target).resize({
    //   edges: { top: true, left: true },
    //   restrictEdges: {
    //     inner: { top: 200, left: 200, right: 400, bottom: 400 },
    //     outer: { top:   0, left:   0, right: 600, bottom: 600 },
    //   },
    // })

    const noInner = {
      top: +Infinity,
      left: +Infinity,
      bottom: -Infinity,
      right: -Infinity
    };
    const noOuter = {
      top: -Infinity,
      left: -Infinity,
      bottom: +Infinity,
      right: +Infinity
    };
    function edges_start(_ref43) {
      let {
        interaction,
        startOffset,
        state
      } = _ref43;
      const {
        options
      } = state;
      let offset;
      if (options) {
        const offsetRect = getRestrictionRect(options.offset, interaction, interaction.coords.start.page);
        offset = rectToXY(offsetRect);
      }
      offset = offset || {
        x: 0,
        y: 0
      };
      state.offset = {
        top: offset.y + startOffset.top,
        left: offset.x + startOffset.left,
        bottom: offset.y - startOffset.bottom,
        right: offset.x - startOffset.right
      };
    }
    function edges_set(_ref44) {
      let {
        coords,
        edges,
        interaction,
        state
      } = _ref44;
      const {
        offset,
        options
      } = state;
      if (!edges) {
        return;
      }
      const page = extend({}, coords);
      const inner = getRestrictionRect(options.inner, interaction, page) || {};
      const outer = getRestrictionRect(options.outer, interaction, page) || {};
      fixRect(inner, noInner);
      fixRect(outer, noOuter);
      if (edges.top) {
        coords.y = Math.min(Math.max(outer.top + offset.top, page.y), inner.top + offset.top);
      } else if (edges.bottom) {
        coords.y = Math.max(Math.min(outer.bottom + offset.bottom, page.y), inner.bottom + offset.bottom);
      }
      if (edges.left) {
        coords.x = Math.min(Math.max(outer.left + offset.left, page.x), inner.left + offset.left);
      } else if (edges.right) {
        coords.x = Math.max(Math.min(outer.right + offset.right, page.x), inner.right + offset.right);
      }
    }
    function fixRect(rect, defaults) {
      for (const edge of ['top', 'left', 'bottom', 'right']) {
        if (!(edge in rect)) {
          rect[edge] = defaults[edge];
        }
      }
      return rect;
    }
    const edges_defaults = {
      inner: null,
      outer: null,
      offset: null,
      endOnly: false,
      enabled: false
    };
    const restrictEdges = {
      noInner,
      noOuter,
      start: edges_start,
      set: edges_set,
      defaults: edges_defaults
    };
    /* harmony default export */
    var restrict_edges = makeModifier(restrictEdges, 'restrictEdges');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/restrict/rect.js

    const rect_defaults = extend({
      get elementRect() {
        return {
          top: 0,
          left: 0,
          bottom: 1,
          right: 1
        };
      },
      set elementRect(_) {}
    }, restrict.defaults);
    const restrictRect = {
      start: restrict.start,
      set: restrict.set,
      defaults: rect_defaults
    };
    /* harmony default export */
    var restrict_rect = makeModifier(restrictRect, 'restrictRect');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/restrict/size.js

    const noMin = {
      width: -Infinity,
      height: -Infinity
    };
    const noMax = {
      width: +Infinity,
      height: +Infinity
    };
    function size_start(arg) {
      return restrictEdges.start(arg);
    }
    function size_set(arg) {
      const {
        interaction,
        state,
        rect,
        edges
      } = arg;
      const {
        options
      } = state;
      if (!edges) {
        return;
      }
      const minSize = tlbrToXywh(getRestrictionRect(options.min, interaction, arg.coords)) || noMin;
      const maxSize = tlbrToXywh(getRestrictionRect(options.max, interaction, arg.coords)) || noMax;
      state.options = {
        endOnly: options.endOnly,
        inner: extend({}, restrictEdges.noInner),
        outer: extend({}, restrictEdges.noOuter)
      };
      if (edges.top) {
        state.options.inner.top = rect.bottom - minSize.height;
        state.options.outer.top = rect.bottom - maxSize.height;
      } else if (edges.bottom) {
        state.options.inner.bottom = rect.top + minSize.height;
        state.options.outer.bottom = rect.top + maxSize.height;
      }
      if (edges.left) {
        state.options.inner.left = rect.right - minSize.width;
        state.options.outer.left = rect.right - maxSize.width;
      } else if (edges.right) {
        state.options.inner.right = rect.left + minSize.width;
        state.options.outer.right = rect.left + maxSize.width;
      }
      restrictEdges.set(arg);
      state.options = options;
    }
    const size_defaults = {
      min: null,
      max: null,
      endOnly: false,
      enabled: false
    };
    const restrictSize = {
      start: size_start,
      set: size_set,
      defaults: size_defaults
    };
    /* harmony default export */
    var size = makeModifier(restrictSize, 'restrictSize');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/snap/pointer.js

    function snap_pointer_start(arg) {
      const {
        interaction,
        interactable,
        element,
        rect,
        state,
        startOffset
      } = arg;
      const {
        options
      } = state;
      const origin = options.offsetWithOrigin ? getOrigin(arg) : {
        x: 0,
        y: 0
      };
      let snapOffset;
      if (options.offset === 'startCoords') {
        snapOffset = {
          x: interaction.coords.start.page.x,
          y: interaction.coords.start.page.y
        };
      } else {
        const offsetRect = resolveRectLike(options.offset, interactable, element, [interaction]);
        snapOffset = rectToXY(offsetRect) || {
          x: 0,
          y: 0
        };
        snapOffset.x += origin.x;
        snapOffset.y += origin.y;
      }
      const {
        relativePoints
      } = options;
      state.offsets = rect && relativePoints && relativePoints.length ? relativePoints.map((relativePoint, index) => ({
        index,
        relativePoint,
        x: startOffset.left - rect.width * relativePoint.x + snapOffset.x,
        y: startOffset.top - rect.height * relativePoint.y + snapOffset.y
      })) : [extend({
        index: 0,
        relativePoint: null
      }, snapOffset)];
    }
    function pointer_set(arg) {
      const {
        interaction,
        coords,
        state
      } = arg;
      const {
        options,
        offsets
      } = state;
      const origin = getOriginXY(interaction.interactable, interaction.element, interaction.prepared.name);
      const page = extend({}, coords);
      const targets = [];
      if (!options.offsetWithOrigin) {
        page.x -= origin.x;
        page.y -= origin.y;
      }
      for (const offset of offsets) {
        const relativeX = page.x - offset.x;
        const relativeY = page.y - offset.y;
        for (let index = 0, len = options.targets.length; index < len; index++) {
          const snapTarget = options.targets[index];
          let target;
          if (is.func(snapTarget)) {
            target = snapTarget(relativeX, relativeY, interaction._proxy, offset, index);
          } else {
            target = snapTarget;
          }
          if (!target) {
            continue;
          }
          targets.push({
            x: (is.number(target.x) ? target.x : relativeX) + offset.x,
            y: (is.number(target.y) ? target.y : relativeY) + offset.y,
            range: is.number(target.range) ? target.range : options.range,
            source: snapTarget,
            index,
            offset
          });
        }
      }
      const closest = {
        target: null,
        inRange: false,
        distance: 0,
        range: 0,
        delta: {
          x: 0,
          y: 0
        }
      };
      for (const target of targets) {
        const range = target.range;
        const dx = target.x - page.x;
        const dy = target.y - page.y;
        const distance = hypot(dx, dy);
        let inRange = distance <= range; // Infinite targets count as being out of range
        // compared to non infinite ones that are in range

        if (range === Infinity && closest.inRange && closest.range !== Infinity) {
          inRange = false;
        }
        if (!closest.target || (inRange // is the closest target in range?
        ? closest.inRange && range !== Infinity // the pointer is relatively deeper in this target
        ? distance / range < closest.distance / closest.range // this target has Infinite range and the closest doesn't
        : range === Infinity && closest.range !== Infinity ||
        // OR this target is closer that the previous closest
        distance < closest.distance :
        // The other is not in range and the pointer is closer to this target
        !closest.inRange && distance < closest.distance)) {
          closest.target = target;
          closest.distance = distance;
          closest.range = range;
          closest.inRange = inRange;
          closest.delta.x = dx;
          closest.delta.y = dy;
        }
      }
      if (closest.inRange) {
        coords.x = closest.target.x;
        coords.y = closest.target.y;
      }
      state.closest = closest;
      return closest;
    }
    function getOrigin(arg) {
      const {
        element
      } = arg.interaction;
      const optionsOrigin = rectToXY(resolveRectLike(arg.state.options.origin, null, null, [element]));
      const origin = optionsOrigin || getOriginXY(arg.interactable, element, arg.interaction.prepared.name);
      return origin;
    }
    const snap_pointer_defaults = {
      range: Infinity,
      targets: null,
      offset: null,
      offsetWithOrigin: true,
      origin: null,
      relativePoints: null,
      endOnly: false,
      enabled: false
    };
    const snap = {
      start: snap_pointer_start,
      set: pointer_set,
      defaults: snap_pointer_defaults
    };
    /* harmony default export */
    var snap_pointer = makeModifier(snap, 'snap');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/snap/size.js
    // This module allows snapping of the size of targets during resize
    // interactions.

    function snap_size_start(arg) {
      const {
        state,
        edges
      } = arg;
      const {
        options
      } = state;
      if (!edges) {
        return null;
      }
      arg.state = {
        options: {
          targets: null,
          relativePoints: [{
            x: edges.left ? 0 : 1,
            y: edges.top ? 0 : 1
          }],
          offset: options.offset || 'self',
          origin: {
            x: 0,
            y: 0
          },
          range: options.range
        }
      };
      state.targetFields = state.targetFields || [['width', 'height'], ['x', 'y']];
      snap.start(arg);
      state.offsets = arg.state.offsets;
      arg.state = state;
    }
    function snap_size_set(arg) {
      const {
        interaction,
        state,
        coords
      } = arg;
      const {
        options,
        offsets
      } = state;
      const relative = {
        x: coords.x - offsets[0].x,
        y: coords.y - offsets[0].y
      };
      state.options = extend({}, options);
      state.options.targets = [];
      for (const snapTarget of options.targets || []) {
        let target;
        if (is.func(snapTarget)) {
          target = snapTarget(relative.x, relative.y, interaction);
        } else {
          target = snapTarget;
        }
        if (!target) {
          continue;
        }
        for (const [xField, yField] of state.targetFields) {
          if (xField in target || yField in target) {
            target.x = target[xField];
            target.y = target[yField];
            break;
          }
        }
        state.options.targets.push(target);
      }
      const returnValue = snap.set(arg);
      state.options = options;
      return returnValue;
    }
    const snap_size_defaults = {
      range: Infinity,
      targets: null,
      offset: null,
      endOnly: false,
      enabled: false
    };
    const snapSize = {
      start: snap_size_start,
      set: snap_size_set,
      defaults: snap_size_defaults
    };
    /* harmony default export */
    var snap_size = makeModifier(snapSize, 'snapSize');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/snap/edges.js
    /**
     * @module modifiers/snapEdges
     *
     * @description
     * WOW> This module allows snapping of the edges of targets during resize
     * interactions.
     *
     * ```js
     * interact(target).resizable({
     *   snapEdges: {
     *     targets: [interact.snappers.grid({ x: 100, y: 50 })],
     *   },
     * })
     *
     * interact(target).resizable({
     *   snapEdges: {
     *     targets: [
     *       interact.snappers.grid({
     *        top: 50,
     *        left: 50,
     *        bottom: 100,
     *        right: 100,
     *       }),
     *     ],
     *   },
     * })
     * ```
     */

    function snap_edges_start(arg) {
      const {
        edges
      } = arg;
      if (!edges) {
        return null;
      }
      arg.state.targetFields = arg.state.targetFields || [[edges.left ? 'left' : 'right', edges.top ? 'top' : 'bottom']];
      return snapSize.start(arg);
    }
    const snapEdges = {
      start: snap_edges_start,
      set: snapSize.set,
      defaults: extend(clone(snapSize.defaults), {
        targets: null,
        range: null,
        offset: {
          x: 0,
          y: 0
        }
      })
    };
    /* harmony default export */
    var snap_edges = makeModifier(snapEdges, 'snapEdges');

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/all.js
    /* eslint-disable node/no-extraneous-import */

    /* harmony default export */
    var modifiers_all = {
      aspectRatio: modifiers_aspectRatio,
      restrictEdges: restrict_edges,
      restrict: restrict_pointer,
      restrictRect: restrict_rect,
      restrictSize: size,
      snapEdges: snap_edges,
      snap: snap_pointer,
      snapSize: snap_size,
      spring: modifiers_noop,
      avoid: modifiers_noop,
      transform: modifiers_noop,
      rubberband: modifiers_noop
    };

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/plugin.js

    const plugin_modifiers = {
      id: 'modifiers',
      install(scope) {
        const {
          interactStatic: interact
        } = scope;
        scope.usePlugin(modifiers_base);
        scope.usePlugin(snappers_plugin);
        interact.modifiers = modifiers_all; // for backwrads compatibility

        for (const type in modifiers_all) {
          const {
            _defaults,
            _methods
          } = modifiers_all[type];
          _defaults._methods = _methods;
          scope.defaults.perAction[type] = _defaults;
        }
      }
    };
    /* harmony default export */
    var modifiers_plugin = plugin_modifiers;

    // CONCATENATED MODULE: ./node_modules/@interactjs/modifiers/index.js
    /* eslint-disable import/order, no-console, eol-last */

    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }
    _interactjs_interact.use(modifiers_plugin);

    // CONCATENATED MODULE: ./node_modules/@interactjs/dev-tools/plugin.js
    /* eslint-disable no-console */

    var CheckName;
    (function (CheckName) {
      CheckName["touchAction"] = "touchAction";
      CheckName["boxSizing"] = "boxSizing";
      CheckName["noListeners"] = "noListeners";
    })(CheckName || (CheckName = {}));
    const prefix = '[interact.js] ';
    const links = {
      touchAction: 'https://developer.mozilla.org/en-US/docs/Web/CSS/touch-action',
      boxSizing: 'https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing'
    }; // eslint-disable-next-line no-undef

    const isProduction = "development" === 'production'; // eslint-disable-next-line no-restricted-syntax

    function dev_tools_plugin_install(scope) {
      let {
        logger
      } = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      const {
        Interactable,
        defaults
      } = scope;
      scope.logger = logger || console;
      defaults.base.devTools = {
        ignore: {}
      };
      Interactable.prototype.devTools = function (options) {
        if (options) {
          extend(this.options.devTools, options);
          return this;
        }
        return this.options.devTools;
      };
    }
    const checks = [{
      name: CheckName.touchAction,
      perform(_ref45) {
        let {
          element
        } = _ref45;
        return !parentHasStyle(element, 'touchAction', /pan-|pinch|none/);
      },
      getInfo(_ref46) {
        let {
          element
        } = _ref46;
        return [element, links.touchAction];
      },
      text: 'Consider adding CSS "touch-action: none" to this element\n'
    }, {
      name: CheckName.boxSizing,
      perform(interaction) {
        const {
          element
        } = interaction;
        return interaction.prepared.name === 'resize' && element instanceof utils_domObjects.HTMLElement && !hasStyle(element, 'boxSizing', /border-box/);
      },
      text: 'Consider adding CSS "box-sizing: border-box" to this resizable element',
      getInfo(_ref47) {
        let {
          element
        } = _ref47;
        return [element, links.boxSizing];
      }
    }, {
      name: CheckName.noListeners,
      perform(interaction) {
        const actionName = interaction.prepared.name;
        const moveListeners = interaction.interactable.events.types["".concat(actionName, "move")] || [];
        return !moveListeners.length;
      },
      getInfo(interaction) {
        return [interaction.prepared.name, interaction.interactable];
      },
      text: 'There are no listeners set for this action'
    }];
    function hasStyle(element, prop, styleRe) {
      const value = element.style[prop] || win.getComputedStyle(element)[prop];
      return styleRe.test((value || '').toString());
    }
    function parentHasStyle(element, prop, styleRe) {
      let parent = element;
      while (is.element(parent)) {
        if (hasStyle(parent, prop, styleRe)) {
          return true;
        }
        parent = parentNode(parent);
      }
      return false;
    }
    const plugin_id = 'dev-tools';
    const defaultExport = isProduction ? {
      id: plugin_id,
      install: () => {}
    } : {
      id: plugin_id,
      install: dev_tools_plugin_install,
      listeners: {
        'interactions:action-start': (_ref48, scope) => {
          let {
            interaction
          } = _ref48;
          for (const check of checks) {
            const options = interaction.interactable && interaction.interactable.options;
            if (!(options && options.devTools && options.devTools.ignore[check.name]) && check.perform(interaction)) {
              scope.logger.warn(prefix + check.text, ...check.getInfo(interaction));
            }
          }
        }
      },
      checks,
      CheckName,
      links,
      prefix
    };
    /* harmony default export */
    var dev_tools_plugin = defaultExport;

    // CONCATENATED MODULE: ./node_modules/@interactjs/dev-tools/index.js
    /* eslint-disable import/order, no-console, eol-last */

    if (typeof window === 'object' && !!window) {
      interact_init(window);
    }
    _interactjs_interact.use(dev_tools_plugin);

    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/GridItem.vue?vue&type=script&lang=js&

    function ownKeys(object, enumerableOnly) {
      var keys = Object.keys(object);
      if (Object.getOwnPropertySymbols) {
        var symbols = Object.getOwnPropertySymbols(object);
        if (enumerableOnly) symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
        keys.push.apply(keys, symbols);
      }
      return keys;
    }
    function _objectSpread(target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i] != null ? arguments[i] : {};
        if (i % 2) {
          ownKeys(Object(source), true).forEach(function (key) {
            Object(defineProperty["a" /* default */])(target, key, source[key]);
          });
        } else if (Object.getOwnPropertyDescriptors) {
          Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
        } else {
          ownKeys(Object(source)).forEach(function (key) {
            Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
          });
        }
      }
      return target;
    }

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    //    var eventBus = require('./eventBus');

    /* harmony default export */
    var GridItemvue_type_script_lang_js_ = {
      name: "GridItem",
      props: {
        /*cols: {
         type: Number,
         required: true
         },*/

        /*containerWidth: {
         type: Number,
         required: true
           },
         rowHeight: {
         type: Number,
         required: true
         },
         margin: {
         type: Array,
         required: true
         },
         maxRows: {
         type: Number,
         required: true
         },*/
        isDraggable: {
          type: Boolean,
          required: false,
          default: null
        },
        isResizable: {
          type: Boolean,
          required: false,
          default: null
        },
        isBounded: {
          type: Boolean,
          required: false,
          default: null
        },
        /*useCssTransforms: {
         type: Boolean,
         required: true
         },
         */
        static: {
          type: Boolean,
          required: false,
          default: false
        },
        minH: {
          type: Number,
          required: false,
          default: 1
        },
        minW: {
          type: Number,
          required: false,
          default: 1
        },
        maxH: {
          type: Number,
          required: false,
          default: Infinity
        },
        maxW: {
          type: Number,
          required: false,
          default: Infinity
        },
        x: {
          type: Number,
          required: true
        },
        y: {
          type: Number,
          required: true
        },
        w: {
          type: Number,
          required: true
        },
        h: {
          type: Number,
          required: true
        },
        i: {
          required: true
        },
        dragIgnoreFrom: {
          type: String,
          required: false,
          default: 'a, button'
        },
        dragAllowFrom: {
          type: String,
          required: false,
          default: null
        },
        resizeIgnoreFrom: {
          type: String,
          required: false,
          default: 'a, button'
        },
        preserveAspectRatio: {
          type: Boolean,
          required: false,
          default: false
        },
        dragOption: {
          type: Object,
          required: false,
          default: function _default() {
            return {};
          }
        },
        resizeOption: {
          type: Object,
          required: false,
          default: function _default() {
            return {};
          }
        }
      },
      inject: ["eventBus", "layout"],
      data: function data() {
        return {
          cols: 1,
          containerWidth: 100,
          rowHeight: 30,
          margin: [10, 10],
          maxRows: Infinity,
          draggable: null,
          resizable: null,
          transformScale: 1,
          useCssTransforms: true,
          useStyleCursor: true,
          isDragging: false,
          dragging: null,
          isResizing: false,
          resizing: null,
          lastX: NaN,
          lastY: NaN,
          lastW: NaN,
          lastH: NaN,
          style: {},
          rtl: false,
          dragEventSet: false,
          resizeEventSet: false,
          previousW: null,
          previousH: null,
          previousX: null,
          previousY: null,
          innerX: this.x,
          innerY: this.y,
          innerW: this.w,
          innerH: this.h
        };
      },
      created: function created() {
        var _this = this;
        var self = this; // Accessible refernces of functions for removing in beforeDestroy

        self.updateWidthHandler = function (width) {
          self.updateWidth(width);
        };
        self.compactHandler = function (layout) {
          self.compact(layout);
        };
        self.setDraggableHandler = function (isDraggable) {
          if (self.isDraggable === null) {
            self.draggable = isDraggable;
          }
        };
        self.setResizableHandler = function (isResizable) {
          if (self.isResizable === null) {
            self.resizable = isResizable;
          }
        };
        self.setBoundedHandler = function (isBounded) {
          if (self.isBounded === null) {
            self.bounded = isBounded;
          }
        };
        self.setTransformScaleHandler = function (transformScale) {
          self.transformScale = transformScale;
        };
        self.setRowHeightHandler = function (rowHeight) {
          self.rowHeight = rowHeight;
        };
        self.setMaxRowsHandler = function (maxRows) {
          self.maxRows = maxRows;
        };
        self.directionchangeHandler = function () {
          _this.rtl = Object(DOM["b" /* getDocumentDir */])() === 'rtl';
          _this.compact();
        };
        self.setColNum = function (colNum) {
          self.cols = parseInt(colNum);
        };
        this.eventBus.$on('updateWidth', self.updateWidthHandler);
        this.eventBus.$on('compact', self.compactHandler);
        this.eventBus.$on('setDraggable', self.setDraggableHandler);
        this.eventBus.$on('setResizable', self.setResizableHandler);
        this.eventBus.$on('setBounded', self.setBoundedHandler);
        this.eventBus.$on('setTransformScale', self.setTransformScaleHandler);
        this.eventBus.$on('setRowHeight', self.setRowHeightHandler);
        this.eventBus.$on('setMaxRows', self.setMaxRowsHandler);
        this.eventBus.$on('directionchange', self.directionchangeHandler);
        this.eventBus.$on('setColNum', self.setColNum);
        this.rtl = Object(DOM["b" /* getDocumentDir */])() === 'rtl';
      },
      beforeDestroy: function beforeDestroy() {
        var self = this; //Remove listeners

        this.eventBus.$off('updateWidth', self.updateWidthHandler);
        this.eventBus.$off('compact', self.compactHandler);
        this.eventBus.$off('setDraggable', self.setDraggableHandler);
        this.eventBus.$off('setResizable', self.setResizableHandler);
        this.eventBus.$off('setBounded', self.setBoundedHandler);
        this.eventBus.$off('setTransformScale', self.setTransformScaleHandler);
        this.eventBus.$off('setRowHeight', self.setRowHeightHandler);
        this.eventBus.$off('setMaxRows', self.setMaxRowsHandler);
        this.eventBus.$off('directionchange', self.directionchangeHandler);
        this.eventBus.$off('setColNum', self.setColNum);
        if (this.interactObj) {
          this.interactObj.unset(); // destroy interact intance
        }
      },

      mounted: function mounted() {
        if (this.layout.responsive && this.layout.lastBreakpoint) {
          this.cols = Object(responsiveUtils["c" /* getColsFromBreakpoint */])(this.layout.lastBreakpoint, this.layout.cols);
        } else {
          this.cols = this.layout.colNum;
        }
        this.rowHeight = this.layout.rowHeight;
        this.containerWidth = this.layout.width !== null ? this.layout.width : 100;
        this.margin = this.layout.margin !== undefined ? this.layout.margin : [10, 10];
        this.maxRows = this.layout.maxRows;
        if (this.isDraggable === null) {
          this.draggable = this.layout.isDraggable;
        } else {
          this.draggable = this.isDraggable;
        }
        if (this.isResizable === null) {
          this.resizable = this.layout.isResizable;
        } else {
          this.resizable = this.isResizable;
        }
        if (this.isBounded === null) {
          this.bounded = this.layout.isBounded;
        } else {
          this.bounded = this.isBounded;
        }
        this.transformScale = this.layout.transformScale;
        this.useCssTransforms = this.layout.useCssTransforms;
        this.useStyleCursor = this.layout.useStyleCursor;
        this.createStyle();
      },
      watch: {
        isDraggable: function isDraggable() {
          this.draggable = this.isDraggable;
        },
        static: function _static() {
          this.tryMakeDraggable();
          this.tryMakeResizable();
        },
        draggable: function draggable() {
          this.tryMakeDraggable();
        },
        isResizable: function isResizable() {
          this.resizable = this.isResizable;
        },
        isBounded: function isBounded() {
          this.bounded = this.isBounded;
        },
        resizable: function resizable() {
          this.tryMakeResizable();
        },
        rowHeight: function rowHeight() {
          this.createStyle();
          this.emitContainerResized();
        },
        cols: function cols() {
          this.tryMakeResizable();
          this.createStyle();
          this.emitContainerResized();
        },
        containerWidth: function containerWidth() {
          this.tryMakeResizable();
          this.createStyle();
          this.emitContainerResized();
        },
        x: function x(newVal) {
          this.innerX = newVal;
          this.createStyle();
        },
        y: function y(newVal) {
          this.innerY = newVal;
          this.createStyle();
        },
        h: function h(newVal) {
          this.innerH = newVal;
          this.createStyle(); // this.emitContainerResized();
        },

        w: function w(newVal) {
          this.innerW = newVal;
          this.createStyle(); // this.emitContainerResized();
        },

        renderRtl: function renderRtl() {
          // console.log("### renderRtl");
          this.tryMakeResizable();
          this.createStyle();
        },
        minH: function minH() {
          this.tryMakeResizable();
        },
        maxH: function maxH() {
          this.tryMakeResizable();
        },
        minW: function minW() {
          this.tryMakeResizable();
        },
        maxW: function maxW() {
          this.tryMakeResizable();
        },
        "$parent.margin": function $parentMargin(margin) {
          if (!margin || margin[0] == this.margin[0] && margin[1] == this.margin[1]) {
            return;
          }
          this.margin = margin.map(function (m) {
            return Number(m);
          });
          this.createStyle();
          this.emitContainerResized();
        }
      },
      computed: {
        classObj: function classObj() {
          return {
            'vue-resizable': this.resizableAndNotStatic,
            'static': this.static,
            'resizing': this.isResizing,
            'vue-draggable-dragging': this.isDragging,
            'cssTransforms': this.useCssTransforms,
            'render-rtl': this.renderRtl,
            'disable-userselect': this.isDragging,
            'no-touch': this.isAndroid && this.draggableOrResizableAndNotStatic
          };
        },
        resizableAndNotStatic: function resizableAndNotStatic() {
          return this.resizable && !this.static;
        },
        draggableOrResizableAndNotStatic: function draggableOrResizableAndNotStatic() {
          return (this.draggable || this.resizable) && !this.static;
        },
        isAndroid: function isAndroid() {
          return navigator.userAgent.toLowerCase().indexOf("android") !== -1;
        },
        renderRtl: function renderRtl() {
          return this.layout.isMirrored ? !this.rtl : this.rtl;
        },
        resizableHandleClass: function resizableHandleClass() {
          if (this.renderRtl) {
            return 'vue-resizable-handle vue-rtl-resizable-handle';
          } else {
            return 'vue-resizable-handle';
          }
        }
      },
      methods: {
        createStyle: function createStyle() {
          if (this.x + this.w > this.cols) {
            this.innerX = 0;
            this.innerW = this.w > this.cols ? this.cols : this.w;
          } else {
            this.innerX = this.x;
            this.innerW = this.w;
          }
          var pos = this.calcPosition(this.innerX, this.innerY, this.innerW, this.innerH);
          if (this.isDragging) {
            pos.top = this.dragging.top; //                    Add rtl support

            if (this.renderRtl) {
              pos.right = this.dragging.left;
            } else {
              pos.left = this.dragging.left;
            }
          }
          if (this.isResizing) {
            pos.width = this.resizing.width;
            pos.height = this.resizing.height;
          }
          var style; // CSS Transforms support (default)

          if (this.useCssTransforms) {
            //                    Add rtl support
            if (this.renderRtl) {
              style = Object(utils["k" /* setTransformRtl */])(pos.top, pos.right, pos.width, pos.height);
            } else {
              style = Object(utils["j" /* setTransform */])(pos.top, pos.left, pos.width, pos.height);
            }
          } else {
            // top,left (slow)
            //                    Add rtl support
            if (this.renderRtl) {
              style = Object(utils["i" /* setTopRight */])(pos.top, pos.right, pos.width, pos.height);
            } else {
              style = Object(utils["h" /* setTopLeft */])(pos.top, pos.left, pos.width, pos.height);
            }
          }
          this.style = style;
        },
        emitContainerResized: function emitContainerResized() {
          // this.style has width and height with trailing 'px'. The
          // resized event is without them
          var styleProps = {};
          for (var _i = 0, _arr = ['width', 'height']; _i < _arr.length; _i++) {
            var prop = _arr[_i];
            var val = this.style[prop];
            var matches = val.match(/^(\d+)px$/);
            if (!matches) return;
            styleProps[prop] = matches[1];
          }
          this.$emit("container-resized", this.i, this.h, this.w, styleProps.height, styleProps.width);
        },
        handleResize: function handleResize(event) {
          if (this.static) return;
          var position = getControlPosition(event); // Get the current drag point from the event. This is used as the offset.

          if (position == null) return; // not possible but satisfies flow

          var x = position.x,
            y = position.y;
          var newSize = {
            width: 0,
            height: 0
          };
          var pos;
          switch (event.type) {
            case "resizestart":
              {
                this.tryMakeResizable();
                this.previousW = this.innerW;
                this.previousH = this.innerH;
                pos = this.calcPosition(this.innerX, this.innerY, this.innerW, this.innerH);
                newSize.width = pos.width;
                newSize.height = pos.height;
                this.resizing = newSize;
                this.isResizing = true;
                break;
              }
            case "resizemove":
              {
                //                        console.log("### resize => " + event.type + ", lastW=" + this.lastW + ", lastH=" + this.lastH);
                var coreEvent = createCoreData(this.lastW, this.lastH, x, y);
                if (this.renderRtl) {
                  newSize.width = this.resizing.width - coreEvent.deltaX / this.transformScale;
                } else {
                  newSize.width = this.resizing.width + coreEvent.deltaX / this.transformScale;
                }
                newSize.height = this.resizing.height + coreEvent.deltaY / this.transformScale; ///console.log("### resize => " + event.type + ", deltaX=" + coreEvent.deltaX + ", deltaY=" + coreEvent.deltaY);

                this.resizing = newSize;
                break;
              }
            case "resizeend":
              {
                //console.log("### resize end => x=" +this.innerX + " y=" + this.innerY + " w=" + this.innerW + " h=" + this.innerH);
                pos = this.calcPosition(this.innerX, this.innerY, this.innerW, this.innerH);
                newSize.width = pos.width;
                newSize.height = pos.height; //                        console.log("### resize end => " + JSON.stringify(newSize));

                this.resizing = null;
                this.isResizing = false;
                break;
              }
          } // Get new WH

          pos = this.calcWH(newSize.height, newSize.width);
          if (pos.w < this.minW) {
            pos.w = this.minW;
          }
          if (pos.w > this.maxW) {
            pos.w = this.maxW;
          }
          if (pos.h < this.minH) {
            pos.h = this.minH;
          }
          if (pos.h > this.maxH) {
            pos.h = this.maxH;
          }
          if (pos.h < 1) {
            pos.h = 1;
          }
          if (pos.w < 1) {
            pos.w = 1;
          }
          this.lastW = x;
          this.lastH = y;
          if (this.innerW !== pos.w || this.innerH !== pos.h) {
            this.$emit("resize", this.i, pos.h, pos.w, newSize.height, newSize.width);
          }
          if (event.type === "resizeend" && (this.previousW !== this.innerW || this.previousH !== this.innerH)) {
            this.$emit("resized", this.i, pos.h, pos.w, newSize.height, newSize.width);
          }
          this.eventBus.$emit("resizeEvent", event.type, this.i, this.innerX, this.innerY, pos.h, pos.w);
        },
        handleDrag: function handleDrag(event) {
          if (this.static) return;
          if (this.isResizing) return;
          var position = getControlPosition(event); // Get the current drag point from the event. This is used as the offset.

          if (position === null) return; // not possible but satisfies flow

          var x = position.x,
            y = position.y; // let shouldUpdate = false;

          var newPosition = {
            top: 0,
            left: 0
          };
          switch (event.type) {
            case "dragstart":
              {
                this.previousX = this.innerX;
                this.previousY = this.innerY;
                var parentRect = event.target.offsetParent.getBoundingClientRect();
                var clientRect = event.target.getBoundingClientRect();
                var cLeft = clientRect.left / this.transformScale;
                var pLeft = parentRect.left / this.transformScale;
                var cRight = clientRect.right / this.transformScale;
                var pRight = parentRect.right / this.transformScale;
                var cTop = clientRect.top / this.transformScale;
                var pTop = parentRect.top / this.transformScale;
                if (this.renderRtl) {
                  newPosition.left = (cRight - pRight) * -1;
                } else {
                  newPosition.left = cLeft - pLeft;
                }
                newPosition.top = cTop - pTop;
                this.dragging = newPosition;
                this.isDragging = true;
                break;
              }
            case "dragend":
              {
                if (!this.isDragging) return;
                var _parentRect = event.target.offsetParent.getBoundingClientRect();
                var _clientRect = event.target.getBoundingClientRect();
                var _cLeft = _clientRect.left / this.transformScale;
                var _pLeft = _parentRect.left / this.transformScale;
                var _cRight = _clientRect.right / this.transformScale;
                var _pRight = _parentRect.right / this.transformScale;
                var _cTop = _clientRect.top / this.transformScale;
                var _pTop = _parentRect.top / this.transformScale; //                        Add rtl support

                if (this.renderRtl) {
                  newPosition.left = (_cRight - _pRight) * -1;
                } else {
                  newPosition.left = _cLeft - _pLeft;
                }
                newPosition.top = _cTop - _pTop; //                        console.log("### drag end => " + JSON.stringify(newPosition));
                //                        console.log("### DROP: " + JSON.stringify(newPosition));

                this.dragging = null;
                this.isDragging = false; // shouldUpdate = true;

                break;
              }
            case "dragmove":
              {
                var coreEvent = createCoreData(this.lastX, this.lastY, x, y); //                        Add rtl support

                if (this.renderRtl) {
                  newPosition.left = this.dragging.left - coreEvent.deltaX / this.transformScale;
                } else {
                  newPosition.left = this.dragging.left + coreEvent.deltaX / this.transformScale;
                }
                newPosition.top = this.dragging.top + coreEvent.deltaY / this.transformScale;
                if (this.bounded) {
                  var bottomBoundary = event.target.offsetParent.clientHeight - this.calcGridItemWHPx(this.h, this.rowHeight, this.margin[1]);
                  newPosition.top = this.clamp(newPosition.top, 0, bottomBoundary);
                  var colWidth = this.calcColWidth();
                  var rightBoundary = this.containerWidth - this.calcGridItemWHPx(this.w, colWidth, this.margin[0]);
                  newPosition.left = this.clamp(newPosition.left, 0, rightBoundary);
                } //                        console.log("### drag => " + event.type + ", x=" + x + ", y=" + y);
                //                        console.log("### drag => " + event.type + ", deltaX=" + coreEvent.deltaX + ", deltaY=" + coreEvent.deltaY);
                //                        console.log("### drag end => " + JSON.stringify(newPosition));

                this.dragging = newPosition;
                break;
              }
          } // Get new XY

          var pos;
          if (this.renderRtl) {
            pos = this.calcXY(newPosition.top, newPosition.left);
          } else {
            pos = this.calcXY(newPosition.top, newPosition.left);
          }
          this.lastX = x;
          this.lastY = y;
          if (this.innerX !== pos.x || this.innerY !== pos.y) {
            this.$emit("move", this.i, pos.x, pos.y);
          }
          if (event.type === "dragend" && (this.previousX !== this.innerX || this.previousY !== this.innerY)) {
            this.$emit("moved", this.i, pos.x, pos.y);
          }
          this.eventBus.$emit("dragEvent", event.type, this.i, pos.x, pos.y, this.innerH, this.innerW);
        },
        calcPosition: function calcPosition(x, y, w, h) {
          var colWidth = this.calcColWidth(); // add rtl support

          var out;
          if (this.renderRtl) {
            out = {
              right: Math.round(colWidth * x + (x + 1) * this.margin[0]),
              top: Math.round(this.rowHeight * y + (y + 1) * this.margin[1]),
              // 0 * Infinity === NaN, which causes problems with resize constriants;
              // Fix this if it occurs.
              // Note we do it here rather than later because Math.round(Infinity) causes deopt
              width: w === Infinity ? w : Math.round(colWidth * w + Math.max(0, w - 1) * this.margin[0]),
              height: h === Infinity ? h : Math.round(this.rowHeight * h + Math.max(0, h - 1) * this.margin[1])
            };
          } else {
            out = {
              left: Math.round(colWidth * x + (x + 1) * this.margin[0]),
              top: Math.round(this.rowHeight * y + (y + 1) * this.margin[1]),
              // 0 * Infinity === NaN, which causes problems with resize constriants;
              // Fix this if it occurs.
              // Note we do it here rather than later because Math.round(Infinity) causes deopt
              width: w === Infinity ? w : Math.round(colWidth * w + Math.max(0, w - 1) * this.margin[0]),
              height: h === Infinity ? h : Math.round(this.rowHeight * h + Math.max(0, h - 1) * this.margin[1])
            };
          }
          return out;
        },
        /**
         * Translate x and y coordinates from pixels to grid units.
         * @param  {Number} top  Top position (relative to parent) in pixels.
         * @param  {Number} left Left position (relative to parent) in pixels.
         * @return {Object} x and y in grid units.
         */
        // TODO check if this function needs change in order to support rtl.
        calcXY: function calcXY(top, left) {
          var colWidth = this.calcColWidth(); // left = colWidth * x + margin * (x + 1)
          // l = cx + m(x+1)
          // l = cx + mx + m
          // l - m = cx + mx
          // l - m = x(c + m)
          // (l - m) / (c + m) = x
          // x = (left - margin) / (coldWidth + margin)

          var x = Math.round((left - this.margin[0]) / (colWidth + this.margin[0]));
          var y = Math.round((top - this.margin[1]) / (this.rowHeight + this.margin[1])); // Capping

          x = Math.max(Math.min(x, this.cols - this.innerW), 0);
          y = Math.max(Math.min(y, this.maxRows - this.innerH), 0);
          return {
            x: x,
            y: y
          };
        },
        // Helper for generating column width
        calcColWidth: function calcColWidth() {
          var colWidth = (this.containerWidth - this.margin[0] * (this.cols + 1)) / this.cols; // console.log("### COLS=" + this.cols + " COL WIDTH=" + colWidth + " MARGIN " + this.margin[0]);

          return colWidth;
        },
        // This can either be called:
        // calcGridItemWHPx(w, colWidth, margin[0])
        // or
        // calcGridItemWHPx(h, rowHeight, margin[1])
        calcGridItemWHPx: function calcGridItemWHPx(gridUnits, colOrRowSize, marginPx) {
          // 0 * Infinity === NaN, which causes problems with resize contraints
          if (!Number.isFinite(gridUnits)) return gridUnits;
          return Math.round(colOrRowSize * gridUnits + Math.max(0, gridUnits - 1) * marginPx);
        },
        // Similar to _.clamp
        clamp: function clamp(num, lowerBound, upperBound) {
          return Math.max(Math.min(num, upperBound), lowerBound);
        },
        /**
         * Given a height and width in pixel values, calculate grid units.
         * @param  {Number} height Height in pixels.
         * @param  {Number} width  Width in pixels.
         * @param  {Boolean} autoSizeFlag  function autoSize identifier.
         * @return {Object} w, h as grid units.
         */
        calcWH: function calcWH(height, width) {
          var autoSizeFlag = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
          var colWidth = this.calcColWidth(); // width = colWidth * w - (margin * (w - 1))
          // ...
          // w = (width + margin) / (colWidth + margin)

          var w = Math.round((width + this.margin[0]) / (colWidth + this.margin[0]));
          var h = 0;
          if (!autoSizeFlag) {
            h = Math.round((height + this.margin[1]) / (this.rowHeight + this.margin[1]));
          } else {
            h = Math.ceil((height + this.margin[1]) / (this.rowHeight + this.margin[1]));
          } // Capping

          w = Math.max(Math.min(w, this.cols - this.innerX), 0);
          h = Math.max(Math.min(h, this.maxRows - this.innerY), 0);
          return {
            w: w,
            h: h
          };
        },
        updateWidth: function updateWidth(width, colNum) {
          this.containerWidth = width;
          if (colNum !== undefined && colNum !== null) {
            this.cols = colNum;
          }
        },
        compact: function compact() {
          this.createStyle();
        },
        tryMakeDraggable: function tryMakeDraggable() {
          var self = this;
          if (this.interactObj === null || this.interactObj === undefined) {
            this.interactObj = _interactjs_interact(this.$refs.item);
            if (!this.useStyleCursor) {
              this.interactObj.styleCursor(false);
            }
          }
          if (this.draggable && !this.static) {
            var opts = _objectSpread({
              ignoreFrom: this.dragIgnoreFrom,
              allowFrom: this.dragAllowFrom
            }, this.dragOption);
            this.interactObj.draggable(opts);
            /*this.interactObj.draggable({allowFrom: '.vue-draggable-handle'});*/

            if (!this.dragEventSet) {
              this.dragEventSet = true;
              this.interactObj.on('dragstart dragmove dragend', function (event) {
                self.handleDrag(event);
              });
            }
          } else {
            this.interactObj.draggable({
              enabled: false
            });
          }
        },
        tryMakeResizable: function tryMakeResizable() {
          var self = this;
          if (this.interactObj === null || this.interactObj === undefined) {
            this.interactObj = _interactjs_interact(this.$refs.item);
            if (!this.useStyleCursor) {
              this.interactObj.styleCursor(false);
            }
          }
          if (this.resizable && !this.static) {
            var maximum = this.calcPosition(0, 0, this.maxW, this.maxH);
            var minimum = this.calcPosition(0, 0, this.minW, this.minH); // console.log("### MAX " + JSON.stringify(maximum));
            // console.log("### MIN " + JSON.stringify(minimum));

            var opts = _objectSpread({
              // allowFrom: "." + this.resizableHandleClass.trim().replace(" ", "."),
              edges: {
                left: false,
                right: "." + this.resizableHandleClass.trim().replace(" ", "."),
                bottom: "." + this.resizableHandleClass.trim().replace(" ", "."),
                top: false
              },
              ignoreFrom: this.resizeIgnoreFrom,
              restrictSize: {
                min: {
                  height: minimum.height * this.transformScale,
                  width: minimum.width * this.transformScale
                },
                max: {
                  height: maximum.height * this.transformScale,
                  width: maximum.width * this.transformScale
                }
              }
            }, this.resizeOption);
            if (this.preserveAspectRatio) {
              opts.modifiers = [_interactjs_interact.modifiers.aspectRatio({
                ratio: 'preserve'
              })];
            }
            this.interactObj.resizable(opts);
            if (!this.resizeEventSet) {
              this.resizeEventSet = true;
              this.interactObj.on('resizestart resizemove resizeend', function (event) {
                self.handleResize(event);
              });
            }
          } else {
            this.interactObj.resizable({
              enabled: false
            });
          }
        },
        autoSize: function autoSize() {
          // ok here we want to calculate if a resize is needed
          this.previousW = this.innerW;
          this.previousH = this.innerH;
          var newSize = this.$slots.default[0].elm.getBoundingClientRect();
          var pos = this.calcWH(newSize.height, newSize.width, true);
          if (pos.w < this.minW) {
            pos.w = this.minW;
          }
          if (pos.w > this.maxW) {
            pos.w = this.maxW;
          }
          if (pos.h < this.minH) {
            pos.h = this.minH;
          }
          if (pos.h > this.maxH) {
            pos.h = this.maxH;
          }
          if (pos.h < 1) {
            pos.h = 1;
          }
          if (pos.w < 1) {
            pos.w = 1;
          } // this.lastW = x; // basically, this is copied from resizehandler, but shouldn't be needed
          // this.lastH = y;

          if (this.innerW !== pos.w || this.innerH !== pos.h) {
            this.$emit("resize", this.i, pos.h, pos.w, newSize.height, newSize.width);
          }
          if (this.previousW !== pos.w || this.previousH !== pos.h) {
            this.$emit("resized", this.i, pos.h, pos.w, newSize.height, newSize.width);
            this.eventBus.$emit("resizeEvent", "resizeend", this.i, this.innerX, this.innerY, pos.h, pos.w);
          }
        }
      }
    };
    // CONCATENATED MODULE: ./src/components/GridItem.vue?vue&type=script&lang=js&
    /* harmony default export */
    var components_GridItemvue_type_script_lang_js_ = GridItemvue_type_script_lang_js_;
    // EXTERNAL MODULE: ./src/components/GridItem.vue?vue&type=style&index=0&lang=css&
    var GridItemvue_type_style_index_0_lang_css_ = __nested_webpack_require_154697__("5ed4");

    // EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
    var componentNormalizer = __nested_webpack_require_154697__("2877");

    // CONCATENATED MODULE: ./src/components/GridItem.vue

    /* normalize component */

    var component = Object(componentNormalizer["a" /* default */])(components_GridItemvue_type_script_lang_js_, render, staticRenderFns, false, null, null, null);

    /* harmony default export */
    var GridItem = __nested_webpack_exports__["a"] = component.exports;

    /***/
  },

  /***/"be13": /***/function be13(module, exports) {
    // 7.2.1 RequireObjectCoercible(argument)
    module.exports = function (it) {
      if (it == undefined) throw TypeError("Can't call method on  " + it);
      return it;
    };

    /***/
  },

  /***/"c274": /***/function c274(module, exports, __nested_webpack_require_392115__) {
    "use strict";

    var utils = __nested_webpack_require_392115__("50bf");
    module.exports = function batchProcessorMaker(options) {
      options = options || {};
      var reporter = options.reporter;
      var asyncProcess = utils.getOption(options, "async", true);
      var autoProcess = utils.getOption(options, "auto", true);
      if (autoProcess && !asyncProcess) {
        reporter && reporter.warn("Invalid options combination. auto=true and async=false is invalid. Setting async=true.");
        asyncProcess = true;
      }
      var batch = Batch();
      var asyncFrameHandler;
      var isProcessing = false;
      function addFunction(level, fn) {
        if (!isProcessing && autoProcess && asyncProcess && batch.size() === 0) {
          // Since this is async, it is guaranteed to be executed after that the fn is added to the batch.
          // This needs to be done before, since we're checking the size of the batch to be 0.
          processBatchAsync();
        }
        batch.add(level, fn);
      }
      function processBatch() {
        // Save the current batch, and create a new batch so that incoming functions are not added into the currently processing batch.
        // Continue processing until the top-level batch is empty (functions may be added to the new batch while processing, and so on).
        isProcessing = true;
        while (batch.size()) {
          var processingBatch = batch;
          batch = Batch();
          processingBatch.process();
        }
        isProcessing = false;
      }
      function forceProcessBatch(localAsyncProcess) {
        if (isProcessing) {
          return;
        }
        if (localAsyncProcess === undefined) {
          localAsyncProcess = asyncProcess;
        }
        if (asyncFrameHandler) {
          cancelFrame(asyncFrameHandler);
          asyncFrameHandler = null;
        }
        if (localAsyncProcess) {
          processBatchAsync();
        } else {
          processBatch();
        }
      }
      function processBatchAsync() {
        asyncFrameHandler = requestFrame(processBatch);
      }
      function clearBatch() {
        batch = {};
        batchSize = 0;
        topLevel = 0;
        bottomLevel = 0;
      }
      function cancelFrame(listener) {
        // var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.clearTimeout;
        var cancel = clearTimeout;
        return cancel(listener);
      }
      function requestFrame(callback) {
        // var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || function(fn) { return window.setTimeout(fn, 20); };
        var raf = function raf(fn) {
          return setTimeout(fn, 0);
        };
        return raf(callback);
      }
      return {
        add: addFunction,
        force: forceProcessBatch
      };
    };
    function Batch() {
      var batch = {};
      var size = 0;
      var topLevel = 0;
      var bottomLevel = 0;
      function add(level, fn) {
        if (!fn) {
          fn = level;
          level = 0;
        }
        if (level > topLevel) {
          topLevel = level;
        } else if (level < bottomLevel) {
          bottomLevel = level;
        }
        if (!batch[level]) {
          batch[level] = [];
        }
        batch[level].push(fn);
        size++;
      }
      function process() {
        for (var level = bottomLevel; level <= topLevel; level++) {
          var fns = batch[level];
          for (var i = 0; i < fns.length; i++) {
            var fn = fns[i];
            fn();
          }
        }
      }
      function getSize() {
        return size;
      }
      return {
        add: add,
        process: process,
        size: getSize
      };
    }

    /***/
  },

  /***/"c366": /***/function c366(module, exports, __nested_webpack_require_396013__) {
    // false -> Array#indexOf
    // true  -> Array#includes
    var toIObject = __nested_webpack_require_396013__("6821");
    var toLength = __nested_webpack_require_396013__("9def");
    var toAbsoluteIndex = __nested_webpack_require_396013__("77f1");
    module.exports = function (IS_INCLUDES) {
      return function ($this, el, fromIndex) {
        var O = toIObject($this);
        var length = toLength(O.length);
        var index = toAbsoluteIndex(fromIndex, length);
        var value;
        // Array#includes uses SameValueZero equality algorithm
        // eslint-disable-next-line no-self-compare
        if (IS_INCLUDES && el != el) while (length > index) {
          value = O[index++];
          // eslint-disable-next-line no-self-compare
          if (value != value) return true;
          // Array#indexOf ignores holes, Array#includes - not
        } else for (; length > index; index++) if (IS_INCLUDES || index in O) {
          if (O[index] === el) return IS_INCLUDES || index || 0;
        }
        return !IS_INCLUDES && -1;
      };
    };

    /***/
  },

  /***/"c5f6": /***/function c5f6(module, exports, __nested_webpack_require_397134__) {
    "use strict";

    var global = __nested_webpack_require_397134__("7726");
    var has = __nested_webpack_require_397134__("69a8");
    var cof = __nested_webpack_require_397134__("2d95");
    var inheritIfRequired = __nested_webpack_require_397134__("5dbc");
    var toPrimitive = __nested_webpack_require_397134__("6a99");
    var fails = __nested_webpack_require_397134__("79e5");
    var gOPN = __nested_webpack_require_397134__("9093").f;
    var gOPD = __nested_webpack_require_397134__("11e9").f;
    var dP = __nested_webpack_require_397134__("86cc").f;
    var $trim = __nested_webpack_require_397134__("aa77").trim;
    var NUMBER = 'Number';
    var $Number = global[NUMBER];
    var Base = $Number;
    var proto = $Number.prototype;
    // Opera ~12 has broken Object#toString
    var BROKEN_COF = cof(__nested_webpack_require_397134__("2aeb")(proto)) == NUMBER;
    var TRIM = ('trim' in String.prototype);

    // 7.1.3 ToNumber(argument)
    var toNumber = function toNumber(argument) {
      var it = toPrimitive(argument, false);
      if (typeof it == 'string' && it.length > 2) {
        it = TRIM ? it.trim() : $trim(it, 3);
        var first = it.charCodeAt(0);
        var third, radix, maxCode;
        if (first === 43 || first === 45) {
          third = it.charCodeAt(2);
          if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
        } else if (first === 48) {
          switch (it.charCodeAt(1)) {
            case 66:
            case 98:
              radix = 2;
              maxCode = 49;
              break;
            // fast equal /^0b[01]+$/i
            case 79:
            case 111:
              radix = 8;
              maxCode = 55;
              break;
            // fast equal /^0o[0-7]+$/i
            default:
              return +it;
          }
          for (var digits = it.slice(2), i = 0, l = digits.length, code; i < l; i++) {
            code = digits.charCodeAt(i);
            // parseInt parses a string to a first unavailable symbol
            // but ToNumber should return NaN if a string contains unavailable symbols
            if (code < 48 || code > maxCode) return NaN;
          }
          return parseInt(digits, radix);
        }
      }
      return +it;
    };
    if (!$Number(' 0o1') || !$Number('0b1') || $Number('+0x1')) {
      $Number = function Number(value) {
        var it = arguments.length < 1 ? 0 : value;
        var that = this;
        return that instanceof $Number
        // check on 1..constructor(foo) case
        && (BROKEN_COF ? fails(function () {
          proto.valueOf.call(that);
        }) : cof(that) != NUMBER) ? inheritIfRequired(new Base(toNumber(it)), that, $Number) : toNumber(it);
      };
      for (var keys = __nested_webpack_require_397134__("9e1e") ? gOPN(Base) : (
        // ES3:
        'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
        // ES6 (in case, if modules with ES6 Number statics required before):
        'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' + 'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger').split(','), j = 0, key; keys.length > j; j++) {
        if (has(Base, key = keys[j]) && !has($Number, key)) {
          dP($Number, key, gOPD(Base, key));
        }
      }
      $Number.prototype = proto;
      proto.constructor = $Number;
      __nested_webpack_require_397134__("2aba")(global, NUMBER, $Number);
    }

    /***/
  },

  /***/"c69a": /***/function c69a(module, exports, __nested_webpack_require_400492__) {
    module.exports = !__nested_webpack_require_400492__("9e1e") && !__nested_webpack_require_400492__("79e5")(function () {
      return Object.defineProperty(__nested_webpack_require_400492__("230e")('div'), 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });

    /***/
  },

  /***/"c8ba": /***/function c8ba(module, exports) {
    var g;

    // This works in non-strict mode
    g = function () {
      return this;
    }();
    try {
      // This works if eval is allowed (see CSP)
      g = g || new Function("return this")();
    } catch (e) {
      // This works if the window reference is available
      if (typeof window === "object") g = window;
    }

    // g can still be undefined, but nothing to do about it...
    // We return undefined, instead of nothing here, so it's
    // easier to handle this case. if(!global) { ...}

    module.exports = g;

    /***/
  },

  /***/"c946": /***/function c946(module, exports, __nested_webpack_require_401451__) {
    "use strict";

    /**
     * Resize detection strategy that injects divs to elements in order to detect resize events on scroll events.
     * Heavily inspired by: https://github.com/marcj/css-element-queries/blob/master/src/ResizeSensor.js
     */
    var forEach = __nested_webpack_require_401451__("b770").forEach;
    module.exports = function (options) {
      options = options || {};
      var reporter = options.reporter;
      var batchProcessor = options.batchProcessor;
      var getState = options.stateHandler.getState;
      var hasState = options.stateHandler.hasState;
      var idHandler = options.idHandler;
      if (!batchProcessor) {
        throw new Error("Missing required dependency: batchProcessor");
      }
      if (!reporter) {
        throw new Error("Missing required dependency: reporter.");
      }

      //TODO: Could this perhaps be done at installation time?
      var scrollbarSizes = getScrollbarSizes();
      var styleId = "erd_scroll_detection_scrollbar_style";
      var detectionContainerClass = "erd_scroll_detection_container";
      function initDocument(targetDocument) {
        // Inject the scrollbar styling that prevents them from appearing sometimes in Chrome.
        // The injected container needs to have a class, so that it may be styled with CSS (pseudo elements).
        injectScrollStyle(targetDocument, styleId, detectionContainerClass);
      }
      initDocument(window.document);
      function buildCssTextString(rules) {
        var seperator = options.important ? " !important; " : "; ";
        return (rules.join(seperator) + seperator).trim();
      }
      function getScrollbarSizes() {
        var width = 500;
        var height = 500;
        var child = document.createElement("div");
        child.style.cssText = buildCssTextString(["position: absolute", "width: " + width * 2 + "px", "height: " + height * 2 + "px", "visibility: hidden", "margin: 0", "padding: 0"]);
        var container = document.createElement("div");
        container.style.cssText = buildCssTextString(["position: absolute", "width: " + width + "px", "height: " + height + "px", "overflow: scroll", "visibility: none", "top: " + -width * 3 + "px", "left: " + -height * 3 + "px", "visibility: hidden", "margin: 0", "padding: 0"]);
        container.appendChild(child);
        document.body.insertBefore(container, document.body.firstChild);
        var widthSize = width - container.clientWidth;
        var heightSize = height - container.clientHeight;
        document.body.removeChild(container);
        return {
          width: widthSize,
          height: heightSize
        };
      }
      function injectScrollStyle(targetDocument, styleId, containerClass) {
        function injectStyle(style, method) {
          method = method || function (element) {
            targetDocument.head.appendChild(element);
          };
          var styleElement = targetDocument.createElement("style");
          styleElement.innerHTML = style;
          styleElement.id = styleId;
          method(styleElement);
          return styleElement;
        }
        if (!targetDocument.getElementById(styleId)) {
          var containerAnimationClass = containerClass + "_animation";
          var containerAnimationActiveClass = containerClass + "_animation_active";
          var style = "/* Created by the element-resize-detector library. */\n";
          style += "." + containerClass + " > div::-webkit-scrollbar { " + buildCssTextString(["display: none"]) + " }\n\n";
          style += "." + containerAnimationActiveClass + " { " + buildCssTextString(["-webkit-animation-duration: 0.1s", "animation-duration: 0.1s", "-webkit-animation-name: " + containerAnimationClass, "animation-name: " + containerAnimationClass]) + " }\n";
          style += "@-webkit-keyframes " + containerAnimationClass + " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }\n";
          style += "@keyframes " + containerAnimationClass + " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }";
          injectStyle(style);
        }
      }
      function addAnimationClass(element) {
        element.className += " " + detectionContainerClass + "_animation_active";
      }
      function addEvent(el, name, cb) {
        if (el.addEventListener) {
          el.addEventListener(name, cb);
        } else if (el.attachEvent) {
          el.attachEvent("on" + name, cb);
        } else {
          return reporter.error("[scroll] Don't know how to add event listeners.");
        }
      }
      function removeEvent(el, name, cb) {
        if (el.removeEventListener) {
          el.removeEventListener(name, cb);
        } else if (el.detachEvent) {
          el.detachEvent("on" + name, cb);
        } else {
          return reporter.error("[scroll] Don't know how to remove event listeners.");
        }
      }
      function getExpandElement(element) {
        return getState(element).container.childNodes[0].childNodes[0].childNodes[0];
      }
      function getShrinkElement(element) {
        return getState(element).container.childNodes[0].childNodes[0].childNodes[1];
      }

      /**
       * Adds a resize event listener to the element.
       * @public
       * @param {element} element The element that should have the listener added.
       * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
       */
      function addListener(element, listener) {
        var listeners = getState(element).listeners;
        if (!listeners.push) {
          throw new Error("Cannot add listener to an element that is not detectable.");
        }
        getState(element).listeners.push(listener);
      }

      /**
       * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
       * @private
       * @param {object} options Optional options object.
       * @param {element} element The element to make detectable
       * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
       */
      function makeDetectable(options, element, callback) {
        if (!callback) {
          callback = element;
          element = options;
          options = null;
        }
        options = options || {};
        function debug() {
          if (options.debug) {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(idHandler.get(element), "Scroll: ");
            if (reporter.log.apply) {
              reporter.log.apply(null, args);
            } else {
              for (var i = 0; i < args.length; i++) {
                reporter.log(args[i]);
              }
            }
          }
        }
        function isDetached(element) {
          function isInDocument(element) {
            var isInShadowRoot = element.getRootNode && element.getRootNode().contains(element);
            return element === element.ownerDocument.body || element.ownerDocument.body.contains(element) || isInShadowRoot;
          }
          if (!isInDocument(element)) {
            return true;
          }

          // FireFox returns null style in hidden iframes. See https://github.com/wnr/element-resize-detector/issues/68 and https://bugzilla.mozilla.org/show_bug.cgi?id=795520
          if (window.getComputedStyle(element) === null) {
            return true;
          }
          return false;
        }
        function isUnrendered(element) {
          // Check the absolute positioned container since the top level container is display: inline.
          var container = getState(element).container.childNodes[0];
          var style = window.getComputedStyle(container);
          return !style.width || style.width.indexOf("px") === -1; //Can only compute pixel value when rendered.
        }

        function getStyle() {
          // Some browsers only force layouts when actually reading the style properties of the style object, so make sure that they are all read here,
          // so that the user of the function can be sure that it will perform the layout here, instead of later (important for batching).
          var elementStyle = window.getComputedStyle(element);
          var style = {};
          style.position = elementStyle.position;
          style.width = element.offsetWidth;
          style.height = element.offsetHeight;
          style.top = elementStyle.top;
          style.right = elementStyle.right;
          style.bottom = elementStyle.bottom;
          style.left = elementStyle.left;
          style.widthCSS = elementStyle.width;
          style.heightCSS = elementStyle.height;
          return style;
        }
        function storeStartSize() {
          var style = getStyle();
          getState(element).startSize = {
            width: style.width,
            height: style.height
          };
          debug("Element start size", getState(element).startSize);
        }
        function initListeners() {
          getState(element).listeners = [];
        }
        function storeStyle() {
          debug("storeStyle invoked.");
          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          var style = getStyle();
          getState(element).style = style;
        }
        function storeCurrentSize(element, width, height) {
          getState(element).lastWidth = width;
          getState(element).lastHeight = height;
        }
        function getExpandChildElement(element) {
          return getExpandElement(element).childNodes[0];
        }
        function getWidthOffset() {
          return 2 * scrollbarSizes.width + 1;
        }
        function getHeightOffset() {
          return 2 * scrollbarSizes.height + 1;
        }
        function getExpandWidth(width) {
          return width + 10 + getWidthOffset();
        }
        function getExpandHeight(height) {
          return height + 10 + getHeightOffset();
        }
        function getShrinkWidth(width) {
          return width * 2 + getWidthOffset();
        }
        function getShrinkHeight(height) {
          return height * 2 + getHeightOffset();
        }
        function positionScrollbars(element, width, height) {
          var expand = getExpandElement(element);
          var shrink = getShrinkElement(element);
          var expandWidth = getExpandWidth(width);
          var expandHeight = getExpandHeight(height);
          var shrinkWidth = getShrinkWidth(width);
          var shrinkHeight = getShrinkHeight(height);
          expand.scrollLeft = expandWidth;
          expand.scrollTop = expandHeight;
          shrink.scrollLeft = shrinkWidth;
          shrink.scrollTop = shrinkHeight;
        }
        function injectContainerElement() {
          var container = getState(element).container;
          if (!container) {
            container = document.createElement("div");
            container.className = detectionContainerClass;
            container.style.cssText = buildCssTextString(["visibility: hidden", "display: inline", "width: 0px", "height: 0px", "z-index: -1", "overflow: hidden", "margin: 0", "padding: 0"]);
            getState(element).container = container;
            addAnimationClass(container);
            element.appendChild(container);
            var onAnimationStart = function onAnimationStart() {
              getState(element).onRendered && getState(element).onRendered();
            };
            addEvent(container, "animationstart", onAnimationStart);

            // Store the event handler here so that they may be removed when uninstall is called.
            // See uninstall function for an explanation why it is needed.
            getState(element).onAnimationStart = onAnimationStart;
          }
          return container;
        }
        function injectScrollElements() {
          function alterPositionStyles() {
            var style = getState(element).style;
            if (style.position === "static") {
              element.style.setProperty("position", "relative", options.important ? "important" : "");
              var removeRelativeStyles = function removeRelativeStyles(reporter, element, style, property) {
                function getNumericalValue(value) {
                  return value.replace(/[^-\d\.]/g, "");
                }
                var value = style[property];
                if (value !== "auto" && getNumericalValue(value) !== "0") {
                  reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
                  element.style[property] = 0;
                }
              };

              //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
              //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
              removeRelativeStyles(reporter, element, style, "top");
              removeRelativeStyles(reporter, element, style, "right");
              removeRelativeStyles(reporter, element, style, "bottom");
              removeRelativeStyles(reporter, element, style, "left");
            }
          }
          function getLeftTopBottomRightCssText(left, top, bottom, right) {
            left = !left ? "0" : left + "px";
            top = !top ? "0" : top + "px";
            bottom = !bottom ? "0" : bottom + "px";
            right = !right ? "0" : right + "px";
            return ["left: " + left, "top: " + top, "right: " + right, "bottom: " + bottom];
          }
          debug("Injecting elements");
          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          alterPositionStyles();
          var rootContainer = getState(element).container;
          if (!rootContainer) {
            rootContainer = injectContainerElement();
          }

          // Due to this WebKit bug https://bugs.webkit.org/show_bug.cgi?id=80808 (currently fixed in Blink, but still present in WebKit browsers such as Safari),
          // we need to inject two containers, one that is width/height 100% and another that is left/top -1px so that the final container always is 1x1 pixels bigger than
          // the targeted element.
          // When the bug is resolved, "containerContainer" may be removed.

          // The outer container can occasionally be less wide than the targeted when inside inline elements element in WebKit (see https://bugs.webkit.org/show_bug.cgi?id=152980).
          // This should be no problem since the inner container either way makes sure the injected scroll elements are at least 1x1 px.

          var scrollbarWidth = scrollbarSizes.width;
          var scrollbarHeight = scrollbarSizes.height;
          var containerContainerStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: hidden", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%", "left: 0px", "top: 0px"]);
          var containerStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: hidden", "z-index: -1", "visibility: hidden"].concat(getLeftTopBottomRightCssText(-(1 + scrollbarWidth), -(1 + scrollbarHeight), -scrollbarHeight, -scrollbarWidth)));
          var expandStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: scroll", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%"]);
          var shrinkStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: scroll", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%"]);
          var expandChildStyle = buildCssTextString(["position: absolute", "left: 0", "top: 0"]);
          var shrinkChildStyle = buildCssTextString(["position: absolute", "width: 200%", "height: 200%"]);
          var containerContainer = document.createElement("div");
          var container = document.createElement("div");
          var expand = document.createElement("div");
          var expandChild = document.createElement("div");
          var shrink = document.createElement("div");
          var shrinkChild = document.createElement("div");

          // Some browsers choke on the resize system being rtl, so force it to ltr. https://github.com/wnr/element-resize-detector/issues/56
          // However, dir should not be set on the top level container as it alters the dimensions of the target element in some browsers.
          containerContainer.dir = "ltr";
          containerContainer.style.cssText = containerContainerStyle;
          containerContainer.className = detectionContainerClass;
          container.className = detectionContainerClass;
          container.style.cssText = containerStyle;
          expand.style.cssText = expandStyle;
          expandChild.style.cssText = expandChildStyle;
          shrink.style.cssText = shrinkStyle;
          shrinkChild.style.cssText = shrinkChildStyle;
          expand.appendChild(expandChild);
          shrink.appendChild(shrinkChild);
          container.appendChild(expand);
          container.appendChild(shrink);
          containerContainer.appendChild(container);
          rootContainer.appendChild(containerContainer);
          function onExpandScroll() {
            var state = getState(element);
            if (state && state.onExpand) {
              state.onExpand();
            } else {
              debug("Aborting expand scroll handler: element has been uninstalled");
            }
          }
          function onShrinkScroll() {
            var state = getState(element);
            if (state && state.onShrink) {
              state.onShrink();
            } else {
              debug("Aborting shrink scroll handler: element has been uninstalled");
            }
          }
          addEvent(expand, "scroll", onExpandScroll);
          addEvent(shrink, "scroll", onShrinkScroll);

          // Store the event handlers here so that they may be removed when uninstall is called.
          // See uninstall function for an explanation why it is needed.
          getState(element).onExpandScroll = onExpandScroll;
          getState(element).onShrinkScroll = onShrinkScroll;
        }
        function registerListenersAndPositionElements() {
          function updateChildSizes(element, width, height) {
            var expandChild = getExpandChildElement(element);
            var expandWidth = getExpandWidth(width);
            var expandHeight = getExpandHeight(height);
            expandChild.style.setProperty("width", expandWidth + "px", options.important ? "important" : "");
            expandChild.style.setProperty("height", expandHeight + "px", options.important ? "important" : "");
          }
          function updateDetectorElements(done) {
            var width = element.offsetWidth;
            var height = element.offsetHeight;

            // Check whether the size has actually changed since last time the algorithm ran. If not, some steps may be skipped.
            var sizeChanged = width !== getState(element).lastWidth || height !== getState(element).lastHeight;
            debug("Storing current size", width, height);

            // Store the size of the element sync here, so that multiple scroll events may be ignored in the event listeners.
            // Otherwise the if-check in handleScroll is useless.
            storeCurrentSize(element, width, height);

            // Since we delay the processing of the batch, there is a risk that uninstall has been called before the batch gets to execute.
            // Since there is no way to cancel the fn executions, we need to add an uninstall guard to all fns of the batch.

            batchProcessor.add(0, function performUpdateChildSizes() {
              if (!sizeChanged) {
                return;
              }
              if (!getState(element)) {
                debug("Aborting because element has been uninstalled");
                return;
              }
              if (!areElementsInjected()) {
                debug("Aborting because element container has not been initialized");
                return;
              }
              if (options.debug) {
                var w = element.offsetWidth;
                var h = element.offsetHeight;
                if (w !== width || h !== height) {
                  reporter.warn(idHandler.get(element), "Scroll: Size changed before updating detector elements.");
                }
              }
              updateChildSizes(element, width, height);
            });
            batchProcessor.add(1, function updateScrollbars() {
              // This function needs to be invoked event though the size is unchanged. The element could have been resized very quickly and then
              // been restored to the original size, which will have changed the scrollbar positions.

              if (!getState(element)) {
                debug("Aborting because element has been uninstalled");
                return;
              }
              if (!areElementsInjected()) {
                debug("Aborting because element container has not been initialized");
                return;
              }
              positionScrollbars(element, width, height);
            });
            if (sizeChanged && done) {
              batchProcessor.add(2, function () {
                if (!getState(element)) {
                  debug("Aborting because element has been uninstalled");
                  return;
                }
                if (!areElementsInjected()) {
                  debug("Aborting because element container has not been initialized");
                  return;
                }
                done();
              });
            }
          }
          function areElementsInjected() {
            return !!getState(element).container;
          }
          function notifyListenersIfNeeded() {
            function isFirstNotify() {
              return getState(element).lastNotifiedWidth === undefined;
            }
            debug("notifyListenersIfNeeded invoked");
            var state = getState(element);

            // Don't notify if the current size is the start size, and this is the first notification.
            if (isFirstNotify() && state.lastWidth === state.startSize.width && state.lastHeight === state.startSize.height) {
              return debug("Not notifying: Size is the same as the start size, and there has been no notification yet.");
            }

            // Don't notify if the size already has been notified.
            if (state.lastWidth === state.lastNotifiedWidth && state.lastHeight === state.lastNotifiedHeight) {
              return debug("Not notifying: Size already notified");
            }
            debug("Current size not notified, notifying...");
            state.lastNotifiedWidth = state.lastWidth;
            state.lastNotifiedHeight = state.lastHeight;
            forEach(getState(element).listeners, function (listener) {
              listener(element);
            });
          }
          function handleRender() {
            debug("startanimation triggered.");
            if (isUnrendered(element)) {
              debug("Ignoring since element is still unrendered...");
              return;
            }
            debug("Element rendered.");
            var expand = getExpandElement(element);
            var shrink = getShrinkElement(element);
            if (expand.scrollLeft === 0 || expand.scrollTop === 0 || shrink.scrollLeft === 0 || shrink.scrollTop === 0) {
              debug("Scrollbars out of sync. Updating detector elements...");
              updateDetectorElements(notifyListenersIfNeeded);
            }
          }
          function handleScroll() {
            debug("Scroll detected.");
            if (isUnrendered(element)) {
              // Element is still unrendered. Skip this scroll event.
              debug("Scroll event fired while unrendered. Ignoring...");
              return;
            }
            updateDetectorElements(notifyListenersIfNeeded);
          }
          debug("registerListenersAndPositionElements invoked.");
          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          getState(element).onRendered = handleRender;
          getState(element).onExpand = handleScroll;
          getState(element).onShrink = handleScroll;
          var style = getState(element).style;
          updateChildSizes(element, style.width, style.height);
        }
        function finalizeDomMutation() {
          debug("finalizeDomMutation invoked.");
          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          var style = getState(element).style;
          storeCurrentSize(element, style.width, style.height);
          positionScrollbars(element, style.width, style.height);
        }
        function ready() {
          callback(element);
        }
        function install() {
          debug("Installing...");
          initListeners();
          storeStartSize();
          batchProcessor.add(0, storeStyle);
          batchProcessor.add(1, injectScrollElements);
          batchProcessor.add(2, registerListenersAndPositionElements);
          batchProcessor.add(3, finalizeDomMutation);
          batchProcessor.add(4, ready);
        }
        debug("Making detectable...");
        if (isDetached(element)) {
          debug("Element is detached");
          injectContainerElement();
          debug("Waiting until element is attached...");
          getState(element).onRendered = function () {
            debug("Element is now attached");
            install();
          };
        } else {
          install();
        }
      }
      function uninstall(element) {
        var state = getState(element);
        if (!state) {
          // Uninstall has been called on a non-erd element.
          return;
        }

        // Uninstall may have been called in the following scenarios:
        // (1) Right between the sync code and async batch (here state.busy = true, but nothing have been registered or injected).
        // (2) In the ready callback of the last level of the batch by another element (here, state.busy = true, but all the stuff has been injected).
        // (3) After the installation process (here, state.busy = false and all the stuff has been injected).
        // So to be on the safe side, let's check for each thing before removing.

        // We need to remove the event listeners, because otherwise the event might fire on an uninstall element which results in an error when trying to get the state of the element.
        state.onExpandScroll && removeEvent(getExpandElement(element), "scroll", state.onExpandScroll);
        state.onShrinkScroll && removeEvent(getShrinkElement(element), "scroll", state.onShrinkScroll);
        state.onAnimationStart && removeEvent(state.container, "animationstart", state.onAnimationStart);
        state.container && element.removeChild(state.container);
      }
      return {
        makeDetectable: makeDetectable,
        addListener: addListener,
        uninstall: uninstall,
        initDocument: initDocument
      };
    };

    /***/
  },

  /***/"ca5a": /***/function ca5a(module, exports) {
    var id = 0;
    var px = Math.random();
    module.exports = function (key) {
      return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
    };

    /***/
  },

  /***/"cadf": /***/function cadf(module, exports, __nested_webpack_require_429612__) {
    "use strict";

    var addToUnscopables = __nested_webpack_require_429612__("9c6c");
    var step = __nested_webpack_require_429612__("d53b");
    var Iterators = __nested_webpack_require_429612__("84f2");
    var toIObject = __nested_webpack_require_429612__("6821");

    // 22.1.3.4 Array.prototype.entries()
    // 22.1.3.13 Array.prototype.keys()
    // 22.1.3.29 Array.prototype.values()
    // 22.1.3.30 Array.prototype[@@iterator]()
    module.exports = __nested_webpack_require_429612__("01f9")(Array, 'Array', function (iterated, kind) {
      this._t = toIObject(iterated); // target
      this._i = 0; // next index
      this._k = kind; // kind
      // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
    }, function () {
      var O = this._t;
      var kind = this._k;
      var index = this._i++;
      if (!O || index >= O.length) {
        this._t = undefined;
        return step(1);
      }
      if (kind == 'keys') return step(0, index);
      if (kind == 'values') return step(0, O[index]);
      return step(0, [index, O[index]]);
    }, 'values');

    // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
    Iterators.Arguments = Iterators.Array;
    addToUnscopables('keys');
    addToUnscopables('values');
    addToUnscopables('entries');

    /***/
  },

  /***/"cb7c": /***/function cb7c(module, exports, __nested_webpack_require_430920__) {
    var isObject = __nested_webpack_require_430920__("d3f4");
    module.exports = function (it) {
      if (!isObject(it)) throw TypeError(it + ' is not an object!');
      return it;
    };

    /***/
  },

  /***/"ce10": /***/function ce10(module, exports, __nested_webpack_require_431189__) {
    var has = __nested_webpack_require_431189__("69a8");
    var toIObject = __nested_webpack_require_431189__("6821");
    var arrayIndexOf = __nested_webpack_require_431189__("c366")(false);
    var IE_PROTO = __nested_webpack_require_431189__("613b")('IE_PROTO');
    module.exports = function (object, names) {
      var O = toIObject(object);
      var i = 0;
      var result = [];
      var key;
      for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
      // Don't enum bug & hidden keys
      while (names.length > i) if (has(O, key = names[i++])) {
        ~arrayIndexOf(result, key) || result.push(key);
      }
      return result;
    };

    /***/
  },

  /***/"d3f4": /***/function d3f4(module, exports) {
    module.exports = function (it) {
      return typeof it === 'object' ? it !== null : typeof it === 'function';
    };

    /***/
  },

  /***/"d53b": /***/function d53b(module, exports) {
    module.exports = function (done, value) {
      return {
        value: value,
        done: !!done
      };
    };

    /***/
  },

  /***/"d6eb": /***/function d6eb(module, exports, __webpack_require__) {
    "use strict";

    var prop = "_erd";
    function initState(element) {
      element[prop] = {};
      return getState(element);
    }
    function getState(element) {
      return element[prop];
    }
    function cleanState(element) {
      delete element[prop];
    }
    module.exports = {
      initState: initState,
      getState: getState,
      cleanState: cleanState
    };

    /***/
  },

  /***/"d8e8": /***/function d8e8(module, exports) {
    module.exports = function (it) {
      if (typeof it != 'function') throw TypeError(it + ' is not a function!');
      return it;
    };

    /***/
  },

  /***/"e11e": /***/function e11e(module, exports) {
    // IE 8- don't enum bug keys
    module.exports = 'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'.split(',');

    /***/
  },

  /***/"e279": /***/function e279(module, __webpack_exports__, __nested_webpack_require_433212__) {
    "use strict";

    /* harmony import */
    var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GridLayout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __nested_webpack_require_433212__("1156");
    /* harmony import */
    var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GridLayout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__nested_webpack_require_433212__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GridLayout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /***/
  },

  /***/"eec4": /***/function eec4(module, exports, __nested_webpack_require_434861__) {
    "use strict";

    var forEach = __nested_webpack_require_434861__("b770").forEach;
    var elementUtilsMaker = __nested_webpack_require_434861__("5be5");
    var listenerHandlerMaker = __nested_webpack_require_434861__("49ad");
    var idGeneratorMaker = __nested_webpack_require_434861__("2cef");
    var idHandlerMaker = __nested_webpack_require_434861__("5058");
    var reporterMaker = __nested_webpack_require_434861__("abb4");
    var browserDetector = __nested_webpack_require_434861__("18e9");
    var batchProcessorMaker = __nested_webpack_require_434861__("c274");
    var stateHandler = __nested_webpack_require_434861__("d6eb");

    //Detection strategies.
    var objectStrategyMaker = __nested_webpack_require_434861__("18d2");
    var scrollStrategyMaker = __nested_webpack_require_434861__("c946");
    function isCollection(obj) {
      return Array.isArray(obj) || obj.length !== undefined;
    }
    function toArray(collection) {
      if (!Array.isArray(collection)) {
        var array = [];
        forEach(collection, function (obj) {
          array.push(obj);
        });
        return array;
      } else {
        return collection;
      }
    }
    function isElement(obj) {
      return obj && obj.nodeType === 1;
    }

    /**
     * @typedef idHandler
     * @type {object}
     * @property {function} get Gets the resize detector id of the element.
     * @property {function} set Generate and sets the resize detector id of the element.
     */

    /**
     * @typedef Options
     * @type {object}
     * @property {boolean} callOnAdd    Determines if listeners should be called when they are getting added.
                                        Default is true. If true, the listener is guaranteed to be called when it has been added.
                                        If false, the listener will not be guarenteed to be called when it has been added (does not prevent it from being called).
     * @property {idHandler} idHandler  A custom id handler that is responsible for generating, setting and retrieving id's for elements.
                                        If not provided, a default id handler will be used.
     * @property {reporter} reporter    A custom reporter that handles reporting logs, warnings and errors.
                                        If not provided, a default id handler will be used.
                                        If set to false, then nothing will be reported.
     * @property {boolean} debug        If set to true, the the system will report debug messages as default for the listenTo method.
     */

    /**
     * Creates an element resize detector instance.
     * @public
     * @param {Options?} options Optional global options object that will decide how this instance will work.
     */
    module.exports = function (options) {
      options = options || {};

      //idHandler is currently not an option to the listenTo function, so it should not be added to globalOptions.
      var idHandler;
      if (options.idHandler) {
        // To maintain compatability with idHandler.get(element, readonly), make sure to wrap the given idHandler
        // so that readonly flag always is true when it's used here. This may be removed next major version bump.
        idHandler = {
          get: function get(element) {
            return options.idHandler.get(element, true);
          },
          set: options.idHandler.set
        };
      } else {
        var idGenerator = idGeneratorMaker();
        var defaultIdHandler = idHandlerMaker({
          idGenerator: idGenerator,
          stateHandler: stateHandler
        });
        idHandler = defaultIdHandler;
      }

      //reporter is currently not an option to the listenTo function, so it should not be added to globalOptions.
      var reporter = options.reporter;
      if (!reporter) {
        //If options.reporter is false, then the reporter should be quiet.
        var quiet = reporter === false;
        reporter = reporterMaker(quiet);
      }

      //batchProcessor is currently not an option to the listenTo function, so it should not be added to globalOptions.
      var batchProcessor = getOption(options, "batchProcessor", batchProcessorMaker({
        reporter: reporter
      }));

      //Options to be used as default for the listenTo function.
      var globalOptions = {};
      globalOptions.callOnAdd = !!getOption(options, "callOnAdd", true);
      globalOptions.debug = !!getOption(options, "debug", false);
      var eventListenerHandler = listenerHandlerMaker(idHandler);
      var elementUtils = elementUtilsMaker({
        stateHandler: stateHandler
      });

      //The detection strategy to be used.
      var detectionStrategy;
      var desiredStrategy = getOption(options, "strategy", "object");
      var importantCssRules = getOption(options, "important", false);
      var strategyOptions = {
        reporter: reporter,
        batchProcessor: batchProcessor,
        stateHandler: stateHandler,
        idHandler: idHandler,
        important: importantCssRules
      };
      if (desiredStrategy === "scroll") {
        if (browserDetector.isLegacyOpera()) {
          reporter.warn("Scroll strategy is not supported on legacy Opera. Changing to object strategy.");
          desiredStrategy = "object";
        } else if (browserDetector.isIE(9)) {
          reporter.warn("Scroll strategy is not supported on IE9. Changing to object strategy.");
          desiredStrategy = "object";
        }
      }
      if (desiredStrategy === "scroll") {
        detectionStrategy = scrollStrategyMaker(strategyOptions);
      } else if (desiredStrategy === "object") {
        detectionStrategy = objectStrategyMaker(strategyOptions);
      } else {
        throw new Error("Invalid strategy name: " + desiredStrategy);
      }

      //Calls can be made to listenTo with elements that are still being installed.
      //Also, same elements can occur in the elements list in the listenTo function.
      //With this map, the ready callbacks can be synchronized between the calls
      //so that the ready callback can always be called when an element is ready - even if
      //it wasn't installed from the function itself.
      var onReadyCallbacks = {};

      /**
       * Makes the given elements resize-detectable and starts listening to resize events on the elements. Calls the event callback for each event for each element.
       * @public
       * @param {Options?} options Optional options object. These options will override the global options. Some options may not be overriden, such as idHandler.
       * @param {element[]|element} elements The given array of elements to detect resize events of. Single element is also valid.
       * @param {function} listener The callback to be executed for each resize event for each element.
       */
      function listenTo(options, elements, listener) {
        function onResizeCallback(element) {
          var listeners = eventListenerHandler.get(element);
          forEach(listeners, function callListenerProxy(listener) {
            listener(element);
          });
        }
        function addListener(callOnAdd, element, listener) {
          eventListenerHandler.add(element, listener);
          if (callOnAdd) {
            listener(element);
          }
        }

        //Options object may be omitted.
        if (!listener) {
          listener = elements;
          elements = options;
          options = {};
        }
        if (!elements) {
          throw new Error("At least one element required.");
        }
        if (!listener) {
          throw new Error("Listener required.");
        }
        if (isElement(elements)) {
          // A single element has been passed in.
          elements = [elements];
        } else if (isCollection(elements)) {
          // Convert collection to array for plugins.
          // TODO: May want to check so that all the elements in the collection are valid elements.
          elements = toArray(elements);
        } else {
          return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
        }
        var elementsReady = 0;
        var callOnAdd = getOption(options, "callOnAdd", globalOptions.callOnAdd);
        var onReadyCallback = getOption(options, "onReady", function noop() {});
        var debug = getOption(options, "debug", globalOptions.debug);
        forEach(elements, function attachListenerToElement(element) {
          if (!stateHandler.getState(element)) {
            stateHandler.initState(element);
            idHandler.set(element);
          }
          var id = idHandler.get(element);
          debug && reporter.log("Attaching listener to element", id, element);
          if (!elementUtils.isDetectable(element)) {
            debug && reporter.log(id, "Not detectable.");
            if (elementUtils.isBusy(element)) {
              debug && reporter.log(id, "System busy making it detectable");

              //The element is being prepared to be detectable. Do not make it detectable.
              //Just add the listener, because the element will soon be detectable.
              addListener(callOnAdd, element, listener);
              onReadyCallbacks[id] = onReadyCallbacks[id] || [];
              onReadyCallbacks[id].push(function onReady() {
                elementsReady++;
                if (elementsReady === elements.length) {
                  onReadyCallback();
                }
              });
              return;
            }
            debug && reporter.log(id, "Making detectable...");
            //The element is not prepared to be detectable, so do prepare it and add a listener to it.
            elementUtils.markBusy(element, true);
            return detectionStrategy.makeDetectable({
              debug: debug,
              important: importantCssRules
            }, element, function onElementDetectable(element) {
              debug && reporter.log(id, "onElementDetectable");
              if (stateHandler.getState(element)) {
                elementUtils.markAsDetectable(element);
                elementUtils.markBusy(element, false);
                detectionStrategy.addListener(element, onResizeCallback);
                addListener(callOnAdd, element, listener);

                // Since the element size might have changed since the call to "listenTo", we need to check for this change,
                // so that a resize event may be emitted.
                // Having the startSize object is optional (since it does not make sense in some cases such as unrendered elements), so check for its existance before.
                // Also, check the state existance before since the element may have been uninstalled in the installation process.
                var state = stateHandler.getState(element);
                if (state && state.startSize) {
                  var width = element.offsetWidth;
                  var height = element.offsetHeight;
                  if (state.startSize.width !== width || state.startSize.height !== height) {
                    onResizeCallback(element);
                  }
                }
                if (onReadyCallbacks[id]) {
                  forEach(onReadyCallbacks[id], function (callback) {
                    callback();
                  });
                }
              } else {
                // The element has been unisntalled before being detectable.
                debug && reporter.log(id, "Element uninstalled before being detectable.");
              }
              delete onReadyCallbacks[id];
              elementsReady++;
              if (elementsReady === elements.length) {
                onReadyCallback();
              }
            });
          }
          debug && reporter.log(id, "Already detecable, adding listener.");

          //The element has been prepared to be detectable and is ready to be listened to.
          addListener(callOnAdd, element, listener);
          elementsReady++;
        });
        if (elementsReady === elements.length) {
          onReadyCallback();
        }
      }
      function uninstall(elements) {
        if (!elements) {
          return reporter.error("At least one element is required.");
        }
        if (isElement(elements)) {
          // A single element has been passed in.
          elements = [elements];
        } else if (isCollection(elements)) {
          // Convert collection to array for plugins.
          // TODO: May want to check so that all the elements in the collection are valid elements.
          elements = toArray(elements);
        } else {
          return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
        }
        forEach(elements, function (element) {
          eventListenerHandler.removeAllListeners(element);
          detectionStrategy.uninstall(element);
          stateHandler.cleanState(element);
        });
      }
      function initDocument(targetDocument) {
        detectionStrategy.initDocument && detectionStrategy.initDocument(targetDocument);
      }
      return {
        listenTo: listenTo,
        removeListener: eventListenerHandler.removeListener,
        removeAllListeners: eventListenerHandler.removeAllListeners,
        uninstall: uninstall,
        initDocument: initDocument
      };
    };
    function getOption(options, name, defaultValue) {
      var value = options[name];
      if ((value === undefined || value === null) && defaultValue !== undefined) {
        return defaultValue;
      }
      return value;
    }

    /***/
  },

  /***/"f1ae": /***/function f1ae(module, exports, __nested_webpack_require_448455__) {
    "use strict";

    var $defineProperty = __nested_webpack_require_448455__("86cc");
    var createDesc = __nested_webpack_require_448455__("4630");
    module.exports = function (object, index, value) {
      if (index in object) $defineProperty.f(object, index, createDesc(0, value));else object[index] = value;
    };

    /***/
  },

  /***/"f6fd": /***/function f6fd(module, exports) {
    // document.currentScript polyfill by Adam Miller

    // MIT license

    (function (document) {
      var currentScript = "currentScript",
        scripts = document.getElementsByTagName('script'); // Live NodeList collection

      // If browser needs currentScript polyfill, add get currentScript() to the document object
      if (!(currentScript in document)) {
        Object.defineProperty(document, currentScript, {
          get: function get() {
            // IE 6-10 supports script readyState
            // IE 10+ support stack trace
            try {
              throw new Error();
            } catch (err) {
              // Find the second match for the "at" string to get file src url from stack.
              // Specifically works with the format of stack traces in IE.
              var i,
                res = (/.*at [^\(]*\((.*):.+:.+\)$/ig.exec(err.stack) || [false])[1];

              // For all scripts on the page, if src matches or if ready state is interactive, return the script tag
              for (i in scripts) {
                if (scripts[i].src == res || scripts[i].readyState == "interactive") {
                  return scripts[i];
                }
              }

              // If no match, return null
              return null;
            }
          }
        });
      }
    })(document);

    /***/
  },

  /***/"f751": /***/function f751(module, exports, __nested_webpack_require_450262__) {
    // 19.1.3.1 Object.assign(target, source)
    var $export = __nested_webpack_require_450262__("5ca1");
    $export($export.S + $export.F, 'Object', {
      assign: __nested_webpack_require_450262__("7333")
    });

    /***/
  },

  /***/"fa5b": /***/function fa5b(module, exports, __nested_webpack_require_450543__) {
    module.exports = __nested_webpack_require_450543__("5537")('native-function-to-string', Function.toString);

    /***/
  },

  /***/"fab2": /***/function fab2(module, exports, __nested_webpack_require_450732__) {
    var document = __nested_webpack_require_450732__("7726").document;
    module.exports = document && document.documentElement;

    /***/
  },

  /***/"fb15": /***/function fb15(module, __nested_webpack_exports__, __nested_webpack_require_450951__) {
    "use strict";

    // ESM COMPAT FLAG
    __nested_webpack_require_450951__.r(__nested_webpack_exports__);

    // EXPORTS
    __nested_webpack_require_450951__.d(__nested_webpack_exports__, "install", function () {
      return (/* reexport */components["d" /* install */]
      );
    });
    __nested_webpack_require_450951__.d(__nested_webpack_exports__, "GridLayout", function () {
      return (/* reexport */components["b" /* GridLayout */]
      );
    });
    __nested_webpack_require_450951__.d(__nested_webpack_exports__, "GridItem", function () {
      return (/* reexport */components["a" /* GridItem */]
      );
    });

    // CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
    // This file is imported into lib/wc client bundles.

    if (typeof window !== 'undefined') {
      if (true) {
        __nested_webpack_require_450951__("f6fd");
      }
      var i;
      if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
        __nested_webpack_require_450951__.p = i[1]; // eslint-disable-line
      }
    }

    // Indicate to webpack that this file can be concatenated
    /* harmony default export */
    var setPublicPath = null;

    // EXTERNAL MODULE: ./src/components/index.js
    var components = __nested_webpack_require_450951__("2af9");

    // CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js

    /* harmony default export */
    var entry_lib = __nested_webpack_exports__["default"] = components["c" /* default */];

    /***/
  },

  /***/"fca0": /***/function fca0(module, exports, __nested_webpack_require_452479__) {
    // 20.1.2.2 Number.isFinite(number)
    var $export = __nested_webpack_require_452479__("5ca1");
    var _isFinite = __nested_webpack_require_452479__("7726").isFinite;
    $export($export.S, 'Number', {
      isFinite: function isFinite(it) {
        return typeof it == 'number' && _isFinite(it);
      }
    });

    /***/
  },

  /***/"fdef": /***/function fdef(module, exports) {
    module.exports = '\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003' + '\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';

    /***/
  }

  /******/
})["default"];

/***/ }),

/***/ 3085:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  ZP: function() { return /* binding */ stock; },
  IS: function() { return /* reexport */ setter; },
  SB: function() { return /* reexport */ state; }
});

// UNUSED EXPORTS: action, getter

// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(8819);
;// CONCATENATED MODULE: ./src/service/stock/state.js
/**
 * @description 数据
*/


/* harmony default export */ var state = ({
  // 忽略目录
  ignoreDir: 'node_modules;npm-cache;.git;.cache;_cacache',
  // 目录类型
  directTypes: new components_js_.DEnum([{
    key: 'DIR',
    text: '系统目录'
  }, {
    key: 'DIR_SIMULATE',
    text: '模拟目录'
  }, {
    key: 'FILE',
    text: '文件'
  }]),
  // 启用与禁用状态
  enableStatus: new components_js_.OptionEnum([{
    key: 'ENABLE',
    id: 1,
    text: '启用'
  }, {
    key: 'DISABLE',
    id: 0,
    text: '禁用'
  }], {
    valueKey: 'id'
  }),
  // 状态列表
  statusList: [{
    id: 1,
    value: 1,
    text: '启用'
  }, {
    id: 0,
    calue: 0,
    text: '禁用'
  }],
  // 判断状态
  judgeOptions: new components_js_.OptionEnum([{
    key: 'TRUE',
    id: 1,
    text: '是'
  }, {
    key: 'FALSE',
    id: 0,
    text: '否'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 页面类型
  pageTypeList: new components_js_.OptionEnum([{
    key: 'NORMAL',
    id: 1,
    text: '普通'
  }, {
    key: 'RESOURCE_PAGE',
    id: 2,
    text: '引用'
  }, {
    key: 'ENVIRMENT_MATCH',
    id: 3,
    text: '环境匹配'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 页签风格
  tabTypeList: new components_js_.OptionEnum([{
    key: 'underline',
    text: '下划线'
  }, {
    key: 'beforeline',
    text: '上划线'
  }, {
    key: 'leftline',
    text: '左划线'
  }, {
    key: 'rightline',
    text: '右划线'
  }], {
    hasAll: false
  }),
  // 页签位置
  tabPositionList: new components_js_.OptionEnum([{
    key: 'top',
    text: '头部'
  }, {
    key: 'side-left',
    text: '左侧'
  }, {
    key: 'side-right',
    text: '右侧'
  }, {
    key: 'bottom',
    text: '底部'
  }], {
    hasAll: false
  }),
  // 文档分类类型
  archiveCatList: new components_js_.OptionEnum([{
    key: 'ARCHIVE',
    id: 1,
    value: 1,
    text: '一般文档'
  }, {
    key: 'CODEWELL',
    id: 2,
    value: 2,
    text: '编码库文档'
  }, {
    key: 'CHAPTER',
    id: 3,
    value: 3,
    text: '目录文档'
  }]),
  // 文档类型
  docTypeList: [{
    id: 1,
    value: 1,
    text: '使用文档'
  }, {
    id: 2,
    value: 2,
    text: '开发文档'
  }],
  // 数据源类型
  resourceTypeList: new components_js_.OptionEnum([{
    key: 'JAVASCRIPT',
    id: 'js',
    text: 'JavaScript'
  }, {
    key: 'CSS',
    id: 'css',
    text: 'CSS'
  }, {
    key: 'LESS',
    id: 'less',
    text: 'LESS'
  }, {
    key: 'TXT',
    id: 'txt',
    text: '文本'
  }
  // {key: 'COMPRESS', id: 1, text: '压缩包'},
  // {key: 'FILE', id: 2, text: '文件'},
  // {key: 'SERVICE', id: 4, text: '服务请求'}
  ], {
    hasAll: false,
    valueKey: 'id',
    enumValueKey: 'id'
  }),
  // 执行单元类型
  exeunitTypeList: new components_js_.OptionEnum([{
    key: 'RESOURCE',
    id: 1,
    text: '数据源'
  }, {
    key: 'COMPUTE',
    id: 2,
    text: '计算'
  }, {
    key: 'VIEW',
    id: 3,
    text: '视图'
  }, {
    key: 'STORE',
    id: 4,
    text: '存储'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 执行方式
  runTypeList: new components_js_.OptionEnum([{
    key: 'COMPONENT',
    id: 1,
    text: '组件'
  }, {
    key: 'CODE',
    id: 2,
    text: '自定义编码'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 接口采集匹配类型
  collectInterFieldsList: new components_js_.OptionEnum([{
    key: 'URL',
    id: 'url',
    text: '接口地址'
  }, {
    key: 'HREF',
    id: 'href',
    text: '页面地址'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 报错采集匹配类型
  collectErrorFieldsList: new components_js_.OptionEnum([{
    key: 'HREF',
    id: 'href',
    text: '页面地址'
  }, {
    key: 'NAME',
    id: 'name',
    text: '错误名称'
  }, {
    key: 'MESSAGE',
    id: 'message',
    text: '报错信息'
  }, {
    key: 'FILENAME',
    id: 'fileName',
    text: '文件名称'
  }, {
    key: 'URL',
    id: 'url',
    text: '文件地址'
  }, {
    key: 'STACK',
    id: 'stack',
    text: '错误栈'
  }, {
    key: 'TIME',
    id: 'time',
    text: '时间点'
  }, {
    key: 'PAGEPROTOCOL',
    id: 'pageProtocol',
    text: '协议类型'
  }, {
    key: 'PAGEHOST',
    id: 'pageHost',
    text: '主机地址'
  }, {
    key: 'PAGEPATHNAME',
    id: 'pagePathname',
    text: '路径'
  }, {
    key: 'USERAGENT',
    id: 'userAgent',
    text: '浏览器'
  }, {
    key: 'LANGUAGE',
    id: 'language',
    text: '语言'
  }, {
    key: 'PLATFORM',
    id: 'platform',
    text: '平台'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 性能采集匹配类型
  collectPerformanceFieldsList: new components_js_.OptionEnum([{
    key: 'HREF',
    id: 'href',
    text: '页面地址'
  }, {
    key: 'TIME',
    id: 'time',
    text: '时间点'
  }, {
    key: 'PAGEPROTOCOL',
    id: 'pageProtocol',
    text: '协议类型'
  }, {
    key: 'PAGEHOST',
    id: 'pageHost',
    text: '主机地址'
  }, {
    key: 'PAGEPATHNAME',
    id: 'pagePathname',
    text: '路径'
  }, {
    key: 'USERAGENT',
    id: 'userAgent',
    text: '浏览器'
  }, {
    key: 'LANGUAGE',
    id: 'language',
    text: '语言'
  }, {
    key: 'PLATFORM',
    id: 'platform',
    text: '平台'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 备份存储类型
  storeTypeList: new components_js_.OptionEnum([{
    key: 'data',
    id: 'data',
    text: '数据'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 流程可视组件列表
  visualList: [],
  // 自定义单元
  customUnit: {
    key: 'CUSTOM',
    id: '-1',
    name: '自定义单元',
    executor: "export default function (payload) {\n      return payload\n}",
    input: "export default function (payload) {\n  return payload\n}",
    output: "export default function (payload) {\n  return payload\n}"
  },
  codeLangTypeList: new components_js_.OptionEnum([{
    key: 'VUE',
    id: 'vue',
    text: 'VUE'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  registerTypeList: new components_js_.OptionEnum([{
    key: 'component',
    id: 'component',
    text: '组件'
  }, {
    key: 'module',
    id: 'module',
    text: '模块'
  }, {
    key: 'package',
    id: 'package',
    text: '资源包'
  }, {
    key: 'runtime',
    id: 'runtime',
    text: '运行环境'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 片段类型
  snippetTypeList: new components_js_.OptionEnum([{
    key: 'HTML',
    id: 'html',
    text: 'HTML'
  }, {
    key: 'VUE',
    id: 'vue',
    text: 'VUE'
  }, {
    key: 'SINGLEPAGE',
    id: 'singlepage',
    text: '独立页面'
  }], {
    hasAll: false,
    valueKey: 'id'
  }),
  // 布局列表
  layoutCollection: [],
  // 组件模板列表
  componentCollection: [],
  // 组件列表
  components: [],
  // 页面数据
  page: {
    meta: {
      name: ''
    }
  },
  // 布局渲染组件
  layoutRenderView: '',
  // 布局属性组件
  layoutPropertyView: '',
  // 布局组件属性
  layoutPropertyStock: {},
  // 选中的布局
  selectedLayout: {},
  // 选中的布局默认属性
  selectedLayoutDefProps: {},
  // 是否设计状态
  isDesign: false,
  // 是否对照状态
  isContrast: false,
  // 是否自动编译状态
  isCompile: true,
  // 属性是否全局状态
  isPropertyPanelFull: false,
  // 选中的组件
  selectedComponent: {}
});
;// CONCATENATED MODULE: ./src/service/stock/getter.js
/**
 * @description 数据读取
*/



/* harmony default export */ var getter = ({
  // 获取状态
  statusList() {
    let list = state.statusList;
    list = Array.isArray(list) ? list : [];
    list = [{
      id: '',
      value: '',
      text: '全部'
    }, ...list];
    return list;
  },
  // 数据源类型
  resourceTypeList() {
    let list = state.resourceTypeList;
    list = Array.isArray(list) ? list : [];
    list = [{
      id: '',
      value: '',
      text: '全部'
    }, ...list];
    return list;
  },
  // 执行单元类型
  exeunitTypeList() {
    let list = state.exeunitTypeList;
    list = Array.isArray(list) ? list : [];
    list = [{
      id: '',
      value: '',
      text: '全部'
    }, ...list];
    return list;
  },
  // 启用与禁用状态
  enableStatus(hasAll) {
    return new components_js_.OptionEnum([{
      key: 'ENABLE',
      id: 1,
      text: '启用'
    }, {
      key: 'DISABLE',
      id: 0,
      text: '禁用'
    }], {
      valueKey: 'id',
      hasAll
    });
  }
});
;// CONCATENATED MODULE: ./src/service/stock/setter.js
/**
 * @description 数据修改
*/



/* harmony default export */ var setter = ({
  // 设置布局集合
  setLayoutCollection(list) {
    list = Array.isArray(list) ? list : [];
    let collection = state.layoutCollection;
    list.forEach(item => {
      let node = collection.find(node => node.name === item.name && node.version === item.version);
      if (!node) {
        state.layoutCollection.push(item);
      } else {
        Object.assign(node, item);
      }
    });
  },
  // 设置组件集合
  setComponentCollection(list) {
    list = Array.isArray(list) ? list : [];
    let collection = state.componentCollection;
    list.forEach(item => {
      let node = collection.find(node => node.name === item.name && node.version === item.version);
      if (!node) {
        state.componentCollection.push(item);
      } else {
        Object.assign(node, item);
      }
    });
  },
  // 选中的属性组件
  selectPropertyView(view) {
    state.layoutPropertyView = view;
  },
  // 选中布局
  selectLayout(meta, defProps) {
    state.selectedLayout = meta;
    if (defProps) {
      state.selectedLayoutDefProps = defProps;
    } else {
      state.selectedLayoutDefProps = {};
    }
  },
  // 新增组件
  insertComponent(meta) {
    let props = meta.props || {};
    state.page.meta.childComponents.push({
      id: components_js_.strer.utid(),
      name: meta.name,
      version: meta.version,
      properts: props.properts || {},
      layout: Object.assign({
        x: 0,
        y: 0,
        w: 16,
        h: 16
      }, props.layout || {}),
      events: props.events || {},
      theme: props.theme || {},
      childComponents: []
    });
  },
  // 移除组件
  removeComponent(meta) {
    state.page.meta.childComponents = state.page.meta.childComponents.filter(item => {
      return item !== meta;
    });
  },
  // 选中组件
  selectComponent(meta) {
    state.selectedComponent = meta;
  }
});
;// CONCATENATED MODULE: ./src/service/stock/action.js
/**
 * @description 函数与动作
*/

/* harmony default export */ var action = ({
  objecter: {},
  dater: {},
  arrayer: {},
  strer: {},
  numer: {}
});
;// CONCATENATED MODULE: ./src/service/stock/index.js
/**
 * @description 数据
*/





/* harmony default export */ var stock = ({
  state: state,
  getter: getter,
  setter: setter,
  action: action
});


/***/ }),

/***/ 5670:
/***/ (function(__unused_webpack_module, __webpack_exports__) {

"use strict";
/* harmony default export */ __webpack_exports__.Z = ({
  layout: {
    columns: 144,
    rows: 135,
    referWidth: null,
    forceReferWidth: false,
    referHeight: 1080,
    isFixHeight: false,
    margin: {
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px'
    },
    padding: {
      top: '8px',
      right: '8px',
      bottom: '8px',
      left: '8px'
    },
    contrast: {
      isAble: true,
      bgColor: '#ffffff'
    }
  },
  theme: {
    bgColor: '',
    bgImage: '',
    bgSize: ''
  }
});

/***/ }),

/***/ 9293:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _absolute_constant__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5670);
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

/* harmony default export */ __webpack_exports__.Z = ({
  layout: _objectSpread(_objectSpread({}, _absolute_constant__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z.layout), {}, {
    rows: [{
      id: 1,
      rowHeight: 'auto'
    }, {
      id: 2,
      rowHeight: 'auto'
    }, {
      id: 3,
      rowHeight: 'auto'
    }],
    columns: [{
      id: 1,
      colWidth: 'auto'
    }, {
      id: 2,
      colWidth: 'auto'
    }, {
      id: 3,
      colWidth: 'auto'
    }],
    areas: [],
    rowGap: '8px',
    colGap: '8px'
  })
});

/***/ }),

/***/ 873:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _absolute_constant__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5670);
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

/* harmony default export */ __webpack_exports__.Z = ({
  layout: _objectSpread(_objectSpread({}, _absolute_constant__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z.layout), {}, {
    padding: {
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px'
    },
    contrast: {
      isAble: false,
      bgColor: '#ffffff'
    }
  })
});

/***/ }),

/***/ 6674:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _absolute_constant__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5670);
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

/* harmony default export */ __webpack_exports__.Z = ({
  layout: _objectSpread(_objectSpread({}, _absolute_constant__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z.layout), {}, {
    referWidth: 1920,
    forceReferWidth: true,
    contrast: {
      isAble: true,
      bgColor: '#000c3b'
    }
  })
});

/***/ }),

/***/ 5203:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(4987);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("0cdca450", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 1711:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(5766);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("8a43c9c8", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 3259:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(4448);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("449a750f", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 8689:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(5976);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("206ac758", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 7978:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2694);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("4883de11", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 3528:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(6383);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("d3ff60bc", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 3711:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2274);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("cc599364", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 9919:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(7749);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("f9dd24d2", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 3046:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(7067);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("ee0dad38", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 1658:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(499);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("7c6a44ce", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 4169:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(9533);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("5be540a5", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 5563:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(867);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("c2907a2e", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 9350:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(9842);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("02dbadcb", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 866:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(5844);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("e84fb75e", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 4196:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(3162);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("48c4e672", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 4160:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(627);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("a51c4fb8", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 2162:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2937);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("5585fb72", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 5747:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2299);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("640ee969", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 7849:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1841);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("a23ef51a", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 3483:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(7114);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("18d19936", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 5075:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2737);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(7037)/* ["default"] */ .Z)
var update = add("474a3674", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ 7037:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  Z: function() { return /* binding */ addStylesClient; }
});

;// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles(parentId, list) {
  var styles = [];
  var newStyles = {};
  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = item[0];
    var css = item[1];
    var media = item[2];
    var sourceMap = item[3];
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    };
    if (!newStyles[id]) {
      styles.push(newStyles[id] = {
        id: id,
        parts: [part]
      });
    } else {
      newStyles[id].parts.push(part);
    }
  }
  return styles;
}
;// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesClient.js
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 9717:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__9717__;

/***/ }),

/***/ 8819:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__8819__;

/***/ }),

/***/ 6470:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__6470__;

/***/ }),

/***/ 7443:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__7443__;

/***/ }),

/***/ 4236:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__4236__;

/***/ }),

/***/ 7792:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__7792__;

/***/ }),

/***/ 748:
/***/ (function(module) {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE__748__;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		__webpack_require__.p = "";
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
!function() {
"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ entry_lib; }
});

// UNUSED EXPORTS: stock

;// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
/* eslint-disable no-var */
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (false) { var getCurrentScript; }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/designer.vue?vue&type=template&id=235f6774&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "view-design d-layout-container d-layout-container-nofooter d-layout-container-dark",
    class: {
      'is-fullscreen': _vm.isFullscreen
    }
  }, [_c('div', {
    staticClass: "design-header d-layout-header",
    class: _vm.isShowActions ? 'is-actions-active' : ''
  }, [_c('a', {
    staticClass: "design-actions-switch",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function click($event) {
        _vm.isShowActions = !_vm.isShowActions;
      }
    }
  }, [_c('i', {
    staticClass: "el-icon-s-tools"
  })]), _c('div', {
    staticClass: "design-actions"
  }, [_vm._t("actions-before"), _vm.isShowLayout ? _c('el-select', {
    attrs: {
      "placeholder": "切换布局",
      "size": "mini"
    },
    on: {
      "change": _vm.handleChageLayout
    },
    model: {
      value: _vm.stock.state.page.meta.name,
      callback: function callback($$v) {
        _vm.$set(_vm.stock.state.page.meta, "name", $$v);
      },
      expression: "stock.state.page.meta.name"
    }
  }, _vm._l(_vm.stock.state.layoutCollection, function (item) {
    return _c('el-option', {
      key: item.name,
      attrs: {
        "label": item.text,
        "value": item.name
      }
    });
  }), 1) : _vm._e(), _vm.isShowFullscreen ? _c('el-button', {
    attrs: {
      "icon": "el-icon-full-screen",
      "size": "mini"
    },
    on: {
      "click": _vm.handleFullScreen
    }
  }, [_vm._v("全屏")]) : _vm._e(), _vm.isShowImport ? _c('FileChooser', {
    attrs: {
      "button": {
        icon: 'el-icon-upload2',
        size: 'mini'
      },
      "text": "导入",
      "accept": "\tapplication/json",
      "title": "导入页面配置元文件"
    },
    on: {
      "change": _vm.handleImport
    }
  }) : _vm._e(), _vm.isShowExport ? _c('el-button', {
    attrs: {
      "icon": "el-icon-download",
      "size": "mini",
      "title": "导出当前页面配置元文件"
    },
    on: {
      "click": _vm.handleExport
    }
  }, [_vm._v("导出")]) : _vm._e(), _c('el-button', {
    attrs: {
      "size": "mini",
      "title": "打开容器属性面板"
    },
    on: {
      "click": _vm.handleShowLayoutProperty
    }
  }, [_vm._v("容器属性")]), _vm.isShowComponent ? _c('Switcher', {
    attrs: {
      "labelOn": "组件",
      "labelOff": "组件",
      "size": "xs",
      "title": "打开或关闭组件面板"
    },
    model: {
      value: _vm.isExpandSide,
      callback: function callback($$v) {
        _vm.isExpandSide = $$v;
      },
      expression: "isExpandSide"
    }
  }) : _vm._e(), _vm.isShowProperty ? _c('Switcher', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.stock.state.layoutPropertyView,
      expression: "stock.state.layoutPropertyView"
    }],
    attrs: {
      "labelOn": "属性",
      "labelOff": "属性",
      "size": "xs",
      "title": "打开或关闭属性面板"
    },
    model: {
      value: _vm.isExpandProp,
      callback: function callback($$v) {
        _vm.isExpandProp = $$v;
      },
      expression: "isExpandProp"
    }
  }) : _vm._e(), _vm.isShowThrough ? _c('Switcher', {
    attrs: {
      "labelOn": "穿透",
      "labelOff": "穿透",
      "size": "xs",
      "title": "隐藏或显示组件实例遮罩层,打开可触发组件相应事件"
    },
    model: {
      value: _vm.isThrough,
      callback: function callback($$v) {
        _vm.isThrough = $$v;
      },
      expression: "isThrough"
    }
  }) : _vm._e(), _vm.isShowCompile && !_vm.isCompile ? _c('el-button', {
    attrs: {
      "icon": "el-icon-cpu",
      "size": "mini",
      "type": "primary",
      "title": "手动编译组件代码"
    },
    on: {
      "click": _vm.handleCompile
    }
  }, [_vm._v("编译")]) : _vm._e(), _vm.isShowCompile ? _c('Switcher', {
    attrs: {
      "labelOn": "编译",
      "labelOff": "编译",
      "size": "xs",
      "title": "自动编译,打开则修改代码时自动编译并渲染视图"
    },
    model: {
      value: _vm.isCompile,
      callback: function callback($$v) {
        _vm.isCompile = $$v;
      },
      expression: "isCompile"
    }
  }) : _vm._e(), _vm._t("actions-after")], 2), _vm._t("header")], 2), _c('div', {
    staticClass: "design-body d-bg-fog",
    class: {
      'design-body-through': _vm.isThrough
    }
  }, [_c('div', {
    staticClass: "design-side",
    class: {
      'design-side-collapse': !_vm.isExpandSide
    }
  }, [_c('DComponents', {
    attrs: {
      "stock": _vm.stock
    }
  })], 1), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.stock.state.layoutPropertyView,
      expression: "stock.state.layoutPropertyView"
    }],
    ref: "dpropertyContainer",
    staticClass: "design-property",
    class: {
      'design-property-collapse': !_vm.isExpandProp,
      'design-property-fullscreen': _vm.stock.state.isPropertyPanelFull
    }
  }, [_c('DProperty', {
    ref: "dproperty",
    attrs: {
      "stock": _vm.stock
    }
  }), _c('span', {
    staticClass: "design-collpase-bar",
    attrs: {
      "title": "拖拽改变属性面板宽度"
    },
    on: {
      "mousedown": _vm.handowDownCollapse,
      "touchstart": _vm.handowDownCollapse
    }
  }, [_c('MoreIcon', {
    staticClass: "bar-icon"
  })], 1)], 1), _c('div', {
    staticClass: "design-render d-layout-content"
  }, [_vm.isShowRender ? _c('DRender', {
    ref: "drender",
    attrs: {
      "stock": _vm.stock
    }
  }) : _vm._e()], 1)])]);
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(748);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);
// EXTERNAL MODULE: external "@daelui/vdog/dist/components.js"
var components_js_ = __webpack_require__(4236);
// EXTERNAL MODULE: external "@daelui/vdog/dist/icons.js"
var icons_js_ = __webpack_require__(7792);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var dist_components_js_ = __webpack_require__(8819);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/uploader/file-chooser.vue?vue&type=template&id=9c7dd2ce
var file_chooservue_type_template_id_9c7dd2ce_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "d-file-chooser"
  }, [_vm._t("button", function () {
    return [_c('el-button', _vm._b({
      on: {
        "click": _vm.handleChooseFile
      }
    }, 'el-button', _vm.$props.button, false), [_vm._v(_vm._s(_vm.text))])];
  }), _c('input', {
    ref: "file",
    staticClass: "d-uploader-file",
    attrs: {
      "type": "file",
      "accept": _vm.accept,
      "multiple": _vm.multiple
    },
    on: {
      "change": _vm.handleChange
    }
  })], 2);
};
var file_chooservue_type_template_id_9c7dd2ce_staticRenderFns = [];

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/uploader/file-chooser.vue?vue&type=script&lang=js
/* harmony default export */ var file_chooservue_type_script_lang_js = ({
  props: {
    name: {
      type: String,
      default() {
        return 'file';
      }
    },
    // 支持的文件
    accept: {
      type: String,
      default() {
        return '*';
      }
    },
    multiple: {
      type: Boolean,
      default() {
        return true;
      }
    },
    text: {
      type: String,
      default: '选择文件'
    },
    button: {
      type: Object,
      default() {
        return {};
      }
    }
  },
  data() {
    return {
      files: []
    };
  },
  methods: {
    // 选择文件
    handleChooseFile() {
      this.$refs.file.click();
    },
    // 文件修改
    handleChange(e) {
      let files = e.target.files;
      files = Array.from(files).map(file => {
        return {
          name: file.name,
          size: file.size,
          type: file.type,
          file: file,
          id: ''
        };
      });
      this.files = [...this.files, ...files];
      this.$emit('change', files, e);
    },
    // 文件移除
    handleRemove(file) {
      this.files = this.files.filter(item => {
        return item !== file;
      });
    },
    clear() {
      this.$refs.file.value = '';
      this.files = [];
    }
  }
});
;// CONCATENATED MODULE: ./src/components/uploader/file-chooser.vue?vue&type=script&lang=js
 /* harmony default export */ var uploader_file_chooservue_type_script_lang_js = (file_chooservue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/uploader/file-chooser.vue?vue&type=style&index=0&id=9c7dd2ce&prod&lang=less
var file_chooservue_type_style_index_0_id_9c7dd2ce_prod_lang_less = __webpack_require__(1711);
;// CONCATENATED MODULE: ./src/components/uploader/file-chooser.vue?vue&type=style&index=0&id=9c7dd2ce&prod&lang=less

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/components/uploader/file-chooser.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  uploader_file_chooservue_type_script_lang_js,
  file_chooservue_type_template_id_9c7dd2ce_render,
  file_chooservue_type_template_id_9c7dd2ce_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var file_chooser = (component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dcomponents.vue?vue&type=template&id=775d0abf&scoped=true
var dcomponentsvue_type_template_id_775d0abf_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "components-paner"
  }, [_c('el-tabs', {
    staticClass: "el-tabs-paner",
    model: {
      value: _vm.activeName,
      callback: function callback($$v) {
        _vm.activeName = $$v;
      },
      expression: "activeName"
    }
  }, [_c('el-tab-pane', {
    attrs: {
      "label": "组件",
      "name": "first"
    }
  }, [_c('el-collapse', {
    model: {
      value: _vm.activeNames,
      callback: function callback($$v) {
        _vm.activeNames = $$v;
      },
      expression: "activeNames"
    }
  }, _vm._l(_vm.assort(_vm.componentCollection), function (sec) {
    return _c('el-collapse-item', {
      key: sec.category,
      attrs: {
        "name": sec.category
      }
    }, [_c('template', {
      slot: "title"
    }, [_c('div', {
      staticClass: "component-type"
    }, [_vm._v(_vm._s(sec.category)), _c('span', {
      staticClass: "d-text-small d-text-lightgray"
    }, [_vm._v("(" + _vm._s(sec.list.length) + ")")])])]), _c('div', {
      staticClass: "list-components"
    }, _vm._l(sec.list, function (item) {
      return _c('div', {
        key: item.name + item.version,
        staticClass: "item-component d-text-linker",
        on: {
          "click": function click($event) {
            return _vm.handleSelectComponent(item);
          }
        }
      }, [_c('div', {
        staticClass: "component-name",
        attrs: {
          "title": item.text
        }
      }, [_c('span', {
        staticClass: "name-text"
      }, [_vm._v(_vm._s(item.text))]), item.description ? _c('el-tooltip', {
        staticClass: "item",
        attrs: {
          "effect": "light",
          "content": item.description,
          "placement": "bottom"
        }
      }, [_c('span', {
        staticClass: "name-desc"
      }, [_c('i', {
        staticClass: "el-icon-warning-outline"
      })])]) : _vm._e()], 1), _c('div', {
        staticClass: "component-icon"
      }, [_c('DIcon', {
        attrs: {
          "src": item.icon
        }
      })], 1)]);
    }), 0)], 2);
  }), 1)], 1)], 1)], 1);
};
var dcomponentsvue_type_template_id_775d0abf_scoped_true_staticRenderFns = [];

// EXTERNAL MODULE: ./src/service/stock/index.js + 4 modules
var stock = __webpack_require__(3085);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dicon.vue?vue&type=template&id=3b18a9b7&scoped=true
var diconvue_type_template_id_3b18a9b7_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _vm.url ? _c('img', {
    staticClass: "dicon-img",
    attrs: {
      "src": _vm.url
    }
  }) : _vm._e();
};
var diconvue_type_template_id_3b18a9b7_scoped_true_staticRenderFns = [];

;// CONCATENATED MODULE: ./src/assets/images/component-icon.png
var component_icon_namespaceObject = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDYuMC1jMDAyIDc5LjE2NDQ2MCwgMjAyMC8wNS8xMi0xNjowNDoxNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjNDREMzOTlEQjE4NzExRURCM0RFQTFDRjgyNjE4M0EzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjNDREMzOTlFQjE4NzExRURCM0RFQTFDRjgyNjE4M0EzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6M0NEQzM5OUJCMTg3MTFFREIzREVBMUNGODI2MTgzQTMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6M0NEQzM5OUNCMTg3MTFFREIzREVBMUNGODI2MTgzQTMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4yNg0LAAAOpUlEQVR42uydC3BU1RnHv293EzYJj4RHoRiQpEAegBIVMBAe0zIqVuvYqiO2toMPAlKwLEg7Y2egM9oHQqKolQRbZlpHrVYpY6ujg+WZ8BKhPJKAFFAQobwhj02ye7+eu6mjlhBw7+59/n8zywbC3nvud87vnPPdx1kWEQIAtI8PIQAAggAAQQCAIABAEAAgCAAQBAAIAgAEAQCCAAAgCAAQBAAIAgAEAQCCAABBAIAgAEAQACAIABAEAABBAIAgAEAQACAIADYgYMZOmPkrfx+4RLoGWsJ3sk+KlKPjSWS4O8LJZ9Ufa4i0TcK+7XWh4HtoYsnDjEUP2ZSdfEmQ/EXhm5nkGWLK80Ad/qV2Ttq9aMoQ5IoEyV/cNF/9tMBrFcnRaFHNvM470KSdJ0jArIMZ8lTTtzUPyhGrSH9gtXrLslOZSirlNvU2UfWQOcLUNflBoCb1xwE1pd65oZQrHdO5mWHhkKdbh4umbVQBCnq2tyN5tm5O+izLxVgm15NG81WJbrewGNtE+IWqafwHu48g5pzF0iIPelmOWE9EPHNoeXOBtaNG5H7S5AOL5dC5nlleHFspf7N7vZkiiBDnEaCIpo2ycEr1AInvTzbLIe4YUyELPS+ICkU+9NBHERppxX4nLJNs1Rp/Zc+YyGMlFTLfrnVmUpLO/Tr67eMTU10hwIpdEao5rnXUGiyZYrVqFFJyZts4QwuNqZS/Vk3lPR4V5PIU9va5QBD7lWlcpYzWRGbbPHRdWbSQen/Qo1MsYBVRkZBDJqAPqFFkEgQBpjG2MnKvmlr9wDE5mg1lhiAu5e7XxC/kn+2wYk9UCftUCAKSzrEzFCKRkc4ruYSKX5TuEAQkLzFfJjlCTsk9LiLPH9VCEAQkrw/WYg2sj3MPgEOjK+3xCAQEcV1i3jpeiH+ajG1npBL17kKUqyZA2ZlEWelEqf5kZOuU5hN7jCIBNCl3oVFgNlNibuLrrgQY2INoSB+mor6X/n9HzxN9cERo73+IDp9LmCX3lyyV1zdM47cgCEjM6LFUfqTf32R0O70yiMbnMo3LvbL/37cr0fcKmaiQaPcxouqPJfZufH4Ty6MsFQRTLJcwYbkENTaemE/KJwqNu3I5/p+hKvOZOopjL330MZaL0ISSCpkBQYBhoi2afr9VkZH8YvZYpkl5HPvZKLooM8YwFV9tVBIJjVomvSEIiJuSyvBgTTju0aN/FtFvJjHlJPjqg18ZO3k4x0YlAwl7bopmXcIOQVwAU6fZzBTXhKZLJ6K54zip5dNHpUE9DR3h7DFL5QYIAuJJzL+jEvNpcfXwqvZ/WMSmlHN6MVN2t7g/nsI+CkEQEEcNxp+Y3zRYf8zAnGIGVEu7Nd+AjCKT1SjyfQgCrjz3WBqZIkK3xvPZbkGi0QPY1PLqiftQA9f32YLbZyCIQxnzuxNdhHxxN5jRA5Qkncwv9+ir2YghY8ZURH8GQcDlKy6rp56YD4338yOy2ZJy6yNIbncjjvDs4hfkKggCOkrMCzUDDxfpeUfPDOvKP+ybhuTs7/ebl7BDEAcirD2qmljc54SG9WFLyz/U6IkB1TnonQQEARcn5vrKiMSGnrozcLo1Ieh3BBu9C1iY7oYg4OI5eNT4M+YZNlhlKcPoCQKRWyAIaAetv9EtdO5kA0GMSso0AIKAdqYWPqP3yFKKDWo91XgZsiAIaG8E+djoFhparT+KBJThCAQB7VXZZ0a3UN9s/VEYLgPTYQgC2mkXVEnMRw313i02GEEMl4FfhyDgItaX8mckZOgbmnYcFUuPYftRw3Ic5gBBENA+rc1Urt7iXgl93wlry7/nmDFBhbTy9Q/wCQgC2mXzLD5PrJXF+/nj9USHzlhX/v2nDH28uqrUX25axofm5kw2TA38Ub29E/fnD1ozzVp3gOh0o4HJFXOZmeV13LI/+unBk/VCLdHk7yszyLHF0QJ27Ub0xiLxfWXAlsNEI/oR5fUyNzFfe8CImPzq+qn8BgS5ZK9H9NpOM3s+UZIQ3X890aCebL94TOVVJRXRynjvzVqnGmteL/OOa50atU40xP3xqJ+pzOwYO2aKtXyrmCxHG2fDRM9WER27YM+4SNSnN5rT8Xx21zGi9/ebU846lVK/U2ekq5KytVN5KwRph8bWRJwaNMYrO8SWsal6hPeq5hN3z7pyj9Ce48ktY1jV3++rDcXvYErEV2ZFfB0hyKlG68ugrz9rV6INscazI97PV2wSWp2kkUQfeee9bfS0LpetmcHHIMglCPqtL0OajbO1jSFuIoNnd1aokeTlBI+S+qj/638a3SavrSrl56yKrSME0c8kdbb4GYac7vaOkUrY/0wGF3re9DHRss1Cew1egjvXrPKNvRLLG42iSbTcyrg64iyWfpr1psFMb+62Lg+YMoLtHygtWkY+/+1GNqEn7ruOCY3sRzQ2h+nqr3FT+ckGoq1HhKoPKUnCCTgBQfJS9bTASghyBUz4FlH/TKaVNRLLB5ojyd+n/v0Y2d2YHnLIN/1tmJ6ypqQi+ryalhheEV2/TrLlsFDfLkSFfYgKvtG2qLX+sFVGClGr1nZHrn5t48i5NqlqEpvsh/2aNYm5IwXRye3RtgI56ChjV43KL3epnxKyZuLRC22vVR+ZO3rrifm66bzd6nDiVhOXseERPsDEZc4+Ct4XDJItjgGCuJBG2qYnth84tfwiWvn7P+FTEAQkhW2lN7SSaE4dRd6vmuZfapfCQBC3TrWmBV5Rb286b/Sw1/QQgrgY1pyWi/Dyqmn8NgQBprB+OlephP1phxT3guYn2wkNQVyORmE9Yf/U/lMrKa9+iHfbrVwmXQfhBhWCS64nvmJXRL080FqFz5q9y6rStE9KlkbLiHmxjSNTE2afLaeDpggipNWooX7EJaNzXPNKf77WmoTdXza2Qu4SkmKb5h4LtpXyOTuWzJQplpKjFpOd2AjyoWVqBmiKTeW4Z0OpOWtc2VYQIToAO+hTTgtaJkjVg7w3onGO+rHaJvFoViPaTDvLEdNXJUfJ3wkzFSxqqlZDSbFn9RDfz2vndlpodTEKX5PUrDNaSI3q96m/DrOio1AtYhlT61vrS1MNdRimtF2zBMl/7nwPbk456Uk5mHfWhoLX2q1YE5ZLZksrDfBFI5lJ7x8kEE4ROpTIJwNdJUis91pYP1z8gdXq0DI9I4fQP2rnpt2GGWYypHOZIJ+Tv7hxiRriZ7rbCzrgI/lFzZz019GUIcjXEkRnaHlzQUTTRqnfjFS5SYErZlKaXBDmzeTjza3+hi37Z/U4j2YMQeISBAAnCIJbTQCAIABAEAAgCAAQBAAIAgAEAQCCAABBAIAgAAAIAgAEAQCCAABBAIAgANgQy75Ap2BheBAHuEhEiolluCuiGVsYTlurL++jr2BSM4PrzdjtwCWnuqZEM0aSJqNYZJT4uItL4rmDmTdKRLbXzgt+ZEURLHlgqmBR4yvqH+91eefzKYlvSbJXMilc3Hi3RvxbFeFcV0dT5NXauemTv/pPLnuiMP+pxmL28yoSSvfMGJ3EFU0KFjX9nZi+651YUqNEZWLdY+kbzRLEtByk8HnpzD6u9pQcbbV4TawhJ1qOsvC/PCVHLJaUHmtDbkzSJRx+wbOZnmrI+lQocSNH8zxdPK+Gs2Bx0xOuykFUbzdfTbIWePlsiL4MUCSlscjoSif6SKw6mzr141WejqfwLbVzOr3rihFEWZhPHkdPomNnmow2jKbwdV6X43+N6kb3TLGY8giQfho2AQ3jOgQy1nBNWefZnOsgzHnUwUyusLd7rld29F0n+jWKBDSN8dRBML0SS3GVIJc5c3XnsIArKvbJVS0dhyERF/BYMukyaePjE1MdH8s3dkYu98VKXd0zxQLAuVM5AAAEAQCCAABBAIAgAEAQACAIABAEAAgCAAQBAEAQACAIABAEAAgCAAQBAIIAAEEAgCAAQBAAIAgAAIIAAEEAgCAAQBAAIAgAEAQACAIABAEAggAAIAgAEAQAIwScVNgDp4hW1ggdPU/UHEn+/rqnE2V3Y3popPsqPtxKtGo/0ZbDQmeb2v7Nn6TuMqoRZaQSDelNNC6XqH8mQ5BEs+bfRG/uFlP3ebpRfwnNWkm05A52jRwNSo7frf5CjC835KTts0WXse318CiiYX0wxUoYEVVx7+0TS8uwfKu4RpBV+y6Ww0xe+tA5sXSEIGdUT17fYm0ZDp52z/Rq0yfW7r9JjWBHz0GQxM2Xo9aXoSniHkGabXAsDREIkjB6pFtfhr5d3SNIYW/ryzCwOwRJGOkpREV9rS3D5OHuSdLH5Vp7LJPymJghSEKZMoLpnmvMj2pmkGjmGKI+XVzjBw3uSfTj65h8FjTSmwcrQfKdEytHXQcpyVEjSTbTyXqhlqgZcjBlqeldwIWXU2/op7+YDp4WOmPCGa20ANOgXs6LZcBpFZuhplsZWUwgMeR0Z8pBGJw/xQIAggAAQQCAIABAEAAgCAAexjaneWuOa6gNxNKzghxSrwGX+uWTq1q8EW3hHYnZhkzoSI6a416IJ+9x0xSrBn2RqlLmjXbYhkt6m03uEYT5I1SoqtKIbLfDNtzR29BG1wjCJG+ot1Zv2yGv1s4LGu4oYttQ2/K4HGsuUPBlU3YlkvzHH9W0gPIXNzzM5Kv0aIU21obSMhK5yYKypgYSSvdiODUteu3exzrvNKPtmnaat25OxjJ1OAs8OXhEZaITtumIWKo2pMthWt9m1gjyOXlP1V/j8/ufUUc6wQP1+WTtnLRfJnMHBYubnlBvj3vAjL1C/Gjd3OC7X8xaxX2C6GSXSVoXCt+nDrpY/fZGdahDXFKN59WRbtTUi4Q3fbkyk0n+ovDNqiZvVNOBYlWbKqbkjgeEWT+lra0VjbdHUoMr9s/i819N61wiCABOBbeaAABBAIAgAEAQACAIABAEAAgCAAQBAIIAAEEAABAEAAgCAAQBAIIAAEEAgCAAQBAAIAgAEAQACAIAgCAAQBAAIAgAEAQAO/BfAQYADheeVpQ4MOsAAAAASUVORK5CYII=";
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dicon.vue?vue&type=script&lang=js

/* harmony default export */ var diconvue_type_script_lang_js = ({
  props: {
    src: {
      default: ''
    }
  },
  data() {
    return {
      url: component_icon_namespaceObject
    };
  },
  watch: {
    src() {
      this.syncUrl();
    }
  },
  methods: {
    // 同步地址
    syncUrl() {
      let img = document.createElement('img');
      img.onload = () => {
        this.url = this.src;
      };
      img.src = this.src;
    }
  },
  beforeMount() {
    this.syncUrl();
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/dicon.vue?vue&type=script&lang=js
 /* harmony default export */ var page_diconvue_type_script_lang_js = (diconvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-12.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dicon.vue?vue&type=style&index=0&id=3b18a9b7&prod&scoped=true&lang=css
var diconvue_type_style_index_0_id_3b18a9b7_prod_scoped_true_lang_css = __webpack_require__(5203);
;// CONCATENATED MODULE: ./src/views/dview/page/dicon.vue?vue&type=style&index=0&id=3b18a9b7&prod&scoped=true&lang=css

;// CONCATENATED MODULE: ./src/views/dview/page/dicon.vue



;


/* normalize component */

var dicon_component = (0,componentNormalizer/* default */.Z)(
  page_diconvue_type_script_lang_js,
  diconvue_type_template_id_3b18a9b7_scoped_true_render,
  diconvue_type_template_id_3b18a9b7_scoped_true_staticRenderFns,
  false,
  null,
  "3b18a9b7",
  null
  
)

/* harmony default export */ var dicon = (dicon_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dcomponents.vue?vue&type=script&lang=js



/* harmony default export */ var dcomponentsvue_type_script_lang_js = ({
  components: {
    DIcon: dicon
  },
  props: {
    stock: {
      type: Object,
      default() {
        return stock/* default */.ZP;
      }
    }
  },
  data() {
    return {
      componentCollection: this.stock.state.componentCollection,
      activeName: 'first',
      activeNames: ['1']
    };
  },
  watch: {},
  methods: {
    // 选择组件模板插入
    handleSelectComponent(component) {
      this.stock.setter.insertComponent(component);
    },
    assort(list) {
      list = Array.isArray(list) ? list : [];
      let map = new Map();
      list.forEach(item => {
        if (!item.category) {
          item.category = '组件';
        }
        let node = map.get(item.category);
        if (!node) {
          node = {
            category: item.category,
            order: item.order,
            list: []
          };
          map.set(item.category, node);
        }
        node.order = node.order < item.order ? item.order : node.order;
        node.list.push(item);
      });
      let res = Array.from(map.keys()).map(key => {
        return map.get(key);
      });
      res = res.sort((a, b) => {
        return dist_components_js_.arrayer.sort(a.order, b.order, 'desc', true);
      });
      return res;
    }
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/dcomponents.vue?vue&type=script&lang=js
 /* harmony default export */ var page_dcomponentsvue_type_script_lang_js = (dcomponentsvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dcomponents.vue?vue&type=style&index=0&id=775d0abf&prod&lang=less&scoped=true
var dcomponentsvue_type_style_index_0_id_775d0abf_prod_lang_less_scoped_true = __webpack_require__(2162);
;// CONCATENATED MODULE: ./src/views/dview/page/dcomponents.vue?vue&type=style&index=0&id=775d0abf&prod&lang=less&scoped=true

;// CONCATENATED MODULE: ./src/views/dview/page/dcomponents.vue



;


/* normalize component */

var dcomponents_component = (0,componentNormalizer/* default */.Z)(
  page_dcomponentsvue_type_script_lang_js,
  dcomponentsvue_type_template_id_775d0abf_scoped_true_render,
  dcomponentsvue_type_template_id_775d0abf_scoped_true_staticRenderFns,
  false,
  null,
  "775d0abf",
  null
  
)

/* harmony default export */ var dcomponents = (dcomponents_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/drender.vue?vue&type=template&id=302965f1
var drendervue_type_template_id_302965f1_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "render-paner"
  }, [_c('CPRender', {
    ref: "render",
    attrs: {
      "cpr": _vm.cpr,
      "properts": _vm.properts,
      "layout": _vm.layout,
      "events": _vm.events,
      "resource": _vm.resource,
      "theme": _vm.theme,
      "childComponents": _vm.childComponents,
      "value": _vm.properts
    }
  })], 1);
};
var drendervue_type_template_id_302965f1_staticRenderFns = [];

// EXTERNAL MODULE: ./src/views/dview/page/components/cprender.vue + 5 modules
var cprender = __webpack_require__(8735);
;// CONCATENATED MODULE: ./src/views/dview/page/components/layouts.js
/**
 * @description 布局组件列表
*/

const layoutCollection = [{
  id: 'ddesign-layout-absolute',
  name: 'ddesign-layout-absolute',
  text: '绝对布局',
  version: '1.0.0',
  icon: '',
  description: '',
  mainComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(9399)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  propertyComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(8760)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  main: '',
  property: ''
}, {
  id: 'ddesign-layout-grid',
  name: 'ddesign-layout-grid',
  text: '栅格布局',
  version: '1.0.0',
  icon: '',
  description: '',
  mainComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(9480)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  propertyComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(7032)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  main: '',
  property: ''
}, {
  id: 'ddesign-layout-screen',
  name: 'ddesign-layout-screen',
  text: '大屏布局',
  version: '1.0.0',
  icon: '',
  description: '',
  mainComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(2565)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  propertyComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(6592)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  main: '',
  property: ''
}, {
  id: 'ddesign-layout-fill',
  name: 'ddesign-layout-fill',
  text: '填充布局',
  version: '1.0.0',
  icon: '',
  description: '',
  mainComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(7494)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  propertyComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(6822)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  main: '',
  property: ''
}];
/* harmony default export */ var layouts = (layoutCollection);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/drender.vue?vue&type=script&lang=js





/* harmony default export */ var drendervue_type_script_lang_js = ({
  name: 'drender',
  components: {
    CPRender: cprender/* default */.Z
  },
  props: {
    stock: {
      type: Object,
      default() {
        return stock/* default */.ZP;
      }
    },
    page: {
      type: Object,
      data() {
        return {};
      }
    }
  },
  data() {
    return {
      cpr: {
        component: 'div',
        isProperty: false
      },
      properts: {},
      layout: {},
      events: {},
      resource: {},
      theme: {},
      childComponents: []
    };
  },
  watch: {
    'stock.state.page': {
      handler() {
        this.updateRender();
      },
      deep: true
    }
  },
  methods: {
    // 初始化
    init() {
      this.stock.setter.setLayoutCollection(layouts);
    },
    // 设置布局列表
    setLayoutCollection(list) {
      this.stock.setter.setLayoutCollection(list);
    },
    // 设置组件列表
    setComponentCollection(list) {
      this.stock.setter.setComponentCollection(list);
    },
    // 设置页面数据
    setPageState(page) {
      this.stock.state.page = page;
    },
    // 更新视图
    updateRender() {
      let page = this.stock.state.page;
      if (!page.id) {
        return false;
      }
      let meta = page.meta;
      meta.id = meta.id || dist_components_js_.strer.utid();
      this.cpr.name = meta.name || page.layout;
      this.cpr.version = meta.version;
      this.properts = meta.properts;
      this.layout = meta.layout;
      this.events = meta.events;
      this.resource = meta.resource;
      this.theme = meta.theme;
      this.childComponents = meta.childComponents;
    },
    // 获取元数据
    getMetaValue() {
      return this.$refs.render.$refs.view.getMetaValue();
    },
    /**
     * @function 更新视图
    */
    updateView() {
      return this.$refs.render.$refs.view.updateView();
    }
  },
  beforeCreate() {
    // 全局事件
    const vbus = new (external_vue_default())();
    (external_vue_default()).prototype.$bindEvent = (external_vue_default()).prototype.$bindEvent || function (name, callback) {
      vbus.$on(name, callback);
    };
    (external_vue_default()).prototype.$triggerEvent = (external_vue_default()).prototype.$triggerEvent || function (name, data) {
      vbus.$emit(name, data);
    };
    (external_vue_default()).prototype.$unbindEvent = (external_vue_default()).prototype.$unbindEvent || function (name, callback) {
      vbus.$off(name, callback);
    };
  },
  created() {
    // 初始化
    this.init();
    if (this.page && this.page.meta) {
      this.setPageState(this.page);
    }
  },
  mounted() {
    this.updateRender();
  },
  stock: stock/* default */.ZP
});

;// CONCATENATED MODULE: ./src/views/dview/page/drender.vue?vue&type=script&lang=js
 /* harmony default export */ var page_drendervue_type_script_lang_js = (drendervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/drender.vue?vue&type=style&index=0&id=302965f1&prod&lang=less
var drendervue_type_style_index_0_id_302965f1_prod_lang_less = __webpack_require__(5075);
;// CONCATENATED MODULE: ./src/views/dview/page/drender.vue?vue&type=style&index=0&id=302965f1&prod&lang=less

;// CONCATENATED MODULE: ./src/views/dview/page/drender.vue



;


/* normalize component */

var drender_component = (0,componentNormalizer/* default */.Z)(
  page_drendervue_type_script_lang_js,
  drendervue_type_template_id_302965f1_render,
  drendervue_type_template_id_302965f1_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var drender = (drender_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dproperty.vue?vue&type=template&id=2a71decf
var dpropertyvue_type_template_id_2a71decf_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "property-paner"
  }, [_c(_vm.component, {
    ref: "properts",
    tag: "component",
    attrs: {
      "stock": _vm.stock
    }
  }), _c('div', {
    staticClass: "paner-tools"
  }, [_c('span', {
    staticClass: "tool-item",
    attrs: {
      "title": "全屏"
    },
    on: {
      "click": _vm.handleFullScreen
    }
  }, [_c('FullScreenIcon', {
    staticClass: "d-text-linker"
  })], 1)])], 1);
};
var dpropertyvue_type_template_id_2a71decf_staticRenderFns = [];

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dproperty.vue?vue&type=script&lang=js


/* harmony default export */ var dpropertyvue_type_script_lang_js = ({
  components: {
    FullScreenIcon: icons_js_.FullScreenIcon
  },
  props: {
    stock: {
      type: Object,
      default() {
        return stock/* default */.ZP;
      }
    }
  },
  watch: {
    // 属性组件监听
    'stock.state.layoutPropertyView': function stockStateLayoutPropertyView() {
      this.component = stock/* default.state */.ZP.state.layoutPropertyView;
    }
  },
  data() {
    return {
      // 渲染组件
      component: stock/* default.state */.ZP.state.layoutPropertyView
    };
  },
  methods: {
    // 全局切换
    handleFullScreen() {
      stock/* default.state */.ZP.state.isPropertyPanelFull = !stock/* default.state */.ZP.state.isPropertyPanelFull;
    }
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/dproperty.vue?vue&type=script&lang=js
 /* harmony default export */ var page_dpropertyvue_type_script_lang_js = (dpropertyvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/dproperty.vue?vue&type=style&index=0&id=2a71decf&prod&lang=less
var dpropertyvue_type_style_index_0_id_2a71decf_prod_lang_less = __webpack_require__(3483);
;// CONCATENATED MODULE: ./src/views/dview/page/dproperty.vue?vue&type=style&index=0&id=2a71decf&prod&lang=less

;// CONCATENATED MODULE: ./src/views/dview/page/dproperty.vue



;


/* normalize component */

var dproperty_component = (0,componentNormalizer/* default */.Z)(
  page_dpropertyvue_type_script_lang_js,
  dpropertyvue_type_template_id_2a71decf_render,
  dpropertyvue_type_template_id_2a71decf_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var dproperty = (dproperty_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/designer.vue?vue&type=script&lang=js









/* harmony default export */ var designervue_type_script_lang_js = ({
  name: 'designer',
  components: {
    Switcher: components_js_.Switcher,
    MoreIcon: icons_js_.MoreIcon,
    FileChooser: file_chooser,
    DComponents: dcomponents,
    DRender: drender,
    DProperty: dproperty
  },
  props: {
    isShowLayout: {
      type: Boolean,
      default: false
    },
    isShowFullscreen: {
      type: Boolean,
      default: true
    },
    isShowImport: {
      type: Boolean,
      default: true
    },
    isShowExport: {
      type: Boolean,
      default: true
    },
    isShowComponent: {
      type: Boolean,
      default: true
    },
    isShowProperty: {
      type: Boolean,
      default: true
    },
    isShowThrough: {
      type: Boolean,
      default: true
    },
    isShowCompile: {
      type: Boolean,
      default: true
    },
    stock: {
      type: Object,
      default() {
        return stock/* default */.ZP;
      }
    }
  },
  computed: {
    isCompile: {
      get() {
        return stock/* default.state */.ZP.state.isCompile;
      },
      set(val) {
        let isCompile = val ? '1' : '0';
        if (window.sessionStorage) {
          sessionStorage.setItem('designer-is-compile', isCompile);
        }
        return stock/* default.state */.ZP.state.isCompile = val;
      }
    }
  },
  data() {
    return {
      activeIndex: '1',
      isShowActions: false,
      isFullscreen: false,
      isExpandSide: true,
      isExpandProp: true,
      isThrough: false,
      isShowRender: true
    };
  },
  watch: {
    isExpandSide() {
      this.updateView();
    },
    isExpandProp() {
      this.updateView();
    },
    isCompile() {
      this.$triggerEvent('update-is-compile', this.isCompile);
    }
  },
  methods: {
    // 初始化
    init() {
      // 进入设计态
      this.stock.state.isDesign = true;
      this.$bindEvent('init-is-compile', callback => {
        if (typeof callback === 'function') {
          callback(this.isCompile);
        }
      });
    },
    // 设置布局列表
    setLayoutCollection(list) {
      this.stock.setter.setLayoutCollection(list);
    },
    // 设置组件列表
    setComponentCollection(list) {
      this.stock.setter.setComponentCollection(list);
    },
    // 设置页面数据
    setPageState(page) {
      this.stock.state.page = page;
    },
    // 插入组件
    handleInsertComponent(component) {
      this.stock.setter.insertComponent(component);
    },
    /**
     * @function 获取元数据
    */
    getMetaValue() {
      return this.$refs.drender.getMetaValue();
    },
    handleSelect() {},
    /**
     * @function 更新视图
    */
    updateView() {
      this.$refs.drender.updateView();
    },
    // 全屏
    handleFullScreen() {
      if (document.documentElement.RequestFullScreen) {
        document.documentElement.RequestFullScreen();
      }
      // 兼容火狐
      if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      }
      // 兼容谷歌等可以webkitRequestFullScreen也可以webkitRequestFullscreen
      if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen();
      }
      // 兼容IE,只能写msRequestFullscreen
      if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      }
    },
    onFullscreen() {
      this.isFullscreen = !this.isFullscreen;
    },
    // 导入
    handleImport(files) {
      files = Array.isArray(files) ? files : [];
      let file = files[0];
      if (file) {
        dist_components_js_.filer.readFileText(file.file).then(text => {
          let data = dist_components_js_.parser.toObject(text, true) || {};
          if (data && data.meta) {
            this.stock.state.page = data;
          }
        }).catch(() => {
          this.$message({
            message: '解析失败',
            type: 'error',
            showClose: true
          });
        });
      } else {
        this.$message({
          message: '未读取到文件',
          type: 'warning',
          showClose: true
        });
      }
    },
    // 导出
    handleExport() {
      let page = this.stock.state.page;
      page.meta = this.getMetaValue();
      let content = dist_components_js_.parser.stringify(page, null, '  ');
      dist_components_js_.filer.downloadFile(this.trimSpecial(page.name || page.code || Date.now()) + '.json', content);
    },
    // 清除特殊字符
    trimSpecial(string) {
      //替换字符串中的所有特殊字符（包含空格）
      if (string !== '') {
        const pattern = /[`~!@#$^\-&*()=|{}':;',\\\[\]\.<>\/?~！@#￥……&*（）——|{}【】'；：""'。，、？\s]/g;
        string = string.replace(pattern, '');
      }
      return string;
    },
    // 修改布局
    handleChageLayout() {
      this.isShowRender = false;
      setTimeout(() => {
        this.isShowRender = true;
      }, 100);
    },
    // 显示容器属性
    handleShowLayoutProperty() {
      this.$refs.drender.$refs.render.$refs.view.handleSelectLayout();
      this.isExpandProp = true;
    },
    // 属性侧边栏伸缩
    handowDownCollapse(e) {
      mousedown(e, this.$refs.dpropertyContainer);
    },
    // 编译组件
    handleCompile() {
      this.$triggerEvent('compile-page-item');
    }
  },
  beforeCreate() {
    // 全局事件
    const vbus = new (external_vue_default())();
    (external_vue_default()).prototype.$bindEvent = (external_vue_default()).prototype.$bindEvent || function (name, callback) {
      vbus.$on(name, callback);
    };
    (external_vue_default()).prototype.$triggerEvent = (external_vue_default()).prototype.$triggerEvent || function (name, data) {
      vbus.$emit(name, data);
    };
    (external_vue_default()).prototype.$unbindEvent = (external_vue_default()).prototype.$unbindEvent || function (name, callback) {
      vbus.$off(name, callback);
    };
  },
  beforeMount() {
    if (window.sessionStorage) {
      let isCompile = sessionStorage.getItem('designer-is-compile');
      isCompile = isCompile !== '0' ? true : false;
      this.isCompile = isCompile;
    }
    this.init();
  },
  mounted() {
    this.triggerFullscreen = this.onFullscreen.bind(this);
    let events = ['fullscreenchange', 'webkitfullscreenchange', 'mozfullscreenchange'];
    events.forEach(item => {
      window.addEventListener(item, this.triggerFullscreen, false);
    });
  },
  beforeDestroy() {
    let events = ['fullscreenchange', 'webkitfullscreenchange', 'mozfullscreenchange'];
    events.forEach(item => {
      window.removeEventListener(item, this.triggerFullscreen);
    });
  },
  stock: stock/* default */.ZP
});


// 按下
function mousedown(e, el) {
  e = e.touches ? e.touches[0] : e;
  let elStyle = getComputedStyle(el);
  let bodyStyle = getComputedStyle(document.body);
  document.collaseActive = {
    e,
    el,
    elStyle: {
      width: parseFloat(elStyle.width)
    },
    bodyStyle: {
      width: parseFloat(bodyStyle.width)
    }
  };
  document.addEventListener('mousemove', mousemove, false);
  document.addEventListener('touchmove', mousemove, false);
  document.addEventListener('mouseup', mouseup, false);
  document.addEventListener('touchend', mouseup, false);
}

// 移动
function mousemove(e) {
  e = e.touches ? e.touches[0] : e;
  let act = document.collaseActive;
  if (act && act.e && act.el) {
    let lastX = act.e.clientX;
    let currentX = e.clientX;
    let width = act.elStyle.width;
    width = width - (currentX - lastX);
    let min = 24;
    let max = act.bodyStyle.width - 168;
    if (width < min) {
      width = min;
    }
    // 大于左侧加min * 2
    else if (width > max) {
      width = max;
    }
    act.el.style.width = width + 'px';
  }
}

// 按起
function mouseup() {
  document.collaseActive = null;
  document.removeEventListener('mousemove', mousemove);
  document.removeEventListener('mouseup', mouseup);
}
;// CONCATENATED MODULE: ./src/views/dview/page/designer.vue?vue&type=script&lang=js
 /* harmony default export */ var page_designervue_type_script_lang_js = (designervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/designer.vue?vue&type=style&index=0&id=235f6774&prod&lang=less&scoped=true
var designervue_type_style_index_0_id_235f6774_prod_lang_less_scoped_true = __webpack_require__(5747);
;// CONCATENATED MODULE: ./src/views/dview/page/designer.vue?vue&type=style&index=0&id=235f6774&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/vue-style-loader/index.js??clonedRuleSet-32.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-32.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[2]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-32.use[3]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-32.use[4]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/designer.vue?vue&type=style&index=1&id=235f6774&prod&lang=less
var designervue_type_style_index_1_id_235f6774_prod_lang_less = __webpack_require__(7849);
;// CONCATENATED MODULE: ./src/views/dview/page/designer.vue?vue&type=style&index=1&id=235f6774&prod&lang=less

;// CONCATENATED MODULE: ./src/views/dview/page/designer.vue



;



/* normalize component */

var designer_component = (0,componentNormalizer/* default */.Z)(
  page_designervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "235f6774",
  null
  
)

/* harmony default export */ var designer = (designer_component.exports);
;// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = (designer);


}();
__webpack_exports__ = __webpack_exports__["default"];
/******/ 	return __webpack_exports__;
/******/ })()
;
});