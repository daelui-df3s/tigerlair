(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ 565:
/***/ (function(module) {

function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
!function (e, t) {
  if (true) module.exports = t();else { var o, r; }
}(this, () => (() => {
  "use strict";

  var e = {
    d: (t, r) => {
      for (var o in r) e.o(r, o) && !e.o(t, o) && Object.defineProperty(t, o, {
        enumerable: !0,
        get: r[o]
      });
    }
  };
  e.g = function () {
    if ("object" == typeof globalThis) return globalThis;
    try {
      return this || new Function("return this")();
    } catch (e) {
      if ("object" == typeof window) return window;
    }
  }(), e.o = (e, t) => Object.prototype.hasOwnProperty.call(e, t), e.r = e => {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
      value: "Module"
    }), Object.defineProperty(e, "__esModule", {
      value: !0
    });
  };
  var t = {};
  e.r(t), e.d(t, {
    default: () => i,
    pig: () => n
  });
  class r {
    constructor() {
      this._$events = {};
    }
    $on(e, t) {
      if ("function" != typeof t) throw new TypeError('Second argument for "on" method must be a function.');
      return (this._$events[e] = this._$events[e] || []).push(t), this;
    }
    $once(e, t) {
      return t._one = !0, this.$on(e, t);
    }
    $only(e, t) {
      return this._$events[e] = [], this.$on(e, t);
    }
    $emit(e, t) {
      let r = this._$events;
      if (!r[e] || !r[e].length) return this;
      t = [].slice.call(arguments, 1);
      let o = [];
      return r[e].forEach(function (n, i) {
        n && (o.push(n.apply(n, t)), n._one && r[e].splice(i, 1));
      }), o;
    }
    $off(e, t) {
      let r = this._$events;
      if ("*" === e) return this._$events = {}, r;
      if (!r[e]) return !1;
      if (t) {
        if ("function" != typeof t) throw new TypeError('Second argument for "off" method must be a function.');
        r[e] = r[e].map(function (o, n) {
          if (o === t) return r[e].splice(n, 1), !1;
        });
      } else delete r[e];
    }
  }
  const o = "undefined" != typeof self ? self : e.g;
  class n extends r {}
  n.init = function (e) {
    if (e = e || {}, this.options = e, o.SystemLoader) this.loadRegistry(), this.defer = Promise.resolve(!0);else {
      if (this.defer) return this.defer;
      this.defer = this.loadSource(_objectSpread({
        path: "@daelui/pigjs/latest/dist/system.min.js"
      }, e)).then(() => {
        this.loader = o.SystemLoader;
        let e = this.loadRegistry();
        return e.then(() => {
          this.registryComponents();
        }), e;
      });
    }
    return this.defer;
  }, n.config = function (e) {
    e = e || {}, this.options = e;
  }, n.loadRegistry = async function () {
    if (this.isRegisted) return !0;
    this.isRegisted = !0;
    let e = [];
    await fetch(this.options.registry).then(async t => {
      e = await t.json();
    }), e = Array.isArray(e) ? e : [], this.registryMap = e;
  }, n.registryComponents = function (e) {
    e = e || this.registryMap, e = Array.isArray(e) ? e : [];
    let t = this.options || {},
      r = t.resolutions || t.versions;
    r = Array.isArray(r) ? r : [];
    let o = new Map();
    (e = e.sort((e, t) => (e = Number("0." + String(e.version).split(".").join(""))) > (t = Number("0." + String(t.version).split(".").join(""))) ? -1 : e < t ? 1 : 0)).forEach(e => {
      let t = o.get(e.name);
      t || (o.set(e.name, []), t = o.get(e.name)), t.push(e);
    });
    let i = {},
      s = {};
    Array.from(o.keys()).forEach(e => {
      let a = o.get(e),
        l = a[0],
        p = r.find(e => e.name === l.name);
      if (p && p.version) {
        let e = a.map((e, t) => {
          let r = String(e.version || "");
          r = r.split(".").map(e => parseInt(e));
          let o = String(p.version || "");
          o = o.split(".");
          let n = 1;
          return "*" !== o[0] && (o[0] = parseInt(o[0]) || 0, n += 1e11 + (r[0] === o[0] ? 1e11 : r[0] - o[0]), "*" !== o[1] && (o[1] = parseInt(o[1]) || 0, n += 1e9 + (r[1] === o[1] ? 1e9 : r[1] - o[1]), "*" !== o[2] && (o[2] = parseInt(o[2]) || 0, n += 1e7 + (r[2] === o[2] ? 1e7 : r[2] - o[2])))), {
            i: t,
            count: n
          };
        });
        e = e.sort((e, t) => e.count > t.count ? -1 : e.count < t.count ? 1 : 0), l = a[e[0].i];
      }
      let c = n.getPath(l, t);
      i[l.name] = c, Array.isArray(l.deps) && l.deps.length && (l.deps = l.deps.filter(e => "string" == typeof e), s[c] = l.deps);
    }), this.addComponentsMap({
      map: i,
      deps: s
    });
  }, n.addComponentsMap = function (e) {
    o.SystemLoader.addImportMap({
      imports: e.map,
      depcache: e.deps
    });
  }, n.loadSource = function (e) {
    return "string" == typeof e && (e = {
      path: e
    }), e = e || {}, e = _objectSpread(_objectSpread({}, this.options), e), new Promise((t, r) => {
      this.loadScript(n.getPath(e), e => {
        e ? t(this.options) : r(this.options);
      });
    });
  }, n.getPath = function (e, t) {
    const r = n.getRootUrl(e, t);
    let o = /^(https?:)?\/\//.test(e.path) ? e.path : r + "/" + e.path,
      i = e.reps;
    return i = Array.isArray(i) ? i : [], i.forEach(e => {
      let t = !1;
      if (/^\/.+\/([igm]+)?$/.test(e.tes)) try {
        t = new Function("return " + e.tes)().test(o);
      } catch (e) {
        t = !1;
      } else o.indexOf(e.tes) > -1 && (t = !0);
      if (!t) return !0;
      if (/^\/.+\/([igm]+)?$/.test(e.mat)) try {
        let t = new Function("return " + e.mat)();
        o = o.replace(t, e.rep);
      } catch (e) {} else o = o.replace(e.mat, e.rep);
    }), o = /^\/\//i.test(o) ? location.protocol + o : o, o = o.replace(/([^:\/]+)\/+/gi, "$1/"), o;
  }, n.getRootUrl = function (e, t) {
    t = t || {};
    let r = e.rootUrl || e.baseUrl || e.root;
    if (!r) {
      let t = e.rootUrls;
      Array.isArray(t) && t.length && (t = t.filter(e => !!e), r = t[Math.floor(Math.random() * t.length)]);
    }
    if (r || (r = t.rootUrl || t.baseUrl || t.root), !r) {
      let e = t.rootUrls;
      Array.isArray(e) && e.length && (e = e.filter(e => !!e), r = e[Math.floor(Math.random() * e.length)]);
    }
    return r = r || location.origin, r = /^(https?:)?\/\//.test(r) ? r : location.origin + "/" + r, r;
  }, n.loadScript = function (e, t) {
    var r = document.createElement("script");
    r.setAttribute("defer", !0), r.setAttribute("async", !0), "onload" in r ? (r.onload = t, r.onerror = function () {
      t(!1, this.options);
    }) : r.onreadystatechange = function () {
      (/loaded|complete/i.test(r.readyState) || r.complete) && t(!0, this.options);
    }, r.setAttribute("src", e), this.getHead().appendChild(r);
  }, n.loadStyle = function (e, t) {
    var r = document.createElement("link");
    r.setAttribute("rel", "stylesheet"), r.setAttribute("type", "text/css"), "onload" in r ? (r.onload = t, r.onerror = function () {
      t(!1);
    }) : r.onreadystatechange = function () {
      (/loaded|complete/i.test(r.readyState) || r.complete) && t();
    }, r.setAttribute("href", e), this.getHead().appendChild(r);
  }, n.getHead = function () {
    return document.head || document.getElementsByTagName("head")[0] || document.documentElement;
  }, n.import = function () {
    return o.SystemLoader ? o.SystemLoader.import.apply(o.SystemLoader, arguments) : Promise.resolve({
      msg: "no loader"
    });
  }, n.addSourceMap = function (e) {
    o.SystemLoader.addImportMap({
      imports: e.map,
      depcache: e.deps
    });
  }, n._register = function (e) {
    const t = {};
    Object.keys(e).forEach(r => {
      t[r] = e[r];
    });
    const r = document.createElement("script");
    return r.type = "systemjs-importmap", r.textContent = JSON.stringify({
      imports: t
    }, null, 3), document.head.appendChild(r), o.SystemLoader.prepareImport().then(() => {
      const t = this.getBaseUrl();
      return Object.keys(e).forEach(r => {
        o.SystemLoader.set((t + "/").replace(/\/+$/, "/") + r, e[r]);
      }), this;
    });
  }, n.load = function (e) {
    return o.SystemLoader.import(e);
  }, n.set = function () {
    return o.SystemLoader.set.apply(o.SystemLoader, arguments);
  }, n.get = function () {
    return o.SystemLoader.get.apply(o.SystemLoader, arguments);
  }, n.getBaseUrl = function () {
    let e;
    const t = document.querySelector("base[href]");
    if (t && (e = t.href), !e && "undefined" != typeof location) {
      e = location.href.split("#")[0].split("?")[0];
      const t = e.lastIndexOf("/");
      -1 !== t && (e = e.slice(0, t + 1));
    }
    return e;
  }, n.define = function () {
    ("undefined" != typeof self ? self : e.g).define.apply(this, arguments);
  }, n = o.$pig && o.$pig.import ? o.$pig : n, o.$pig = n;
  const i = n;
  return t;
})());

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		__webpack_require__.p = "";
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
!function() {
"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

;// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
/* eslint-disable no-var */
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (false) { var getCurrentScript; }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: ./node_modules/@daelui/pigjs/dist/index.js
var dist = __webpack_require__(565);
var dist_default = /*#__PURE__*/__webpack_require__.n(dist);
;// CONCATENATED MODULE: ./src/views/mapp/deploy/mapp.js


// 部署
function deploy(config) {
  // 初始化模块配置
  const loader = config.loader || {};
  const domain = config.domain || location.origin;
  const rootUrl = loader.rootUrl || location.origin;
  const meta = config.meta || {};
  dist_default().init({
    path: loader.path || '@daelui/pigjs/latest/dist/system.min.js',
    registry: loader.registry || domain + '/packages.json',
    rootUrl: rootUrl,
    rootUrls: loader.rootUrls,
    resolutions: [{
      name: 'vue',
      version: '2.*.*'
    }]
  }).then(function () {
    // 加载元数据
    return dist_default()["import"](meta.app || '/app.meta.js').then(res => {
      let app = res.default;
      let appName = app.name;
      let rootPath = app.path;
      let routes = app.meta;
      return {
        rootPath,
        routes,
        appName
      };
    });
  }).then(_ref => {
    let {
      rootPath,
      routes,
      appName
    } = _ref;
    dist_default()["import"]('./napp.umd.js').then(res => {
      const {
        bootstrap
      } = res;
      bootstrap({
        appName: appName,
        routes: routes,
        rootPath: rootPath
      });
    });
  });

  // 可替换标识符
  (dist_default()).identifyMap = (dist_default()).identifyMap || {};
  let identifyMap = config.identifyMap || {};
  (dist_default()).identifyMap = Object.assign((dist_default()).identifyMap, identifyMap);
  const rules = config.rules || [];
  const host = rules.find(item => item.rule === 'host');
  if (host) {
    (dist_default()).identifyMap.host = host.url;
  }
}
(dist_default()).deploy = deploy;
;// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib-no-default.js



}();
/******/ 	return __webpack_exports__;
/******/ })()
;
});