/**
 * @description 应用配置
*/

/* 运行模式配置 */
(function () {
  let origin = location.origin
  let domain = /daelui.com/.test(origin) ? '//www.daelui.com' : '//www.daelui.cn'
  // 开发环境配置
  const dev = {
    mode: 'dev',
    title: '开发模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: '//127.0.0.1:3601'}
    ],
    // 环境域名
    domain,
    // 模块加载器
    loader: {
      path: '@daelui/pigjs/latest/dist/system.min.js',
      registry: domain + '/pool/prod/packages.json',
      rootUrl: domain + '/pool/prod/packages'
    },
    // 替换标识符
    identifyMap: {},
    // 元数据
    meta: {
      app: './app.meta.js'
    }
  }

  // 生产环境配置
  const prod = {
    mode: 'prod',
    title: '生产模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: '//www.daelui.cn'}
    ],
    // 环境域名
    domain,
    // 模块加载器
    loader: {
      path: '@daelui/pigjs/latest/dist/system.min.js',
      registry: domain + '/pool/prod/packages.json',
      rootUrl: domain + '/pool/prod/packages'
    },
    // 替换标识符
    identifyMap: {},
    // 元数据
    meta: {
      app: './app.meta.js'
    }
  }

  /* 应用配置 */
  const application = {
    dev, prod,
    // 运行模式配置
    mode: {dev, prod}.prod
  }

  window.application = window.application || {}
  window.application.app = application
  window.application.tigerlair = application
})();