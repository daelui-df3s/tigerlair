(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@daelui/dogjs/dist/components.js"), require("@daelui/dogjs/dist/core.js"), require("@daelui/dogjs/dist/index.js"), require("@daelui/dogjs/dist/libs/coder.js"), require("@daelui/dogui/dist/css/boost.min.css"), require("@daelui/dogui/dist/css/theme/black.min.css"), require("@daelui/vdog/dist/coder.js"), require("@daelui/vdog/dist/directives.js"), require("@daelui/vdog/dist/pages.js"), require("babel.min.js"), require("element-ui"), require("element-ui/lib/theme-chalk/index.css"), require("vue"), require("vue-router"), require("vuex"));
	else if(typeof define === 'function' && define.amd)
		define(["@daelui/dogjs/dist/components.js", "@daelui/dogjs/dist/core.js", "@daelui/dogjs/dist/index.js", "@daelui/dogjs/dist/libs/coder.js", "@daelui/dogui/dist/css/boost.min.css", "@daelui/dogui/dist/css/theme/black.min.css", "@daelui/vdog/dist/coder.js", "@daelui/vdog/dist/directives.js", "@daelui/vdog/dist/pages.js", "babel.min.js", "element-ui", "element-ui/lib/theme-chalk/index.css", "vue", "vue-router", "vuex"], factory);
	else if(typeof exports === 'object')
		exports["napp"] = factory(require("@daelui/dogjs/dist/components.js"), require("@daelui/dogjs/dist/core.js"), require("@daelui/dogjs/dist/index.js"), require("@daelui/dogjs/dist/libs/coder.js"), require("@daelui/dogui/dist/css/boost.min.css"), require("@daelui/dogui/dist/css/theme/black.min.css"), require("@daelui/vdog/dist/coder.js"), require("@daelui/vdog/dist/directives.js"), require("@daelui/vdog/dist/pages.js"), require("babel.min.js"), require("element-ui"), require("element-ui/lib/theme-chalk/index.css"), require("vue"), require("vue-router"), require("vuex"));
	else
		root["napp"] = factory(root["@daelui/dogjs/dist/components.js"], root["@daelui/dogjs/dist/core.js"], root["@daelui/dogjs/dist/index.js"], root["@daelui/dogjs/dist/libs/coder.js"], root["@daelui/dogui/dist/css/boost.min.css"], root["@daelui/dogui/dist/css/theme/black.min.css"], root["@daelui/vdog/dist/coder.js"], root["@daelui/vdog/dist/directives.js"], root["@daelui/vdog/dist/pages.js"], root["babel.min.js"], root["element-ui"], root["element-ui/lib/theme-chalk/index.css"], root["vue"], root["vue-router"], root["vuex"]);
})((typeof self !== 'undefined' ? self : this), function(__WEBPACK_EXTERNAL_MODULE__819__, __WEBPACK_EXTERNAL_MODULE__854__, __WEBPACK_EXTERNAL_MODULE__68__, __WEBPACK_EXTERNAL_MODULE__470__, __WEBPACK_EXTERNAL_MODULE__759__, __WEBPACK_EXTERNAL_MODULE__785__, __WEBPACK_EXTERNAL_MODULE__443__, __WEBPACK_EXTERNAL_MODULE__313__, __WEBPACK_EXTERNAL_MODULE__722__, __WEBPACK_EXTERNAL_MODULE__924__, __WEBPACK_EXTERNAL_MODULE__10__, __WEBPACK_EXTERNAL_MODULE__410__, __WEBPACK_EXTERNAL_MODULE__748__, __WEBPACK_EXTERNAL_MODULE__557__, __WEBPACK_EXTERNAL_MODULE__342__) {
return /******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 276:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ screen; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/screen/index.vue?vue&type=template&id=bb8b1cf0
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c(_vm.component, {
    tag: "component"
  });
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(748);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);
// EXTERNAL MODULE: external "babel.min.js"
var external_babel_min_js_ = __webpack_require__(924);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/libs/coder.js"
var coder_js_ = __webpack_require__(470);
var coder_js_default = /*#__PURE__*/__webpack_require__.n(coder_js_);
// EXTERNAL MODULE: ./src/views/dview/page/components/screen/default.js
var screen_default = __webpack_require__(120);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/screen/index.vue?vue&type=script&lang=js




const win = typeof self !== 'undefined' ? window : __webpack_require__.g;
win.Babel = external_babel_min_js_;
/* harmony default export */ var screenvue_type_script_lang_js = ({
  props: {
    value: {
      type: Object,
      default() {
        return {
          code: screen_default/* default */.Z.code
        };
      }
    }
  },
  data() {
    return {
      component: 'div',
      isCompile: true
    };
  },
  watch: {
    value: {
      handler() {
        if (this.isCompile === false || String(this.isCompile) === '0') {
          return true;
        }
        this.compile();
      },
      deep: true
    }
  },
  methods: {
    compile() {
      coder_js_default().vue.resolve(this.value.code || screen_default/* default */.Z.code, (external_vue_default()), $pig).then(cp => {
        this.component = cp;
      });
    }
  },
  created() {
    this.compile();
    this.emiter = () => {
      this.compile();
    };
    // 初始化编译状态
    this.$triggerEvent('init-is-compile', value => {
      this.isCompile = value;
    });
  },
  mounted() {
    this.$bindEvent('compile-page-item', this.emiter);
    this.$bindEvent('update-is-compile', value => {
      this.isCompile = value;
    });
  },
  beforeDestroy() {
    this.$unbindEvent('compile-page-item', this.emiter);
    this.$unbindEvent('update-is-compile');
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/screen/index.vue?vue&type=script&lang=js
 /* harmony default export */ var components_screenvue_type_script_lang_js = (screenvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);
;// CONCATENATED MODULE: ./src/views/dview/page/components/screen/index.vue





/* normalize component */
;
var component = (0,componentNormalizer/* default */.Z)(
  components_screenvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var screen = (component.exports);

/***/ }),

/***/ 351:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/screen/property.vue?vue&type=template&id=bbcf84b6&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "component-property"
  }, [_c('el-tabs', {
    model: {
      value: _vm.activeName,
      callback: function callback($$v) {
        _vm.activeName = $$v;
      },
      expression: "activeName"
    }
  }, [_c('el-tab-pane', {
    attrs: {
      "label": "编码",
      "name": "first"
    }
  }, [_c('Coder', {
    ref: "coder",
    attrs: {
      "height": '480px',
      "language": "html"
    },
    model: {
      value: _vm.state.code,
      callback: function callback($$v) {
        _vm.$set(_vm.state, "code", $$v);
      },
      expression: "state.code"
    }
  })], 1)], 1)], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/element-resize-detector/src/element-resize-detector.js
var element_resize_detector = __webpack_require__(498);
var element_resize_detector_default = /*#__PURE__*/__webpack_require__.n(element_resize_detector);
// EXTERNAL MODULE: external "@daelui/vdog/dist/coder.js"
var coder_js_ = __webpack_require__(443);
// EXTERNAL MODULE: ./src/views/dview/page/components/screen/default.js
var screen_default = __webpack_require__(120);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/screen/property.vue?vue&type=script&lang=js



/* harmony default export */ var propertyvue_type_script_lang_js = ({
  components: {
    Coder: coder_js_.Coder
  },
  props: {
    value: {
      type: Object,
      default() {
        return {
          code: screen_default/* default */.Z.code
        };
      }
    }
  },
  data() {
    return {
      activeName: 'first',
      state: {
        code: screen_default/* default */.Z.code
      }
    };
  },
  watch: {
    value: {
      handler() {
        this.syncState();
      },
      deep: true
    },
    'state.code': {
      handler() {
        this.syncValue();
      },
      deep: true
    }
  },
  methods: {
    syncState() {
      Object.assign(this.state, this.value);
    },
    syncValue() {
      Object.assign(this.value, this.state);
      this.$emit('update', this.state);
    }
  },
  mounted() {
    this.syncState();
    //监听元素变化
    let erd = element_resize_detector_default()();
    erd.listenTo(this.$el, () => {
      let monacoEditor = this.$refs.coder.monacoEditor;
      monacoEditor && monacoEditor.layout();
    });
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/screen/property.vue?vue&type=script&lang=js
 /* harmony default export */ var screen_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./node_modules/mini-css-extract-plugin/dist/loader.js??clonedRuleSet-74.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-74.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-74.use[2]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-74.use[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/screen/property.vue?vue&type=style&index=0&id=bbcf84b6&prod&lang=less&scoped=true
// extracted by mini-css-extract-plugin

;// CONCATENATED MODULE: ./src/views/dview/page/components/screen/property.vue?vue&type=style&index=0&id=bbcf84b6&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);
;// CONCATENATED MODULE: ./src/views/dview/page/components/screen/property.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  screen_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "bbcf84b6",
  null
  
)

/* harmony default export */ var property = (component.exports);

/***/ }),

/***/ 641:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ vue; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/vue/index.vue?vue&type=template&id=0f603300
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c(_vm.component, {
    tag: "component"
  });
};
var staticRenderFns = [];

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(748);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);
// EXTERNAL MODULE: external "babel.min.js"
var external_babel_min_js_ = __webpack_require__(924);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/libs/coder.js"
var coder_js_ = __webpack_require__(470);
var coder_js_default = /*#__PURE__*/__webpack_require__.n(coder_js_);
// EXTERNAL MODULE: ./src/views/dview/page/components/vue/default.js
var vue_default = __webpack_require__(62);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/vue/index.vue?vue&type=script&lang=js




const win = typeof self !== 'undefined' ? window : __webpack_require__.g;
win.Babel = external_babel_min_js_;
/* harmony default export */ var vuevue_type_script_lang_js = ({
  props: {
    value: {
      type: Object,
      default() {
        return {
          code: vue_default/* default */.Z.code
        };
      }
    }
  },
  data() {
    return {
      component: 'div',
      isCompile: true
    };
  },
  watch: {
    value: {
      handler() {
        if (this.isCompile === false || String(this.isCompile) === '0') {
          return true;
        }
        this.compile();
      },
      deep: true
    }
  },
  methods: {
    compile() {
      coder_js_default().vue.resolve(this.value.code || vue_default/* default */.Z.code, (external_vue_default()), $pig).then(cp => {
        this.component = cp;
      });
    }
  },
  created() {
    this.compile();
    this.emiter = () => {
      this.compile();
    };
    // 初始化编译状态
    this.$triggerEvent('init-is-compile', value => {
      this.isCompile = value;
    });
  },
  mounted() {
    this.$bindEvent('compile-page-item', this.emiter);
    this.$bindEvent('update-is-compile', value => {
      this.isCompile = value;
    });
  },
  beforeDestroy() {
    this.$unbindEvent('compile-page-item', this.emiter);
    this.$unbindEvent('update-is-compile');
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/vue/index.vue?vue&type=script&lang=js
 /* harmony default export */ var components_vuevue_type_script_lang_js = (vuevue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);
;// CONCATENATED MODULE: ./src/views/dview/page/components/vue/index.vue





/* normalize component */
;
var component = (0,componentNormalizer/* default */.Z)(
  components_vuevue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var vue = (component.exports);

/***/ }),

/***/ 492:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ property; }
});

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/vue/property.vue?vue&type=template&id=3096b49d&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "component-property"
  }, [_c('el-tabs', {
    model: {
      value: _vm.activeName,
      callback: function callback($$v) {
        _vm.activeName = $$v;
      },
      expression: "activeName"
    }
  }, [_c('el-tab-pane', {
    attrs: {
      "label": "编码",
      "name": "first"
    }
  }, [_c('Coder', {
    ref: "coder",
    attrs: {
      "height": '480px',
      "language": "html"
    },
    model: {
      value: _vm.state.code,
      callback: function callback($$v) {
        _vm.$set(_vm.state, "code", $$v);
      },
      expression: "state.code"
    }
  })], 1)], 1)], 1);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/element-resize-detector/src/element-resize-detector.js
var element_resize_detector = __webpack_require__(498);
var element_resize_detector_default = /*#__PURE__*/__webpack_require__.n(element_resize_detector);
// EXTERNAL MODULE: external "@daelui/vdog/dist/coder.js"
var coder_js_ = __webpack_require__(443);
// EXTERNAL MODULE: ./src/views/dview/page/components/vue/default.js
var vue_default = __webpack_require__(62);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/vue/property.vue?vue&type=script&lang=js



/* harmony default export */ var propertyvue_type_script_lang_js = ({
  components: {
    Coder: coder_js_.Coder
  },
  props: {
    value: {
      type: Object,
      default() {
        return {
          code: vue_default/* default */.Z.code
        };
      }
    }
  },
  data() {
    return {
      activeName: 'first',
      state: {
        code: vue_default/* default */.Z.code
      }
    };
  },
  watch: {
    value: {
      handler() {
        this.syncState();
      },
      deep: true
    },
    'state.code': {
      handler() {
        this.syncValue();
      },
      deep: true
    }
  },
  methods: {
    syncState() {
      Object.assign(this.state, this.value);
    },
    syncValue() {
      Object.assign(this.value, this.state);
      this.$emit('update', this.state);
    }
  },
  mounted() {
    this.syncState();
    //监听元素变化
    let erd = element_resize_detector_default()();
    erd.listenTo(this.$el, () => {
      let monacoEditor = this.$refs.coder.monacoEditor;
      monacoEditor && monacoEditor.layout();
    });
  }
});
;// CONCATENATED MODULE: ./src/views/dview/page/components/vue/property.vue?vue&type=script&lang=js
 /* harmony default export */ var vue_propertyvue_type_script_lang_js = (propertyvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./node_modules/mini-css-extract-plugin/dist/loader.js??clonedRuleSet-74.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-74.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-74.use[2]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-74.use[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/dview/page/components/vue/property.vue?vue&type=style&index=0&id=3096b49d&prod&lang=less&scoped=true
// extracted by mini-css-extract-plugin

;// CONCATENATED MODULE: ./src/views/dview/page/components/vue/property.vue?vue&type=style&index=0&id=3096b49d&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);
;// CONCATENATED MODULE: ./src/views/dview/page/components/vue/property.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  vue_propertyvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  "3096b49d",
  null
  
)

/* harmony default export */ var property = (component.exports);

/***/ }),

/***/ 1:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   Z: function() { return /* binding */ normalizeComponent; }
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent(
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */,
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options =
    typeof scriptExports === 'function' ? scriptExports.options : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) {
    // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
          injectStyles.call(
            this,
            (options.functional ? this.parent : this).$root.$options.shadowRoot
          )
        }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 365:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {



var utils = __webpack_require__(628);
module.exports = function batchProcessorMaker(options) {
  options = options || {};
  var reporter = options.reporter;
  var asyncProcess = utils.getOption(options, "async", true);
  var autoProcess = utils.getOption(options, "auto", true);
  if (autoProcess && !asyncProcess) {
    reporter && reporter.warn("Invalid options combination. auto=true and async=false is invalid. Setting async=true.");
    asyncProcess = true;
  }
  var batch = Batch();
  var asyncFrameHandler;
  var isProcessing = false;
  function addFunction(level, fn) {
    if (!isProcessing && autoProcess && asyncProcess && batch.size() === 0) {
      // Since this is async, it is guaranteed to be executed after that the fn is added to the batch.
      // This needs to be done before, since we're checking the size of the batch to be 0.
      processBatchAsync();
    }
    batch.add(level, fn);
  }
  function processBatch() {
    // Save the current batch, and create a new batch so that incoming functions are not added into the currently processing batch.
    // Continue processing until the top-level batch is empty (functions may be added to the new batch while processing, and so on).
    isProcessing = true;
    while (batch.size()) {
      var processingBatch = batch;
      batch = Batch();
      processingBatch.process();
    }
    isProcessing = false;
  }
  function forceProcessBatch(localAsyncProcess) {
    if (isProcessing) {
      return;
    }
    if (localAsyncProcess === undefined) {
      localAsyncProcess = asyncProcess;
    }
    if (asyncFrameHandler) {
      cancelFrame(asyncFrameHandler);
      asyncFrameHandler = null;
    }
    if (localAsyncProcess) {
      processBatchAsync();
    } else {
      processBatch();
    }
  }
  function processBatchAsync() {
    asyncFrameHandler = requestFrame(processBatch);
  }
  function clearBatch() {
    batch = {};
    batchSize = 0;
    topLevel = 0;
    bottomLevel = 0;
  }
  function cancelFrame(listener) {
    // var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.clearTimeout;
    var cancel = clearTimeout;
    return cancel(listener);
  }
  function requestFrame(callback) {
    // var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || function(fn) { return window.setTimeout(fn, 20); };
    var raf = function raf(fn) {
      return setTimeout(fn, 0);
    };
    return raf(callback);
  }
  return {
    add: addFunction,
    force: forceProcessBatch
  };
};
function Batch() {
  var batch = {};
  var size = 0;
  var topLevel = 0;
  var bottomLevel = 0;
  function add(level, fn) {
    if (!fn) {
      fn = level;
      level = 0;
    }
    if (level > topLevel) {
      topLevel = level;
    } else if (level < bottomLevel) {
      bottomLevel = level;
    }
    if (!batch[level]) {
      batch[level] = [];
    }
    batch[level].push(fn);
    size++;
  }
  function process() {
    for (var level = bottomLevel; level <= topLevel; level++) {
      var fns = batch[level];
      for (var i = 0; i < fns.length; i++) {
        var fn = fns[i];
        fn();
      }
    }
  }
  function getSize() {
    return size;
  }
  return {
    add: add,
    process: process,
    size: getSize
  };
}

/***/ }),

/***/ 628:
/***/ (function(module) {



var utils = module.exports = {};
utils.getOption = getOption;
function getOption(options, name, defaultValue) {
  var value = options[name];
  if ((value === undefined || value === null) && defaultValue !== undefined) {
    return defaultValue;
  }
  return value;
}

/***/ }),

/***/ 294:
/***/ (function(module) {



var detector = module.exports = {};
detector.isIE = function (version) {
  function isAnyIeVersion() {
    var agent = navigator.userAgent.toLowerCase();
    return agent.indexOf("msie") !== -1 || agent.indexOf("trident") !== -1 || agent.indexOf(" edge/") !== -1;
  }
  if (!isAnyIeVersion()) {
    return false;
  }
  if (!version) {
    return true;
  }

  //Shamelessly stolen from https://gist.github.com/padolsey/527683
  var ieVersion = function () {
    var undef,
      v = 3,
      div = document.createElement("div"),
      all = div.getElementsByTagName("i");
    do {
      div.innerHTML = "<!--[if gt IE " + ++v + "]><i></i><![endif]-->";
    } while (all[0]);
    return v > 4 ? v : undef;
  }();
  return version === ieVersion;
};
detector.isLegacyOpera = function () {
  return !!window.opera;
};

/***/ }),

/***/ 495:
/***/ (function(module) {



var utils = module.exports = {};

/**
 * Loops through the collection and calls the callback for each element. if the callback returns truthy, the loop is broken and returns the same value.
 * @public
 * @param {*} collection The collection to loop through. Needs to have a length property set and have indices set from 0 to length - 1.
 * @param {function} callback The callback to be called for each element. The element will be given as a parameter to the callback. If this callback returns truthy, the loop is broken and the same value is returned.
 * @returns {*} The value that a callback has returned (if truthy). Otherwise nothing.
 */
utils.forEach = function (collection, callback) {
  for (var i = 0; i < collection.length; i++) {
    var result = callback(collection[i]);
    if (result) {
      return result;
    }
  }
};

/***/ }),

/***/ 0:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

/**
 * Resize detection strategy that injects objects to elements in order to detect resize events.
 * Heavily inspired by: http://www.backalleycoder.com/2013/03/18/cross-browser-event-based-element-resize-detection/
 */



var browserDetector = __webpack_require__(294);
module.exports = function (options) {
  options = options || {};
  var reporter = options.reporter;
  var batchProcessor = options.batchProcessor;
  var getState = options.stateHandler.getState;
  if (!reporter) {
    throw new Error("Missing required dependency: reporter.");
  }

  /**
   * Adds a resize event listener to the element.
   * @public
   * @param {element} element The element that should have the listener added.
   * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
   */
  function addListener(element, listener) {
    function listenerProxy() {
      listener(element);
    }
    if (browserDetector.isIE(8)) {
      //IE 8 does not support object, but supports the resize event directly on elements.
      getState(element).object = {
        proxy: listenerProxy
      };
      element.attachEvent("onresize", listenerProxy);
    } else {
      var object = getObject(element);
      if (!object) {
        throw new Error("Element is not detectable by this strategy.");
      }
      object.contentDocument.defaultView.addEventListener("resize", listenerProxy);
    }
  }
  function buildCssTextString(rules) {
    var seperator = options.important ? " !important; " : "; ";
    return (rules.join(seperator) + seperator).trim();
  }

  /**
   * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
   * @private
   * @param {object} options Optional options object.
   * @param {element} element The element to make detectable
   * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
   */
  function makeDetectable(options, element, callback) {
    if (!callback) {
      callback = element;
      element = options;
      options = null;
    }
    options = options || {};
    var debug = options.debug;
    function injectObject(element, callback) {
      var OBJECT_STYLE = buildCssTextString(["display: block", "position: absolute", "top: 0", "left: 0", "width: 100%", "height: 100%", "border: none", "padding: 0", "margin: 0", "opacity: 0", "z-index: -1000", "pointer-events: none"]);

      //The target element needs to be positioned (everything except static) so the absolute positioned object will be positioned relative to the target element.

      // Position altering may be performed directly or on object load, depending on if style resolution is possible directly or not.
      var positionCheckPerformed = false;

      // The element may not yet be attached to the DOM, and therefore the style object may be empty in some browsers.
      // Since the style object is a reference, it will be updated as soon as the element is attached to the DOM.
      var style = window.getComputedStyle(element);
      var width = element.offsetWidth;
      var height = element.offsetHeight;
      getState(element).startSize = {
        width: width,
        height: height
      };
      function mutateDom() {
        function alterPositionStyles() {
          if (style.position === "static") {
            element.style.setProperty("position", "relative", options.important ? "important" : "");
            var removeRelativeStyles = function removeRelativeStyles(reporter, element, style, property) {
              function getNumericalValue(value) {
                return value.replace(/[^-\d\.]/g, "");
              }
              var value = style[property];
              if (value !== "auto" && getNumericalValue(value) !== "0") {
                reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
                element.style.setProperty(property, "0", options.important ? "important" : "");
              }
            };

            //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
            //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
            removeRelativeStyles(reporter, element, style, "top");
            removeRelativeStyles(reporter, element, style, "right");
            removeRelativeStyles(reporter, element, style, "bottom");
            removeRelativeStyles(reporter, element, style, "left");
          }
        }
        function onObjectLoad() {
          // The object has been loaded, which means that the element now is guaranteed to be attached to the DOM.
          if (!positionCheckPerformed) {
            alterPositionStyles();
          }

          /*jshint validthis: true */

          function getDocument(element, callback) {
            //Opera 12 seem to call the object.onload before the actual document has been created.
            //So if it is not present, poll it with an timeout until it is present.
            //TODO: Could maybe be handled better with object.onreadystatechange or similar.
            if (!element.contentDocument) {
              var state = getState(element);
              if (state.checkForObjectDocumentTimeoutId) {
                window.clearTimeout(state.checkForObjectDocumentTimeoutId);
              }
              state.checkForObjectDocumentTimeoutId = setTimeout(function checkForObjectDocument() {
                state.checkForObjectDocumentTimeoutId = 0;
                getDocument(element, callback);
              }, 100);
              return;
            }
            callback(element.contentDocument);
          }

          //Mutating the object element here seems to fire another load event.
          //Mutating the inner document of the object element is fine though.
          var objectElement = this;

          //Create the style element to be added to the object.
          getDocument(objectElement, function onObjectDocumentReady(objectDocument) {
            //Notify that the element is ready to be listened to.
            callback(element);
          });
        }

        // The element may be detached from the DOM, and some browsers does not support style resolving of detached elements.
        // The alterPositionStyles needs to be delayed until we know the element has been attached to the DOM (which we are sure of when the onObjectLoad has been fired), if style resolution is not possible.
        if (style.position !== "") {
          alterPositionStyles(style);
          positionCheckPerformed = true;
        }

        //Add an object element as a child to the target element that will be listened to for resize events.
        var object = document.createElement("object");
        object.style.cssText = OBJECT_STYLE;
        object.tabIndex = -1;
        object.type = "text/html";
        object.setAttribute("aria-hidden", "true");
        object.onload = onObjectLoad;

        //Safari: This must occur before adding the object to the DOM.
        //IE: Does not like that this happens before, even if it is also added after.
        if (!browserDetector.isIE()) {
          object.data = "about:blank";
        }
        if (!getState(element)) {
          // The element has been uninstalled before the actual loading happened.
          return;
        }
        element.appendChild(object);
        getState(element).object = object;

        //IE: This must occur after adding the object to the DOM.
        if (browserDetector.isIE()) {
          object.data = "about:blank";
        }
      }
      if (batchProcessor) {
        batchProcessor.add(mutateDom);
      } else {
        mutateDom();
      }
    }
    if (browserDetector.isIE(8)) {
      //IE 8 does not support objects properly. Luckily they do support the resize event.
      //So do not inject the object and notify that the element is already ready to be listened to.
      //The event handler for the resize event is attached in the utils.addListener instead.
      callback(element);
    } else {
      injectObject(element, callback);
    }
  }

  /**
   * Returns the child object of the target element.
   * @private
   * @param {element} element The target element.
   * @returns The object element of the target.
   */
  function getObject(element) {
    return getState(element).object;
  }
  function uninstall(element) {
    if (!getState(element)) {
      return;
    }
    var object = getObject(element);
    if (!object) {
      return;
    }
    if (browserDetector.isIE(8)) {
      element.detachEvent("onresize", object.proxy);
    } else {
      element.removeChild(object);
    }
    if (getState(element).checkForObjectDocumentTimeoutId) {
      window.clearTimeout(getState(element).checkForObjectDocumentTimeoutId);
    }
    delete getState(element).object;
  }
  return {
    makeDetectable: makeDetectable,
    addListener: addListener,
    uninstall: uninstall
  };
};

/***/ }),

/***/ 254:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

/**
 * Resize detection strategy that injects divs to elements in order to detect resize events on scroll events.
 * Heavily inspired by: https://github.com/marcj/css-element-queries/blob/master/src/ResizeSensor.js
 */



var forEach = (__webpack_require__(495).forEach);
module.exports = function (options) {
  options = options || {};
  var reporter = options.reporter;
  var batchProcessor = options.batchProcessor;
  var getState = options.stateHandler.getState;
  var hasState = options.stateHandler.hasState;
  var idHandler = options.idHandler;
  if (!batchProcessor) {
    throw new Error("Missing required dependency: batchProcessor");
  }
  if (!reporter) {
    throw new Error("Missing required dependency: reporter.");
  }

  //TODO: Could this perhaps be done at installation time?
  var scrollbarSizes = getScrollbarSizes();
  var styleId = "erd_scroll_detection_scrollbar_style";
  var detectionContainerClass = "erd_scroll_detection_container";
  function initDocument(targetDocument) {
    // Inject the scrollbar styling that prevents them from appearing sometimes in Chrome.
    // The injected container needs to have a class, so that it may be styled with CSS (pseudo elements).
    injectScrollStyle(targetDocument, styleId, detectionContainerClass);
  }
  initDocument(window.document);
  function buildCssTextString(rules) {
    var seperator = options.important ? " !important; " : "; ";
    return (rules.join(seperator) + seperator).trim();
  }
  function getScrollbarSizes() {
    var width = 500;
    var height = 500;
    var child = document.createElement("div");
    child.style.cssText = buildCssTextString(["position: absolute", "width: " + width * 2 + "px", "height: " + height * 2 + "px", "visibility: hidden", "margin: 0", "padding: 0"]);
    var container = document.createElement("div");
    container.style.cssText = buildCssTextString(["position: absolute", "width: " + width + "px", "height: " + height + "px", "overflow: scroll", "visibility: none", "top: " + -width * 3 + "px", "left: " + -height * 3 + "px", "visibility: hidden", "margin: 0", "padding: 0"]);
    container.appendChild(child);
    document.body.insertBefore(container, document.body.firstChild);
    var widthSize = width - container.clientWidth;
    var heightSize = height - container.clientHeight;
    document.body.removeChild(container);
    return {
      width: widthSize,
      height: heightSize
    };
  }
  function injectScrollStyle(targetDocument, styleId, containerClass) {
    function injectStyle(style, method) {
      method = method || function (element) {
        targetDocument.head.appendChild(element);
      };
      var styleElement = targetDocument.createElement("style");
      styleElement.innerHTML = style;
      styleElement.id = styleId;
      method(styleElement);
      return styleElement;
    }
    if (!targetDocument.getElementById(styleId)) {
      var containerAnimationClass = containerClass + "_animation";
      var containerAnimationActiveClass = containerClass + "_animation_active";
      var style = "/* Created by the element-resize-detector library. */\n";
      style += "." + containerClass + " > div::-webkit-scrollbar { " + buildCssTextString(["display: none"]) + " }\n\n";
      style += "." + containerAnimationActiveClass + " { " + buildCssTextString(["-webkit-animation-duration: 0.1s", "animation-duration: 0.1s", "-webkit-animation-name: " + containerAnimationClass, "animation-name: " + containerAnimationClass]) + " }\n";
      style += "@-webkit-keyframes " + containerAnimationClass + " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }\n";
      style += "@keyframes " + containerAnimationClass + " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }";
      injectStyle(style);
    }
  }
  function addAnimationClass(element) {
    element.className += " " + detectionContainerClass + "_animation_active";
  }
  function addEvent(el, name, cb) {
    if (el.addEventListener) {
      el.addEventListener(name, cb);
    } else if (el.attachEvent) {
      el.attachEvent("on" + name, cb);
    } else {
      return reporter.error("[scroll] Don't know how to add event listeners.");
    }
  }
  function removeEvent(el, name, cb) {
    if (el.removeEventListener) {
      el.removeEventListener(name, cb);
    } else if (el.detachEvent) {
      el.detachEvent("on" + name, cb);
    } else {
      return reporter.error("[scroll] Don't know how to remove event listeners.");
    }
  }
  function getExpandElement(element) {
    return getState(element).container.childNodes[0].childNodes[0].childNodes[0];
  }
  function getShrinkElement(element) {
    return getState(element).container.childNodes[0].childNodes[0].childNodes[1];
  }

  /**
   * Adds a resize event listener to the element.
   * @public
   * @param {element} element The element that should have the listener added.
   * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
   */
  function addListener(element, listener) {
    var listeners = getState(element).listeners;
    if (!listeners.push) {
      throw new Error("Cannot add listener to an element that is not detectable.");
    }
    getState(element).listeners.push(listener);
  }

  /**
   * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
   * @private
   * @param {object} options Optional options object.
   * @param {element} element The element to make detectable
   * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
   */
  function makeDetectable(options, element, callback) {
    if (!callback) {
      callback = element;
      element = options;
      options = null;
    }
    options = options || {};
    function debug() {
      if (options.debug) {
        var args = Array.prototype.slice.call(arguments);
        args.unshift(idHandler.get(element), "Scroll: ");
        if (reporter.log.apply) {
          reporter.log.apply(null, args);
        } else {
          for (var i = 0; i < args.length; i++) {
            reporter.log(args[i]);
          }
        }
      }
    }
    function isDetached(element) {
      function isInDocument(element) {
        var isInShadowRoot = element.getRootNode && element.getRootNode().contains(element);
        return element === element.ownerDocument.body || element.ownerDocument.body.contains(element) || isInShadowRoot;
      }
      if (!isInDocument(element)) {
        return true;
      }

      // FireFox returns null style in hidden iframes. See https://github.com/wnr/element-resize-detector/issues/68 and https://bugzilla.mozilla.org/show_bug.cgi?id=795520
      if (window.getComputedStyle(element) === null) {
        return true;
      }
      return false;
    }
    function isUnrendered(element) {
      // Check the absolute positioned container since the top level container is display: inline.
      var container = getState(element).container.childNodes[0];
      var style = window.getComputedStyle(container);
      return !style.width || style.width.indexOf("px") === -1; //Can only compute pixel value when rendered.
    }

    function getStyle() {
      // Some browsers only force layouts when actually reading the style properties of the style object, so make sure that they are all read here,
      // so that the user of the function can be sure that it will perform the layout here, instead of later (important for batching).
      var elementStyle = window.getComputedStyle(element);
      var style = {};
      style.position = elementStyle.position;
      style.width = element.offsetWidth;
      style.height = element.offsetHeight;
      style.top = elementStyle.top;
      style.right = elementStyle.right;
      style.bottom = elementStyle.bottom;
      style.left = elementStyle.left;
      style.widthCSS = elementStyle.width;
      style.heightCSS = elementStyle.height;
      return style;
    }
    function storeStartSize() {
      var style = getStyle();
      getState(element).startSize = {
        width: style.width,
        height: style.height
      };
      debug("Element start size", getState(element).startSize);
    }
    function initListeners() {
      getState(element).listeners = [];
    }
    function storeStyle() {
      debug("storeStyle invoked.");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      var style = getStyle();
      getState(element).style = style;
    }
    function storeCurrentSize(element, width, height) {
      getState(element).lastWidth = width;
      getState(element).lastHeight = height;
    }
    function getExpandChildElement(element) {
      return getExpandElement(element).childNodes[0];
    }
    function getWidthOffset() {
      return 2 * scrollbarSizes.width + 1;
    }
    function getHeightOffset() {
      return 2 * scrollbarSizes.height + 1;
    }
    function getExpandWidth(width) {
      return width + 10 + getWidthOffset();
    }
    function getExpandHeight(height) {
      return height + 10 + getHeightOffset();
    }
    function getShrinkWidth(width) {
      return width * 2 + getWidthOffset();
    }
    function getShrinkHeight(height) {
      return height * 2 + getHeightOffset();
    }
    function positionScrollbars(element, width, height) {
      var expand = getExpandElement(element);
      var shrink = getShrinkElement(element);
      var expandWidth = getExpandWidth(width);
      var expandHeight = getExpandHeight(height);
      var shrinkWidth = getShrinkWidth(width);
      var shrinkHeight = getShrinkHeight(height);
      expand.scrollLeft = expandWidth;
      expand.scrollTop = expandHeight;
      shrink.scrollLeft = shrinkWidth;
      shrink.scrollTop = shrinkHeight;
    }
    function injectContainerElement() {
      var container = getState(element).container;
      if (!container) {
        container = document.createElement("div");
        container.className = detectionContainerClass;
        container.style.cssText = buildCssTextString(["visibility: hidden", "display: inline", "width: 0px", "height: 0px", "z-index: -1", "overflow: hidden", "margin: 0", "padding: 0"]);
        getState(element).container = container;
        addAnimationClass(container);
        element.appendChild(container);
        var onAnimationStart = function onAnimationStart() {
          getState(element).onRendered && getState(element).onRendered();
        };
        addEvent(container, "animationstart", onAnimationStart);

        // Store the event handler here so that they may be removed when uninstall is called.
        // See uninstall function for an explanation why it is needed.
        getState(element).onAnimationStart = onAnimationStart;
      }
      return container;
    }
    function injectScrollElements() {
      function alterPositionStyles() {
        var style = getState(element).style;
        if (style.position === "static") {
          element.style.setProperty("position", "relative", options.important ? "important" : "");
          var removeRelativeStyles = function removeRelativeStyles(reporter, element, style, property) {
            function getNumericalValue(value) {
              return value.replace(/[^-\d\.]/g, "");
            }
            var value = style[property];
            if (value !== "auto" && getNumericalValue(value) !== "0") {
              reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
              element.style[property] = 0;
            }
          };

          //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
          //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
          removeRelativeStyles(reporter, element, style, "top");
          removeRelativeStyles(reporter, element, style, "right");
          removeRelativeStyles(reporter, element, style, "bottom");
          removeRelativeStyles(reporter, element, style, "left");
        }
      }
      function getLeftTopBottomRightCssText(left, top, bottom, right) {
        left = !left ? "0" : left + "px";
        top = !top ? "0" : top + "px";
        bottom = !bottom ? "0" : bottom + "px";
        right = !right ? "0" : right + "px";
        return ["left: " + left, "top: " + top, "right: " + right, "bottom: " + bottom];
      }
      debug("Injecting elements");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      alterPositionStyles();
      var rootContainer = getState(element).container;
      if (!rootContainer) {
        rootContainer = injectContainerElement();
      }

      // Due to this WebKit bug https://bugs.webkit.org/show_bug.cgi?id=80808 (currently fixed in Blink, but still present in WebKit browsers such as Safari),
      // we need to inject two containers, one that is width/height 100% and another that is left/top -1px so that the final container always is 1x1 pixels bigger than
      // the targeted element.
      // When the bug is resolved, "containerContainer" may be removed.

      // The outer container can occasionally be less wide than the targeted when inside inline elements element in WebKit (see https://bugs.webkit.org/show_bug.cgi?id=152980).
      // This should be no problem since the inner container either way makes sure the injected scroll elements are at least 1x1 px.

      var scrollbarWidth = scrollbarSizes.width;
      var scrollbarHeight = scrollbarSizes.height;
      var containerContainerStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: hidden", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%", "left: 0px", "top: 0px"]);
      var containerStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: hidden", "z-index: -1", "visibility: hidden"].concat(getLeftTopBottomRightCssText(-(1 + scrollbarWidth), -(1 + scrollbarHeight), -scrollbarHeight, -scrollbarWidth)));
      var expandStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: scroll", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%"]);
      var shrinkStyle = buildCssTextString(["position: absolute", "flex: none", "overflow: scroll", "z-index: -1", "visibility: hidden", "width: 100%", "height: 100%"]);
      var expandChildStyle = buildCssTextString(["position: absolute", "left: 0", "top: 0"]);
      var shrinkChildStyle = buildCssTextString(["position: absolute", "width: 200%", "height: 200%"]);
      var containerContainer = document.createElement("div");
      var container = document.createElement("div");
      var expand = document.createElement("div");
      var expandChild = document.createElement("div");
      var shrink = document.createElement("div");
      var shrinkChild = document.createElement("div");

      // Some browsers choke on the resize system being rtl, so force it to ltr. https://github.com/wnr/element-resize-detector/issues/56
      // However, dir should not be set on the top level container as it alters the dimensions of the target element in some browsers.
      containerContainer.dir = "ltr";
      containerContainer.style.cssText = containerContainerStyle;
      containerContainer.className = detectionContainerClass;
      container.className = detectionContainerClass;
      container.style.cssText = containerStyle;
      expand.style.cssText = expandStyle;
      expandChild.style.cssText = expandChildStyle;
      shrink.style.cssText = shrinkStyle;
      shrinkChild.style.cssText = shrinkChildStyle;
      expand.appendChild(expandChild);
      shrink.appendChild(shrinkChild);
      container.appendChild(expand);
      container.appendChild(shrink);
      containerContainer.appendChild(container);
      rootContainer.appendChild(containerContainer);
      function onExpandScroll() {
        var state = getState(element);
        if (state && state.onExpand) {
          state.onExpand();
        } else {
          debug("Aborting expand scroll handler: element has been uninstalled");
        }
      }
      function onShrinkScroll() {
        var state = getState(element);
        if (state && state.onShrink) {
          state.onShrink();
        } else {
          debug("Aborting shrink scroll handler: element has been uninstalled");
        }
      }
      addEvent(expand, "scroll", onExpandScroll);
      addEvent(shrink, "scroll", onShrinkScroll);

      // Store the event handlers here so that they may be removed when uninstall is called.
      // See uninstall function for an explanation why it is needed.
      getState(element).onExpandScroll = onExpandScroll;
      getState(element).onShrinkScroll = onShrinkScroll;
    }
    function registerListenersAndPositionElements() {
      function updateChildSizes(element, width, height) {
        var expandChild = getExpandChildElement(element);
        var expandWidth = getExpandWidth(width);
        var expandHeight = getExpandHeight(height);
        expandChild.style.setProperty("width", expandWidth + "px", options.important ? "important" : "");
        expandChild.style.setProperty("height", expandHeight + "px", options.important ? "important" : "");
      }
      function updateDetectorElements(done) {
        var width = element.offsetWidth;
        var height = element.offsetHeight;

        // Check whether the size has actually changed since last time the algorithm ran. If not, some steps may be skipped.
        var sizeChanged = width !== getState(element).lastWidth || height !== getState(element).lastHeight;
        debug("Storing current size", width, height);

        // Store the size of the element sync here, so that multiple scroll events may be ignored in the event listeners.
        // Otherwise the if-check in handleScroll is useless.
        storeCurrentSize(element, width, height);

        // Since we delay the processing of the batch, there is a risk that uninstall has been called before the batch gets to execute.
        // Since there is no way to cancel the fn executions, we need to add an uninstall guard to all fns of the batch.

        batchProcessor.add(0, function performUpdateChildSizes() {
          if (!sizeChanged) {
            return;
          }
          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          if (!areElementsInjected()) {
            debug("Aborting because element container has not been initialized");
            return;
          }
          if (options.debug) {
            var w = element.offsetWidth;
            var h = element.offsetHeight;
            if (w !== width || h !== height) {
              reporter.warn(idHandler.get(element), "Scroll: Size changed before updating detector elements.");
            }
          }
          updateChildSizes(element, width, height);
        });
        batchProcessor.add(1, function updateScrollbars() {
          // This function needs to be invoked event though the size is unchanged. The element could have been resized very quickly and then
          // been restored to the original size, which will have changed the scrollbar positions.

          if (!getState(element)) {
            debug("Aborting because element has been uninstalled");
            return;
          }
          if (!areElementsInjected()) {
            debug("Aborting because element container has not been initialized");
            return;
          }
          positionScrollbars(element, width, height);
        });
        if (sizeChanged && done) {
          batchProcessor.add(2, function () {
            if (!getState(element)) {
              debug("Aborting because element has been uninstalled");
              return;
            }
            if (!areElementsInjected()) {
              debug("Aborting because element container has not been initialized");
              return;
            }
            done();
          });
        }
      }
      function areElementsInjected() {
        return !!getState(element).container;
      }
      function notifyListenersIfNeeded() {
        function isFirstNotify() {
          return getState(element).lastNotifiedWidth === undefined;
        }
        debug("notifyListenersIfNeeded invoked");
        var state = getState(element);

        // Don't notify if the current size is the start size, and this is the first notification.
        if (isFirstNotify() && state.lastWidth === state.startSize.width && state.lastHeight === state.startSize.height) {
          return debug("Not notifying: Size is the same as the start size, and there has been no notification yet.");
        }

        // Don't notify if the size already has been notified.
        if (state.lastWidth === state.lastNotifiedWidth && state.lastHeight === state.lastNotifiedHeight) {
          return debug("Not notifying: Size already notified");
        }
        debug("Current size not notified, notifying...");
        state.lastNotifiedWidth = state.lastWidth;
        state.lastNotifiedHeight = state.lastHeight;
        forEach(getState(element).listeners, function (listener) {
          listener(element);
        });
      }
      function handleRender() {
        debug("startanimation triggered.");
        if (isUnrendered(element)) {
          debug("Ignoring since element is still unrendered...");
          return;
        }
        debug("Element rendered.");
        var expand = getExpandElement(element);
        var shrink = getShrinkElement(element);
        if (expand.scrollLeft === 0 || expand.scrollTop === 0 || shrink.scrollLeft === 0 || shrink.scrollTop === 0) {
          debug("Scrollbars out of sync. Updating detector elements...");
          updateDetectorElements(notifyListenersIfNeeded);
        }
      }
      function handleScroll() {
        debug("Scroll detected.");
        if (isUnrendered(element)) {
          // Element is still unrendered. Skip this scroll event.
          debug("Scroll event fired while unrendered. Ignoring...");
          return;
        }
        updateDetectorElements(notifyListenersIfNeeded);
      }
      debug("registerListenersAndPositionElements invoked.");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      getState(element).onRendered = handleRender;
      getState(element).onExpand = handleScroll;
      getState(element).onShrink = handleScroll;
      var style = getState(element).style;
      updateChildSizes(element, style.width, style.height);
    }
    function finalizeDomMutation() {
      debug("finalizeDomMutation invoked.");
      if (!getState(element)) {
        debug("Aborting because element has been uninstalled");
        return;
      }
      var style = getState(element).style;
      storeCurrentSize(element, style.width, style.height);
      positionScrollbars(element, style.width, style.height);
    }
    function ready() {
      callback(element);
    }
    function install() {
      debug("Installing...");
      initListeners();
      storeStartSize();
      batchProcessor.add(0, storeStyle);
      batchProcessor.add(1, injectScrollElements);
      batchProcessor.add(2, registerListenersAndPositionElements);
      batchProcessor.add(3, finalizeDomMutation);
      batchProcessor.add(4, ready);
    }
    debug("Making detectable...");
    if (isDetached(element)) {
      debug("Element is detached");
      injectContainerElement();
      debug("Waiting until element is attached...");
      getState(element).onRendered = function () {
        debug("Element is now attached");
        install();
      };
    } else {
      install();
    }
  }
  function uninstall(element) {
    var state = getState(element);
    if (!state) {
      // Uninstall has been called on a non-erd element.
      return;
    }

    // Uninstall may have been called in the following scenarios:
    // (1) Right between the sync code and async batch (here state.busy = true, but nothing have been registered or injected).
    // (2) In the ready callback of the last level of the batch by another element (here, state.busy = true, but all the stuff has been injected).
    // (3) After the installation process (here, state.busy = false and all the stuff has been injected).
    // So to be on the safe side, let's check for each thing before removing.

    // We need to remove the event listeners, because otherwise the event might fire on an uninstall element which results in an error when trying to get the state of the element.
    state.onExpandScroll && removeEvent(getExpandElement(element), "scroll", state.onExpandScroll);
    state.onShrinkScroll && removeEvent(getShrinkElement(element), "scroll", state.onShrinkScroll);
    state.onAnimationStart && removeEvent(state.container, "animationstart", state.onAnimationStart);
    state.container && element.removeChild(state.container);
  }
  return {
    makeDetectable: makeDetectable,
    addListener: addListener,
    uninstall: uninstall,
    initDocument: initDocument
  };
};

/***/ }),

/***/ 498:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {



var forEach = (__webpack_require__(495).forEach);
var elementUtilsMaker = __webpack_require__(547);
var listenerHandlerMaker = __webpack_require__(284);
var idGeneratorMaker = __webpack_require__(110);
var idHandlerMaker = __webpack_require__(535);
var reporterMaker = __webpack_require__(256);
var browserDetector = __webpack_require__(294);
var batchProcessorMaker = __webpack_require__(365);
var stateHandler = __webpack_require__(613);

//Detection strategies.
var objectStrategyMaker = __webpack_require__(0);
var scrollStrategyMaker = __webpack_require__(254);
function isCollection(obj) {
  return Array.isArray(obj) || obj.length !== undefined;
}
function toArray(collection) {
  if (!Array.isArray(collection)) {
    var array = [];
    forEach(collection, function (obj) {
      array.push(obj);
    });
    return array;
  } else {
    return collection;
  }
}
function isElement(obj) {
  return obj && obj.nodeType === 1;
}

/**
 * @typedef idHandler
 * @type {object}
 * @property {function} get Gets the resize detector id of the element.
 * @property {function} set Generate and sets the resize detector id of the element.
 */

/**
 * @typedef Options
 * @type {object}
 * @property {boolean} callOnAdd    Determines if listeners should be called when they are getting added.
                                    Default is true. If true, the listener is guaranteed to be called when it has been added.
                                    If false, the listener will not be guarenteed to be called when it has been added (does not prevent it from being called).
 * @property {idHandler} idHandler  A custom id handler that is responsible for generating, setting and retrieving id's for elements.
                                    If not provided, a default id handler will be used.
 * @property {reporter} reporter    A custom reporter that handles reporting logs, warnings and errors.
                                    If not provided, a default id handler will be used.
                                    If set to false, then nothing will be reported.
 * @property {boolean} debug        If set to true, the the system will report debug messages as default for the listenTo method.
 */

/**
 * Creates an element resize detector instance.
 * @public
 * @param {Options?} options Optional global options object that will decide how this instance will work.
 */
module.exports = function (options) {
  options = options || {};

  //idHandler is currently not an option to the listenTo function, so it should not be added to globalOptions.
  var idHandler;
  if (options.idHandler) {
    // To maintain compatability with idHandler.get(element, readonly), make sure to wrap the given idHandler
    // so that readonly flag always is true when it's used here. This may be removed next major version bump.
    idHandler = {
      get: function get(element) {
        return options.idHandler.get(element, true);
      },
      set: options.idHandler.set
    };
  } else {
    var idGenerator = idGeneratorMaker();
    var defaultIdHandler = idHandlerMaker({
      idGenerator: idGenerator,
      stateHandler: stateHandler
    });
    idHandler = defaultIdHandler;
  }

  //reporter is currently not an option to the listenTo function, so it should not be added to globalOptions.
  var reporter = options.reporter;
  if (!reporter) {
    //If options.reporter is false, then the reporter should be quiet.
    var quiet = reporter === false;
    reporter = reporterMaker(quiet);
  }

  //batchProcessor is currently not an option to the listenTo function, so it should not be added to globalOptions.
  var batchProcessor = getOption(options, "batchProcessor", batchProcessorMaker({
    reporter: reporter
  }));

  //Options to be used as default for the listenTo function.
  var globalOptions = {};
  globalOptions.callOnAdd = !!getOption(options, "callOnAdd", true);
  globalOptions.debug = !!getOption(options, "debug", false);
  var eventListenerHandler = listenerHandlerMaker(idHandler);
  var elementUtils = elementUtilsMaker({
    stateHandler: stateHandler
  });

  //The detection strategy to be used.
  var detectionStrategy;
  var desiredStrategy = getOption(options, "strategy", "object");
  var importantCssRules = getOption(options, "important", false);
  var strategyOptions = {
    reporter: reporter,
    batchProcessor: batchProcessor,
    stateHandler: stateHandler,
    idHandler: idHandler,
    important: importantCssRules
  };
  if (desiredStrategy === "scroll") {
    if (browserDetector.isLegacyOpera()) {
      reporter.warn("Scroll strategy is not supported on legacy Opera. Changing to object strategy.");
      desiredStrategy = "object";
    } else if (browserDetector.isIE(9)) {
      reporter.warn("Scroll strategy is not supported on IE9. Changing to object strategy.");
      desiredStrategy = "object";
    }
  }
  if (desiredStrategy === "scroll") {
    detectionStrategy = scrollStrategyMaker(strategyOptions);
  } else if (desiredStrategy === "object") {
    detectionStrategy = objectStrategyMaker(strategyOptions);
  } else {
    throw new Error("Invalid strategy name: " + desiredStrategy);
  }

  //Calls can be made to listenTo with elements that are still being installed.
  //Also, same elements can occur in the elements list in the listenTo function.
  //With this map, the ready callbacks can be synchronized between the calls
  //so that the ready callback can always be called when an element is ready - even if
  //it wasn't installed from the function itself.
  var onReadyCallbacks = {};

  /**
   * Makes the given elements resize-detectable and starts listening to resize events on the elements. Calls the event callback for each event for each element.
   * @public
   * @param {Options?} options Optional options object. These options will override the global options. Some options may not be overriden, such as idHandler.
   * @param {element[]|element} elements The given array of elements to detect resize events of. Single element is also valid.
   * @param {function} listener The callback to be executed for each resize event for each element.
   */
  function listenTo(options, elements, listener) {
    function onResizeCallback(element) {
      var listeners = eventListenerHandler.get(element);
      forEach(listeners, function callListenerProxy(listener) {
        listener(element);
      });
    }
    function addListener(callOnAdd, element, listener) {
      eventListenerHandler.add(element, listener);
      if (callOnAdd) {
        listener(element);
      }
    }

    //Options object may be omitted.
    if (!listener) {
      listener = elements;
      elements = options;
      options = {};
    }
    if (!elements) {
      throw new Error("At least one element required.");
    }
    if (!listener) {
      throw new Error("Listener required.");
    }
    if (isElement(elements)) {
      // A single element has been passed in.
      elements = [elements];
    } else if (isCollection(elements)) {
      // Convert collection to array for plugins.
      // TODO: May want to check so that all the elements in the collection are valid elements.
      elements = toArray(elements);
    } else {
      return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
    }
    var elementsReady = 0;
    var callOnAdd = getOption(options, "callOnAdd", globalOptions.callOnAdd);
    var onReadyCallback = getOption(options, "onReady", function noop() {});
    var debug = getOption(options, "debug", globalOptions.debug);
    forEach(elements, function attachListenerToElement(element) {
      if (!stateHandler.getState(element)) {
        stateHandler.initState(element);
        idHandler.set(element);
      }
      var id = idHandler.get(element);
      debug && reporter.log("Attaching listener to element", id, element);
      if (!elementUtils.isDetectable(element)) {
        debug && reporter.log(id, "Not detectable.");
        if (elementUtils.isBusy(element)) {
          debug && reporter.log(id, "System busy making it detectable");

          //The element is being prepared to be detectable. Do not make it detectable.
          //Just add the listener, because the element will soon be detectable.
          addListener(callOnAdd, element, listener);
          onReadyCallbacks[id] = onReadyCallbacks[id] || [];
          onReadyCallbacks[id].push(function onReady() {
            elementsReady++;
            if (elementsReady === elements.length) {
              onReadyCallback();
            }
          });
          return;
        }
        debug && reporter.log(id, "Making detectable...");
        //The element is not prepared to be detectable, so do prepare it and add a listener to it.
        elementUtils.markBusy(element, true);
        return detectionStrategy.makeDetectable({
          debug: debug,
          important: importantCssRules
        }, element, function onElementDetectable(element) {
          debug && reporter.log(id, "onElementDetectable");
          if (stateHandler.getState(element)) {
            elementUtils.markAsDetectable(element);
            elementUtils.markBusy(element, false);
            detectionStrategy.addListener(element, onResizeCallback);
            addListener(callOnAdd, element, listener);

            // Since the element size might have changed since the call to "listenTo", we need to check for this change,
            // so that a resize event may be emitted.
            // Having the startSize object is optional (since it does not make sense in some cases such as unrendered elements), so check for its existance before.
            // Also, check the state existance before since the element may have been uninstalled in the installation process.
            var state = stateHandler.getState(element);
            if (state && state.startSize) {
              var width = element.offsetWidth;
              var height = element.offsetHeight;
              if (state.startSize.width !== width || state.startSize.height !== height) {
                onResizeCallback(element);
              }
            }
            if (onReadyCallbacks[id]) {
              forEach(onReadyCallbacks[id], function (callback) {
                callback();
              });
            }
          } else {
            // The element has been unisntalled before being detectable.
            debug && reporter.log(id, "Element uninstalled before being detectable.");
          }
          delete onReadyCallbacks[id];
          elementsReady++;
          if (elementsReady === elements.length) {
            onReadyCallback();
          }
        });
      }
      debug && reporter.log(id, "Already detecable, adding listener.");

      //The element has been prepared to be detectable and is ready to be listened to.
      addListener(callOnAdd, element, listener);
      elementsReady++;
    });
    if (elementsReady === elements.length) {
      onReadyCallback();
    }
  }
  function uninstall(elements) {
    if (!elements) {
      return reporter.error("At least one element is required.");
    }
    if (isElement(elements)) {
      // A single element has been passed in.
      elements = [elements];
    } else if (isCollection(elements)) {
      // Convert collection to array for plugins.
      // TODO: May want to check so that all the elements in the collection are valid elements.
      elements = toArray(elements);
    } else {
      return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
    }
    forEach(elements, function (element) {
      eventListenerHandler.removeAllListeners(element);
      detectionStrategy.uninstall(element);
      stateHandler.cleanState(element);
    });
  }
  function initDocument(targetDocument) {
    detectionStrategy.initDocument && detectionStrategy.initDocument(targetDocument);
  }
  return {
    listenTo: listenTo,
    removeListener: eventListenerHandler.removeListener,
    removeAllListeners: eventListenerHandler.removeAllListeners,
    uninstall: uninstall,
    initDocument: initDocument
  };
};
function getOption(options, name, defaultValue) {
  var value = options[name];
  if ((value === undefined || value === null) && defaultValue !== undefined) {
    return defaultValue;
  }
  return value;
}

/***/ }),

/***/ 547:
/***/ (function(module) {



module.exports = function (options) {
  var getState = options.stateHandler.getState;

  /**
   * Tells if the element has been made detectable and ready to be listened for resize events.
   * @public
   * @param {element} The element to check.
   * @returns {boolean} True or false depending on if the element is detectable or not.
   */
  function isDetectable(element) {
    var state = getState(element);
    return state && !!state.isDetectable;
  }

  /**
   * Marks the element that it has been made detectable and ready to be listened for resize events.
   * @public
   * @param {element} The element to mark.
   */
  function markAsDetectable(element) {
    getState(element).isDetectable = true;
  }

  /**
   * Tells if the element is busy or not.
   * @public
   * @param {element} The element to check.
   * @returns {boolean} True or false depending on if the element is busy or not.
   */
  function isBusy(element) {
    return !!getState(element).busy;
  }

  /**
   * Marks the object is busy and should not be made detectable.
   * @public
   * @param {element} element The element to mark.
   * @param {boolean} busy If the element is busy or not.
   */
  function markBusy(element, busy) {
    getState(element).busy = !!busy;
  }
  return {
    isDetectable: isDetectable,
    markAsDetectable: markAsDetectable,
    isBusy: isBusy,
    markBusy: markBusy
  };
};

/***/ }),

/***/ 110:
/***/ (function(module) {



module.exports = function () {
  var idCount = 1;

  /**
   * Generates a new unique id in the context.
   * @public
   * @returns {number} A unique id in the context.
   */
  function generate() {
    return idCount++;
  }
  return {
    generate: generate
  };
};

/***/ }),

/***/ 535:
/***/ (function(module) {



module.exports = function (options) {
  var idGenerator = options.idGenerator;
  var getState = options.stateHandler.getState;

  /**
   * Gets the resize detector id of the element.
   * @public
   * @param {element} element The target element to get the id of.
   * @returns {string|number|null} The id of the element. Null if it has no id.
   */
  function getId(element) {
    var state = getState(element);
    if (state && state.id !== undefined) {
      return state.id;
    }
    return null;
  }

  /**
   * Sets the resize detector id of the element. Requires the element to have a resize detector state initialized.
   * @public
   * @param {element} element The target element to set the id of.
   * @returns {string|number|null} The id of the element.
   */
  function setId(element) {
    var state = getState(element);
    if (!state) {
      throw new Error("setId required the element to have a resize detection state.");
    }
    var id = idGenerator.generate();
    state.id = id;
    return id;
  }
  return {
    get: getId,
    set: setId
  };
};

/***/ }),

/***/ 284:
/***/ (function(module) {



module.exports = function (idHandler) {
  var eventListeners = {};

  /**
   * Gets all listeners for the given element.
   * @public
   * @param {element} element The element to get all listeners for.
   * @returns All listeners for the given element.
   */
  function getListeners(element) {
    var id = idHandler.get(element);
    if (id === undefined) {
      return [];
    }
    return eventListeners[id] || [];
  }

  /**
   * Stores the given listener for the given element. Will not actually add the listener to the element.
   * @public
   * @param {element} element The element that should have the listener added.
   * @param {function} listener The callback that the element has added.
   */
  function addListener(element, listener) {
    var id = idHandler.get(element);
    if (!eventListeners[id]) {
      eventListeners[id] = [];
    }
    eventListeners[id].push(listener);
  }
  function removeListener(element, listener) {
    var listeners = getListeners(element);
    for (var i = 0, len = listeners.length; i < len; ++i) {
      if (listeners[i] === listener) {
        listeners.splice(i, 1);
        break;
      }
    }
  }
  function removeAllListeners(element) {
    var listeners = getListeners(element);
    if (!listeners) {
      return;
    }
    listeners.length = 0;
  }
  return {
    get: getListeners,
    add: addListener,
    removeListener: removeListener,
    removeAllListeners: removeAllListeners
  };
};

/***/ }),

/***/ 256:
/***/ (function(module) {



/* global console: false */

/**
 * Reporter that handles the reporting of logs, warnings and errors.
 * @public
 * @param {boolean} quiet Tells if the reporter should be quiet or not.
 */
module.exports = function (quiet) {
  function noop() {
    //Does nothing.
  }
  var reporter = {
    log: noop,
    warn: noop,
    error: noop
  };
  if (!quiet && window.console) {
    var attachFunction = function attachFunction(reporter, name) {
      //The proxy is needed to be able to call the method with the console context,
      //since we cannot use bind.
      reporter[name] = function reporterProxy() {
        var f = console[name];
        if (f.apply) {
          //IE9 does not support console.log.apply :)
          f.apply(console, arguments);
        } else {
          for (var i = 0; i < arguments.length; i++) {
            f(arguments[i]);
          }
        }
      };
    };
    attachFunction(reporter, "log");
    attachFunction(reporter, "warn");
    attachFunction(reporter, "error");
  }
  return reporter;
};

/***/ }),

/***/ 613:
/***/ (function(module) {



var prop = "_erd";
function initState(element) {
  element[prop] = {};
  return getState(element);
}
function getState(element) {
  return element[prop];
}
function cleanState(element) {
  delete element[prop];
}
module.exports = {
  initState: initState,
  getState: getState,
  cleanState: cleanState
};

/***/ }),

/***/ 120:
/***/ (function(__unused_webpack_module, __webpack_exports__) {

/* harmony default export */ __webpack_exports__.Z = ({
  code: "<template>\n  <div\n    :class=\"{ 'is-show-angle': value.isShowAngle, 'is-screen-bg': value.isShowBg }\"\n    class=\"dog-screen-panel\"\n  >\n    <div v-if=\"value.isShowHead\" class=\"screen-panel-head\">\n      <div :title=\"value.name\" class=\"head-title\">\n        <label>{{ value.name }}</label>\n      </div>\n    </div>\n    <div class=\"screen-panel-body\">\n      <div class=\"panel-content\"></div>\n    </div>\n    <div class=\"border-foot\"></div>\n  </div>\n</template>\n\n<script>\nconst props = {\n  name: '\u5927\u5C4FVue\u6A21\u677F',\n  isShowBg: true, // \u663E\u793A\u80CC\u666F\n  isShowAngle: true, // \u663E\u793A\u56DB\u89D2\n  isShowHead: true // \u663E\u793A\u5934\n}\nexport default {\n  props: {\n    value: {\n      type: Object,\n      default() {\n        return Object.assign({}, props)\n      }\n    }\n  },\n\n  created() {\n    if (this.value.name === undefined) {\n      Object.assign(this.value, props)\n    }\n  },\n\n  mounted() {\n    this.initView()\n  },\n\n  methods: {\n    // \u521D\u59CB\u5316\u89C6\u56FE\n    initView() {}\n  }\n}\n</script>\n\n<style scoped>\n.dog-screen-panel {\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n  height: 100%;\n  box-shadow: inset 0 0 30px #07417a;\n}\n.dog-screen-panel .screen-panel-head {\n  padding: 0 16px;\n  width: 100%;\n}\n.screen-panel-head .head-title {\n  height: 48px;\n  font-size: 1.4rem;\n  color: #fff;\n  text-align: center;\n  line-height: 48px;\n  border-bottom: 1px solid rgba(255, 255, 255, 0.2);\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden;\n}\n.dog-screen-panel .screen-panel-body {\n  height: 100%;\n}\n.screen-panel-head + .screen-panel-body {\n  height: calc(100% - 48px);\n}\n.screen-panel-body .panel-content {\n  padding: 8px 16px;\n  color: #fff;\n}\n.dog-screen-panel.is-screen-bg {\n  background: #000c3b;\n}\n.dog-screen-panel.is-show-angle:before {\n  position: absolute;\n  width: 1rem;\n  height: 1rem;\n  content: '';\n  border-top: 2px solid #26c6f0;\n  border-left: 2px solid #26c6f0;\n  left: -1px;\n  top: -1px;\n}\n.dog-screen-panel.is-show-angle:after {\n  position: absolute;\n  width: 1rem;\n  height: 1rem;\n  content: '';\n  border-top: 2px solid #26c6f0;\n  border-right: 2px solid #26c6f0;\n  right: -1px;\n  top: -1px;\n}\n.dog-screen-panel.is-show-angle .border-foot:before {\n  position: absolute;\n  width: 1rem;\n  height: 1rem;\n  content: '';\n  border-bottom: 2px solid #26c6f0;\n  border-left: 2px solid #26c6f0;\n  left: -1px;\n  bottom: -1px;\n}\n.dog-screen-panel.is-show-angle .border-foot:after {\n  position: absolute;\n  width: 1rem;\n  height: 1rem;\n  content: '';\n  border-bottom: 2px solid #26c6f0;\n  border-right: 2px solid #26c6f0;\n  right: -1px;\n  bottom: -1px;\n}\n</style>\n"
});

/***/ }),

/***/ 62:
/***/ (function(__unused_webpack_module, __webpack_exports__) {

/* harmony default export */ __webpack_exports__.Z = ({
  code: "<template>\n  <div\n    :class=\"{'is-desktop-bg': value.isShowBg }\"\n    class=\"dog-desktop-panel\"\n  >\n    <div v-if=\"value.isShowHead\" class=\"desktop-panel-head\">\n      <div :title=\"value.name\" class=\"head-title\">\n        <label>{{ value.name }}</label>\n      </div>\n    </div>\n    <div class=\"desktop-panel-body\">\n      <div class=\"panel-content\"></div>\n    </div>\n    <div class=\"border-foot\"></div>\n  </div>\n</template>\n\n<script>\nconst props = {\n  name: 'Vue\u6A21\u677F',\n  isShowBg: true, // \u663E\u793A\u80CC\u666F\n  isShowHead: true // \u663E\u793A\u5934\n}\nexport default {\n  props: {\n    value: {\n      type: Object,\n      default() {\n        return Object.assign({}, props)\n      }\n    }\n  },\n  \n  data () {\n\t  return {}\n  },\n\n  created() {\n    if (this.value.name === undefined) {\n      Object.assign(this.value, props)\n    }\n  },\n\n  mounted() {\n    this.initView()\n  },\n\n  methods: {\n    // \u521D\u59CB\u5316\u89C6\u56FE\n    initView() {}\n  }\n}\n</script>\n\n<style scoped>\n.dog-desktop-panel {\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n  height: 100%;\n  border: 1px solid #e0e0e0;\n  border-radius: 4px;\n  box-shadow: 0 2px 12px 0 rgba(0,0,0,.1);\n}\n.dog-desktop-panel .desktop-panel-head {\n  padding: 0 16px;\n  width: 100%;\n}\n.desktop-panel-head .head-title {\n  height: 40px;\n  font-size: 14px;\n  color: #333;\n  text-align: center;\n  line-height: 40px;\n  border-bottom: 1px solid #e0e0e0;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden;\n}\n.dog-desktop-panel .desktop-panel-body {\n  height: 100%;\n}\n.desktop-panel-head + .desktop-panel-body {\n  height: calc(100% - 40px);\n}\n.dog-desktop-panel.is-desktop-bg {\n  background: #fff;\n}\n.desktop-panel-body .panel-content {\n  padding: 8px 16px;\n  color: #666;\n}\n</style>\n",
  property: "<template>\n  <div class=\"d-vue-property\">\n    Template Property\n  </div>\n</template>\n\n<script>\nexport default {\n  components: {},\n  props: {\n    value: {\n      type: Object,\n      default () {\n        return {}\n      }\n    }\n  },\n  data () {\n    return {}\n  },\n  methods: {}\n}\n</script>\n\n<style>\n.d-vue-property {\n  padding: 8px;\n  height: 100%;\n  text-align: center;\n  background: #fff;\n  color: #666;\n}\n</style>\n"
});

/***/ }),

/***/ 211:
/***/ (function(module) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtJJREFUeNrEV0toE1EUPYnRNMWYUBoTP7XQ0nZhDdIIirrQIkpdZKMiiLgQPzu31gp1I1pwFRA3goguCm4UkdaKNiKCgo1Cq9HGD/0JiaWh1aJtFep9vAmZN53P64QkFy7Mzdy8e+b+nyMSW4RCm4k7iFuJQygOpYn7ia8Sv2c/OJUXR4kTxMeKaBzK2czGgGITLuItxHeIV6F05FZsJpkH2ktsPEfMZrtTiXm5qJWFIGilFagAes/InTiRBUangJ4k0DdqqR5kABxWWi1B+U/aWMV5VwNwZAy48BiYnDNUdzhlDm1aa8+/WzcBsai5jkvmoPpqUX7yAZid1/Gnl7xVC3hUKd1IhXcyDNwaLABAwCvKHc/M8+XmYR6GHEVNAEiFoFHVmlJpc10W7/OPluaFEVkCaPJrDPwy1g2vAypXAsPTvBrUtCNkMwRhTQVkDAD4KnjCzf8Dbrzipaj+cp/bJoCGgCgnxvX1zm4HvG7OnfuA6d9ylWIZgjpNBbzN6Os9/AgMqfLDXym+H5mxCaB6df75z4JxU/n0g8rtHnCRGk9aJ0xtzdw7ywagjuN41lyXbRZ9KeDQ3aXvjrcA909Qd6SSXOGUBHCgVuPGKbm4Ruv1f/d7qET3ANcOSgLY4BPlL5NyAHbXifJtqorERF5+kJSsgtAaUf4+IzE3/HwQ5Sg7C1x/Qw/Ee8kzO8mrL75JAmheL8oS4xWX9ovy81T+Of6Vs3QS1lSJX2K1M8TaxLbN/nPlpc1pyA5UT7WxrL4O2xUiNbzMPJrFrrPH2mMu2SWEzfaBc3JJyPrF5V7gdboAAHaXEDaEup7KGTcFoF1CZAz3DwPdQ6YrmC6ARb29sPud8eRj9Jl6ws+5/HxYjlEtgIzebYi5UNaNhVzVWBnGy3gviDuVi+JCGYz/Je5iAChtcLrEIJitU8SDuU7ILooRlntKThSLMoqNbYpN/BdgAF4UsUgZhWRGAAAAAElFTkSuQmCC";

/***/ }),

/***/ 819:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__819__;

/***/ }),

/***/ 854:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__854__;

/***/ }),

/***/ 68:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__68__;

/***/ }),

/***/ 470:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__470__;

/***/ }),

/***/ 759:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__759__;

/***/ }),

/***/ 785:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__785__;

/***/ }),

/***/ 443:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__443__;

/***/ }),

/***/ 313:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__313__;

/***/ }),

/***/ 722:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__722__;

/***/ }),

/***/ 924:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__924__;

/***/ }),

/***/ 10:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__10__;

/***/ }),

/***/ 410:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__410__;

/***/ }),

/***/ 748:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__748__;

/***/ }),

/***/ 557:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__557__;

/***/ }),

/***/ 342:
/***/ (function(module) {

module.exports = __WEBPACK_EXTERNAL_MODULE__342__;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		__webpack_require__.p = "";
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
!function() {
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  App: function() { return /* reexport */ app; },
  Vue: function() { return /* reexport */ (external_vue_default()); },
  VueRouter: function() { return /* reexport */ (external_vue_router_default()); },
  Vuex: function() { return /* reexport */ (external_vuex_default()); },
  bootstrap: function() { return /* reexport */ bootstrap; },
  store: function() { return /* reexport */ store; }
});

;// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
/* eslint-disable no-var */
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (false) { var getCurrentScript; }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(748);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);
// EXTERNAL MODULE: external "vue-router"
var external_vue_router_ = __webpack_require__(557);
var external_vue_router_default = /*#__PURE__*/__webpack_require__.n(external_vue_router_);
// EXTERNAL MODULE: external "vuex"
var external_vuex_ = __webpack_require__(342);
var external_vuex_default = /*#__PURE__*/__webpack_require__.n(external_vuex_);
// EXTERNAL MODULE: external "element-ui"
var external_element_ui_ = __webpack_require__(10);
var external_element_ui_default = /*#__PURE__*/__webpack_require__.n(external_element_ui_);
// EXTERNAL MODULE: external "element-ui/lib/theme-chalk/index.css"
var index_css_ = __webpack_require__(410);
// EXTERNAL MODULE: external "@daelui/vdog/dist/directives.js"
var directives_js_ = __webpack_require__(313);
var directives_js_default = /*#__PURE__*/__webpack_require__.n(directives_js_);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/index.js"
var index_js_ = __webpack_require__(68);
var index_js_default = /*#__PURE__*/__webpack_require__.n(index_js_);
// EXTERNAL MODULE: external "@daelui/dogui/dist/css/boost.min.css"
var boost_min_css_ = __webpack_require__(759);
// EXTERNAL MODULE: external "@daelui/dogui/dist/css/theme/black.min.css"
var black_min_css_ = __webpack_require__(785);
// EXTERNAL MODULE: external "@daelui/dogjs/dist/components.js"
var components_js_ = __webpack_require__(819);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/mapp/deploy/app.vue?vue&type=template&id=465128ea
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "dog"
  }, [_c('kennel', {
    ref: "kennel",
    attrs: {
      "navList": _vm.navList,
      "navClass": 'd-nav-bar-black',
      "navActiveId": _vm.navActiveId,
      "isShowFooter": _vm.isShowFooter,
      "isLogin": _vm.isLogin
    },
    on: {
      "login": _vm.handleLogin,
      "logout": _vm.handleLogout
    },
    scopedSlots: _vm._u([{
      key: "header-side",
      fn: function fn() {
        return [_c('div', {
          staticClass: "d-header-side"
        }, [_c('a', {
          staticClass: "d-text-link header-logo",
          attrs: {
            "href": _vm.getMicroPath('#/')
          }
        }, [_c('img', {
          staticClass: "d-img logo-image",
          attrs: {
            "src": __webpack_require__(211)
          }
        }), _c('span', {
          staticClass: "logo-text"
        }, [_vm._v(_vm._s(_vm.appName))])])])];
      },
      proxy: true
    }, {
      key: "header-menu",
      fn: function fn() {
        return [_c('div', {
          staticClass: "d-header-menu"
        })];
      },
      proxy: true
    }, {
      key: "layout-body",
      fn: function fn() {
        return [_c('div', {
          staticClass: "design-body d-layout-body d-bg-fog"
        }, [_c('div', {
          staticClass: "design-render"
        }, [_c('router-view')], 1)])];
      },
      proxy: true
    }])
  }), _c('Login')], 1);
};
var staticRenderFns = [];

;// CONCATENATED MODULE: ./src/views/mapp/deploy/app.vue?vue&type=template&id=465128ea

// EXTERNAL MODULE: external "@daelui/vdog/dist/pages.js"
var pages_js_ = __webpack_require__(722);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/login/login.vue?vue&type=template&id=2162d6ce&scoped=true
var loginvue_type_template_id_2162d6ce_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _vm.isShow ? _c('div', {
    staticClass: "login-wraper"
  }, [_c('el-form', {
    ref: "form",
    staticClass: "login-box dam-fade-in",
    attrs: {
      "model": _vm.form,
      "rules": _vm.rules,
      "label-width": "64px"
    }
  }, [_c('h3', {
    staticClass: "login-title"
  }, [_vm._v("欢迎登陆")]), _c('el-form-item', {
    attrs: {
      "label": "账号",
      "prop": "account"
    }
  }, [_c('el-input', {
    nativeOn: {
      "keyup": function keyup($event) {
        if (!$event.type.indexOf('key') && _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")) return null;
        return _vm.submitForm('form');
      }
    },
    model: {
      value: _vm.form.account,
      callback: function callback($$v) {
        _vm.$set(_vm.form, "account", $$v);
      },
      expression: "form.account"
    }
  })], 1), _c('el-form-item', {
    attrs: {
      "label": "密码",
      "prop": "password"
    }
  }, [_c('el-input', {
    attrs: {
      "show-password": ""
    },
    nativeOn: {
      "keyup": function keyup($event) {
        if (!$event.type.indexOf('key') && _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")) return null;
        return _vm.submitForm('form');
      }
    },
    model: {
      value: _vm.form.password,
      callback: function callback($$v) {
        _vm.$set(_vm.form, "password", $$v);
      },
      expression: "form.password"
    }
  })], 1), _c('div', {
    staticClass: "login-btn"
  }, [_c('el-button', {
    attrs: {
      "type": "primary",
      "size": "medium"
    },
    on: {
      "click": function click($event) {
        return _vm.submitForm('form');
      }
    }
  }, [_vm._v("确定")]), _c('el-button', {
    attrs: {
      "size": "medium"
    },
    on: {
      "click": function click($event) {
        _vm.isShow = false;
      }
    }
  }, [_vm._v("取消")])], 1)], 1)], 1) : _vm._e();
};
var loginvue_type_template_id_2162d6ce_scoped_true_staticRenderFns = [];

;// CONCATENATED MODULE: ./src/config/application.js
/**
 * @description 环境配置
*/

var application = (window.application || {}).tigerlair || {};
/* harmony default export */ var config_application = (application);
;// CONCATENATED MODULE: ./src/service/components/resolver.js
/**
 * @description 解析器
 * @author Rid King
 * @since 2018-07-06
*/



var resolver = new components_js_.Resolver({
  application: config_application
});
/* harmony default export */ var components_resolver = (resolver);
;// CONCATENATED MODULE: ./src/service/components/ds.js
/**
 * @description 数据请求组件
 * @since 2019-03-06
 * @author Rid King
*/




// 设置token
components_js_.ds.$on('onBeforeRequest', function (options) {
  let token = localStorage.getItem('daelui-token');
  if (token) {
    options.headers.token = token;
  }
});
// 设置解析器
components_js_.ds.setOptions({
  resolver: components_resolver
});
/* harmony default export */ var ds = (components_js_.ds);

;// CONCATENATED MODULE: ./src/service/api/user.api.js
/**
* @description 用户管理(※自动化生成,勿手动更改※)
*/

/* harmony default export */ var user_api = ({
  "login": {
    "url": "{host}/{pot}/user/login",
    "desc": "登录",
    "method": "post"
  },
  "logout": {
    "url": "{host}/{pot}/user/logout",
    "desc": "登出",
    "method": "post"
  },
  "queryUserInfo": {
    "url": "{host}/{pot}/user/queryUserInfo",
    "desc": "登录",
    "method": "get"
  }
});
;// CONCATENATED MODULE: ./src/service/service/user.service.js
/**
* @description 用户管理(※自动化生成,勿手动更改※)
*/





// api地址解析
const api = components_resolver.solveAPI(user_api);
/* harmony default export */ var user_service = ({
  /**
   * @function 登录
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  login() {
    for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
      params[_key] = arguments[_key];
    }
    return ds.post(api.login.url, ...params, {});
  },
  /**
   * @function 登出
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  logout() {
    for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      params[_key2] = arguments[_key2];
    }
    return ds.post(api.logout.url, ...params, {});
  },
  /**
   * @function 获取用户信息
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryUserInfo() {
    for (var _len3 = arguments.length, params = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      params[_key3] = arguments[_key3];
    }
    return ds.get(api.queryUserInfo.url, ...params, {});
  }
});

;// CONCATENATED MODULE: ./src/service/action/user.action.js
/**
* @description 用户管理
*/




// 服务
/* harmony default export */ var user_action = (Object.assign({}, user_service, {}));

// EXTERNAL MODULE: external "@daelui/dogjs/dist/core.js"
var core_js_ = __webpack_require__(854);
;// CONCATENATED MODULE: ./src/components/ebus.js
/**
 * @function 事件管理
*/


const ebus = new core_js_.Model();
/* harmony default export */ var components_ebus = (ebus);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/login/login.vue?vue&type=script&lang=js


/* harmony default export */ var loginvue_type_script_lang_js = ({
  name: "Login",
  data() {
    return {
      isShow: false,
      form: {
        account: '',
        password: ''
      },
      rules: {
        account: [{
          required: true,
          message: '请输入账号',
          trigger: 'blur'
        }],
        password: [{
          required: true,
          message: '请输入密码',
          trigger: 'change'
        }]
      }
    };
  },
  created() {
    components_ebus.$on('onShowLogin', () => {
      this.isShow = true;
    });
    components_ebus.$on('onLoginSuccess', () => {
      this.isShow = false;
    });
  },
  methods: {
    // 提交
    submitForm(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          user_action.login({
            username: this.form.account,
            password: this.form.password
          }).then(res => {
            let data = res.data || {};
            if (data.token) {
              // 保存token
              localStorage.setItem('daelui-token', data.token);
              this.$message({
                message: '登录成功',
                type: 'success',
                showClose: true
              });
              // 关闭窗口
              components_ebus.$emit('onLoginSuccess');
              // 登录成功
              components_ebus.$emit('onLoginComplete');
              return true;
            } else {
              this.$message({
                message: res.msg ? res.msg : '登录失败',
                type: 'error',
                showClose: true
              });
              return false;
            }
          });
        } else {
          this.$message.error('请输入正确用户名或密码!');
          return false;
        }
      });
    }
  }
});
;// CONCATENATED MODULE: ./src/components/login/login.vue?vue&type=script&lang=js
 /* harmony default export */ var login_loginvue_type_script_lang_js = (loginvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./node_modules/mini-css-extract-plugin/dist/loader.js??clonedRuleSet-74.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-74.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-74.use[2]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-74.use[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/login/login.vue?vue&type=style&index=0&id=2162d6ce&prod&lang=less&scoped=true
// extracted by mini-css-extract-plugin

;// CONCATENATED MODULE: ./src/components/login/login.vue?vue&type=style&index=0&id=2162d6ce&prod&lang=less&scoped=true

// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);
;// CONCATENATED MODULE: ./src/components/login/login.vue



;


/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  login_loginvue_type_script_lang_js,
  loginvue_type_template_id_2162d6ce_scoped_true_render,
  loginvue_type_template_id_2162d6ce_scoped_true_staticRenderFns,
  false,
  null,
  "2162d6ce",
  null
  
)

/* harmony default export */ var login = (component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-82.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/mapp/deploy/app.vue?vue&type=script&lang=js





/* harmony default export */ var appvue_type_script_lang_js = ({
  name: 'mapp',
  components: {
    Login: login,
    Kennel: pages_js_.Kennel
  },
  props: {
    appName: {
      default: '微应用'
    },
    navList: {
      type: Array,
      default: []
    }
  },
  computed: (0,external_vuex_.mapState)({
    // 选中的导航id
    navActiveId: state => state.navActiveId
  }),
  watch: {
    '$route': function $route() {
      this.syncState();
    }
  },
  data() {
    return {
      // 是否显示底部
      isShowFooter: false,
      // 是否登录
      isLogin: false
    };
  },
  created() {
    ds.$on('onAfterRequest', result => {
      let options = result.options;
      if (result && result.response && String(result.response.status) === '403') {
        // 弹出窗口
        components_ebus.$emit('onShowLogin');
        // 登录超时
        components_ebus.$emit('onLoginExpire');
      } else if (result && result.response && String(result.response.status) === '401') {
        // 弹出窗口
        this.$message({
          message: '暂无权限进行此操作',
          type: 'error',
          showClose: true
        });
      } else {
        if (options.isShowError !== false) {}
      }
    });
  },
  beforeMount() {
    // 标题设置
    if (this.appName) {
      document.title = this.appName;
    }
  },
  mounted() {
    // 隐藏加载条
    let lm = document.querySelector('.d-loading-maker');
    if (lm) {
      lm.style.display = 'none';
    }
    // 初始化
    this.initView();
  },
  methods: {
    initView() {
      let rpath = location.hash.replace('#', '').replace(/\?.*/, '') || '/'; // this.$route.path
      let defaultNav = this.navList.find(item => {
        return item.path === rpath;
      });
      // 初始化的路由地址未与菜单匹配时
      if (!defaultNav) {
        defaultNav = this.navList.find(item => item.isDefault);
        if (!defaultNav) {
          defaultNav = this.navList[0];
        }
        if (defaultNav) {
          this.$router.push(defaultNav.path);
        }
      }
    },
    // 同步状态
    syncState() {
      let rpath = this.$route.path;
      let node = this.navList.find(item => {
        return item.path === rpath;
      });
      // 动态标题
      if (node && node.name) {
        document.title = node.name;
      }
    }
  }
});
;// CONCATENATED MODULE: ./src/views/mapp/deploy/app.vue?vue&type=script&lang=js
 /* harmony default export */ var deploy_appvue_type_script_lang_js = (appvue_type_script_lang_js); 
;// CONCATENATED MODULE: ./node_modules/mini-css-extract-plugin/dist/loader.js??clonedRuleSet-74.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-74.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-74.use[2]!./node_modules/less-loader/dist/cjs.js??clonedRuleSet-74.use[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/views/mapp/deploy/app.vue?vue&type=style&index=0&id=465128ea&prod&lang=less
// extracted by mini-css-extract-plugin

;// CONCATENATED MODULE: ./src/views/mapp/deploy/app.vue?vue&type=style&index=0&id=465128ea&prod&lang=less

;// CONCATENATED MODULE: ./src/views/mapp/deploy/app.vue



;


/* normalize component */

var app_component = (0,componentNormalizer/* default */.Z)(
  deploy_appvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var app = (app_component.exports);
;// CONCATENATED MODULE: ./src/service/api/viewrelease.api.js
/**
* @description 注册表管理(※自动化生成,勿手动更改※)
*/

/* harmony default export */ var viewrelease_api = ({
  "queryItem": {
    "url": "{host}/tigerlair/viewrelease/item",
    "desc": "查询页面",
    "method": "get"
  },
  "queryItemSaas": {
    "url": "{host}/tigerlair/saas/viewrelease/item",
    "desc": "查询页面",
    "method": "get"
  },
  "removeItem": {
    "url": "{host}/tigerlair/viewrelease/item",
    "desc": "删除组件",
    "method": "delete"
  },
  "addItem": {
    "url": "{host}/tigerlair/viewrelease/item",
    "desc": "添加组件",
    "method": "put"
  },
  "queryListPage": {
    "url": "{host}/tigerlair/viewrelease/list/page",
    "desc": "查询组件列表",
    "method": "get"
  },
  "queryListAll": {
    "url": "{host}/tigerlair/viewrelease/list/all",
    "desc": "查询组件",
    "method": "get"
  }
});
;// CONCATENATED MODULE: ./src/service/service/viewrelease.service.js
/**
* @description 注册表管理(※自动化生成,勿手动更改※)
*/





// api地址解析
const viewrelease_service_api = components_resolver.solveAPI(viewrelease_api);
/* harmony default export */ var viewrelease_service = ({
  /**
   * @function 查询组件列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage() {
    for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
      params[_key] = arguments[_key];
    }
    return ds.get(viewrelease_service_api.queryListPage.url, ...params, {});
  },
  /**
   * @function 添加组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem() {
    for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      params[_key2] = arguments[_key2];
    }
    return ds.put(viewrelease_service_api.addItem.url, ...params, {});
  },
  /**
   * @function 删除组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem() {
    for (var _len3 = arguments.length, params = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      params[_key3] = arguments[_key3];
    }
    return ds.delete(viewrelease_service_api.removeItem.url, ...params, {});
  },
  /**
   * @function 查询组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem() {
    for (var _len4 = arguments.length, params = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      params[_key4] = arguments[_key4];
    }
    return ds.get(viewrelease_service_api.queryItem.url, ...params, {});
  },
  /**
   * @function 查询组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAll() {
    for (var _len5 = arguments.length, params = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      params[_key5] = arguments[_key5];
    }
    return ds.get(viewrelease_service_api.queryListAll.url, ...params, {});
  },
  /**
   * @function 查询页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItemSaas() {
    for (var _len6 = arguments.length, params = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
      params[_key6] = arguments[_key6];
    }
    return ds.get(viewrelease_service_api.queryItemSaas.url, ...params, {});
  }
});

;// CONCATENATED MODULE: ./src/service/action/viewrelease.action.js
/**
* @description 注册表管理
*/





// 服务
/* harmony default export */ var viewrelease_action = (Object.assign({}, viewrelease_service, {}));

;// CONCATENATED MODULE: ./src/service/api/viewcomp.api.js
/**
* @description 视图组件管理(※自动化生成,勿手动更改※)
*/

/* harmony default export */ var viewcomp_api = ({
  "queryItem": {
    "url": "{host}/tigerlair/viewcomp/item",
    "desc": "查询组件",
    "method": "get"
  },
  "removeItem": {
    "url": "{host}/tigerlair/viewcomp/item",
    "desc": "删除组件",
    "method": "delete"
  },
  "editItem": {
    "url": "{host}/tigerlair/viewcomp/item",
    "desc": "编辑组件",
    "method": "post"
  },
  "addItem": {
    "url": "{host}/tigerlair/viewcomp/item",
    "desc": "添加组件",
    "method": "put"
  },
  "queryListPage": {
    "url": "{host}/tigerlair/viewcomp/list/page",
    "desc": "查询组件列表",
    "method": "get"
  },
  "queryListAll": {
    "url": "{host}/tigerlair/viewcomp/list/all",
    "desc": "查询组件",
    "method": "get"
  },
  "queryListAllSaas": {
    "url": "{host}/tigerlair/saas/viewcomp/list/all",
    "desc": "查询组件",
    "method": "get"
  }
});
;// CONCATENATED MODULE: ./src/service/service/viewcomp.service.js
/**
* @description 视图组件管理(※自动化生成,勿手动更改※)
*/





// api地址解析
const viewcomp_service_api = components_resolver.solveAPI(viewcomp_api);
/* harmony default export */ var viewcomp_service = ({
  /**
   * @function 查询组件列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage() {
    for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
      params[_key] = arguments[_key];
    }
    return ds.get(viewcomp_service_api.queryListPage.url, ...params, {});
  },
  /**
   * @function 添加组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem() {
    for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      params[_key2] = arguments[_key2];
    }
    return ds.put(viewcomp_service_api.addItem.url, ...params, {});
  },
  /**
   * @function 编辑组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem() {
    for (var _len3 = arguments.length, params = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      params[_key3] = arguments[_key3];
    }
    return ds.post(viewcomp_service_api.editItem.url, ...params, {});
  },
  /**
   * @function 删除组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem() {
    for (var _len4 = arguments.length, params = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      params[_key4] = arguments[_key4];
    }
    return ds.delete(viewcomp_service_api.removeItem.url, ...params, {});
  },
  /**
   * @function 查询组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem() {
    for (var _len5 = arguments.length, params = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      params[_key5] = arguments[_key5];
    }
    return ds.get(viewcomp_service_api.queryItem.url, ...params, {});
  },
  /**
   * @function 查询组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAll() {
    for (var _len6 = arguments.length, params = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
      params[_key6] = arguments[_key6];
    }
    return ds.get(viewcomp_service_api.queryListAll.url, ...params, {});
  },
  /**
   * @function 查询组件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAllSaas() {
    for (var _len7 = arguments.length, params = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
      params[_key7] = arguments[_key7];
    }
    return ds.get(viewcomp_service_api.queryListAllSaas.url, ...params, {});
  }
});

// EXTERNAL MODULE: external "@daelui/dogjs/dist/libs/coder.js"
var coder_js_ = __webpack_require__(470);
var coder_js_default = /*#__PURE__*/__webpack_require__.n(coder_js_);
;// CONCATENATED MODULE: ./src/service/action/viewcomp.action.js
/**
* @description 视图组件管理
*/






// 服务
/* harmony default export */ var viewcomp_action = (Object.assign({}, viewcomp_service, {
  queryAllListOption(isAll) {
    // 查询文档列表
    return viewcomp_service.queryListPage({
      pageSize: 1000
    }).then(res => {
      let list = res.data.list;
      list = Array.isArray(list) ? list : [];
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        };
      });
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        });
      }
      return list;
    });
  },
  // 查询所有视图组件
  queryAllComponents() {
    let {
      isSaas
    } = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    let defer = Promise.resolve({});
    if (isSaas) {
      defer = this.queryListAllSaas();
    } else {
      defer = this.queryListAll();
    }
    return defer.then(res => {
      let data = res.data;
      data = Array.isArray(data) ? data : [];
      let cps = data.map(item => {
        return {
          id: item.id,
          name: item.name,
          text: item.text,
          version: item.version,
          icon: item.icon,
          description: item.description,
          component: function component() {
            return coder_js_default().vue.resolve(item.viewCode, (external_vue_default()), $pig);
          },
          url: item.url,
          propertyComponent: function propertyComponent() {
            return coder_js_default().vue.resolve(item.propertyCode, (external_vue_default()), $pig);
          },
          propertyUrl: item.propertyUrl
        };
      });
      return cps;
    });
  }
}));

;// CONCATENATED MODULE: ./src/views/dview/page/components/components.js
/**
 * @description 视图组件列表
*/

const componentCollection = [{
  id: 1,
  name: 'ddesign-vue-template',
  text: 'Vue模板',
  version: '1.0.0',
  icon: '',
  description: '',
  mainComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(641)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  propertyComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(492)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  main: '',
  property: '',
  repository: {
    main: '{rootUrl}/name/verion/index.js',
    property: '{rootUrl}/name/verion/property.js'
  },
  category: '基础'
}, {
  id: 2,
  name: 'ddesign-screen-template',
  text: '大屏Vue模板',
  version: '1.0.0',
  icon: '',
  description: '',
  mainComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(276)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  propertyComponent: resolve => Promise.resolve(/* AMD require */).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(351)]; (resolve).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}.bind(this))['catch'](__webpack_require__.oe),
  main: '',
  property: '',
  repository: {
    main: '{rootUrl}/name/verion/index.js',
    property: '{rootUrl}/name/verion/property.js'
  },
  category: '基础'
}];
/* harmony default export */ var components = (componentCollection);
;// CONCATENATED MODULE: ./src/views/mapp/deploy/app.js














(external_vue_default()).config.productionTip = false;
external_vue_default().use((external_vue_router_default()));
external_vue_default().use((external_vuex_default()));
external_vue_default().use((external_element_ui_default()));

// 指令
directives_js_default()((external_vue_default()));
const win = typeof self !== 'undefined' ? window : __webpack_require__.g;
var $dog = win.$dog;
if ($dog) {
  for (let key in $dog) {
    (index_js_default())[key] = $dog[key];
  }
}
win.$dog = $dog = (index_js_default());
win.$dog.Vue = win.$dog.Vue || (external_vue_default());
win.Vue = (external_vue_default());
let microPath = '';
(external_vue_default()).prototype.getMicroPath = function (path) {
  return path.replace('#', '#' + microPath).replace(/\/+/, '/');
};
// 状态
const store = new (external_vuex_default()).Store({
  state: {}
});

// 启动
const bootstrap = function bootstrap(_ref) {
  let {
    appName,
    routes,
    rootPath
  } = _ref;
  // 导航解析
  let navList = solveNav(routes, rootPath);
  // 路由解析
  routes = solveRoute(navList);
  // 路由生成
  const router = new (external_vue_router_default())({
    routes: routes
  });
  // 视图挂载
  new (external_vue_default())({
    router,
    store,
    render: h => h(app, {
      props: {
        appName: appName,
        navList: navList
      }
    })
  }).$mount('#micro-app');
};


// 导航解析
function solveNav(list, parentPath, topPath) {
  list = Array.isArray(list) ? list : [];
  parentPath = parentPath || '';
  list.forEach(item => {
    item.text = item.name;
    let path = item.path || item.id;
    let navPath = ('/' + (parentPath + '/' + path)).replace(/\/+/g, '/').replace(/\/+$/g, '');
    item.url = '#' + navPath;
    // 非一层路径则使用子路径
    item.path = topPath ? path : navPath;
    // 一层匹配路径
    let matchPath = topPath ? topPath : navPath;
    item.match = matchPath;
    // 子级菜单
    if (item.children) {
      solveNav(item.children, navPath, matchPath);
    }
  });
  return list;
}

// 路由解析
function solveRoute(list, routes, parentRoute) {
  list = Array.isArray(list) ? list : [];
  routes = Array.isArray(routes) ? routes : [];
  parentRoute = parentRoute || {};
  list.forEach(item => {
    let node = {
      path: parentRoute.path ? parentRoute.path + '/' + item.path : item.path,
      component: resolve => getRender(item, resolve)
    };
    routes.push(node);
    // 子级菜单
    if (item.children) {
      solveRoute(item.children, routes, node);
    }
  });
  return routes;
}

// 获取渲染器
async function getRender(data, resolve) {
  let res = await $pig.import('@daelui/drender/dist/drender.umd.min.js');
  let stock = res.stock;
  if (!stock.state.isInitComponents) {
    stock.state.isInitComponents = true;
    // 查询所有视图组件
    await viewcomp_action.queryAllComponents({
      isSaas: true
    }).then(cps => {
      components.push(...cps);
      stock.setter.setComponentCollection(components);
    });
    external_vue_default().component('drender', res);
  }
  // 加载页面元数据
  let page = await loadPage(data.pageId);
  const comp = external_vue_default().extend({
    template: '<component :page="page" is="drender" />',
    data() {
      return {
        page: page
      };
    }
  });
  resolve(comp);
}

// 加载页面
function loadPage(id) {
  return viewrelease_action.queryItemSaas({
    vpid: id
  }).then(result => {
    let data = result.data || {};
    // 组件元数据
    let meta = components_js_.parser.parse(data.meta) || {
      component: 'div',
      properts: {},
      events: {},
      resource: {},
      theme: {},
      childComponents: []
    };
    meta.childComponents = meta.childComponents || [];
    data.meta = meta;
    return data;
  });
}
;// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib-no-default.js



}();
/******/ 	return __webpack_exports__;
/******/ })()
;
});