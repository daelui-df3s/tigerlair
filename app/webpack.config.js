const path = require('path')

module.exports = {
  // 定义（告诉webpack）入口文件
  entry: {
    app: './src/extensions/entry.js'
  },
  target: 'web',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, '../dist/extensions'), // 定义输出文件将存放的文件夹名称，这里需要绝对路径，因此开头引入path,利用path方法
    // filename: '[name].js'                    // 输出文件名称定义，这样写默认是main.js
    filename: 'entry.js', //也可以定为index.js
    libraryTarget: 'umd'
  },
  resolve: {
    extensions: ['.js']
  },
  module: {
    // rules中的每一项是一个规则
    rules:[
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
        }
      },
    ]
  },
  resolve: {
    alias: {
      '@': 'src',
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: 'vendor',
          chunks: "initial",
          minChunks: 2
        }
      }
    },
    // minimize: {}
  },
  // externals: ['vue', 'vue-router', 'vuex']
}