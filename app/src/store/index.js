import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import { OptionEnum } from '@daelui/dogjs/dist/components.js'

Vue.use(Vuex)

const state = {
  // 选中的导航id
  navActiveId: 1,
  // 页面类型
  pageTypeEnum: OptionEnum([
    {key: 'DIR', value: 'DIR', text: '目录'},
    {key: 'PAGE', value: 'PAGE', text: '页面'}
  ])
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
