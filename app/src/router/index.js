import Vue from 'vue'
import Router from 'vue-router'
import config from '@/micros/config'

Vue.use(Router)

// 判断环境是否是微应用打开
let microPath = ''
if (window.__POWERED_BY_QIANKUN__) {
  microPath = config.microPath
}

const router = new Router({
  routes: [
    {
      path: microPath + '/',
      name: 'root',
      component: () => import('@/views/index'),
      redirect: microPath + '/home',
      children: [
        // 首页
        {
          path: 'home',
          name: 'home',
          component: () => import('@/views/index/index')
        },
        // 导航
        {
          path: 'nave',
          name: 'nave',
          component: () => import(/* webpackChunkName: "nave" */ '@/views/nave'),
          redirect: to => {
            return microPath + '/nave/list'
          },
          children: [
            {
              path: 'list/:id?',
              name: 'nave-list',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/list')
            },
            {
              path: 'item/:id?',
              name: 'nave-item',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/item')
            },
            {
              path: 'edit/:id?',
              name: 'nave-edit',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/edit')
            },
            {
              path: 'rule',
              name: 'nave-rule',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/rule/list')
            },
            {
              path: 'envirment',
              name: 'nave-envirment',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/envirment/list')
            }
          ]
        },
        // 项目
        {
          path: 'project',
          name: 'project',
          component: () => import(/* webpackChunkName: "project" */ '@/views/project/index'),
          children: [
            {
              path: 'list',
              name: 'project-list',
              component: () => import(/* webpackChunkName: "project" */ '@/views/project/list')
            },
            {
              path: 'item/:id?',
              name: 'project-edit',
              component: () => import(/* webpackChunkName: "project" */ '@/views/project/edit')
            }
          ]
        },
        // 产品
        {
          path: 'product',
          name: 'product',
          component: () => import(/* webpackChunkName: "product" */ '@/views/product/index'),
          children: [
            {
              path: 'list',
              name: 'product-list',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/list')
            },
            {
              path: 'view/:id?',
              name: 'product-view',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/view')
            },
            {
              path: 'item/:id?',
              name: 'product-edit',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/edit')
            },
            {
              path: 'tree/:productId?',
              name: 'product-tree',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/tree'),
              children: [
                {
                  path: 'view/:id',
                  name: 'product-tree-view',
                  component: () => import(/* webpackChunkName: "product" */ '@/views/product/view')
                },
                {
                  path: 'panel/:id',
                  name: 'product-tree-page',
                  component: () => import(/* webpackChunkName: "product" */ '@/views/panel/view')
                },
                {
                  path: 'func/:id',
                  name: 'product-tree-func',
                  component: () => import(/* webpackChunkName: "product" */ '@/views/panel/func/view')
                }
              ]
            }
          ]
        },
        // 页面
        {
          path: 'panel',
          name: 'panel',
          component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/index'),
          children: [
            {
              path: 'list',
              name: 'panel-list',
              component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/list')
            },
            {
              path: 'item/:id?',
              name: 'panel-edit',
              component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/edit')
            },
            {
              path: 'view/:id?',
              name: 'panel-view',
              component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/view')
            },
            {
              path: 'tree/:pageId?',
              name: 'panel-tree',
              component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/tree'),
              children: [
                {
                  path: 'panel/:id',
                  name: 'panel-tree-page',
                  component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/edit')
                },
                {
                  path: 'func/:id',
                  name: 'panel-tree-func',
                  component: () => import(/* webpackChunkName: "panel" */ '@/views/panel/func/edit')
                }
              ]
            }
          ]
        },
        // 功能
        {
          path: 'func',
          name: 'func',
          component: () => import(/* webpackChunkName: "func" */ '@/views/panel/func/index'),
          children: [
            {
              path: 'list',
              name: 'func-list',
              component: () => import(/* webpackChunkName: "func" */ '@/views/panel/func/list')
            },
            {
              path: 'item/:id?',
              name: 'func-edit',
              component: () => import(/* webpackChunkName: "func" */ '@/views/panel/func/edit')
            },
            {
              path: 'view/:id?',
              name: 'func-view',
              component: () => import(/* webpackChunkName: "func" */ '@/views/panel/func/view')
            }
          ]
        },
        // 编码
        {
          path: 'codewell',
          name: 'codewell',
          component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/index'),
          children: [
            {
              path: 'list',
              name: 'codewell-list',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/list')
            },
            {
              path: 'item/:id?',
              name: 'codewell-edit',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/edit')
            },
            {
              path: 'tree/:cid?',
              name: 'codewell-tree',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/tree'),
              children: [
                {
                  path: 'encode/:id',
                  name: 'codewell-tree-encode',
                  component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/encode/edit')
                }
              ]
            },
            {
              path: 'encode/list',
              name: 'encode-list',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/encode/list')
            },
            {
              path: 'encode/item/:id?',
              name: 'encode-edit',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/encode/edit')
            }
          ]
        },
        // 服务
        {
          path: 'service',
          name: 'service',
          component: () => import(/* webpackChunkName: "service" */ '@/views/service/index'),
          children: [
            // 数据库
            {
              path: 'database',
              name: 'database',
              component: () => import(/* webpackChunkName: "database" */ '@/views/database/index.vue'),
              redirect: to => {
                return microPath + '/database/list'
              },
              children: [
                {
                  path: 'list',
                  name: 'database-list',
                  component: () => import(/* webpackChunkName: "database" */ '@/views/database/list.vue')
                },
                {
                  path: 'item/:id?',
                  name: 'database-item',
                  component: () => import(/* webpackChunkName: "database" */ '@/views/database/item.vue')
                },
                {
                  path: 'edit/:id?',
                  name: 'database-edit',
                  component: () => import(/* webpackChunkName: "database" */ '@/views/database/edit.vue')
                }
              ]
            },
            // 表
            {
              path: 'table',
              name: 'table',
              component: () => import(/* webpackChunkName: "table" */ '@/views/table/index.vue'),
              redirect: to => {
                return microPath + '/table/list'
              },
              children: [
                {
                  path: 'list',
                  name: 'table-list',
                  component: () => import(/* webpackChunkName: "table" */ '@/views/table/list.vue')
                },
                {
                  path: 'item/:id?',
                  name: 'table-item',
                  component: () => import(/* webpackChunkName: "table" */ '@/views/table/item.vue')
                },
                {
                  path: 'edit/:id?',
                  name: 'table-edit',
                  component: () => import(/* webpackChunkName: "table" */ '@/views/table/edit.vue')
                }
              ]
            },
            // 分组
            {
              path: 'group',
              name: 'group',
              component: () => import(/* webpackChunkName: "group" */ '@/views/group/index.vue'),
              redirect: to => {
                return microPath + '/group/list'
              },
              children: [
                {
                  path: 'list',
                  name: 'group-list',
                  component: () => import(/* webpackChunkName: "group" */ '@/views/group/list.vue')
                },
                {
                  path: 'item/:id?',
                  name: 'group-item',
                  component: () => import(/* webpackChunkName: "group" */ '@/views/group/item.vue')
                },
                {
                  path: 'edit/:id?',
                  name: 'group-edit',
                  component: () => import(/* webpackChunkName: "group" */ '@/views/group/edit.vue')
                }
              ]
            }
          ]
        },
        // 实体
        {
          path: 'service/entity',
          name: 'service-entity',
          component: () => import(/* webpackChunkName: "entity" */ '@/views/service/entity/index'),
          children: [
            {
              path: 'list',
              name: 'entity-list',
              component: () => import(/* webpackChunkName: "entity" */ '@/views/service/entity/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'entity-item',
              component: () => import(/* webpackChunkName: "entity" */ '@/views/service/entity/item.vue')
            }
          ]
        },
        // 动态服务
        {
          path: 'service/api',
          name: 'service-api',
          component: () => import(/* webpackChunkName: "api" */ '@/views/service/api/index'),
          children: [
            // 动态服务
            {
              path: 'list',
              name: 'api-list',
              component: () => import(/* webpackChunkName: "api" */ '@/views/service/api/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'api-item',
              component: () => import(/* webpackChunkName: "api" */ '@/views/service/api/item.vue')
            },
            {
              path: 'test/:id?',
              name: 'api-test',
              component: () => import(/* webpackChunkName: "api" */ '@/views/service/api/test.vue')
            }
          ]
        },
        // 模拟服务
        {
          path: 'service/inter',
          name: 'service-inter',
          component: () => import(/* webpackChunkName: "service" */ '@/views/service/inter/index'),
          children: [
            // 服务
            {
              path: 'list',
              name: 'inter-list',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'inter-edit',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/item.vue')
            },
            {
              path: 'view/:id?',
              name: 'inter-item',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/view.vue')
            },
            {
              path: 'syntax',
              name: 'inter-syntax',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/syntax.vue')
            }
          ]
        },
        // 微应用
        {
          path: 'mapp',
          name: 'mapp-index',
          component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/index'),
          children: [
            {
              path: 'list',
              name: 'mapp-list',
              component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/list')
            },
            {
              path: 'item/:id?',
              name: 'mapp-item',
              component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/item')
            }
          ]
        },
        // 应用导航
        {
          path: 'nav',
          name: 'nav',
          component: () => import(/* webpackChunkName: "nav" */ '@/views/nav/index'),
          children: [
            {
              path: 'list',
              name: 'nav-list',
              component: () => import(/* webpackChunkName: "nav" */ '@/views/nav/list')
            },
            {
              path: 'item/:id?',
              name: 'nav-edit',
              component: () => import(/* webpackChunkName: "nav" */ '@/views/nav/item')
            },
          ]
        },
        // 导航发布版本
        {
          path: 'nav/release',
          name: 'nav-release',
          component: () => import(/* webpackChunkName: "nav-release" */ '@/views/nav/release/index'),
          children: [
            {
              path: 'list',
              name: 'nav-release-list',
              component: () => import(/* webpackChunkName: "nav-release" */ '@/views/nav/release/list'),
            },
            {
              path: 'item/:id?',
              name: 'nav-release-item',
              component: () => import(/* webpackChunkName: "nav-release" */ '@/views/nav/release/edit')
            }
          ]
        },
        // 视图
        {
          path: 'dview',
          name: 'dview-index',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/index'),
          children: [
            {
              path: 'page',
              name: 'dview-page',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/index'),
              children: [
                {
                  path: 'list',
                  name: 'dview-list',
                  component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/list'),
                },
                {
                  path: 'item/:id?',
                  name: 'dview-item',
                  component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/item')
                }
              ]
            }
          ]
        },
        // 视图发布版本
        {
          path: 'dview/page/release',
          name: 'page-release',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/release/index'),
          children: [
            {
              path: 'list',
              name: 'page-release-list',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/release/list'),
            },
            {
              path: 'item/:id?',
              name: 'page-release-item',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/release/edit')
            }
          ]
        },
        // 视图组件
        {
          path: 'dview/component',
          name: 'dview-component',
          component: () => import(/* webpackChunkName: "dview-component" */ '@/views/dview/component/index'),
          children: [
            {
              path: 'list',
              name: 'dview-component-list',
              component: () => import(/* webpackChunkName: "dview-component" */ '@/views/dview/component/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-component-edit',
              component: () => import(/* webpackChunkName: "dview-component" */ '@/views/dview/component/edit')
            },
            {
              path: 'view/:id?',
              name: 'dview-component-view',
              component: () => import(/* webpackChunkName: "dview-component" */ '@/views/dview/component/view')
            }
          ]
        },
        // 组件发布版本
        {
          path: 'dview/component/release',
          name: 'component-release',
          component: () => import(/* webpackChunkName: "component-release" */ '@/views/dview/component/release/index'),
          children: [
            {
              path: 'list',
              name: 'dview-component-release-list',
              component: () => import(/* webpackChunkName: "component-release" */ '@/views/dview/component/release/list'),
            },
            {
              path: 'item/:id?',
              name: 'dview-component-release-item',
              component: () => import(/* webpackChunkName: "component-release" */ '@/views/dview/component/release/edit')
            }
          ]
        },
        // 组件注册
        {
          path: 'dview/register',
          name: 'dview-register',
          component: () => import(/* webpackChunkName: "dview-register" */ '@/views/dview/register/index'),
          children: [
            {
              path: 'list',
              name: 'dview-register-list',
              component: () => import(/* webpackChunkName: "dview-register" */ '@/views/dview/register/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-register-edit',
              component: () => import(/* webpackChunkName: "dview-register" */ '@/views/dview/register/edit')
            }
          ]
        },
        // 组件注册路径
        {
          path: 'dview/path',
          name: 'dview-path',
          component: () => import(/* webpackChunkName: "dview-path" */ '@/views/dview/path/index'),
          children: [
            {
              path: 'list',
              name: 'dview-path-list',
              component: () => import(/* webpackChunkName: "dview-path" */ '@/views/dview/path/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-path-edit',
              component: () => import(/* webpackChunkName: "dview-path" */ '@/views/dview/path/edit')
            }
          ]
        },
        // 组件注册适配器
        {
          path: 'dview/rep',
          name: 'dview-rep',
          component: () => import(/* webpackChunkName: "dview-rep" */ '@/views/dview/rep/index'),
          children: [
            {
              path: 'list',
              name: 'dview-rep-list',
              component: () => import(/* webpackChunkName: "dview-rep" */ '@/views/dview/rep/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-rep-edit',
              component: () => import(/* webpackChunkName: "dview-rep" */ '@/views/dview/rep/edit')
            }
          ]
        },
        // 视图片段
        {
          path: 'dview/snippet',
          name: 'dview-snippet',
          component: () => import(/* webpackChunkName: "dview-snippet" */ '@/views/dview/snippet/index'),
          children: [
            {
              path: 'list',
              name: 'dview-snippet-list',
              component: () => import(/* webpackChunkName: "dview-snippet" */ '@/views/dview/snippet/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-snippet-edit',
              component: () => import(/* webpackChunkName: "dview-snippet" */ '@/views/dview/snippet/edit')
            }
          ]
        },
        // 片段发布版本
        {
          path: 'dview/snippet/release',
          name: 'snippet-release',
          component: () => import(/* webpackChunkName: "snippet-release" */ '@/views/dview/snippet/release/index'),
          children: [
            {
              path: 'list',
              name: 'dview-snippet-release-list',
              component: () => import(/* webpackChunkName: "snippet-release" */ '@/views/dview/snippet/release/list'),
            },
            {
              path: 'item/:id?',
              name: 'dview-snippet-release-item',
              component: () => import(/* webpackChunkName: "snippet-release" */ '@/views/dview/snippet/release/edit')
            }
          ]
        },
        // 视图模板
        {
          path: 'dview/tpl',
          name: 'dview-tpl',
          component: () => import(/* webpackChunkName: "dview-tpl" */ '@/views/dview/tpl/index'),
          children: [
            {
              path: 'list',
              name: 'dview-tpl-list',
              component: () => import(/* webpackChunkName: "dview-tpl" */ '@/views/dview/tpl/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-tpl-edit',
              component: () => import(/* webpackChunkName: "dview-tpl" */ '@/views/dview/tpl/edit')
            },
            {
              path: 'render',
              name: 'dview-tpl-render',
              component: () => import(/* webpackChunkName: "dview-tpl" */ '@/views/dview/tpl/render')
            }
          ]
        },
        // 模板发布版本
        {
          path: 'dview/tpl/release',
          name: 'tpl-release',
          component: () => import(/* webpackChunkName: "tpl-release" */ '@/views/dview/tpl/release/index'),
          children: [
            {
              path: 'list',
              name: 'dview-tpl-release-list',
              component: () => import(/* webpackChunkName: "tpl-release" */ '@/views/dview/tpl/release/list'),
            },
            {
              path: 'item/:id?',
              name: 'dview-tpl-release-item',
              component: () => import(/* webpackChunkName: "tpl-release" */ '@/views/dview/tpl/release/edit')
            }
          ]
        },
        // 资源
        {
          path: 'resource',
          name: 'resource-index',
          component: () => import(/* webpackChunkName: "resource" */ '@/views/process/index'),
          children: [
            {
              path: 'list',
              name: 'resource-list',
              component: () => import(/* webpackChunkName: "resource" */ '@/views/resource/list'),
            },
            {
              path: 'item/:id?',
              name: 'resource-item',
              component: () => import(/* webpackChunkName: "resource" */ '@/views/resource/item')
            }
          ]
        },
        // 文件列表
        {
          path: 'arcfile',
          name: 'arcfile',
          component: () => import(/* webpackChunkName: "arcfile" */ '@/views/arcfile/index'),
          children: [
            {
              path: 'list',
              name: 'arcfile-list',
              component: () => import(/* webpackChunkName: "arcfile" */ '@/views/arcfile/list')
            },
            {
              path: 'item/:id?',
              name: 'arcfile-edit',
              component: () => import(/* webpackChunkName: "arcfile" */ '@/views/arcfile/edit')
            }
          ]
        },
        // 流程
        {
          path: 'process',
          name: 'process',
          component: () => import(/* webpackChunkName: "process" */ '@/views/process/index'),
          children: [
            {
              path: 'execute/list',
              name: 'process-execute-list',
              component: () => import(/* webpackChunkName: "process-execute" */ '@/views/process/execute/list')
            },
            {
              path: 'execute/item/:id?',
              name: 'process-execute-edit',
              component: () => import(/* webpackChunkName: "process-execute" */ '@/views/process/execute/edit')
            },
            {
              path: 'execute/design/:id',
              name: 'process-execute-design',
              component: () => import(/* webpackChunkName: "execute-design" */ '@/views/process/execute/design')
            },
            {
              path: 'execute/view/:id?',
              name: 'process-execute-view',
              component: () => import(/* webpackChunkName: "execute-view" */ '@/views/process/execute/design')
            },
            {
              path: 'exeunit/list',
              name: 'process-exeunit-list',
              component: () => import(/* webpackChunkName: "process-exeunit" */ '@/views/process/exeunit/list')
            },
            {
              path: 'exeunit/item/:id?',
              name: 'process-exeunit-edit',
              component: () => import(/* webpackChunkName: "process-exeunit" */ '@/views/process/exeunit/edit')
            },
            {
              path: 'view/list',
              name: 'process-view-list',
              component: () => import(/* webpackChunkName: "process-view" */ '@/views/process/view/list')
            },
            {
              path: 'view/item/:id?',
              name: 'process-view-edit',
              component: () => import(/* webpackChunkName: "process-view" */ '@/views/process/view/edit')
            },
            {
              path: 'view/list-defined',
              name: 'process-view-list-defined',
              component: () => import(/* webpackChunkName: "process-view" */ '@/views/process/view/list-defined')
            },
            {
              path: 'store/list',
              name: 'process-store-list',
              component: () => import(/* webpackChunkName: "store" */ '@/views/process/store/list')
            },
            {
              path: 'store/item/:id?',
              name: 'process-store-edit',
              component: () => import(/* webpackChunkName: "store" */ '@/views/process/store/edit')
            },
            {
              path: 'exegroup/list',
              name: 'process-group-list',
              component: () => import(/* webpackChunkName: "group" */ '@/views/process/exegroup/list')
            },
            {
              path: 'exegroup/item/:id?',
              name: 'process-group-edit',
              component: () => import(/* webpackChunkName: "group" */ '@/views/process/exegroup/edit')
            },
            {
              path: 'exegroup/design/:id?',
              name: 'process-group-design',
              component: () => import(/* webpackChunkName: "execute" */ '@/views/process/exegroup/design')
            },
            {
              path: 'exegroup/view/:id?',
              name: 'process-group-view',
              component: () => import(/* webpackChunkName: "execute" */ '@/views/process/exegroup/design')
            }
          ]
        },
        // 文件
        {
          path: 'dfile',
          name: 'dfile-index',
          component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/index'),
          children: [
            {
              path: 'list',
              name: 'dfile-list',
              component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/list'),
            },
            {
              path: 'upload',
              name: 'dfile-upload',
              component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/upload')
            },
            {
              path: 'item/:id?',
              name: 'dfile-item',
              component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/item')
            }
          ]
        },
        // 资源目录
        {
          path: 'pool',
          name: 'pool-index',
          component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/index'),
          children: [
            {
              path: 'list',
              name: 'pool-list',
              component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/list'),
            },
            {
              path: 'item/:id?',
              name: 'pool-edit',
              component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/edit')
            },
            {
              path: ':pid?/file/list',
              name: 'pool-file-list',
              component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/file/list')
            },
            {
              path: 'file/item',
              name: 'pool-file-item',
              component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/file/item')
            }
          ]
        },
        // 测试
        {
          path: 'test',
          name: 'test',
          component: () => import(/* webpackChunkName: "test" */ '@/views/test/index.vue'),
          redirect: to => {
            return microPath + '/test/list'
          },
          children: [
            {
              path: 'list',
              name: 'interface-list',
              component: () => import(/* webpackChunkName: "test" */ '@/views/test/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'test-item',
              component: () => import(/* webpackChunkName: "test" */ '@/views/test/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'test-edit',
              component: () => import(/* webpackChunkName: "test" */ '@/views/test/edit.vue')
            }
          ]
        },
        // 采集
        {
          path: 'collect',
          name: 'collect',
          component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/index.vue'),
          redirect: to => {
            return microPath + '/collect/list'
          },
          children: [
            {
              path: 'inter/list',
              name: 'collect-inter',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter/list.vue')
            },
            {
              path: 'inter/item/:id?',
              name: 'collect-inter-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter/item.vue')
            },
            {
              path: 'inter/view/:id?',
              name: 'collect-inter-view',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter/item.vue')
            },
            {
              path: 'inter/group/list',
              name: 'collect-inter-group-list',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter-group/list.vue')
            },
            {
              path: 'inter/group/item/:id?',
              name: 'collect-inter-group-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter-group/item.vue')
            },
            {
              path: 'error/list',
              name: 'collect-error',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error/list.vue')
            },
            {
              path: 'error/item/:id?',
              name: 'collect-error-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error/item.vue')
            },
            {
              path: 'error/view/:id?',
              name: 'collect-error-view',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error/item.vue')
            },
            {
              path: 'error/group/list',
              name: 'collect-error-group-list',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error-group/list.vue')
            },
            {
              path: 'error/group/item/:id?',
              name: 'collect-error-group-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error-group/item.vue')
            },
            {
              path: 'performance/list',
              name: 'collect-performance',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance/list.vue')
            },
            {
              path: 'performance/item/:id?',
              name: 'collect-performance-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance/item.vue')
            },
            {
              path: 'performance/view/:id?',
              name: 'collect-performance-view',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance/item.vue')
            },
            {
              path: 'performance/group/list',
              name: 'collect-performance-group-list',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance-group/list.vue')
            },
            {
              path: 'performance/group/item/:id?',
              name: 'collect-performance-group-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance-group/item.vue')
            }
          ]
        },
        // 代理
        {
          path: 'proxier',
          name: 'proxier',
          component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/index.vue'),
          redirect: to => {
            return microPath + '/proxier/list'
          },
          children: [
            {
              path: 'list',
              name: 'proxier-list',
              component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'proxier-item',
              component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'proxier-edit',
              component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/edit.vue')
            }
          ]
        },
        // 插件
        {
          path: 'plugins',
          name: 'plugins',
          component: () => import(/* webpackChunkName: "plugins" */ '@/views/plugins/index.vue'),
          redirect: to => {
            return microPath + '/plugins/list'
          },
          children: [
            {
              path: 'list',
              name: 'plugins-list',
              component: () => import(/* webpackChunkName: "plugins" */ '@/views/plugins/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'plugins-item',
              component: () => import(/* webpackChunkName: "plugins" */ '@/views/plugins/edit.vue')
            }
          ]
        },
        // 文档页面
        {
          path: 'section',
          name: 'section',
          component: () => import(/* webpackChunkName: "section" */ '@/views/section/index'),
          children: [
            {
              path: 'list',
              name: 'section-list',
              component: () => import(/* webpackChunkName: "section" */ '@/views/section/list')
            },
            {
              path: 'item/:id?',
              name: 'section-edit',
              component: () => import(/* webpackChunkName: "section" */ '@/views/section/edit')
            }
          ]
        },
        // 文档列表
        {
          path: 'archive',
          name: 'archive',
          component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/index'),
          children: [
            {
              path: 'list',
              name: 'archive-list',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/list')
            },
            {
              path: 'item/:id?',
              name: 'archive-edit',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/item')
            },
            {
              path: 'view/:id?',
              name: 'archive-view',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/view')
            },
            {
              path: 'cat/list',
              name: 'archive-cat-list',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/cat/list')
            },
            {
              path: 'cat/item/:id?',
              name: 'archive-cat-edit',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/cat/item')
            },
            {
              path: 'chapter/list',
              name: 'archive-chapter-list',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/list')
            },
            {
              path: 'chapter/item/:id?',
              name: 'archive-chapter-edit',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/item')
            },
            {
              path: 'chapter/tree/:chapterId?',
              name: 'chapter-tree',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/tree'),
              children: [
                {
                  path: 'view/:id',
                  name: 'chapter-tree-view',
                  component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/view')
                },
                {
                  path: 'page/:id',
                  name: 'chapter-tree-page',
                  component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/view')
                },
                {
                  path: 'item/:id?',
                  name: 'chapter-tree-edit',
                  component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/archive')
                }
              ]
            }
          ]
        },
        // 字典
        {
          path: 'dictionary',
          name: 'dictionary',
          component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/index.vue'),
          redirect: to => {
            return microPath + '/dictionary/list'
          },
          children: [
            {
              path: 'list',
              name: 'dictionary-list',
              component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'dictionary-item',
              component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'dictionary-edit',
              component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/edit.vue')
            }
          ]
        },
        // 系统
        {
          path: 'system',
          name: 'system',
          component: () => import(/* webpackChunkName: "system" */ '@/views/system/index.vue'),
          redirect: to => {
            return microPath + '/backup/list'
          }
        },
        // 备份
        {
          path: 'backup',
          name: 'backup',
          component: () => import(/* webpackChunkName: "backup" */ '@/views/backup/index.vue'),
          redirect: to => {
            return microPath + '/backup/list'
          },
          children: [
            {
              path: 'list',
              name: 'backup-list',
              component: () => import(/* webpackChunkName: "backup" */ '@/views/backup/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'backup-item',
              component: () => import(/* webpackChunkName: "backup" */ '@/views/backup/item.vue')
            }
          ]
        }
      ]
    },
    // 应用
    {
      path: microPath + '/mapp/view/:id?',
      name: 'mapp-view',
      component: () => import(/* webpackChunkName: "mapp-view" */ '@/views/mapp/view')
    },
    // 应用-预览
    {
      path: microPath + '/saas/mapp/:id?',
      name: 'saas-mapp',
      component: () => import(/* webpackChunkName: "saas-mapp" */ '@/views/mapp/release')
    },
    // 导航-预览
    {
      path: microPath + '/nav/view/:id?',
      name: 'nav-view',
      component: () => import(/* webpackChunkName: "nav-view" */ '@/views/nav/view')
    },
    // 导航-开放预览
    {
      path: microPath + '/saas/nav/:id?',
      name: 'saas-nav',
      component: () => import(/* webpackChunkName: "saas-nav" */ '@/views/nav/release')
    },
    // 片段-预览
    {
      path: microPath + '/snippet/view/:id?',
      name: 'snippet-view',
      component: () => import(/* webpackChunkName: "snippet-view" */ '@/views/dview/snippet/view')
    },
    // 片段-开放预览
    {
      path: microPath + '/saas/snippet/:id?',
      name: 'saas-snippet',
      component: () => import(/* webpackChunkName: "saas-snippet" */ '@/views/dview/snippet/release')
    },
    // 视图-设计
    {
      path: microPath + '/page/design/:id?',
      name: 'dview-design',
      component: () => import(/* webpackChunkName: "dview-design" */ '@/views/dview/page/design')
    },
    // 视图-预览
    {
      path: microPath + '/page/view/:id?',
      name: 'dview-view',
      component: () => import(/* webpackChunkName: "dview-view" */ '@/views/dview/page/view')
    },
    // 视图-开放预览
    {
      path: microPath + '/saas/page/:id?',
      name: 'saas-page',
      component: () => import(/* webpackChunkName: "saas-page" */ '@/views/dview/page/release')
    },
    // 视图-开放预览(带编辑通道)
    {
      path: microPath + '/saas/preview/:id?',
      name: 'saas-preview',
      component: () => import(/* webpackChunkName: "saas-preview" */ '@/views/dview/page/preview')
    },
    // 视图-开放演练
    {
      path: microPath + '/saas/practice/:id?',
      name: 'saas-practice',
      component: () => import(/* webpackChunkName: "saas-practice" */ '@/views/dview/page/practice')
    }
  ]
})

export default router
