/**
 * @description 应用加载器
 * */

import $pig from '@daelui/pigjs'

const isProd = /production|single/i.test(process.env.NODE_ENV)
// 是否运行态
const ap = (window.application || {}).tigerlair || {}
ap.mode = ap[isProd ? 'prod' : 'dev']
// 初始化模块配置
const loader = ap.mode.loader || {}
const domain = ap.mode.domain || location.origin
const rootUrl = loader.rootUrl || location.origin

export default $pig.init({
  path: loader.path || '@daelui/pigjs/latest/dist/system.min.js',
  rootUrl: rootUrl,
  rootUrls: loader.rootUrls,
  registry: loader.registry,
  registries: loader.registries,
  resolutions: [{name: 'vue', version: '2.*.*'}]
}).then(function(){
  $pig.import(domain + '/df3s/tigerlair/app/static/css/main.css')
  return $pig.import(domain + '/df3s/tigerlair/app/static/js/main.js')
})
