/**
 * @description 解析器
 * @author Rid King
 * @since 2018-07-06
*/

import { Resolver } from '@daelui/dogjs/dist/components.js'
import application from '../../config/application'

var resolver = new Resolver({
  application
})

export default resolver

