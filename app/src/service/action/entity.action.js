/**
* @description 页面管理
*/

import ds from '../components/ds'
import service, {api} from '../service/entity.service'

// 服务
export default Object.assign({}, service, {
  queryAllListOption (params, isAll) {
    params = params || {}
    // 查询文档列表
    return service.queryAll({...params}).then(res => {
      let list = res.data
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.code + (item.name ? '(' + item.name + ')' : item.name)
        }
      })
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        })
      }
      return list
    })
  }
})

export {
  api, service, ds
}
