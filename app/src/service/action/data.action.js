/**
* @description 数据管理
*/

import ds from '../components/ds'
import service, {api} from '../service/data.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
