/**
* @description 流程组管理
*/

import ds from '../components/ds'
import service, {api} from '../service/exegroup.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
