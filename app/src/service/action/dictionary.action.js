/**
* @description 字典模块
*/

import ds, { CACHE_TYPE } from '../components/ds'
import service, {api} from '../service/dictionary.service'
import { treer, funcer, objecter } from '@daelui/dogjs/dist/components.js'

// 服务
export default Object.assign({}, service, {
  /**
   * @function 查询环境树形数据
  */
  queryTreeEnv () {
    // 查询页面数据
    return service.queryTree({cat: 'page-envirment'}, {cache: true, buffer: true}).then(res => {
      let list = (res?.data || {}).list
      list = Array.isArray(list) ? list : []
      let arr = []
      treer.forEach({data: list, callback: function (item, level) {
        item = objecter.clone(item)
        item.code = funcer.loop(level, ' - ') + item.code
        arr.push(item)
      }})
      return arr
    })
  },
  /**
   * @function 查询字典列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListByGroup (...params) {
    let typeDefer = service.queryList({
      cat: 'envirment-type'
    }, {cache: true, buffer: true})
    let ruleDefer = service.queryList({
      cat: 'envirment'
    }, {cache: true, buffer: true})
    return Promise.all([typeDefer, ruleDefer]).then(results => {
      let [r1, r2] = results
      return {data: {groups: r1.data, envirmentList: r2.data}}
    }).catch(e => {
      return {data: {groups: [], envirmentList: []}}
    })
  },
  /**
   * @function 查询请求方式列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
   queryMethodList (...params) {
    return service.queryList({
      cat: 'inter-method'
    }, {cache: true, cacheType: CACHE_TYPE.SESSION, buffer: true})
    .then(res => {
      res = res || {}
      let data = res.data
      data = Array.isArray(data) ? data : []
      data = data.map(item => {
        return {
          id: item.key,
          text: item.value
        }
      })
      return data
    })
  },
  /**
   * @function 查询协议类型列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryProtocolList (...params) {
    return service.queryList({
      cat: 'inter-protocol'
    }, {cache: true, cacheType: CACHE_TYPE.SESSION, buffer: true})
    .then(res => {
      res = res || {}
      let data = res.data
      data = Array.isArray(data) ? data : []
      data = data.map(item => {
        return {
          id: item.key,
          text: item.value
        }
      })
      return data
    })
  },
  /**
   * @function 查询服务状态列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryInterStatusList (...params) {
    return service.queryList({
      cat: 'inter-status'
    }, {cache: true, cacheType: CACHE_TYPE.SESSION, buffer: true})
    .then(res => {
      res = res || {}
      let data = res.data
      data = Array.isArray(data) ? data : []
      data = data.map(item => {
        return {
          id: item.key,
          text: item.value
        }
      })
      return data
    })
  },
  /**
   * @function 查询服务类型列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryServiceTypeList (...params) {
    return service.queryList({
      cat: 'service-type'
    }, {cache: true, cacheType: CACHE_TYPE.SESSION, buffer: true})
    .then(res => {
      res = res || {}
      let data = res.data
      data = Array.isArray(data) ? data : []
      data = data.map(item => {
        return {
          id: item.key,
          text: item.value
        }
      })
      return data
    })
  },
  /**
   * @function 查询同步类型列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryAsyncList (...params) {
    return service.queryList({
      cat: 'service-async'
    }, {cache: true, cacheType: CACHE_TYPE.SESSION, buffer: true})
    .then(res => {
      res = res || {}
      let data = res.data
      data = Array.isArray(data) ? data : []
      data = data.map(item => {
        return {
          id: item.key,
          text: item.value
        }
      })
      return data
    })
  },
  /**
   * @function 查询文档分类列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryAllArcCidList () {
    return service.queryList({
      cat: 'archive-cid'
    }, {buffer: true})
    .then(res => {
      res = res || {}
      let data = res.data
      data = Array.isArray(data) ? data : []
      data = data.map(item => {
        return {
          id: item.id,
          text: item.code
        }
      })
      return data
    })
  }
})

export {
  api, service, ds
}
