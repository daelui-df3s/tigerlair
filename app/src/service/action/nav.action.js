/**
* @description 导航管理
*/

import ds from '../components/ds'
import service, {api} from '../service/nav.service'
import { treer, funcer } from '@daelui/dogjs/dist/components.js'

// 服务
export default Object.assign({}, service, {
  queryListAllOption (isAll) {
    // 查询文档列表
    return service.queryListAll().then(res => {
      let list = res.data
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        })
      }
      return list
    })
  },
  /**
   * @function 查询导航数据
  */
  queryTreeData () {
    // 查询页面数据
    return service.queryListTree({}).then(res => {
      let list = (res?.data || {}).list
      list = Array.isArray(list) ? list : []
      let arr = []
      treer.forEach({data: list, callback: function (item, level) {
        arr.push({
          id: item.id,
          name: funcer.loop(level, ' - ') + item.name
        })
      }})
      return arr
    })
  }
})

export {
  api, service, ds
}
