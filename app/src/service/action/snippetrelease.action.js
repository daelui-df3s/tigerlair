/**
* @description 导航发布管理
*/

import ds from '../components/ds'
import service, {api} from '../service/snippetrelease.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
