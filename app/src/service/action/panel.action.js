/**
* @description 页面管理
*/

import ds from '../components/ds'
import service, {api} from '../service/panel.service'

// 服务
export default Object.assign({}, service, {
  queryAllListOption (isAll) {
    // 查询文档列表
    return service.queryListPage({pageSize: 1000}).then(res => {
      let list = res.data.list
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        })
      }
      return list
    })
  }
})

export {
  api, service, ds
}
