/**
* @description 视图组件管理
*/

import ds from '../components/ds'
import service, {api} from '../service/viewcomp.service'

// 服务
export default Object.assign({}, service, {
  queryAllListOption (isAll) {
    // 查询文档列表
    return service.queryListPage({pageSize: 1000}).then(res => {
      let list = res.data.list
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        })
      }
      return list
    })
  },

  // 查询所有视图组件
  queryAllComponents ({ isSaas } = {}) {
    let defer = Promise.resolve({})
    if (isSaas) {
      defer = this.queryListAllSaas()
    } else {
      defer = this.queryListAll()
    }
    return defer.then(res => {
      let data = res.data
      data = Array.isArray(data) ? data : []
      return data
    })
  }
})

export {
  api, service, ds
}
