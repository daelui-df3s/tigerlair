/**
* @description 注册表管理
*/

import Vue from 'vue'
import ds from '../components/ds'
import service, {api} from '../service/viewrelease.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
