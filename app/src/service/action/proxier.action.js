/**
* @description 代理管理
*/

import ds from '../components/ds'
import service, {api} from '../service/proxier.service'

// 代理
export default Object.assign({}, service, {
  /**
   * @function 查询代理列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage () {
    return service.queryListPage.apply(service, arguments).then(res => {
      res.data = res.data || {}
      res.data.list = Array.isArray(res.data.list) ? res.data.list : []
      res.data.list.forEach(item => {
        if (!item.resTime && item.reqEnd && item.reqStart) {
          item.resTime = item.reqEnd - item.reqStart
        }
      })
      return res
    })
  },

  /**
   * @function 添加代理
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (data) {
    data = data || {}
    if (typeof data.method === 'string') {
      data.method = data.method.toUpperCase()
    }
    return service.addItem.apply(service, [data])
  }
})

export {
  api, service, ds
}
