/**
* @description 目录文件管理
*/

import ds from '../components/ds'
import service, {api} from '../service/pfile.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
