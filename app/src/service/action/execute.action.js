/**
* @description 处理流管理
*/

import ds, { CACHE_TYPE } from '../components/ds'
import service, {api} from '../service/execute.service'

// 服务
export default Object.assign({}, service, {
  /**
   * @function 查询所有列表数据
  */
  queryAll () {
    // 查询页面数据
    return service.queryAll({}, {cache: true, cacheType: CACHE_TYPE.MEMORY, buffer: true}).then(res => {
      let list = res.data || []
      list = Array.isArray(list) ? list : []
      return list
    })
  }
})

export {
  api, service, ds
}
