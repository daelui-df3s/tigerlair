/**
* @description 目录文件管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/pfile/list/page",
		"desc": "查询目录文件列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/pfile/list/all",
		"desc": "查询目录文件",
		"method": "get"
	},
	"editItem": {
		"url": "{host}/tigerlair/pfile/item",
		"desc": "编辑资源目录",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/pfile/item",
		"desc": "删除资源目录",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/pfile/item",
		"desc": "查询目录文件",
		"method": "get"
	}
}
