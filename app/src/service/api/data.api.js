/**
* @description 数据管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/data/item",
		"desc": "查询数据",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/data/item",
		"desc": "删除数据",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/data/item",
		"desc": "编辑数据",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/data/item",
		"desc": "添加数据",
		"method": "put"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/data/list/all",
		"desc": "查询数据列表",
		"method": "get"
	}
}
