/**
* @description 功能管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryTree": {
		"url": "{host}/tigerlair/func/list/tree",
		"desc": "查询功能列表",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}/tigerlair/func/item",
		"desc": "查询功能",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/func/item",
		"desc": "删除功能",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/func/item",
		"desc": "编辑功能",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/func/item",
		"desc": "添加功能",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/func/list/page",
		"desc": "查询功能列表",
		"method": "get"
	}
}
