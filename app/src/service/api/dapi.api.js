/**
* @description api管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/dapi/item",
		"desc": "查询api",
		"method": "get"
	},
	"queryAll": {
		"url": "{host}/tigerlair/dapi/list/all",
		"desc": "查询所有api列表",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/dapi/item",
		"desc": "删除api",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/dapi/item",
		"desc": "编辑api",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/dapi/item",
		"desc": "添加api",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/dapi/list/page",
		"desc": "查询api列表",
		"method": "get"
	}
}
