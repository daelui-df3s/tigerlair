/**
* @description 备份管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/backup/list/page",
		"desc": "查询备份列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/backup/list/all",
		"desc": "查询备份列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/backup/item",
		"desc": "添加备份",
		"method": "put"
	},
	"removeItem": {
		"url": "{host}/tigerlair/backup/item",
		"desc": "删除备份",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/backup/item",
		"desc": "查询备份",
		"method": "get"
	}
}
