/**
* @description 导航发布管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/navrelease/item",
		"desc": "查询导航",
		"method": "get"
	},
	"queryItemSaas": {
		"url": "{host}/tigerlair/saas/nav/release/item",
		"desc": "查询导航",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/navrelease/item",
		"desc": "删除组件",
		"method": "delete"
	},
	"addItem": {
		"url": "{host}/tigerlair/navrelease/item",
		"desc": "添加组件",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/navrelease/list/page",
		"desc": "查询组件列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/navrelease/list/all",
		"desc": "查询组件",
		"method": "get"
	}
}
