/**
* @description 注册表管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/viewregister/item",
		"desc": "查询组件",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/viewregister/item",
		"desc": "删除组件",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/viewregister/item",
		"desc": "编辑组件",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/viewregister/item",
		"desc": "添加组件",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/viewregister/list/page",
		"desc": "查询组件列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/viewregister/list/all",
		"desc": "查询组件",
		"method": "get"
	},
	"sync": {
		"url": "{host}/tigerlair/viewregister/sync",
		"desc": "同步注册表",
		"method": "post"
	}
}
