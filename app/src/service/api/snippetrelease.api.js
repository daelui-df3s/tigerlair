/**
* @description 导航发布管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/snippetrelease/item",
		"desc": "查询片段",
		"method": "get"
	},
	"queryItemSaas": {
		"url": "{host}/tigerlair/saas/snippet/release",
		"desc": "查询片段",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/snippetrelease/item",
		"desc": "删除片段",
		"method": "delete"
	},
	"addItem": {
		"url": "{host}/tigerlair/snippetrelease/item",
		"desc": "添加片段",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/snippetrelease/list/page",
		"desc": "查询片段列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/snippetrelease/list/all",
		"desc": "查询片段",
		"method": "get"
	}
}
