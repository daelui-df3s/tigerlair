/**
* @description 文档管理(※自动化生成,勿手动更改※)
*/

export default {
	"readFile": {
		"url": "{host}/tigerlair/doc/file/read",
		"desc": "取消文件内容",
		"method": "get"
	},
	"downloadFile": {
		"url": "{host}/tigerlair/doc/file/download",
		"desc": "下载文件",
		"method": "get"
	},
	"openFileDir": {
		"url": "{host}/tigerlair/doc/file/path",
		"desc": "打开文件所有目录",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}/tigerlair/doc/item",
		"desc": "查询文档",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/doc/item",
		"desc": "删除文档",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/doc/item",
		"desc": "编辑文档",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/doc/item",
		"desc": "添加文档",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/doc/list/page",
		"desc": "查询文档列表",
		"method": "get"
	}
}
