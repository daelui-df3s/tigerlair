/**
* @description 导航发布管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/comprelease/item",
		"desc": "查询组件",
		"method": "get"
	},
	"queryItemSaas": {
		"url": "{host}/tigerlair/saas/comp/release/item",
		"desc": "查询组件",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/comprelease/item",
		"desc": "删除组件",
		"method": "delete"
	},
	"addItem": {
		"url": "{host}/tigerlair/comprelease/item",
		"desc": "添加组件",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/comprelease/list/page",
		"desc": "查询组件列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/comprelease/list/all",
		"desc": "查询组件",
		"method": "get"
	}
}
