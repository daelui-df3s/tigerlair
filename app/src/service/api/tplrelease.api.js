/**
* @description 导航发布管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/tplrelease/item",
		"desc": "查询模板",
		"method": "get"
	},
	"queryItemSaas": {
		"url": "{host}/tigerlair/saas/tpl/release/item",
		"desc": "查询模板",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/tplrelease/item",
		"desc": "删除模板",
		"method": "delete"
	},
	"addItem": {
		"url": "{host}/tigerlair/tplrelease/item",
		"desc": "添加模板",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/tplrelease/list/page",
		"desc": "查询模板列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/tplrelease/list/all",
		"desc": "查询模板",
		"method": "get"
	}
}
