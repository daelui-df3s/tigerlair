/**
* @description 资源目录管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/pool/list/page",
		"desc": "查询资源目录列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/pool/list/all",
		"desc": "查询资源目录",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/pool/item",
		"desc": "添加资源目录",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/pool/item",
		"desc": "编辑资源目录",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/pool/item",
		"desc": "删除资源目录",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/pool/item",
		"desc": "查询资源目录",
		"method": "get"
	}
}
