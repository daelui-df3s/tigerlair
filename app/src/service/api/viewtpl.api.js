/**
* @description 模板管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/viewtpl/item",
		"desc": "查询模板",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/viewtpl/item",
		"desc": "删除模板",
		"method": "delete"
	},
	"addItem": {
		"url": "{host}/tigerlair/viewtpl/item",
		"desc": "添加模板",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/viewtpl/item",
		"desc": "修改模板",
		"method": "post"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/viewtpl/list/page",
		"desc": "查询模板列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/viewtpl/list/all",
		"desc": "查询模板",
		"method": "get"
	}
}
