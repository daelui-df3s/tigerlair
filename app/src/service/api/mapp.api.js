/**
* @description 微应用管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/mapp/list/page",
		"desc": "查询微应用列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/mapp/item",
		"desc": "添加微应用",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/mapp/item",
		"desc": "编辑微应用",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/mapp/item",
		"desc": "删除微应用",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/mapp/item",
		"desc": "查询微应用",
		"method": "get"
	},
	"queryNav": {
		"url": "{host}/tigerlair/mapp/release/item",
		"desc": "查询微应用",
		"method": "get"
	},
	"queryNavSaas": {
		"url": "{host}/tigerlair/saas/mapp/release/item",
		"desc": "查询微应用",
		"method": "get"
	}
}
