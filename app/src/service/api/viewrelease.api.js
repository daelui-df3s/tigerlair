/**
* @description 注册表管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/viewrelease/item",
		"desc": "查询页面",
		"method": "get"
	},
	"queryItemSaas": {
		"url": "{host}/tigerlair/saas/viewpage/release/item",
		"desc": "查询页面",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/viewrelease/item",
		"desc": "删除组件",
		"method": "delete"
	},
	"addItem": {
		"url": "{host}/tigerlair/viewrelease/item",
		"desc": "添加组件",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/viewrelease/list/page",
		"desc": "查询组件列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/viewrelease/list/all",
		"desc": "查询组件",
		"method": "get"
	}
}
