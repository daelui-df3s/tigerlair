/**
* @description 处理流管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/execute/list/page",
		"desc": "查询处理流列表",
		"method": "get"
	},
	"queryAll": {
		"url": "{host}/tigerlair/execute/list/all",
		"desc": "查询所有处理流列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/execute/item",
		"desc": "添加处理流",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/execute/item",
		"desc": "编辑处理流",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/execute/item",
		"desc": "删除处理流",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/execute/item",
		"desc": "查询处理流",
		"method": "get"
	}
}
