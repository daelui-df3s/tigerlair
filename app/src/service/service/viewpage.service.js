/**
* @description 页面管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/viewpage.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询页面列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  },

  /**
   * @function 添加页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 编辑页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 删除页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 查询页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  },

  /**
   * @function 查询页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItemSaas (...params) {
    return ds.get(api.queryItemSaas.url, ...params, {})
  },

  /**
   * @function 查询页面列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryTreeAll (...params) {
    return ds.get(api.queryTreeAll.url, ...params, {})
  },

  /**
   * @function 查询页面下的所有页面与功能
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryNodeTree (...params) {
    return ds.get(api.queryNodeTree.url, ...params, {})
  },

  /**
   * @function 查询页面
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAll (...params) {
    return ds.get(api.queryListAll.url, ...params, {})
  }
}

export {
  api, ds
}
