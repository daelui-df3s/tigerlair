/**
* @description 目录文件管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/pfile.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询数据源列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  },
  /**
   * @function 查询数据源列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAll (...params) {
    return ds.get(api.queryListAll.url, ...params, {})
  },

  /**
   * @function 编辑数据源
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 删除数据源
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 查询数据源
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  }
}

export {
  api, ds
}
