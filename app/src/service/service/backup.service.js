/**
* @description 备份管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/backup.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询导航列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  },
  /**
   * @function 查询导航列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAll (...params) {
    return ds.get(api.queryListAll.url, ...params, {})
  },

  /**
   * @function 添加导航
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 删除导航
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 查询导航
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  }
}

export {
  api, ds
}
