/**
* @description 导航发布管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/comprelease.api'

// api地址解析
const api = resolver.solveAPI(API)
const service = resolver.solveService({ api, ds })

export default service

export {
  api, ds
}
