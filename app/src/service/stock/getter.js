/**
 * @description 数据读取
*/

import state from './state.js'
import { OptionEnum } from '@daelui/dogjs/dist/components.js'

export default {
  // 获取状态
  statusList () {
    let list = state.statusList
    list = Array.isArray(list) ? list : []
    list = [{id: '', value: '', text: '全部'}, ...list]
    return list
  },
  // 数据源类型
  resourceTypeList () {
    let list = state.resourceTypeList
    list = Array.isArray(list) ? list : []
    list = [{id: '', value: '', text: '全部'}, ...list]
    return list
  },
  // 执行单元类型
  exeunitTypeList () {
    let list = state.exeunitTypeList
    list = Array.isArray(list) ? list : []
    list = [{id: '', value: '', text: '全部'}, ...list]
    return list
  },
  // 启用与禁用状态
  enableStatus (hasAll) {
    return new OptionEnum([
      {key: 'ENABLE', id: 1, text: '启用'},
      {key: 'DISABLE', id: 0, text: '禁用'}
    ], { valueKey: 'id', hasAll })
  }
}
