import Vue from 'vue'
import Vuex from 'vuex'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'
import App from './App'
import router from './router'
import store from './store'
import { domer } from '@daelui/dogjs/dist/components.js'
import directives from '@daelui/vdog/dist/directives.js'
import dmap from './components/dmap'
import config from '@/micros/config'
import dog from '@daelui/dogjs/dist/index'
import $pig from '@daelui/pigjs'
import '@daelui/dogui/dist/css/boost.min.css'
import '@daelui/dogui/dist/css/theme/black.min.css'

Vue.use(ElementUI)
Vue.config.productionTip = false
// 指令
directives(Vue)

// 是否运行态
const isProd = /production|single/i.test(process.env.NODE_ENV)
const isDev = /dev/i.test(process.env.NODE_ENV)
const isSingle = /single/i.test(process.env.NODE_ENV)
const ap = (window.application || {}).tigerlair || {}
ap.mode = ap[isProd ? 'prod' : 'dev']
const mode = ap.mode
// 初始化模块配置
const loader = mode.loader || {}
const domain = mode.domain || location.origin
const rootUrl = loader.rootUrl || location.origin
$pig.init({
  path: loader.path || '@daelui/pigjs/latest/dist/system.min.js',
  rootUrl: rootUrl,
  rootUrls: loader.rootUrls,
  registry: loader.registry,
  registries: loader.registries,
  resolutions: [{name: 'vue', version: '2.*.*'}]
})
// .then(() => {
//   $pig.define('http://www.daelui.cn/pool/prod/packages/vue/2.7.14/dist/vue.min.js', [], function(){
//     return Vue
//   })
// })
// 可替换标识符
let identifyMap = mode.identifyMap || {}
const rules = mode.rules || []
rules.forEach(item => {
  if(identifyMap[item.rule] === undefined) identifyMap[item.rule] = item.url
})
$pig.identifyMap = $pig.identifyMap || {}
$pig.identifyMap = Object.assign($pig.identifyMap, identifyMap)

/**
 * @function 获取document的body元素
*/
domer.getDocBody = function () {
  let doc = this.getDoc()
  return doc.querySelector('#micro-app') || doc.querySelector('[data-sandbox-configuration]') || doc.body
}

// 初次加载
dmap.set('onload', 1)

const win = typeof self !== 'undefined' ? window : global
// 路径处理
let microPath = ''
if (win.__POWERED_BY_QIANKUN__) {
  microPath = config.microPath
}
Vue.prototype.getMicroPath = function (path) {
  return path.replace('#', '#' + microPath).replace(/\/+/, '/')
}
Vue.prototype.getMicroRoute = function (path) {
  return path.replace(/^\//, '/' + microPath + '/').replace(/\/+/, '/')
}
var $dog = win.$dog
if ($dog) {
  for (let key in $dog) {
    dog[key] = $dog[key]
  }
}
win.$dog = $dog = dog
win.$dog.Vue = win.$dog.Vue || Vue
win.MonacoWorkerBaseUrl = isDev ? '/static/' : isSingle ? '/app/static/' : rootUrl + '/monaco-editor/'

// debugger加载
;(function(){
  let win = typeof self !== 'undefined' ? window: global
  let href = (win.location || {}).href
  let b = /debugger=true/i.test(href)
  if (!b) return
  if (win.$pig) {
    win.$pig.load('eruda').then(eruda => {
      eruda.init()
    })
  } else {
    var script = document.createElement('script')
    let src = location.origin + '/pool/prod/packages/eruda/1.2.6/eruda.min.js'
    script.src = src
    script.async = true
    document.getElementsByTagName('head')[0].appendChild(script)
    script.onload = function () {
      eruda.init()
    }
  }
})();

/* 原始启动代码 */
if (!win.__POWERED_BY_QIANKUN__) {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#micro-app')
}

export default {
  Vue, Vuex, VueRouter, router, store, App
}

export {
  Vue, Vuex, VueRouter, router, store, App
}