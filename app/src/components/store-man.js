/**
 * @description 数据存取
*/

import { CACHE_TYPE, StoreMan } from '@daelui/dogjs/dist/components.js'

const storeMan = new StoreMan({
  cacheTypes: [
    CACHE_TYPE.DB,
    CACHE_TYPE.LOCAL,
    CACHE_TYPE.MEMORY
  ]
})

export default storeMan
