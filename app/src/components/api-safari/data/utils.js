// 工具
import { objecter, parser } from '@daelui/dogjs/dist/components.js'

export default {
  // 按类型格式化值
  formatValue (value, cat) {
    if (!cat) {
      return value
    }
    // 数字格式
    if (/num|int|float|double|decimal|real/i.test(cat)) {
      let val = parseFloat(value)
      if (!isNaN(val)) {
        return val
      }
    }
    // 布尔格式
    else if (/bool/i.test(cat)) {
      let list = /true|false|1|0/i.exec(value)
      if (list) {
        return /true|1/.test(list[0]) ? true : false
      }
    }
    // 数组格式
    else if (/array|list/i.test(cat)) {
      if (typeof value === 'object' || !value) {
        return value
      }
      return parser.parse(value)
    }
    // 对象格式
    else if (/object|json/i.test(cat)) {
      if (typeof value === 'object' || !value) {
        return value
      }
      return parser.parse(value)
    }
    return value
  },
  // 数据净化
  pureRequest (data) {
    let result = {}
    for (let key in data) {
      let value = data[key]
      if (Array.isArray(value)) {
        value = value.filter(node => {
          if (node.hasOwnProperty('key') && (!node.key || node.required === false)) {
            return false
          }
          return true
        })
        result[key] = value.map(node => {
          return this.pure(node)
        })
      } else if (objecter.isPlainObject(value)) {
        result[key] = this.pure(value)
      } else {
        result[key] = data[key]
      }
    }
    return result
  },
  // 数据净化
  pureMeta (data) {
    let result = {}
    for (let key in data) {
      let value = data[key]
      if (Array.isArray(value)) {
        value = value.filter(node => {
          if (node.hasOwnProperty('key') && !node.key && !node.value) {
            return false
          }
          return true
        })
        result[key] = value.map(node => {
          return this.pureMeta(node)
        })
      } else if (objecter.isPlainObject(value)) {
        result[key] = this.pureMeta(value)
      } else {
        result[key] = data[key]
      }
    }
    return result
  }
}