export default {
  method: 'GET',
  url: {
    raw: '',
    query: []
  },
  header: [
    { key: 'Content-Type', value: 'application/json;charset=UTF-8', required: true },
    { key: 'Accept', value: '*/*', required: true },
    { key: 'Token', value: '' },
    { key: 'Authorization', value: '' },
    { key: 'Referer', value: '' }
  ],
  cookie: [],
  body: {
    mode: 'none',
    formdataValue: [],
    urlencodedValue: [],
    rawValue: '',
    fileValue: '',
    options: {
      raw: {
        language: 'text'
      }
    }
  },
  examples: []
}