export default [
  {label: '', field: 'required', width: '40', type: 'checkbox', default: true},
  {label: '键', field: 'key', minWidth: '30%', attend: true},
  {label: '值', field: 'value', width: 'auto', attend: true},
  {label: '描述', field: 'description', minWidth: '30%', attend: true}
]