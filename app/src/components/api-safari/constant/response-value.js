export default {
  header: [],
  cookie: [],
  body: {
    mode: 'none',
    formdataValue: [],
    urlencodedValue: [],
    rawValue: '',
    fileValue: '',
    options: {
      raw: {
        language: 'text'
      }
    }
  },
  examples: []
}