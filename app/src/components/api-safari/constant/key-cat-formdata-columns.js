export default [
  {label: '', field: 'required', width: '40', type: 'checkbox', default: true},
  {label: '键', field: 'key', minWidth: '30%', attend: true},
  {
    label: '类型', field: 'type', width: 120,
    default: 'string', forField: 'value',
    cat: 'select',
    select: {
      options: [
        {label: '字符串', value: 'string'},
        {label: '数字', value: 'number'},
        {label: '布尔值', value: 'boolean'},
        {label: '整数', value: 'integer'},
        {label: '文件', value: 'file'}
      ]
    }
  },
  {label: '值', field: 'value', width: 'auto', attend: true, more: true},
  {label: '描述', field: 'description', minWidth: '30%', attend: true}
]