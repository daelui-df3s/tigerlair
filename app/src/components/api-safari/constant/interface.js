import requestValue from './request-value.js'
import responseValue from './response-value.js'

export default {
  name: 'New Interface',
  request: requestValue,
  response: responseValue,
  examples: []
}
