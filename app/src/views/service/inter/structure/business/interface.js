/**
 * @description 接口业务
*/

import { objecter, strer } from '@daelui/dogjs/dist/components.js'

export default {
  /**
  * @function 分解参数
  * @param {Array} params // 参数数组数据
  * @param {Object} parentParam // 父级参数对象
  * @param {Int} level // 参数层级
  * @param {Boolean} isNew // 是否新节点
  * @return {Array}
  */
  unPackParams (params, parentParam, level, isNew) {
    let arr = []
    level = isNaN(level) ? 0 : level
    if (Array.isArray(params)) {
      params.forEach(item => {
        // 复制参数对象
        let param = {level}
        for (let key in item) {
          if (typeof item[key] !== 'object' && key !== 'level') {
            param[key] = item[key]
          }
        }
        // 记录子节点长度
        let childrenLength = 0
        if (Array.isArray(item.params)) {
          childrenLength = item.params.length
        }
        param.childrenLength = childrenLength
        // 自身id与父id设置
        param.id = isNew === true ? Math.random() : (item.id || Math.random())
        param.pid = parentParam ? parentParam.id : 0
        // 加入数组
        arr.push(param)
        // 子级
        if (item.params && item.params.length > 0) {
          arr = arr.concat(this.unPackParams(item.params, param, level + 1, isNew))
        }
      })
    }

    return arr
  },

  /**
  * @function 组装参数
  * @param {Array} params // 参数数组数据
  * @param {Array} parentParams // 父参数数组数据
  * @return {Array}
  */
  packParams (params, parentParams) {
    // 根参数组
    if (!parentParams) {
      parentParams = []
      params.forEach(item => {
        if (item.pid === 0) {
          parentParams.push(item)
        }
      })
    }

    // 遍历父级参数
    parentParams.forEach(item => {
      params.forEach(node => {
        if (node.pid === item.id) {
          if (!item.params) {
            item.params = []
          }
          item.params.push(node)
        }
      })
      // 继续查找子参数组
      if (item.params) {
        this.packParams(params, item.params)
      }
    })

    return parentParams
  },

  /**
   * @function 获取参数模拟类型
   * @param {Object} item 参数对象
   * @return {string}
  */
  getParamRuleType (item) {
    item = item || {}
    return !item.ruleType || item.ruleType === 'default' ? item.type : item.ruleType
  },

  /**
   * @function 格式化参数
   * @param {Array<Object>} // 参数组
   * @param {Boolean} // 是否创建新的节点
   * @return {Array<Object>}
  */
  formatParams (params, isNew) {
    // 遍历参数
    if (Array.isArray(params)) {
      params.forEach(item => {
        let type = this.getParamRuleType(item)
        // 数字类型
        if (type === 'number') {
          item.ruleValue = 1
          item.rule = item.rule || '1-10'
        }
        // 布尔类型
        else if (type === 'boolean') {
          item.ruleValue = true
          item.rule = item.rule || '1'
        }
        // 正则类型
        else if (type === 'regexp') {
          let ruleValue = item.ruleValue || item.value
          try {
            let reg = ruleValue.replace(/^\/|\/\w*$/g, '')
            reg = new RegExp(reg)
            ruleValue = String(reg)
          } catch (e) {
            ruleValue = '/[a-z][A-Z][0-9]/'
          }
          item.ruleValue = ruleValue
          item.rule = item.rule || '1'
        }
        // 数组类型
        else if (type === 'array') {
          if (String(item.rule) === '1') {
            item.rule = '1-1'
          }
        }
        // 对外类型
        else if (type === 'object') {
          item.rule = ''
          item.ruleValue = ''
        }
        // 新节点
        if (isNew === true) {
          item.id = strer.utid(12)
        }
        // 子级处理
        if (Array.isArray(item.params)) {
          item.params = this.formatParams(item.params, isNew)
        }
      })
    }
    // 新节点
    if (params && !Array.isArray(params) && isNew === true) {
      params.id = strer.utid(12)
    }

    return params
  },

  /**
  * @function 解析导入参数
  */
  resolveParams (params, key, parItem) {
    // 参数属性
    let [name, type, required, value, rule, ruleType, ruleValue, desc] =
      [key || '', '', '', '', '', '', '']
    // 子参数
    let childParams = null
    let childKey = ''
    // 数组形式
    if (Array.isArray(params)) {
      type = 'array'
      name = name || type
      required = 1
      let len = params.length
      if (len <= 10) {
        rule = '1-5'
      } else {
        rule = '10-' + len
      }

      // 子参数类型集合
      let types = []
      params.forEach(item => {
        let type = this.getParamType(item)
        if (!types.indexOf(type)) {
          types.push(type)
        }
      })
      // 子参数含有多种类型
      if (types.length > 1) {
        value = params
      }
      // 只有一种类型
      else {
        childParams = params[0]
      }
    } else {
      type = typeof params
      name = name || type
      required = 1
      value = params
      // 格式不为对象时设置默认值
      if (type !== 'object') {
        // 数字类型
        if (type === 'number') {
          // 整数与小数分离
          let nums = String(value).split('.')
          let float = ''
          if (nums[1]) {
            float = '.' + nums[1].length
          }
          rule = nums[0] + float
          value = 1
        }
        // 字符串类型
        else if (type === 'string') {
          rule = '1'
          if (value === 'null') {
            rule = '1'
            value = ''
          }
        }
        // 布尔类型
        else if (type === 'boolean') {
          rule = '0-1'
          value = true
        }
      }
      // null
      else if (value === null) {
        type = 'string'
        rule = '1'
        value = ''
      }
    }
    ruleValue = value
    ruleType = 'default'
    // 参数属性设置
    let item = {
      name, type, required, value, rule, ruleType, ruleValue, desc
    }

    // 对象类型
    if (type === 'object' && value !== null) {
      item.params = []
      for (let k in params) {
        item.params.push(this.resolveParams(params[k], k))
      }
    }

    // 继续解析子参数
    if (childParams) {
      item.params = [this.resolveParams(childParams, childKey, item)]
    }

    return item
  },

  /**
  * @function 解析导入Bean参数
  */
  resolveBeanParams (params) {
    return {
      name: 'object',
      type: 'object',
      desc: '',
      params: params,
      required: 1,
      rule: '',
      ruleType: 'default',
      valid: 0,
      validRule: ''
    }
  },

  /**
  * @function 获取参数类型
  * @param {any} item // 参数值
  * @return {String}
  */
  getParamType (item) {
    let type = ''
    if (Array.isArray(item)) {
      type = 'array'
    } else {
      type = typeof item
    }

    return type
  },

  /**
  * @function 计算节点下子节点的数目
  * @param {Array} params // 参数组
  * @return {String}
  */
  countChildren (params) {
    let packParams = this.packParams(objecter.clone(params))
    // 重新解析的参数
    let newUnpackParams = this.unPackParams(packParams)
    // 重置参数子节点数
    params.forEach(item => {
      newUnpackParams.forEach(node => {
        if (item.id === node.id && item.name === node.name) {
          item.childrenLength = node.childrenLength
        }
      })
    })
  },

  /**
   * @function 获取匹配的节点树
  */
  getMatchNode (node, nodes) {
    let matchNode = null
    nodes.forEach(item => {
      // 节点匹配
      if (item.id === node.id && item.name === node.name) {
        matchNode = item
        return false
      }
      // 子节点
      else if (Array.isArray(item.params)) {
        let result = this.getMatchNode(node, item.params)
        if (result) {
          matchNode = result
          return false
        }
      }
    })

    return matchNode
  }
}
