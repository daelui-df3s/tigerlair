import Vue from 'vue'
import { arrayer } from '@daelui/dogjs/dist/components.js'
import coder from '@daelui/dogjs/dist/libs/coder.js'
import { state } from '@/service/stock'

import * as Babel from '@babel/standalone'
const win = typeof self !== 'undefined' ? window : global
win.Babel = Babel

/**
 * @function 获取展示组件
*/
const getComponent = function (options) {
  let component = resolveComponent(options)
  return {
    component: component.mainComponent,
    name: component.name,
    version: component.version
  }
}

/**
 * @function 获取属性组件
*/
const getPropertyComponent = function (options) {
  let component = resolveComponent(options)
  component = component || {}
  return {
    component: component.propertyComponent,
    name: component.name,
    version: component.version
  }
}

// 解析组件对象
const resolveComponent = function (options) {
  options = options || {}
  // 从注册表中取出
  let components = [...state.layoutCollection, ...state.componentCollection].filter(item => {
    return item.name === options.name
  })
  // 按版本号进行排序
  components = components.sort((a, b) => {
    let arrA = String(a.version || '').split(/\s*.\s*/)
    let arrB = String(b.version || '').split(/\s*.\s*/)
    let res = arrayer.sortGroup(arrA, arrB, 'asc', true)
    return res
  })

  let component = components.find(item => {
    return item.version === options.version
  })

  // 无匹配则默认选最新版本
  component = component || components[0]
  component = component || { component: 'div', name: 'div', version: '1.0.0' }
  // 远程解析
  component = resolveRemote(component)

  return component
}

// 远程解析
const resolveRemote = function (component) {
  let repository = component.repository || {}
  // 远程加载
  if (!component.mainComponent) {
    // 直接地址
    if (component.main) {
      component.mainComponent = resolve => remoteComponent(resolve, component.main)
    }
    // 编码
    else if (component.mainCompile) {
      component.mainComponent = resolve => coder.vue.import(component.mainCompile, Vue, $pig).then(res => resolve(res))
    }
    else if (component.mainCode) {
      component.mainComponent = resolve => coder.vue.resolve(component.mainCode, Vue, $pig).then(res => resolve(res))
    }
    // 模板地址
    else if (repository.main) {
      let url = repository.main
      url = url.replace('{name}', component.name).replace('{version}', component.version || 'latest')
      component.mainComponent = resolve => remoteComponent(resolve, url)
    }
  }
  if (!component.propertyComponent) {
    // 直接地址
    if (component.property) {
      component.propertyComponent = remoteComponent(resolve, component.property)
    }
    // 编码
    else if (component.propertyCompile) {
      component.propertyComponent = resolve => coder.vue.import(component.propertyCompile, Vue, $pig).then(res => resolve(res))
    }
    else if (component.propertyCode) {
      component.propertyComponent = resolve => coder.vue.resolve(component.propertyCode, Vue, $pig).then(res => resolve(res))
    }
    // 模板地址
    else if (repository.property) {
      let url = repository.property
      url = url.replace('{name}', component.name).replace('{version}', component.version || 'latest')
      component.propertyComponent = resolve => remoteComponent(resolve, url)
    }
  }

  return component
}

// 获取组件
async function remoteComponent (resolve, url) {
  let res = await $pig.import(url)
  resolve(res)
}

export default {
  getComponent, getPropertyComponent
}

export {
  getComponent, getPropertyComponent
}