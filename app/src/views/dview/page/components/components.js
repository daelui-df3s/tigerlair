/**
 * @description 视图组件列表
*/

const componentCollection = [
  {
    id: 1,
    name: 'ddesign-vue-template',
    text: 'Vue模板',
    version: '1.0.0',
    icon: '',
    description: '',
    props: {
      layout: {
        w: 16,
        h: 16
      }
    },
    category: '基础',
    order: 2,
    mainComponent: resolve => require(['./vue/index.vue'], resolve),
    propertyComponent: resolve => require(['./vue/property.vue'], resolve),
    main: '',
    property: '',
    repository: {
      main: '{rootUrl}/name/verion/index.js',
      property: '{rootUrl}/name/verion/property.js'
    }
  },
  {
    id: 2,
    name: 'ddesign-screen-template',
    text: '大屏Vue模板',
    version: '1.0.0',
    icon: '',
    description: '',
    props: {
      layout: {
        w: 16,
        h: 16
      }
    },
    category: '基础',
    order: 2,
    mainComponent: resolve => require(['./screen/index.vue'], resolve),
    propertyComponent: resolve => require(['./screen/property.vue'], resolve),
    main: '',
    property: '',
    repository: {
      main: '{rootUrl}/name/verion/index.js',
      property: '{rootUrl}/name/verion/property.js'
    }
  }
]

export default componentCollection