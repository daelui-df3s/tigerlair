import constant from '../absolute/constant'

export default {
  layout: {
    ...constant.layout,
    padding: {
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px'
    },
    contrast: {
      isAble: false,
      bgColor: '#ffffff'
    }
  }
}