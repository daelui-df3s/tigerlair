/**
 * @description 布局组件列表
*/

const layoutCollection = [
  {
    id: 'ddesign-layout-absolute',
    name: 'ddesign-layout-absolute',
    text: '绝对布局',
    version: '1.0.0',
    icon: '',
    description: '',
    mainComponent: resolve => require(['./layout/absolute/index.vue'], resolve),
    propertyComponent: resolve => require(['./layout/absolute/property.vue'], resolve),
    main: '',
    property: ''
  },
  {
    id: 'ddesign-layout-grid',
    name: 'ddesign-layout-grid',
    text: '栅格布局',
    version: '1.0.0',
    icon: '',
    description: '',
    mainComponent: resolve => require(['./layout/grid/index.vue'], resolve),
    propertyComponent: resolve => require(['./layout/grid/property.vue'], resolve),
    main: '',
    property: ''
  },
  {
    id: 'ddesign-layout-screen',
    name: 'ddesign-layout-screen',
    text: '大屏布局',
    version: '1.0.0',
    icon: '',
    description: '',
    mainComponent: resolve => require(['./layout/screen/index.vue'], resolve),
    propertyComponent: resolve => require(['./layout/screen/property.vue'], resolve),
    main: '',
    property: ''
  },
  {
    id: 'ddesign-layout-fill',
    name: 'ddesign-layout-fill',
    text: '填充布局',
    version: '1.0.0',
    icon: '',
    description: '',
    mainComponent: resolve => require(['./layout/fill/index.vue'], resolve),
    propertyComponent: resolve => require(['./layout/fill/property.vue'], resolve),
    main: '',
    property: ''
  }
]

export default layoutCollection