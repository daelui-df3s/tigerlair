// 示例模板
export default {
  tpl: `<template>
<el-form ref="form" :model="form" label-width="80px">
  {% each setting.fileds of item index %}
    <el-form-item label="{% item.label %}">
      {% if item.type === 'input' %}
        <el-input v-model="form.{% item.name %}"></el-input>
      {% elseif item.type === 'textarea' %}
        <el-input v-model="form.{% item.name %}" type="textarea"></el-input>
      {% elseif item.type === 'select' %}
        <el-select v-model="form.{% item.name %}" placeholder="请选择{% item.label %}">
          <el-option v-for="node in {% item.selectList %}" :key="node.value" :label="node.text" :value="node.value"></el-option>
        </el-select>
      {% elseif item.type === 'date-picker' %}
        <el-col :span="11">
          <el-date-picker type="date" placeholder="选择日期" v-model="form.{% item.nameDate %}" style="width: 100%;"></el-date-picker>
        </el-col>
        <el-col class="line" :span="2">-</el-col>
        <el-col :span="11">
          <el-time-picker placeholder="选择时间" v-model="form.{% item.nameTime %}" style="width: 100%;"></el-time-picker>
        </el-col>
      {% elseif item.type === 'switch' %}
        <el-switch v-model="form.{% item.name %}"></el-switch>
      {% elseif item.type === 'checkbox-group' %}
        <el-checkbox-group v-model="form.{% item.name %}">
          <el-checkbox v-for="node in {% item.selectList %}" :key="node.value" :label="node.text" name="{% item.name %}"></el-checkbox>
        </el-checkbox-group>
      {% elseif item.type === 'radio-group' %}
        <el-radio-group v-model="form.{% item.name %}">
          <el-radio v-for="node in {% item.selectList %}" :key="node.value" :label="node.text" name="{% item.name %}"></el-radio>
        </el-radio-group>
      {% /if %}
    </el-form-item>
  {% /each %}
  <el-form-item>
    <el-button type="primary" @click="onSubmit">提交</el-button>
    <el-button>取消</el-button>
  </el-form-item>
</el-form>
</template>

<script>
export default {
  data() {
    return {
      form: {
      {% each setting.fileds of item index %}
        {% if item.type === 'date-picker' %}
          {% item.nameDate %}: {% item.dateDefault || '""' %},
          {% item.nameTime %}: {% item.timeDefault || '""' %},
        {% else %}
          {% item.name %}: {% item.default || '""' %},
        {% /if %}
      {% /each %}
      }
    }
  },

  methods: {
    onSubmit() {
      console.log(this.form);
    }
  }
}
</script>

<style lang="less">

</style>
  `,
  data: `{
  setting: {
    fileds: [
      { name: 'name', label: '活动名称', type: 'input', default: '' },
      {
        name: 'region', label: '活动区域', type: 'select', default: '',
        selectList: "[{value: 'beijing', text: '北京'},{value: 'shanghai', text: '上海'},{value: 'guangzhou', text: '广州'}]"
      },
      { nameDate: 'date1', nameTime: 'date2', label: '活动时间', type: 'date-picker', dateDefault: '', timeDefault: '' },
      { name: 'delivery', label: '即时配送', type: 'switch', default: 'false' },
      {
        name: 'type', label: '活动性质', type: 'checkbox-group', default: '[]',
        selectList: "[{value: '1', text: '美食/餐厅线上活动'},{value: '2', text: '地推活动'},{value: '3', text: '线下主题活动'},{value: '4', text: '单纯品牌曝光'}]"
      },
      {
        name: 'resource', label: '特殊资源', type: 'radio-group', default: '[]',
        selectList: "[{value: '1', text: '线上品牌商赞助'},{value: '2', text: '线下场地免费'}]"
      },
      { name: 'desc', label: '活动形式', type: 'textarea', default: '' }
    ]
  }
}`
}