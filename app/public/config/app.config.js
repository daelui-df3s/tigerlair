/**
 * @description 应用配置
*/

/* 运行模式配置 */
(function () {
  const origin = location.origin
  const domain = location.origin

  // 开发环境配置
  const dev = {
    mode: 'dev',
    title: '开发模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: '//127.0.0.1:3601'},
      {rule: 'proxy', url: '//127.0.0.1:3601'},
      {rule: 'pot', url: 'tigerlair'},
      {rule: 'domain', url: domain}
    ],
    // 环境域名
    domain,
    // 模块加载器
    loader: {
      rootUrl: domain + '/pool/prod/packages',
      path: '@daelui/pigjs/latest/dist/system.min.js',
      registries: [
        {
          registry: domain + '/pool/prod/packages.json',
          // rootUrl: domain + '/pool/prod/packages'
        },
        {
          registry: domain + '/pool/prod/components.json',
          // rootUrl: domain + '/pool/prod/packages'
        }
      ]
    }
  }

  // 生产环境配置
  const prod = {
    mode: 'prod',
    title: '生产模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: origin},
      {rule: 'proxy', url: origin + '/tigerlair/saas/proxy'},
      {rule: 'pot', url: 'foundation'},
      {rule: 'domain', url: domain}
    ],
    // 环境域名
    domain,
    // 模块加载器
    loader: {
      rootUrl: domain + '/pool/prod/packages',
      path: '@daelui/pigjs/latest/dist/system.min.js',
      registries: [
        {
          registry: domain + '/pool/prod/packages.json',
          // rootUrl: domain + '/pool/prod/packages'
        },
        {
          registry: domain + '/pool/prod/components.json',
          // rootUrl: domain + '/pool/prod/packages'
        }
      ]
    }
  }

  /* 应用配置 */
  const application = {
    dev, prod,
    // 运行模式配置
    mode: {dev, prod}.dev
  }

  window.application = window.application || {}
  window.application.tigerlair = application
})();